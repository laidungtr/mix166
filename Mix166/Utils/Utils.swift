//
//  Utils.swift
//  Karaoke_Plus_Remote
//
//  Created by ttiamap on 5/9/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class Utils {
    
    class func getDeviceId() -> String{
        if let deviceID = UIDevice.current.identifierForVendor{
           return String(describing: deviceID)
        }
        return ""
    }
    
    class func getDeviceName() -> String{
        return UIDevice.current.name
    }
    
    class func getAppVersion() -> String{
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        return ""
    }
    
    class func getTotalDiskSpaceInBytes() -> Int64{
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
        guard
            let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: documentDirectory),
            let size = systemAttributes[.systemSize] as? NSNumber
            else {
                // something failed
                return 0
        }
        return size.int64Value
        
    }
    
    class func freeDiskSpaceInBytes() -> Int64{
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
        guard
            let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: documentDirectory),
            let size = systemAttributes[.systemFreeSize] as? NSNumber
            else {
                // something failed
                return 0
        }
        return size.int64Value
        
    }
    
    class func usedDiskSpaceInBytes() -> Int64{
        let totalSpace = self.getTotalDiskSpaceInBytes()
        let freeSpace = self.freeDiskSpaceInBytes()
        
        return totalSpace - freeSpace
    }
    
    /*
     #pragma mark - get Fee Memory
     + (CGFloat)totalDiskSpaceInBytes {
     long long space = [[[[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:nil] objectForKey:NSFileSystemSize] longLongValue];
     return space;
     }
     
     + (CGFloat)freeDiskSpaceInBytes {
     long long freeSpace = [[[[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:nil] objectForKey:NSFileSystemFreeSize] longLongValue];
     return freeSpace;
     }
     
     + (CGFloat)usedDiskSpaceInBytes {
     long long usedSpace = [self totalDiskSpaceInBytes] - [self freeDiskSpaceInBytes];
     return usedSpace;
     }
     
     
     #pragma mark - Convert
     + (NSString *)bytesTransformedValue:(double)value{
     
     float multiplyFactor = 0;
     
     NSArray *tokens = [NSArray arrayWithObjects:@"bytes",@"KB",@"MB",@"GB",@"TB",nil];
     
     while (value > (1024/2)) {
     
     if (value > 1024) {
     value /= 1024;
     multiplyFactor++;
     }else{
     value /= 1024;
     multiplyFactor++;
     }
     
     }
     
     return [NSString stringWithFormat:@"%4.2f %@",value, [tokens objectAtIndex:multiplyFactor]];
     }
 */
    
}
