//
//  config.swift
//  Karaoke_Plus_Remote
//
//  Created by ttiamap on 5/9/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import UIKit

let screenWidth: CGFloat = UIScreen.main.bounds.width
let screenHeight: CGFloat = UIScreen.main.bounds.height


let paddingBetweenItem: CGFloat = 5.0
let paddingLeftRight: CGFloat = 10.0

let sizeZero        = CGSize(width: 0, height: 0)
let headerTitleSize = CGSize(width: screenWidth, height: 60.0)

let bannerHomeSize      = CGSize(width: screenWidth, height: screenWidth * 0.5714) // 0.315789 web
let bannerSize = CGSize(width: screenWidth, height: screenWidth)//CGSize(width: screenWidth, height: screenWidth)
let mixSize         = CGSize(width: screenWidth/2 - (paddingLeftRight + paddingBetweenItem),
                             height: screenWidth/2 - (paddingLeftRight + paddingBetweenItem) + 40.0)
let trackSize       = CGSize(width: screenWidth, height: 80.0)
let trackDownloadSize       = CGSize(width: screenWidth, height: 60.0)
let chartSize       = CGSize(width: screenWidth, height: 80.0)
let chartSizeStrecth = CGSize(width: screenWidth, height: 90.0)
let chartVideoSizeStrecth  = CGSize(width: screenWidth, height: 70.0)
let deejaySize          = CGSize(width: screenWidth/5.0, height: screenWidth/5.0 + 30.0)
let deejayOverlaySize   = CGSize(width: screenWidth/2.7, height: screenWidth/2.7 + 30.0)
let deejayCircleHomeSize   = CGSize(width: screenWidth/3.7, height: screenWidth/3.7 + 30.0)
let deejayListSize  = CGSize(width: screenWidth, height: 60.0)
let deejayListStretchSize  = CGSize(width: screenWidth, height: 65.0)
let videoSize       = CGSize(width: screenWidth/2 - (paddingLeftRight + paddingBetweenItem),
                             height: ((screenWidth/2 - (paddingLeftRight + paddingBetweenItem)) * 9/16) + 48.0)
let loadingListSize  = CGSize(width: screenWidth, height: 40.0)
let profileMoreItemSize = CGSize(width: screenWidth, height: 28.0)
let profileUserSize = CGSize(width: screenWidth, height: 90.0)
let headerTitleProfileSize = CGSize(width: screenWidth, height: 40.0)
let videoInfo_Header = CGSize(width: screenWidth, height: 200)
let navigationBarHeight: CGFloat = 64.0

// new feed
let newFeed_header = CGSize(width: screenWidth, height: 160 + screenWidth * 0.5714)

//Search
let searchKeywordSize = CGSize(width: screenWidth, height: 35.0)

//Playlist
let playlistSize    = CGSize(width: screenWidth, height: 90.0)

let playlistDetailHeaderSize = CGSize(width: screenWidth, height: screenWidth/3 + 64.0 + 60.0)


let playerControlHeight: CGFloat = 54.0
let playerViewheight = screenHeight + playerControlHeight
let colorNavigationBar = UIColor.red(red: 20, green: 20, blue: 20, alpha: 1)
let colorSlideMenu = UIColor.init(red: 34/255.0, green: 34/255.0, blue: 34/255.0, alpha: 1)

/*******************************************************************************/
//MARK: AppInfo
/*******************************************************************************/
let domainApi = "http://api.mix166.com/api/v1.1_a/"
let image_songPlaceholder = UIImage.init(named: "song_placeholder")
let image_userPlaceholder = UIImage.init(named: "user_placeholder")

///Number of Item in a page
let limitDefault = 30


/*******************************************************************************/
//MARK: Enum
/*******************************************************************************/
public enum AppViewTitle: String{
    case home = "Home"
    case mixset = "Mixsets Hot"
    case track  = "Tracks Hot"
    case video  = "Videos Hot"
    case topDJ  = ""
    case artist = "Artist"
    case playlist = "Playlist"
    case addToPlaylist = "Add to Playlist"
    case search = "Search"
    case profile = "Profile"
    case profileFollowing = "Following"
    case profileFavorutie = "Favourite"
    case profileMusicOffline = "Personal Music"
    case chart   = "Chart Mix166"
    case station = "Station"
    case more    = "More"
    case `default` = "Mix166"
}

public enum ColorText: String{
    case primaryTitleColor = "F0F0F0"
    case secondaryTitleColor = "818181"
    case redColor = "E8060B"
    case rankUp = "40900B"   // 64,144,11
    case rankDown = "FF1844" // 255,24,68
    case rankKeep = "D4D4D4" // 212,212,212
}

public enum AppColor: String{
    case backgroundColor,clearColor = "000000"
    case appRedColor = "E8060B"
    case searchBarColor = "383838"
    case follow_unactive = "181818"
}

public enum AppMixError: String {
    case data_notfound = "Data not found"
    case song_notfound = "Cant found song to sync"
    case delete_songPlayingError = "The music is playing, try again later"
    case facebook_cancelByUser = "Facebook login cancel by user"
    case playlist_notfound = "Playlist not found"
    case userinfo_notfound = "User Info not found"
    case addToFavouriteSucess = "Add to favourite list"
    case unknow            = "Undefine error"
}

public enum Image_Name: String{
    case imgRankUp = "ic_RankUp"
    case imgRankDown = "ic_RankDown"
    case imgRankKeep = "ic_RankKeep"
    
    case img_repeat = "ic_repeat"
    case img_repeatSelected = "ic_repeatSelected"
    case img_repeatOne = "ic_repeatOne"
    
    case ic_addToPlaylist   = "ic_addToPlaylist"
    case ic_addToQueue      = "ic_addToQueue"
    case ic_goToQueue       = "ic_goToQueue"
    case ic_share           = "ic_share"
    case ic_cancel          = "ic_cancel"
    case ic_add             = "ic_add"
    case ic_addBig          = "ic_addBig"
    case ic_search          = "ic_search"
    case ic_clearText     = "ic_clearText"
    case ic_fullscreen = "ic_fullscreen"
    case ic_scalescreen = "ic_fullscreenexist"
    case ic_play_small = "ic_play_small"
    case ic_playCircle = "ic_play_circle"
    case ic_seekbar = "ic_seekbar"
    case ic_pauseCircle = "ic_pause_circle"
    case ic_pause = "ic_pause"
    case ic_playlist = "ic_playlist"
    case ic_favourite_white     = "ic_like"
    case ic_favourite = "ic_heart_fill"
    case ic_followings = "ic_follow"
    case ic_download = "ic_download"
    case ic_history = "ic_history"
    case ic_goNext = "ic_goNext"
    case ic_profile = "ic_profile"
    case ic_dismiss = "ic_dissmiss"
}

//MARK: Product Enum
public enum ProductType: String{
    case mixset = "mixset"
    case track = "tracks"
    case trackPlayer = "tracksPlayer"
    case trackPlayerMasup = "masupTracksPlayer"
    case chart = "chart"
    case deejay_horizontal = "deejay_horizontal"
    case deejay_vertical = "deejay_vertical"
    case keyword_trend = "keyword_trend"
    case keyword_recently = "keyword_recently"
    case video = "video"
    case station = "station"
    case all = "all"
    case unknow = "unknow"
    
    init(str_type: String){
        self = ProductType.init(rawValue: str_type) ?? .unknow
    }
}

public enum Product_UI_Header_Type: String{

    case artist_detailHeader        = "artist_detailHeader"
    case header_title               = "header_tittle"
    case song_infoHeader            = "songInfo_header"
    case unknow                     = "unknow"
    
    init(str_type: String){
        self = Product_UI_Header_Type.init(rawValue: str_type) ?? .unknow
    }
}

public enum Product_UI_Type: String{
    case mixset_horizontal          = "mix"
    case mixset_vertical            = "mix_vertical"
    case mixset_vertical_delete     = "mix_vertical_delete"
    case track_vertical             = "track"
    case track_vertical_delete      = "track_vertical_delete"
    case track_horizontal           = "track_horizontal"
    case track_player_vertical      = "track_player_vertical"
    case deejay_horizontal          = "deejay_horizontal"
    case deejay_vertical            = "deejay_vertical"
    case deejay_vertical_follow     = "deejay_vertical_follow"
    case deejay_station_horizontal  = "artist_station_vertical"
    case keyword_roundRect          = "keyword_roundRect"
    case keyword_normal             = "keyword_normal"
    case video_horizontal           = "video"
    case video_vertical             = "video_vertical"
    case video_vertical_delete      = "video_vertical_delete"
    case unknow                     = "unknow"
    
    func paggingTitle() -> String{
        switch self {
        case .mixset_vertical, .mixset_horizontal:
            return "mixset"
            
        case .track_vertical, .track_player_vertical:
            return "tracks"
            
        case .video_vertical, .video_horizontal:
            return "video"
            
        default:
           return ""
        }
    }
    
    init(str_type: String){
        self = Product_UI_Type.init(rawValue: str_type) ?? .unknow
    }
}

public enum SearchType: String {
    case trend          = "TRENDING"
    case recent_search  = "RECENT SEARCH"
    case mixset         = "MIXSETS"
    case track          = "TRACKS"
    case video          = "VIDEO"
    case deejay         = "ARTIST"
    case unknow         = "UNKNOW"
}

public enum SongType: String {
    case Track  = "track"
    case Mix    = "mixset"
    case Video  = "video"
    case Artist = "artist"
    
    init(type: String) {
        self.init(rawValue: type)!
    }
}

class Config{
    
    static let userAgent = Config.appInfor()
    
    static func appInfor() ->String {
        let systemInfor = "iOS: " + UIDevice.current.systemVersion + "\n" +
            "model: " + UIDevice.current.model + "\n" +
            "client_name: " + UIDevice.current.name
        
        var outputStr =  "\(systemInfor) \nMix166 v1.x.x"
        
        if let dict = Bundle.main.infoDictionary {
            if let version = dict["CFBundleShortVersionString"] as? String,
                let bundleVersion = dict["CFBundleVersion"] as? String,
                let appName = dict["CFBundleName"] as? String {
                
                outputStr =  "\(systemInfor) \napp: \(appName) v\(version) (Build \(bundleVersion))."
            }
        }
        
        return outputStr
    }
    
    static var host: String {
        var host = domainApi.replacingOccurrences(of: "http://", with: "")
        host = domainApi.replacingOccurrences(of: "https://", with: "")
        return host
    }
}
