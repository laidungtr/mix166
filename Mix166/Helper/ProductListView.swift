//
//  ProductListView.swift
//  Mix166
//
//  Created by ttiamap on 6/13/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ProductListInfo {
    // request proxy
    enum RequestProxy: String{
        case download               = "download"
        case feature                = "feature"
        case chart                  = "SearchStyle"
        case search                 = "search"
        case genres                 = "genres"
        case station                = "station"
        case video                  = "video"
        case user                   = "user"
        case playlistDetail         = "playlist"
        case artist                 = "artist"
        case artistDetail           = "artistDetail"
        case automatically          = "NoneRequestStyle" // user setup datasource programatically
    }
    
    var data:   [AnyObject]          = []
    var paggingTypes: [ProductType]!
    var type:   ProductType     = .unknow
    var type_listing: Product_UI_Type = .unknow // use this for station and playlist only, wait for second version to remake api
    var isDataEnd = false
    var isEnablePullToRefresh = false
    
    
    private var isRequesting = false
    private var filter: Filter          = Filter.init(_keyword: "")
    private var proxy:  RequestProxy    = .automatically
    
    func getFilter() -> Filter{
        return self.filter
    }
    
    func updateFilter(_filter: Filter){
        self.filter = _filter
    }
    
    func dataEmpty() -> Bool{
        
        if self.data.count > 0{
            
            if let itemProductList = self.data.first as? ProductList{
                return itemProductList.object?.count ?? 0 == 0
                
            }else{
                
                //
                return false
            }
        }
        
        return true
    }
    
    func itemStationAtIndexPath(indexPath: IndexPath) -> AnyObject?{
        if let itemStation = self.itemAtIndex(index: indexPath.section) as? Station{
            if itemStation.recommend_object.count > indexPath.row{
                return itemStation.recommend_object[indexPath.row]
            }
        }
        return nil
    }
    
    func itemsStationRecomendAtIndex(index: Int) -> [AnyObject]?{
        if let itemStation = self.itemAtIndex(index: index) as? Station{
            return itemStation.recommend_object
        }
        
        return nil
    }
    
    func itemProductAtIndexPath(indexPath: IndexPath) -> AnyObject?{
        if let item = self.itemAtIndex(index: indexPath.section) as? ProductList{
            if let _objects = item.object, _objects.count > indexPath.row{
                return _objects[indexPath.row] as AnyObject
            }
        }
        return nil
    }
    
    func itemsProductAt(index: Int) -> [AnyObject]?{
        if let item = self.itemAtIndex(index: index) as? ProductList{
            return item.object
        }
        return nil
    }
    
    func itemAtIndexPath(indexPath: IndexPath) -> AnyObject?{
        if self.data.count > indexPath.row{
            return self.data[indexPath.row]
        }
        return nil
    }
    
    func itemAtIndex(index: Int) -> AnyObject?{
        if index >= 0 && self.data.count > index{
            return self.data[index]
        }
        return nil
    }
    
    func setProductType(paggingIndex: Int){
        if paggingIndex >= 0 && paggingIndex < self.paggingTypes.count{
            self.type = self.paggingTypes[paggingIndex]
            print(self.type)
        }
    }
    
    func proxyType() -> RequestProxy{
        return self.proxy
    }
    
    
    func paggingEnable(section: Int) -> Bool{
        if  self.isDataEnd || self.proxy == .automatically{
            return false
        }
        
        if self.proxy == .station{
            if self.data.count - 1 > section{
                return false
            }
        }
        
        return true
    }
    
    init(_proxy: RequestProxy, _type: ProductType, filter: Filter? ,_data: [Song]?, _paggingTypes: [ProductType]?, pullRefresh enable: Bool) {
        
        if _proxy == .automatically{
            if _data == nil{
                fatalError(" request proxy is none request and data ([Song]) is nil will crash your app")
            }else{
                self.data = _data!
            }
        }
        
        self.isEnablePullToRefresh = enable
        self.proxy = _proxy
        self.type = _type
        
        if self.type == .chart && _paggingTypes == nil{
            fatalError(" type with chart and paggingTypes is nil will crash your app")
        }
        
        if _paggingTypes != nil {
            self.paggingTypes = _paggingTypes
        }
        
        if filter != nil{
            self.filter = filter!
        }
        
    }
    
    internal func requestData(newRequest: Bool ,
                              success:@escaping (_ dataEnd: Bool, _ insertPaths: [IndexPath]?) -> Void,
                              fail:@escaping (_ error: Error) -> Void){
        
        if self.isRequesting{
            return
        }
        
        if newRequest == true && self.proxyType() != .automatically{
            self.filter.offset = 0
            self.data = []
        }
        
        var request = DataRequest(.Default)
        
        request.onSuccess = {(_data, moreInfo) in
            switch self.proxy {
            case .feature:
                //
                self.requestDataFeatureResponse(_data as AnyObject, moreInfo, success: success, fail: fail)
                break
                
            case .search:
                //
                self.requestDataSearchResponse(_data as AnyObject, moreInfo, success: success, fail: fail)
                break
                
            case .genres:
                //
                break
                
            case .chart:
                //
                self.requestDataChartResponse(_data, moreInfo, success: success, fail: fail)
                break
                
            case .artist:
                self.requestDataArtistResponse(_data, moreInfo, success: success, fail: fail)
                break
                
            case .artistDetail:
                self.requestDataArtistDetailResponse(_data, moreInfo, success: success, fail: fail)
                break
                
            case .video, .user:
                self.requestDataProductsResponse(_data, moreInfo, success: success, fail: fail)
                break
                
                
            case .download:
                self.requestDownloadSongResponse(_data as AnyObject, moreInfo, success: success, fail: fail)
                break
                
            case .station:
                //
                self.requestDataStationResponse(_data as AnyObject, moreInfo, success: success, fail: fail)
                break
                
            case .playlistDetail:
                //
                self.requestDataPlaylistResponse(_data as AnyObject, moreInfo, success: success, fail: fail)
                break
                
                
                
            case .automatically:
                //
                self.isDataEnd = true
                success(true, nil)
                break
                
            }
            self.isRequesting = false
        }
        
        request.onFailure = {(_errorResponse, moreInfo) in
            if (_errorResponse != nil) {
                fail(_errorResponse!)
            }
            self.isRequesting = false
        }
        
        
        
        self.isRequesting = true
        
        switch self.proxy {
        case .feature:
            //
            self.requestWithFeature(request: request)
            break
            
        case .search:
            //
            self.requestWithSearch(request: request)
            break
            
        case .user:
            //
            self.requestwithUser(request: request)
            
        case .genres:
            //
            break
            
        case .artist:
            //
            self.requestWithArtst(userId: DataProxy.user.userInfo?.id, request: request)
            break
            
        case .artistDetail:
            //
            self.requestWithArtistDetail(request: request)
            break
            
        case .video:
            self.requestWithVideo(request: request)
            break
            
            
        case .chart:
            //
            self.requestWithChart(request: request)
            break
            
        case .download:
            self.requestWithDownload(request: request)
            break
            
        case .station:
            self.requestWithStation(request: request)
            break
            
        case .playlistDetail:
            self.requestWithPlaylist(request: request)
            break
            
        case .automatically:
            //
            self.isDataEnd = true
            success(true, nil)
            
            break
            
        }
    }
    
    internal func requestDataFeatureResponse(_ data: AnyObject?,  _ moreInfo: AnyObject? ,
                                             success:@escaping (_ dataEnd: Bool, _ insertPaths: [IndexPath]?) -> Void,
                                             fail:@escaping (_ error: Error) -> Void){
        
        if let response = data as? [Song], response.count > 0{
            
            let insertIndexPaths = self.handleResponseData(response_data: response)
            success(self.isDataEnd, insertIndexPaths)
            
        }else{
            self.isDataEnd = true
            success(true, nil)
        }
    }
    
    internal func requestDataSearchResponse(_ data: AnyObject?,  _ moreInfo: AnyObject? ,
                                            success:@escaping (_ dataEnd: Bool, _ insertPaths: [IndexPath]?) -> Void,
                                            fail:@escaping (_ error: Error) -> Void){
        
        if let response = data as? Search, response._datas?.count ?? 0 > 0{
            
            let insertIndexPaths = self.handleResponseData(response_data: response._datas!)
            success(self.isDataEnd, insertIndexPaths)
            
        }else{
            self.isDataEnd = true
            success(true, nil)
        }
    }
    
    internal func requestDataArtistResponse(_ data: AnyObject?,  _ moreInfo: AnyObject? ,
                                            success:@escaping (_ dataEnd: Bool, _ insertPaths: [IndexPath]?) -> Void,
                                            fail:@escaping (_ error: Error) -> Void){
        
        
        self.type_listing = .deejay_vertical
        if let response = data as? [Artist], response.count > 0{
            
            let insertIndexPaths = self.handleResponseData(response_data: response)
            
            success(self.isDataEnd, insertIndexPaths)
            
        }else{
            self.isDataEnd = true
            success(true, nil)
        }
    }
    
    internal func requestDataProductsResponse(_ data: AnyObject?,  _ moreInfo: AnyObject? ,
                                              success:@escaping (_ dataEnd: Bool, _ insertPaths: [IndexPath]?) -> Void,
                                              fail:@escaping (_ error: Error) -> Void){
        
        if let response = data as? [ProductList], response.count > 0{
            
            let insertIndexPaths = self.handleResponseData(response_data: response)
            
            success(self.isDataEnd, insertIndexPaths)
            
        }else{
            self.isDataEnd = true
            success(true, nil)
        }
        
    }
    
    
    internal func requestDataArtistDetailResponse(_ data: AnyObject?,  _ moreInfo: AnyObject? ,
                                                  success:@escaping (_ dataEnd: Bool, _ insertPaths: [IndexPath]?) -> Void,
                                                  fail:@escaping (_ error: Error) -> Void){
        
        
        if var response = data as? [ProductList], response.count > 0{
            
            // make object artist to header
            
            if let _artist = self.filter.requestObject as? Artist, self.type == .unknow{
                let itemProduct = ProductList.init(dict: ["type_header": Product_UI_Header_Type.artist_detailHeader.rawValue as AnyObject, "object_header": _artist])
                
                response.insert(itemProduct, at: 0)
            }
            
            // productlist Artist detail not pagging
            self.isDataEnd = true
            
            let insertIndexPaths = self.handleResponseData(response_data: response)
            
            success(self.isDataEnd, insertIndexPaths)
            
        }else{
            self.isDataEnd = true
            success(true, nil)
        }
    }
    
    internal func requestDataChartResponse(_ data: AnyObject?,  _ moreInfo: AnyObject? ,
                                           success:@escaping (_ dataEnd: Bool, _ insertPaths: [IndexPath]?) -> Void,
                                           fail:@escaping (_ error: Error) -> Void){
        
        if let response = data as? [Song], response.count > 0{
            
            let insertIndexPaths = self.handleResponseData(response_data: response)
            success(self.isDataEnd, insertIndexPaths)
            
        }else{
            self.isDataEnd = true
            success(true, nil)
        }
    }
    
    internal func requestDataStationResponse(_ data: AnyObject?,  _ moreInfo: AnyObject? ,
                                             success:@escaping (_ dataEnd: Bool, _ insertPaths: [IndexPath]?) -> Void,
                                             fail:@escaping (_ error: Error) -> Void){
        
        if let response = data as? [Station], response.count > 0{
            
            let insertIndexPaths = self.handleResponseData(response_data: response)
            success(self.isDataEnd, insertIndexPaths)
            
        }else{
            self.isDataEnd = true
            success(true, nil)
        }
    }
    
    internal func requestDataPlaylistResponse(_ data: AnyObject?,  _ moreInfo: AnyObject? ,
                                              success:@escaping (_ dataEnd: Bool, _ insertPaths: [IndexPath]?) -> Void,
                                              fail:@escaping (_ error: Error) -> Void){
        if let reponse = data as? Playlist_Detail{
            self.type_listing = reponse.type
            let insertIndexPaths = self.handleResponseData(response_data: reponse.songs)
            success(self.isDataEnd, insertIndexPaths)
        }else{
            self.isDataEnd = true
            success(true, nil)
        }
    }
    
    internal func requestDownloadSongResponse(_ data: AnyObject?,  _ moreInfo: AnyObject? ,
                                              success:@escaping (_ dataEnd: Bool, _ insertPaths: [IndexPath]?) -> Void,
                                              fail:@escaping (_ error: Error) -> Void){
        
        if let response = data as? [Song], response.count > 0{
            
            let insertIndexPaths = self.handleResponseData(response_data: response)
            success(self.isDataEnd, insertIndexPaths)
            
        }else{
            self.isDataEnd = true
            success(true, nil)
        }
        
    }
    
    
    internal func requestWithPullToRefresh(success:@escaping (_ dataEnd: Bool, _ insertPaths: [IndexPath]?) -> Void,
                                           fail:@escaping (_ error: Error) -> Void){
        self.filter.offset = 0
        self.data = []
        
        self.requestData(newRequest: true, success: success, fail: fail)
    }
    
    internal func handleResponseData( response_data: [AnyObject]) -> [IndexPath]{
        
        
        var indexPaths: [IndexPath] = []
        let lastIndex = self.data.count
        
        // detect pagging end data
        self.isDataEnd = response_data.count < limitDefault
        
        self.data += response_data
        
        if self.data.count > limitDefault{
            // is pagging
            if self.proxy == .station{
                for index in 0...response_data.count - 1{
                    indexPaths.append(IndexPath.init(row: 0, section: (lastIndex + index)))
                }
            }else{
                for index in 0...response_data.count - 1{
                    indexPaths.append(IndexPath.init(row: (lastIndex + index), section: 0))
                }
            }
        }
        
        self.filter.offset = self.data.count
        
        return indexPaths
    }
    
    private func requestwithUser(request: DataRequest){
        switch self.type {
        case .deejay_vertical, .deejay_horizontal:
            //
            DataProxy.user.getObjectsUser(object_type: .artist, filter: self.filter, dataRequest: request)
            break
            
        case .mixset:
            //
            DataProxy.user.getObjectsUser(object_type: .mix, filter: self.filter, dataRequest: request)
            break
            
        case .track:
            //
            DataProxy.user.getObjectsUser(object_type: .track, filter: self.filter, dataRequest: request)
            break
            
        case .video:
            //
            DataProxy.user.getObjectsUser(object_type: .video, filter: self.filter, dataRequest: request)
            break
            
        default:
            //
            break
        }
    }
    
    private func requestWithArtst(userId: String?, request: DataRequest){
        DataProxy.artist.artistsTopDJ(userId: userId, filter: self.filter, dataRequest: request)
    }
    
    
    private func requestWithVideo(request: DataRequest){
        DataProxy.video.videoRelated(filter: self.filter, dataRequest: request)
    }
    
    private func requestWithArtistDetail(request: DataRequest){
        switch self.type {
        case .mixset:
            //
            DataProxy.artist.artistsMixMore(filter: self.filter, dataRequest: request)
            break
            
        case .track:
            //
            DataProxy.artist.artistsTrackMore(filter: self.filter, dataRequest: request)
            break
            
        case .video:
            //
            DataProxy.artist.artistsVideoMore(filter: self.filter, dataRequest: request)
            break
            
        case .unknow:
            // get artist detail
            DataProxy.artist.artistsDetail(user_id: nil, filter: self.filter, dataRequest: request)
            break
            
        default:
            //
            print("ProductListView requestWithSearch fail with type \(self.type) ")
            break
        }
        
    }
    
    private func requestWithChart(request: DataRequest){
        switch self.type {
        case .mixset:
            //
            DataProxy.chart.chartGet(type: .Mix, filter: self.filter, dataRequest: request)
            break
            
        case .track:
            //
            DataProxy.chart.chartGet(type: .Track, filter: self.filter, dataRequest: request)
            break
            
        case .video:
            //
            DataProxy.chart.chartGet(type: .Video, filter: self.filter, dataRequest: request)
            break
            
        default:
            //
            print("ProductListView requestWithSearch fail with type \(self.type) ")
            break
        }
    }
    
    private func requestWithPlaylist(request: DataRequest){
        DataProxy.playlist.playlistDetail(filter: self.filter, dataRequest: request)
    }
    
    private func requestWithStation(request: DataRequest){
        DataProxy.station.fetch_stationNewfeed(filter: self.filter, user_id: DataProxy.user.userInfo?.id, dataRequest: request)
    }
    
    private func requestWithDownload(request: DataRequest){
        switch self.type {
        case .track:
            //
            DataProxy.download.fetchDownloadSongData(type: .Track, dataRequest: request)
            break;
            
        case .mixset:
            //
            DataProxy.download.fetchDownloadSongData(type: .Mix, dataRequest: request)
            break
            
        case .all:
            // get all
            DataProxy.download.fetchDownloadSongData(type: nil, dataRequest: request)
            break
        default:
            // get all
            break
        }
    }
    
    private func requestWithSearch(request: DataRequest){
        switch self.type {
        case .mixset:
            //
            DataProxy.search.searchWith(type: .mixset, filter: self.filter, dataRequest: request)
            break
            
        case .track:
            //
            DataProxy.search.searchWith(type: .track, filter: self.filter, dataRequest: request)
            break
            
        case .video:
            //
            DataProxy.search.searchWith(type: .video, filter: self.filter, dataRequest: request)
            break
            
        case .deejay_horizontal, .deejay_vertical:
            //
            DataProxy.search.searchWith(type: .deejay, filter: self.filter, dataRequest: request)
            break
            
        default:
            //
            print("ProductListView requestWithSearch fail with type \(self.type) ")
            break
        }
    }
    
    
    private func requestWithFeature(request: DataRequest){
        switch self.type {
        case .mixset:
            //
            DataProxy.feature.featureMixMore(offset: self.data.count, dataRequest: request)
            break
            
        case .track:
            //
            DataProxy.feature.featureTrackMore(offset: self.data.count, dataRequest: request)
            break
            
        case .video:
            //
            DataProxy.feature.featureVideoMore(offset: self.data.count, dataRequest: request)
            break
            
        case .chart:
            fatalError("chart must be call in chartlistviewcontroller")
            break
            
        default:
            //
            fatalError("cannot request data with unknow type")
            break
        }
    }
    
    func removeSongItemAt(indexPath: IndexPath) -> Bool{
        
        if var objects = self.itemsProductAt(index: indexPath.section), objects.count > indexPath.row{
            
            objects.remove(at: indexPath.row)
            
            if let productList = self.itemAtIndex(index: indexPath.section) as? ProductList{
                productList.object = objects
                
                self.data[indexPath.section] = productList
                
                return true
            }
            
        }else if self.data.count > indexPath.row{
            
            self.data.remove(at: indexPath.row)
            return true
        }
        
        return false
    }
    
}

struct Filter{
    var offset: Int = 0
    var keyword: String = ""
    
    var requestObject: AnyObject?
    
    init(_keyword: String) {
        self.keyword = _keyword
    }
    
}

protocol ProductListViewDataSource {
    //
    func productListView_spacingBetweenItem() -> CGFloat
    func productListView_scrollDirection() -> UICollectionViewScrollDirection
    func productListView_contentInsect() -> UIEdgeInsets
}

protocol ProductListViewDelegate {
    //
    func productListView_BtnShuffleSongDidTap(datas: [AnyObject]?, type: SongType)
    func productListView_shouldDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) -> Bool
    func productListView_didDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?)
    func productListView_didSelectedAtIndexPath(indexPath: IndexPath, withData Datas: [AnyObject]?, type: SongType)
    func productListView_HeaderTitleDidTapButtonMore(productInfo: ProductListInfo)
    func productListView_MoreTapAtIndexPath(index: Int, withData Datas: [AnyObject]?)
    func productListView_BtnAutoPlayVideoDidChange(isOn: Bool)
    
    func productListView_BtnShareDidTap(sender: UIButton, object: AnyObject)
    func productListView_BtnLikeDidTap(sender: UIButton, object: AnyObject)
    func productListview_BtnMoreDidTap(sender: UIButton, object: AnyObject)
}

protocol ProductListScrollingDelegate {
    func productListView_ScrollViewDidScroll(scrollView: UIScrollView)
    func productListView_ScrollViewDidEndDecelerating(scrollView: UIScrollView)
    func productListView_ScrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool)
}

class ProductListView: UIView, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, SongCellDelegate, SlideMenuWithCoverImageCollectionReusableViewDelegate, StationCollectionReusableViewDelegate, HorizontalCollectionDelegate, TitleCollectionHeaderDelegate, SongInfoReusableViewDelegate, ArtistCollectionCellDelegate, SongDownloadDelegate {
    
    private let queueConcurrent = DispatchQueue(label: "ProductListView", attributes: .concurrent)
    
    internal var myCollectionView: UICollectionView?
    internal var lblError: UILabel?
    internal var refreshControl: UIRefreshControl?
    internal var isUserDragging = false
    internal var productInfo: ProductListInfo?
    
    public var delegate: ProductListViewDelegate?
    public var delegate_scrolling: ProductListScrollingDelegate?
    public var datasource: ProductListViewDataSource?
    public var isEnablePullRefresh = false{
        didSet{
            // setup refresh control
            if isEnablePullRefresh == true{
                self.setupRefreshControl()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupFristLoad()
        
    }
    
    init() {
        super.init(frame: CGRect())
        self.setupFristLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupFristLoad()
    }
    
    func reloadData(){
        self.myCollectionView?.reloadData()
    }
    
    func reloadItemAtIndexPaths(indexPaths: [IndexPath]){
        
        self.myCollectionView?.performBatchUpdates({
            self.myCollectionView?.reloadItems(at: indexPaths)
        }, completion: { (finish) in
            //
        })
    }
    
    func reloadItemAtSection(section: Int){
        self.myCollectionView?.performBatchUpdates({
            self.myCollectionView?.reloadSections(IndexSet(integer: section))
        }, completion: { (finish) in
            //
        })
    }
    
    private func setupFristLoad(){
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize.init(width: screenWidth/2, height: 100.0)
        layout.scrollDirection = self.datasource?.productListView_scrollDirection() ?? .vertical
        
        myCollectionView?.collectionViewLayout = layout
        myCollectionView?.scrollsToTop = true
        myCollectionView?.contentInset = UIEdgeInsetsMake(8.0, 0, 20.0, 0)
        myCollectionView = UICollectionView.init(frame: CGRect(), collectionViewLayout: layout)
        
        self.setScrollIndicator(hidden: true)
        self.setAlwaysBouncingVertical(enable: true)
        self.setProductListBackgroundColor(color: UIColor.black) // background color default
        
        if self.myCollectionView != nil {
            
            if !self.subviews.contains(self.myCollectionView!){
                self.addSubview(self.myCollectionView!)
            }
            
            // auto layout for myCollectionView
            self.myCollectionView?.addTopConstraint(toView: self, attribute: .top, relation: .equal, constant: 0)
            self.myCollectionView?.addLeftConstraint(toView: self, attribute: .left, relation: .equal, constant: 0)
            self.myCollectionView?.addBottomConstraint(toView: self, attribute: .bottom, relation: .equal, constant: 0)
            self.myCollectionView?.addRightConstraint(toView: self, attribute: .right, relation: .equal, constant: 0)
            
            self.myCollectionView?.layoutIfNeeded()
            
            // register cell
            self.registerCell()
        }
        
        if self.lblError == nil {
            self.lblError = UILabel.init()
            self.lblError?.textAlignment = .center
            self.lblError?.font = UIFont.fontSemiBold_extraLarge()
            self.lblError?.textColor = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue).withAlphaComponent(0.6)
            self.lblError?.numberOfLines = 0
            
            self.lblError?.isUserInteractionEnabled = true
            
            let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.labelErrorDidTap))
            tapGesture.numberOfTapsRequired = 1
            self.lblError?.addGestureRecognizer(tapGesture)
            
            self.lblError?.layer.zPosition = 0
            
            self.addSubview(self.lblError!)
            self.bringSubview(toFront: self.lblError!)
            self.lblError?.isHidden = true
            
            self.lblError?.addTopConstraint(toView: self, attribute: .top, relation: .equal, constant: 0)
            self.lblError?.addLeftConstraint(toView: self, attribute: .left, relation: .equal, constant: 10)
            self.lblError?.addBottomConstraint(toView: self, attribute: .bottom, relation: .equal, constant: 0)
            self.lblError?.addRightConstraint(toView: self, attribute: .right, relation: .equal, constant: -10)
            
            self.layoutIfNeeded()
        }
    }
    
    @objc private func labelErrorDidTap(){
        if self.productInfo != nil {
            self.requestWithProductListInfo(_productInfo: self.productInfo!, newRequest: true)
        }
    }
    
    func requestWithProductListInfo(_productInfo: ProductListInfo, newRequest: Bool){
        
        // hidden lbl error wheen user retry
        self.lblError?.isHidden = true
        
        self.productInfo = _productInfo
        
        if newRequest == true{
            Loader.show(message: "", view: self)
        }
        
        self.productInfo?.requestData(newRequest: newRequest, success: { (isDataEnd, insertIndexPaths) in
            //
            
            if self.myCollectionView?.delegate == nil {
                if newRequest == true{
                    Loader.hide(view: self)
                }
                
                if self.myCollectionView?.dataSource == nil {
                    self.myCollectionView?.dataSource = self
                    self.myCollectionView?.delegate = self
                }
                
            }else{
                
                if insertIndexPaths?.count ?? 0 == 0{
                    self.myCollectionView?.collectionViewLayout.invalidateLayout()
                    self.myCollectionView?.reloadData()
                    
                }else{
                    
                    if self.productInfo?.proxyType() == .station{
                        let first = insertIndexPaths!.first
                        let last = insertIndexPaths!.last
                        
                        if first != nil{
                            self.myCollectionView?.collectionViewLayout.invalidateLayout()
                            self.myCollectionView?.performBatchUpdates({
                                //
                                self.myCollectionView?.insertSections(IndexSet(first!.section...last!.section))
                            }, completion: { (finish) in
                                //
                            })
                        }
                       
                        
                    }else{
                        self.myCollectionView?.collectionViewLayout.invalidateLayout()
                        self.myCollectionView?.performBatchUpdates({
                            //
                            self.myCollectionView?.insertItems(at: insertIndexPaths!)
                            
                        }, completion: { (finish) in
                            //
                        })
                    }
                }
                
                if newRequest == true{
                    Loader.hide(view: self)
                    self.endRefreshingWithAnimate(animate: true)
                }
            }
            
        }, fail: { (error) in
            //
            self.lblError?.text = error.localizedDescription + "\n Tap screen to try again"
            
            if newRequest == true{
                self.productInfo?.isDataEnd = true
                if self.myCollectionView?.dataSource == nil {
                    self.myCollectionView?.dataSource = self
                    self.myCollectionView?.delegate = self
                }else{
                    self.myCollectionView?.collectionViewLayout.invalidateLayout()
                    self.myCollectionView?.reloadData()
                }
                
                Loader.hide(view: self)
                
            }else{
                Loader.show(message: error.localizedDescription, view: self)
            }
            
            self.endRefreshingWithAnimate(animate: true)
        })
    }
    
    //MARK: - Register cell
    private func registerCell(){
        let cellNib = UINib.init(nibName: SongListCollectionCell.nameOfClass, bundle: nil)
        let cellNib2 = UINib.init(nibName: ChartListCollectionCell.nameOfClass, bundle: nil)
        let cellNib3 = UINib.init(nibName: SongGridCollectionCell.nameOfClass, bundle: nil)
        let cellNib4 = UINib.init(nibName: ArtistGridCollectionCell.nameOfClass, bundle: nil)
        let cellNib5 = UINib.init(nibName: VideoGridCollectionCell.nameOfClass, bundle: nil)
        let cellNib6 = UINib.init(nibName: LoadingListCollectionCell.nameOfClass, bundle: nil)
        let cellNib7 = UINib.init(nibName: ArtistListCollectionCell.nameOfClass, bundle: nil)
        let cellNib8 = UINib.init(nibName: ChartListVideoCollectionCell.nameOfClass, bundle: nil)
        let cellNib9 = UINib.init(nibName: ChartListStretchCollectionCell.nameOfClass, bundle: nil)
        let cellNib10 = UINib.init(nibName: ChartListVideoStretchCollectionCell.nameOfClass, bundle: nil)
        let cellNib11 = UINib.init(nibName: SongListDownloadCollectionCell.nameOfClass, bundle: nil)
        let cellNib12 = UINib.init(nibName: HorizontalCollectionViewCell.nameOfClass, bundle: nil)
        let cellNib13 = UINib.init(nibName: ArtistBannerCell.nameOfClass, bundle: nil)
        let cellNib14 = UINib.init(nibName: VideoListCollectionCell.nameOfClass, bundle: nil)
        
        self.myCollectionView?.register(cellNib, forCellWithReuseIdentifier: SongListCollectionCell.nameOfClass)
        self.myCollectionView?.register(cellNib2, forCellWithReuseIdentifier: ChartListCollectionCell.nameOfClass)
        self.myCollectionView?.register(cellNib3, forCellWithReuseIdentifier:  SongGridCollectionCell.nameOfClass)
        self.myCollectionView?.register(cellNib4, forCellWithReuseIdentifier: ArtistGridCollectionCell.nameOfClass)
        self.myCollectionView?.register(cellNib5, forCellWithReuseIdentifier: VideoGridCollectionCell.nameOfClass)
        self.myCollectionView?.register(cellNib6, forCellWithReuseIdentifier: LoadingListCollectionCell.nameOfClass)
        self.myCollectionView?.register(cellNib7, forCellWithReuseIdentifier: ArtistListCollectionCell.nameOfClass)
        self.myCollectionView?.register(cellNib8, forCellWithReuseIdentifier: ChartListVideoCollectionCell.nameOfClass)
        self.myCollectionView?.register(cellNib9, forCellWithReuseIdentifier: ChartListStretchCollectionCell.nameOfClass)
        self.myCollectionView?.register(cellNib10, forCellWithReuseIdentifier: ChartListVideoStretchCollectionCell.nameOfClass)
        self.myCollectionView?.register(cellNib11, forCellWithReuseIdentifier: SongListDownloadCollectionCell.nameOfClass)
        self.myCollectionView?.register(cellNib12, forCellWithReuseIdentifier: HorizontalCollectionViewCell.nameOfClass)
        self.myCollectionView?.register(cellNib13, forCellWithReuseIdentifier: ArtistBannerCell.nameOfClass)
        self.myCollectionView?.register(cellNib14, forCellWithReuseIdentifier: VideoListCollectionCell.nameOfClass)
        
        
        let nibFooter = UINib(nibName: LoadingCollectionReusableCell.nameOfClass, bundle: nil)
        self.myCollectionView?.register(nibFooter, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: LoadingCollectionReusableCell.nameOfClass)
        
        let nibHeader = UINib(nibName: SlideMenuWithCoverImageCollectionReusableView.nameOfClass, bundle: nil)
        self.myCollectionView?.register(nibHeader, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: SlideMenuWithCoverImageCollectionReusableView.nameOfClass)
        
        let nibHeader2 = UINib(nibName: StationCollectionReusableView.nameOfClass, bundle: nil)
        self.myCollectionView?.register(nibHeader2, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: StationCollectionReusableView.nameOfClass)
        
        let nibHeader3 = UINib(nibName: PlaylistDetailReusableView.nameOfClass, bundle: nil)
        self.myCollectionView?.register(nibHeader3, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: PlaylistDetailReusableView.nameOfClass)
        
        let nibHeader4 = UINib(nibName: TitleCollectionReusableView.nameOfClass, bundle: nil)
        self.myCollectionView?.register(nibHeader4, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: TitleCollectionReusableView.nameOfClass)
        
        let nibHeader5 = UINib(nibName: ArtistDetailReusableView.nameOfClass, bundle: nil)
        self.myCollectionView?.register(nibHeader5, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ArtistDetailReusableView.nameOfClass)
        
        let nibHeader6 = UINib(nibName: SongInfoResuableView.nameOfClass, bundle: nil)
        self.myCollectionView?.register(nibHeader6, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: SongInfoResuableView.nameOfClass)
    }
    
    
    //MARK: - CollectionView Delegate - Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        var number = self.productInfo?.data.count ?? 0
        
        if self.productInfo?.proxyType() == .station{
            
            
        }else if self.productInfo?.proxyType() == .user{
            if let itemProduct = self.productInfo?.itemsProductAt(index: 0){
                number = itemProduct.count > 0 ? 1 : 0
            }else{ number = 0 }
            
        }else if self.productInfo?.proxyType() == .artistDetail && self.productInfo?.type == .unknow{
            
            
        }else if self.productInfo?.proxyType() == .video && self.productInfo?.type == .unknow{
            
        }else {
            if number > 0{
                number = 1
            }
        }
        
        if !(self.lblError?.text?.length ?? 0 > 0){
            self.lblError?.text = AppMixError.data_notfound.rawValue
        }
        
        self.lblError?.isHidden = number > 0
        
        if !(number > 0){
            self.bringSubview(toFront: self.lblError!)
        }
        
        return number
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var number = 0
        if self.productInfo?.proxyType() == .station{
            number = self.station_numberItemInSection(section: section)
            
        }else if self.productInfo?.proxyType() == .artistDetail || self.productInfo?.proxyType() == .video ||
            self.productInfo?.proxyType() == .user{
            
            number = numberItemsProductListInSection(section: section)
        }else{
            number = self.productInfo?.data.count ?? 0
        }
        
        return number
    }
    
    func numberItemsProductListInSection(section: Int) -> Int{
        if let _productList = self.productInfo?.itemAtIndex(index: section) as? ProductList{
            
            if let _object = _productList.object{
                switch _productList.type {
                    
                case .deejay_horizontal, .mixset_horizontal, .track_horizontal,
                     .video_horizontal, .deejay_station_horizontal:
                    return _object.count > 0 ? 1 : 0
                    
                default:
                    return _object.count
                }
            }
        }
        return 0
    }
    
    func station_numberItemInSection(section: Int) -> Int{
        if let itemStation = self.productInfo?.itemAtIndex(index: section) as? Station{
            switch itemStation.type {
                
            case .deejay_horizontal, .mixset_horizontal, .track_horizontal,
                 .video_horizontal, .deejay_station_horizontal:
                return itemStation.recommend_object.count > 0 ? 1 : 0
                
            case .deejay_vertical, .track_vertical_delete,
                 .track_vertical, .video_vertical, .video_vertical_delete
            , .mixset_vertical, .mixset_vertical_delete:
                return itemStation.recommend_object.count
            default:
                return itemStation.object != nil ? 1 : 0
            }
        }
        return 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch self.productInfo!.proxyType() {
        case .chart:
            return self.cellForRow_withChart(collectionView:collectionView, atIndexPath:indexPath)
            
        case .download:
            return self.cellForRow_withDownload(collectionView:collectionView, atIndexPath:indexPath)
            
        case .playlistDetail:
            return self.cellForRow_withPlaylist(collectionView:collectionView, atIndexPath:indexPath)
            
        case .artist:
            return self.cellForRow_withArtist(collectionView:collectionView, atIndexPath:indexPath)
            
        case .artistDetail, .video, .user:
            return self.cellForRow_withProductList(collectionView:collectionView, atIndexPath:indexPath)
            
        case .station:
            return self.cellForRow_withStation(collectionView:collectionView, atIndexPath:indexPath)
            
        default:
            return self.cellForRow_withNormal(collectionView:collectionView, atIndexPath:indexPath)
        }
    }
    
    //MARK: - Cell for row with product type
    func cellForRow_withProductList(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        
        var cell: UICollectionViewCell = UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        if let itemProduct = self.productInfo?.itemAtIndex(index: indexPath.section) as? ProductList{
            switch itemProduct.type {
                
            case .mixset_vertical :
                cell = self.configureMixsetProductCell(collectionView: collectionView, atIndexPath: indexPath)
                break
                
            case .mixset_vertical_delete:
                cell = self.configureTrackProductCell(collectionView: collectionView, atIndexPath: indexPath, enableSwipeDelete: true)
                break
                
            case .track_vertical:
                cell = self.configureTrackProductCell(collectionView:collectionView, atIndexPath:indexPath, enableSwipeDelete: false)
                break
                
            case .track_vertical_delete:
                cell = self.configureTrackProductCell(collectionView:collectionView, atIndexPath:indexPath, enableSwipeDelete: true)
                break
                
                
            case .track_player_vertical:
                
                break
                
            case .deejay_horizontal, .track_horizontal, .mixset_horizontal, .video_horizontal, .deejay_station_horizontal:
                cell = self.configureHorizontalProductCell(collectionView: collectionView, indexPath: indexPath)
                break
                
            case .deejay_vertical:
                //
                cell = self.configureProductListDeejayListCell(collectionView: collectionView, indexPath: indexPath, enableFollow: false)
                break
                
            case .deejay_vertical_follow:
                cell = self.configureProductListDeejayListCell(collectionView: collectionView, indexPath: indexPath, enableFollow: true)
                break
                
            case .video_vertical:
                cell = self.configureVideoProductListGridCell(collectionView: collectionView, atIndexPath: indexPath)
                break
                
            case .video_vertical_delete:
                cell = self.configureVideoProductListCell(collectionView: collectionView, atIndexPath: indexPath, enableSwipeDelete: true)
                break
                
            case .keyword_roundRect:
                break
                
            case .keyword_normal:
                break
                
            case .unknow:
                break
                
            }
        }
        
        return cell
    }
    
    func cellForRow_withNormal(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        
        var cell: UICollectionViewCell = UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        switch self.productInfo!.type {
        case .mixset:
            cell = self.configureMixsetCell(collectionView: collectionView, atIndexPath: indexPath)
            break
            
        case .track:
            if self.productInfo?.proxyType() == .user || self.productInfo?.proxyType() == .playlistDetail{
                cell = self.configureTrackCell(collectionView:collectionView, atIndexPath:indexPath, enableSwipeDelete: true)
            }else{
                cell = self.configureTrackCell(collectionView:collectionView, atIndexPath:indexPath, enableSwipeDelete: false)
            }
            
            break
            
        case .trackPlayer:
            cell = self.configureTrackListCell(collectionView:collectionView, atIndexPath:indexPath)
            break
            
        case .trackPlayerMasup:
            cell = self.configureTrackListMasupCell(collectionView:collectionView, atIndexPath:indexPath)
            break
            
        case .chart:
            cell = self.configureChartCell(collectionView:collectionView, atIndexPath:indexPath)
            break
            
        case .deejay_vertical, .deejay_horizontal:
            //fixme
            cell = self.configureDeejayListCell(collectionView: collectionView, indexPath: indexPath)
            break
            
        case .video:
            cell = self.configureVideoCell(collectionView:collectionView, atIndexPath:indexPath)
            break
            
        case .station:
            break
            
        case .keyword_trend:
            break
            
        case .keyword_recently:
            break
            
            
        case .all:
            break
            
            
        case .unknow:
            break
            
        }
        
        return cell
    }
    
    func cellForRow_withArtist(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        
        var cell: UICollectionViewCell = UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        switch self.productInfo!.type_listing {
        case .deejay_vertical:
            
            if indexPath.row == 0{
                cell = self.configureDeejayTop1Cell(collectionView: collectionView, indexPath: indexPath)
            }else{
                cell = self.configureDeejayListCell(collectionView: collectionView, indexPath: indexPath)
            }
            
        case .deejay_horizontal, .deejay_station_horizontal:
            cell = self.configureHorizontalCell(collectionView: collectionView, indexPath: indexPath)
            
        default:
            break
        }
        
        
        return cell
    }
    
    func cellForRow_withStation(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        
        var cell: UICollectionViewCell = UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        if let itemStation = self.productInfo?.itemAtIndex(index: indexPath.section) as? Station{
            switch itemStation.type {
                
            case .mixset_vertical, .mixset_horizontal, .mixset_vertical_delete:
                cell = self.configureMixsetCell(collectionView: collectionView, atIndexPath: indexPath)
                break
                
            case .track_vertical:
                cell = self.configureTrackCell(collectionView:collectionView, atIndexPath:indexPath, enableSwipeDelete: true)
                break
                
            case .track_player_vertical:
                
                break
                
            case .deejay_horizontal, .track_horizontal, .deejay_station_horizontal:
                //fixme
                cell = self.configureHorizontalCell(collectionView: collectionView, indexPath: indexPath)
                break
                
            case .deejay_vertical:
                //
                cell = self.configureStationDeejayListCell(collectionView: collectionView, indexPath: indexPath)
                break
                
            case .video_vertical, .video_horizontal, .video_vertical_delete:
                cell = self.configureVideoCell(collectionView:collectionView, atIndexPath:indexPath)
                break
                
            case .keyword_roundRect:
                break
                
            case .keyword_normal:
                break
                
            case .unknow:
                break
                
            default:
                break
                
            }
        }
        
        return cell
    }
    
    func cellForRow_withPlaylist(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        
        var cell: UICollectionViewCell = UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        switch self.productInfo!.type_listing {
            
        case .mixset_vertical:
            cell = self.configureTrackPlaylistCell(collectionView:collectionView, atIndexPath:indexPath, enableSwipeDelete: true)
            break
            
        case .mixset_vertical_delete:
            cell = self.configureTrackPlaylistCell(collectionView:collectionView, atIndexPath:indexPath, enableSwipeDelete: true)
            
        case .track_vertical_delete:
            cell = self.configureTrackPlaylistCell(collectionView:collectionView, atIndexPath:indexPath, enableSwipeDelete: true)
            break
            
        case .track_vertical:
            cell = self.configureTrackPlaylistCell(collectionView:collectionView, atIndexPath:indexPath, enableSwipeDelete: true)
            break
            
        case .track_player_vertical:
            
            break
            
        case .deejay_horizontal, .track_horizontal, .mixset_horizontal, .video_horizontal, .deejay_station_horizontal:
            //fixme
            cell = self.configureHorizontalCell(collectionView: collectionView, indexPath: indexPath)
            break
            
        case .deejay_vertical:
            //
            cell = self.configureStationDeejayListCell(collectionView: collectionView, indexPath: indexPath)
            break
            
        case .video_vertical, .video_vertical_delete:
            cell = self.configureVideoCell(collectionView:collectionView, atIndexPath:indexPath)
            break
            
        case .keyword_roundRect:
            break
            
        case .keyword_normal:
            break
            
        case .unknow:
            break
            
        default:
            break
        }
        
        return cell
    }
    
    func cellForRow_withDownload(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        
        var cell: UICollectionViewCell = UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        switch self.productInfo!.type {
        case .mixset:
            cell = self.configureDownloadSongCell(collectionView: collectionView, atIndexPath: indexPath)
            break
            
        case .track:
            cell = self.configureDownloadSongCell(collectionView: collectionView, atIndexPath: indexPath)
            break
            
        case .trackPlayer:
            break
            
        case .trackPlayerMasup:
            break
            
        case .chart:
            break
            
        case .deejay_vertical, .deejay_horizontal:
            break
            
        case .video:
            cell = self.configureChartVideoStretchCell(collectionView:collectionView, atIndexPath:indexPath)
            break
            
            
        case .all:
            cell = self.configureDownloadSongCell(collectionView: collectionView, atIndexPath: indexPath)
            break
            
        case .station:
            break
            
        case .keyword_trend:
            break
            
        case .keyword_recently:
            break
            
            
        case .unknow:
            break
            
        }
        
        return cell
    }
    
    
    func cellForRow_withChart(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        
        var cell: UICollectionViewCell = UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        switch self.productInfo!.type {
        case .mixset:
            cell = self.configureChartStretchCell(collectionView:collectionView, atIndexPath:indexPath)
            break
            
        case .track:
            cell = self.configureChartStretchCell(collectionView:collectionView, atIndexPath:indexPath)
            break
            
        case .trackPlayer:
            break
            
        case .trackPlayerMasup:
            break
            
        case .chart:
            break
            
        case .deejay_vertical, .deejay_horizontal:
            break
            
        case .video:
            cell = self.configureChartVideoStretchCell(collectionView:collectionView, atIndexPath:indexPath)
            break
            
        case .station:
            break
            
        case .all:
            break
            
        case .keyword_trend:
            break
            
        case .keyword_recently:
            break
            
            
        case .unknow:
            break
            
        }
        
        return cell
    }
    
    // MARK: Mixset
    func configureMixsetProductCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SongGridCollectionCell.nameOfClass, for: indexPath) as? SongGridCollectionCell{
            
            if let _song = self.productInfo?.itemProductAtIndexPath(indexPath: indexPath) as? Song{
                cell.configureCellWith(_song: _song)
                cell.delegate = self
                
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureMixsetCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SongGridCollectionCell.nameOfClass, for: indexPath) as? SongGridCollectionCell{
            
            if let _song = self.productInfo?.itemAtIndexPath(indexPath: indexPath) as? Song{
                cell.configureCellWith(_song: _song)
                cell.delegate = self
                
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Video
    func configureVideoCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:VideoGridCollectionCell.nameOfClass, for: indexPath) as? VideoGridCollectionCell{
            
            if let _song = self.productInfo?.itemAtIndexPath(indexPath: indexPath) as? Song{
                cell.configureCellWith(_song: _song)
                cell.delegate = self
                
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    
    
    func configureVideoProductListGridCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:VideoGridCollectionCell.nameOfClass, for: indexPath) as? VideoGridCollectionCell{
            
            if let _song = self.productInfo?.itemProductAtIndexPath(indexPath: indexPath) as? Song{
                cell.configureCellWith(_song: _song)
                cell.delegate = self
                
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureVideoProductListCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath, enableSwipeDelete: Bool) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:VideoListCollectionCell.nameOfClass, for: indexPath) as? VideoListCollectionCell{
            
            if let _song = self.productInfo?.itemProductAtIndexPath(indexPath: indexPath) as? Song{
                cell.configureCellWith(_song: _song)
                cell.currentIndexPath = indexPath
                
                if cell.delegate == nil {
                    cell.delegate = self
                    cell.isEnableSwipeToDelete = enableSwipeDelete
                }
                
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Download
    func configureDownloadSongCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:SongListDownloadCollectionCell.nameOfClass, for: indexPath) as? SongListDownloadCollectionCell{
            
            if let _song = self.productInfo?.itemAtIndexPath(indexPath: indexPath) as? Song{
                
                cell.currentIndexPath = indexPath
                
                if cell.delegate == nil {
                    cell.delegate = self
                    cell.isEnableSwipeToDelete = true
                }
                
                if cell.delegate_songDownload == nil {
                    cell.delegate_songDownload = self
                }
                
                if self.productInfo!.type == .track{
                    let info = DataProxy.download.getDownloadInfo(itemSong: _song, type: .Track)
                    cell.configureCellWith(song: _song, type: .Track, downloadInfo: info)
                    
                }else if self.productInfo!.type == .mixset{
                    let info = DataProxy.download.getDownloadInfo(itemSong: _song, type: .Mix)
                    cell.configureCellWith(song: _song, type: .Mix, downloadInfo: info)
                }else{
                    let info = DataProxy.download.getDownloadInfo(itemSong: _song, type: nil)
                    cell.configureCellWith(song: _song, type: .Mix, downloadInfo: info)
                }
                
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Track
    func configureTrackCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath, enableSwipeDelete: Bool) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:SongListCollectionCell.nameOfClass, for: indexPath) as? SongListCollectionCell{
            
            if let _song = self.productInfo?.itemAtIndexPath(indexPath: indexPath) as? Song{
                cell.configureCellWith(_song: _song)
                cell.currentIndexPath = indexPath
                
                if cell.delegate == nil  {
                    cell.delegate = self
                    cell.isEnableSwipeToDelete = enableSwipeDelete
                }
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureTrackProductCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath, enableSwipeDelete: Bool) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:SongListCollectionCell.nameOfClass, for: indexPath) as? SongListCollectionCell{
            
            if let _song = self.productInfo?.itemProductAtIndexPath(indexPath: indexPath) as? Song{
                cell.configureCellWith(_song: _song)
                cell.currentIndexPath = indexPath
                
                if cell.delegate == nil {
                    cell.delegate = self
                    cell.isEnableSwipeToDelete = enableSwipeDelete
                }
                
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: TrackList
    func configureTrackListCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:SongListCollectionCell.nameOfClass, for: indexPath) as? SongListCollectionCell{
            
            cell.currentIndexPath = indexPath
            
            if cell.delegate == nil {
                cell.delegate = self
                cell.isEnableSwipeToDelete = true
            }
            
            cell.cell_viewContain?.backgroundColor = UIColor.clear
            
            if let _song = self.productInfo?.itemAtIndexPath(indexPath: indexPath) as? Song{
                cell.configureCellWithHighLightEnable(_song: _song)
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureTrackListMasupCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:SongListCollectionCell.nameOfClass, for: indexPath) as? SongListCollectionCell{
            
            cell.currentIndexPath = indexPath
            
            if cell.delegate == nil {
                cell.delegate = self
            }
            
            cell.cell_viewContain?.backgroundColor = UIColor.clear
            
            let preivous_second = (self.productInfo?.itemAtIndex(index: indexPath.row - 1) as? Song_Masup)?.location_second
            let next_second = (self.productInfo?.itemAtIndex(index: indexPath.row + 1) as? Song_Masup)?.location_second
            
            if let _song = self.productInfo?.itemAtIndex(index: indexPath.row) as? Song_Masup{
                
                let currentTime = Int(DLTPlayer.sharedInstance.getPlayerItemCurrentTime())
                let songDuration = Int(DLTPlayer.sharedInstance.getPlayerItemDuration())
                
                let isHightLight = (currentTime >= (_song.location_second) &&
                                    currentTime < (next_second ?? songDuration))
                
                cell.configureCellTrackMasupWith(_song: _song,enableHightLight: !isHightLight)
                
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureTrackPlaylistCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath, enableSwipeDelete: Bool) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:SongListCollectionCell.nameOfClass, for: indexPath) as? SongListCollectionCell{
            
            if let _song = self.productInfo?.itemAtIndexPath(indexPath: indexPath) as? Song{
                cell.configureCellWith(_song: _song)
                cell.currentIndexPath = indexPath
                
                cell.cell_viewContain?.backgroundColor = UIColor.clear
                
                if cell.delegate == nil  {
                    cell.delegate = self
                    cell.isEnableSwipeToDelete = enableSwipeDelete
                }
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Chart
    func configureChartCell(collectionView: UICollectionView,atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChartListCollectionCell.nameOfClass, for: indexPath) as? ChartListCollectionCell{
            
            if let _song = self.productInfo?.itemAtIndexPath(indexPath: indexPath) as? Song{
                cell.configureCellWith(_song: _song, rankIndex: indexPath.row)
                cell.currentIndexPath = indexPath
                
                if cell.delegate == nil {
                    cell.delegate = self
                }
                
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureChartStretchCell(collectionView: UICollectionView,atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChartListStretchCollectionCell.nameOfClass, for: indexPath) as? ChartListStretchCollectionCell{
            
            if let _song = self.productInfo?.itemAtIndexPath(indexPath: indexPath) as? Song{
                cell.configureCellWith(_song: _song, rankIndex: indexPath.row)
                cell.currentIndexPath = indexPath
                
                if cell.delegate == nil {
                    cell.delegate = self
                }
                
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureChartVideoCell(collectionView: UICollectionView,atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChartListVideoCollectionCell.nameOfClass, for: indexPath) as? ChartListVideoCollectionCell{
            
            if let _song = self.productInfo?.itemAtIndexPath(indexPath: indexPath) as? Song{
                cell.currentIndexPath = indexPath
                if cell.delegate == nil {
                    cell.delegate = self
                }
                cell.configureCellWith(_song: _song, rankIndex: indexPath.row)
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureChartVideoStretchCell(collectionView: UICollectionView,atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChartListVideoStretchCollectionCell.nameOfClass, for: indexPath) as? ChartListVideoStretchCollectionCell{
            
            if let _song = self.productInfo?.itemAtIndexPath(indexPath: indexPath) as? Song{
                cell.currentIndexPath = indexPath
                if cell.delegate == nil {
                    cell.delegate = self
                }
                cell.configureCellWith(_song: _song, rankIndex: indexPath.row)
                return cell
            }
        }
        
        return configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Deejay
    func configureDeejayListCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ArtistListCollectionCell.nameOfClass, for: indexPath) as? ArtistListCollectionCell{
            
            if let _artist = self.productInfo?.itemAtIndexPath(indexPath: indexPath) as? Artist{
                
                cell.configureCellWith(_artist: _artist, enableFollow: true)
                cell.delegate = self
                
                return cell
            }else if let _artist = self.productInfo?.itemAtIndex(index: indexPath.row) as? Artist{
                cell.configureCellWith(_artist: _artist, enableFollow: true)
                cell.delegate = self
                
                return cell
            }
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureDeejayTop1Cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ArtistBannerCell.nameOfClass, for: indexPath) as? ArtistBannerCell{
            
            if let _artist = self.productInfo?.itemAtIndexPath(indexPath: indexPath) as? Artist{
                
                cell.configureWith(itemArtist: _artist)
                return cell
            }else if let _artist = self.productInfo?.itemAtIndex(index: indexPath.row) as? Artist{
                cell.configureWith(itemArtist: _artist)
                return cell
            }
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureProductListDeejayListCell(collectionView: UICollectionView, indexPath: IndexPath, enableFollow: Bool) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ArtistListCollectionCell.nameOfClass, for: indexPath) as? ArtistListCollectionCell{
            
            if let _aritst = self.productInfo?.itemProductAtIndexPath(indexPath: indexPath) as? Artist{
                cell.delegate = self
                cell.configureCellWith(_artist: _aritst, enableFollow: enableFollow)
            }
            return cell
        }
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    
    func configureStationDeejayListCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ArtistListCollectionCell.nameOfClass, for: indexPath) as? ArtistListCollectionCell{
            
            if let _artist = self.productInfo?.itemStationAtIndexPath(indexPath: indexPath) as? Artist{
                cell.configureCellWith(_artist: _artist, enableFollow: false)
                return cell
            }
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Loading
    func configureLoadingListCollectionCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LoadingListCollectionCell.nameOfClass, for: indexPath) as!LoadingListCollectionCell
        return cell
    }
    
    //MARK: Horizontal
    func configureHorizontalCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.nameOfClass, for: indexPath) as? HorizontalCollectionViewCell{
            
            cell.backgroundColor = UIColor.black
            
            queueConcurrent.sync(execute: {
                if cell.delegate == nil {
                    if self.productInfo?.proxyType() == .station{
                        if let itemStation = self.productInfo?.itemAtIndex(index: indexPath.section) as? Station{
                            switch itemStation.type {
                                
                            case .deejay_station_horizontal, .deejay_horizontal:
                                cell.type = .deejayOverlay
                                break
                                
                            case .video_horizontal:
                                cell.type = .video
                                break
                                
                            case .mixset_horizontal, .track_horizontal:
                                cell.type = .mixset
                                break
                                
                            default:
                                return cell.type = .unknow
                            }
                        }
                    }
                    
                    cell.currentSection = indexPath.section
                    cell.delegate = self
                }
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureHorizontalProductCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.nameOfClass, for: indexPath) as? HorizontalCollectionViewCell{
            
            cell.backgroundColor = UIColor.black
            
            queueConcurrent.sync(execute: {
                //                    if cell.delegate == nil {
                if let itemStation = self.productInfo?.itemAtIndex(index: indexPath.section) as? ProductList{
                    switch itemStation.type {
                        
                    case .deejay_station_horizontal, .deejay_horizontal:
                        cell.type = .deejayOverlay
                        break
                        
                    case .video_horizontal:
                        cell.type = .video
                        break
                        
                    case .mixset_horizontal, .track_horizontal:
                        cell.type = .mixset
                        break
                        
                    default:
                        return cell.type = .unknow
                    }
                }
                //                    }
                cell.currentSection = indexPath.section
                cell.delegate = self
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    
    
    func numberOfItemInSection(type: HorizontalType, parentSection: Int?) -> Int? {
        
        if parentSection == nil {
            return 0
        }
        
        if self.productInfo?.proxyType() == .station{
            if let itemStation = self.productInfo?.itemAtIndex(index: parentSection!) as? Station{
                switch itemStation.type {
                case .deejay_horizontal, .video_horizontal, .mixset_horizontal, .track_horizontal, .deejay_station_horizontal:
                    return itemStation.recommend_object.count
                default:
                    return itemStation.object != nil ? 1 : 0
                }
            }
            
        }else if let itemProduct = self.productInfo?.itemAtIndex(index: parentSection!) as? ProductList {
            if let _objects = itemProduct.object, _objects.count > 0{
                return _objects.count
            }
        }
        return 0
    }
    
    func horizontal_objectItem(type: HorizontalType, indexPath: IndexPath, parentSection: Int?) -> AnyObject? {
        if parentSection == nil {
            return nil
        }
        
        if self.productInfo?.proxyType() == .station{
            if let itemStation = self.productInfo?.itemAtIndex(index: parentSection!) as? Station{
                switch itemStation.type {
                case .deejay_horizontal, .video_horizontal, .mixset_horizontal, .track_horizontal, .deejay_station_horizontal:
                    if indexPath.row < itemStation.recommend_object.count {
                        return itemStation.recommend_object[indexPath.row]
                    }
                default:
                    return nil
                }
            }
        }else {
            let actuallyIndexPath = IndexPath.init(row: indexPath.row, section: parentSection!)
            return self.productInfo?.itemProductAtIndexPath(indexPath: actuallyIndexPath)
        }
        return nil
    }
    
    func horizontal_shouldReloadData(type: HorizontalType, parentSection: Int?) -> Bool {
        return true
    }
    
    func horizontal_shoudSelectedItem(type: HorizontalType, indexPath: IndexPath, parentSection: Int?) -> Bool {
        
        if parentSection == nil{
            return false
        }
        
        let actuallyIndexPath = IndexPath.init(row: indexPath.row, section: parentSection!)
        
        if self.productInfo?.proxyType() == .station{
            if let items = self.productInfo?.itemsStationRecomendAtIndex(index: parentSection!){
                
                self.delegate?.productListView_didSelectedAtIndexPath(indexPath: actuallyIndexPath,
                                                                      withData: items,type: .Artist)
            }
            
        }else{
            if let items = self.productInfo?.itemsProductAt(index: parentSection!){
                self.delegate?.productListView_didSelectedAtIndexPath(indexPath: actuallyIndexPath,
                                                                      withData: items,type: .Artist)
            }
        }
        
        return true
    }
    
    //MARK: - Selected Action
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        switch self.productInfo!.proxyType() {
            
        case .station:
            self.shouldSelectedItemStationAt(indexPath: indexPath)
            break
            
            
        default:
            self.shouldSelectedItemNormalAt(indexPath: indexPath)
        }
        
        
        return true
    }
    
    func shouldSelectedItemStationAt(indexPath: IndexPath){
        if let itemStation = self.productInfo?.itemAtIndex(index: indexPath.section) as? Station, itemStation.object != nil{
            
            let stationIndexPath = IndexPath(row: 0, section: 0)
            
            switch itemStation.type {
            case .mixset_vertical, .mixset_vertical_delete, .mixset_horizontal:
                self.delegate?.productListView_didSelectedAtIndexPath(indexPath: stationIndexPath,
                                                                      withData: [itemStation.object!],type: .Mix)
                break
                
            case .track_vertical, .track_player_vertical, .track_vertical_delete, .track_horizontal:
                self.delegate?.productListView_didSelectedAtIndexPath(indexPath: stationIndexPath,
                                                                      withData: [itemStation.object!],type: .Track)
                break
                
            case .deejay_vertical_follow, .deejay_vertical:
                break
                
            case .video_vertical, .video_vertical_delete, .video_horizontal:
                self.delegate?.productListView_didSelectedAtIndexPath(indexPath: stationIndexPath,
                                                                      withData: [itemStation.object!],type: .Video)
                break
                
            default:
                break
                
            }
        }
    }
    
    func shouldSelectedItemNormalAt(indexPath: IndexPath){
        if self.productInfo != nil {
            
            if let itemProduct = self.productInfo?.itemAtIndex(index: indexPath.section) as? ProductList {
                if let _objects = itemProduct.object, _objects.count > 0{
                    switch itemProduct.type {
                    case .mixset_vertical, .mixset_horizontal, .mixset_vertical_delete:
                        //
                        self.delegate?.productListView_didSelectedAtIndexPath(indexPath: indexPath, withData: _objects,type: .Mix)
                        break
                        
                    case .video_horizontal, .video_vertical, .video_vertical_delete:
                        //
                        self.delegate?.productListView_didSelectedAtIndexPath(indexPath: indexPath, withData: _objects,type: .Video)
                        break
                        
                    case .track_player_vertical, .track_horizontal, .track_vertical, .track_vertical_delete:
                        self.delegate?.productListView_didSelectedAtIndexPath(indexPath: indexPath, withData: _objects,type: .Track)
                        break
                        
                    case .deejay_vertical, .deejay_horizontal, .deejay_station_horizontal, .deejay_vertical_follow:
                        self.delegate?.productListView_didSelectedAtIndexPath(indexPath: indexPath, withData: _objects,type: .Artist)
                        break
                        
                    default:
                        break
                    }
                }
                return
            }
            
            switch self.productInfo!.type {
            case .mixset:
                //
                self.delegate?.productListView_didSelectedAtIndexPath(indexPath: indexPath, withData: self.productInfo?.data,type: .Mix)
                break
                
            case .video:
                //
                self.delegate?.productListView_didSelectedAtIndexPath(indexPath: indexPath, withData: self.productInfo?.data,type: .Video)
                break
                
            default:
                self.delegate?.productListView_didSelectedAtIndexPath(indexPath: indexPath, withData: self.productInfo?.data,type: .Track)
            }
        }
    }
    
    func shouldShufflePlaylistDetail(){
        self.delegate?.productListView_BtnShuffleSongDidTap(datas: self.productInfo?.data, type: .Track)
    }
    
    
    // Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.productInfo?.proxyType() == .station{
            if let itemStation = self.productInfo?.itemAtIndex(index: indexPath.section) as? Station{
                switch itemStation.type {
                case .deejay_horizontal, .deejay_station_horizontal:
                    return CGSize.init(width: self.frame.width, height: deejayOverlaySize.height)
                    
                    
                case .deejay_vertical, .deejay_vertical_follow:
                    return deejayListSize
                    
                default:
                    return CGSize.init(width: 0, height: 0)
                }
            }
            return CGSize.init(width: 0, height: 0)
        }
        
        if self.productInfo?.proxyType() == .artistDetail || self.productInfo?.proxyType() == .user{
            if let itemProduct = self.productInfo?.itemAtIndex(index: indexPath.section) as? ProductList{
                switch itemProduct.type {
                case .deejay_horizontal, .deejay_station_horizontal:
                    return CGSize.init(width: self.frame.width, height: deejayOverlaySize.height)
                    
                case .mixset_horizontal:
                    return CGSize.init(width: self.frame.width, height: mixSize.height)
                    
                case .track_horizontal:
                    return CGSize.init(width: self.frame.width, height: trackSize.height * 3)
                    
                case .video_horizontal:
                    return CGSize.init(width: self.frame.width, height: videoSize.height)
                    
                case .track_vertical, .track_vertical_delete, .mixset_vertical_delete, .video_vertical_delete:
                    return trackSize
                    
                case .mixset_vertical:
                    return mixSize
                    
                case .video_vertical:
                    return videoSize
                    
                case .deejay_vertical, .deejay_vertical_follow:
                    return CGSize.init(width: self.frame.width, height: deejayListSize.height)
                    
                default:
                    return CGSize.init(width: 0, height: 0)
                }
            }
            return CGSize.init(width: 0, height: 0)
        }
        
        if self.productInfo?.proxyType() == .download{
            return trackDownloadSize
        }
        
        if self.productInfo?.proxyType() == .artist {
            
            switch self.productInfo!.type_listing {
            case .deejay_vertical, .deejay_horizontal, .deejay_vertical_follow:
                //
                if indexPath.row == 0{
                    return bannerSize
                }else{
                    return deejayListSize
                }
                
            case .deejay_station_horizontal :
                return CGSize.init(width: self.frame.width, height: deejayOverlaySize.height)
            default:
                return sizeZero
            }
        }
        
        switch self.productInfo!.type {
            
        case .mixset:
            if self.productInfo!.proxyType() == .chart{
                return chartSizeStrecth
            }
            return mixSize
            
        case .track:
            if self.productInfo!.proxyType() == .chart{
                return chartSizeStrecth
            }
            return trackSize
            
        case .trackPlayer, .trackPlayerMasup:
            return trackSize
            
        case .chart:
            return chartSize
            
        case .video:
            if self.productInfo!.proxyType() == .chart{
                return chartVideoSizeStrecth
            }
            return videoSize
            
        case .deejay_vertical, .deejay_horizontal:
            return deejayListSize
            
        case .all:
            return trackDownloadSize
            
        case .unknow, .station, .keyword_recently, .keyword_trend:
            return CGSize.init(width: 0, height: 0)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        
        if self.productInfo?.proxyType() == .station{
            return UIEdgeInsetsMake(paddingBetweenItem, 0, paddingBetweenItem, 0)
        }
        
        if self.productInfo!.proxyType() == .chart || self.productInfo!.proxyType() == .artist{
            return UIEdgeInsetsMake(0, 0, 0, 0)
        }
        
        if self.productInfo?.proxyType() == .artistDetail{
            
            switch self.productInfo!.type {
            case .unknow:
                // get artist detail
                return UIEdgeInsetsMake(0, paddingLeftRight, 0, paddingLeftRight)
                
            default:
                //
                return UIEdgeInsetsMake(paddingLeftRight, paddingLeftRight, 0, paddingLeftRight)
            }
        }
        
        
        switch self.productInfo!.type {
        case .mixset, .track, .chart, .video, .deejay_vertical, .deejay_horizontal, .all:
            return UIEdgeInsetsMake(paddingLeftRight, paddingLeftRight, 0, paddingLeftRight)
            
        case .unknow, .trackPlayer, .trackPlayerMasup, .station, .keyword_recently, .keyword_trend:
            break
        }

        
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        if self.productInfo?.proxyType() == .station || self.productInfo?.proxyType() == .artist{
            return paddingBetweenItem * 2
        }
        
        if self.productInfo!.proxyType() == .chart{
            return 0
        }
        return paddingBetweenItem
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        if self.productInfo!.proxyType() == .chart{
            return 0
        }
        
        if self.productInfo?.proxyType() == .station || self.productInfo?.proxyType() == .artist{
            return paddingBetweenItem * 2
        }
        
        switch self.productInfo!.type {
        case .mixset:
            return paddingBetweenItem
        default:
            return paddingBetweenItem * 2
        }
    }
    
    //MARK: Header - Footer
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if let itemProduct = self.productInfo?.itemAtIndex(index: section) as? ProductList{
            switch itemProduct.header_type {
            case .header_title:
                return headerTitleSize
                
            case .artist_detailHeader:
                return bannerSize
                
            case .song_infoHeader:
                return videoInfo_Header
                
            default:
                break
            }
        }
        
        if self.productInfo?.proxyType() == .chart{
            return bannerSize
            
        }else if self.productInfo?.proxyType() == .station{
            
            if let itemStation = self.productInfo?.itemAtIndex(index: section) as? Station{
                switch itemStation.type {
                case .mixset_vertical, .track_vertical, .video_vertical, .track_horizontal, .mixset_horizontal, .video_horizontal, .video_vertical_delete:
                    return newFeed_header
                default:
                    return sizeZero
                }
            }
            
        }else if self.productInfo?.proxyType() == .playlistDetail{
            return playlistDetailHeaderSize
            
        }else if self.productInfo?.proxyType() == .artistDetail && self.productInfo?.type == .unknow{
            if section == 0{
                return bannerSize
            }else{
                return headerTitleSize
            }
        }
        
        return sizeZero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if (self.productInfo == nil || !(self.productInfo!.paggingEnable(section: section))){
            // paggign disable
            return sizeZero
        }
        
        return trackSize
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if let itemProduct = self.productInfo?.itemAtIndex(index: indexPath.section) as? ProductList{
            switch itemProduct.header_type {
            case .header_title:
                return self.reusableView_headerTitleCell(collectionView:collectionView, viewForSupplementaryElementOfKind:kind, at:indexPath)
                
            case .artist_detailHeader:
                return self.reusableView_artistDetailHeaderCell(collectionView:collectionView, viewForSupplementaryElementOfKind:kind, at:indexPath)
                
            case .song_infoHeader:
                return self.reusableView_songInfoHeaderCell(collectionView:collectionView, viewForSupplementaryElementOfKind:kind, at:indexPath)
                
            default:
                break
            }
        }
        
        if self.productInfo?.proxyType() == .chart{
            return self.reusableView_chart(collectionView:collectionView, viewForSupplementaryElementOfKind:kind, at:indexPath)
            
        }else if self.productInfo?.proxyType() == .station{
            return self.reusableView_station(collectionView:collectionView, viewForSupplementaryElementOfKind:kind, at:indexPath)
            
        }else if self.productInfo?.proxyType() == .playlistDetail{
            return self.reusableView_playlistDetail(collectionView:collectionView, viewForSupplementaryElementOfKind:kind, at:indexPath)
        }else if self.productInfo?.proxyType() == .artistDetail{
            if indexPath.section == 0{
                return self.reusableView_artistDetailHeaderCell(collectionView:collectionView, viewForSupplementaryElementOfKind:kind, at:indexPath)
            }else{
                return self.reusableView_headerTitleCell(collectionView:collectionView, viewForSupplementaryElementOfKind:kind, at:indexPath)
            }
        }else{
            
            let loadingView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: LoadingCollectionReusableCell.nameOfClass, for: indexPath) as! LoadingCollectionReusableCell
            
            self.requestWithProductListInfo(_productInfo: self.productInfo!, newRequest: false)
            
            return loadingView
        }
    }
    
    func reusableView_headerTitleCell(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        
        if kind == UICollectionElementKindSectionHeader{
            if let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: TitleCollectionReusableView.nameOfClass, for: indexPath) as? TitleCollectionReusableView{
                header.delegate = self
                header.headerSection = indexPath.section
                header.backgroundColor = UIColor.black
                
                self.queueConcurrent.async(execute: {
                    if let itemProduct = self.productInfo?.itemAtIndex(index: indexPath.section) as? ProductList{
                        header.configureHeader(str_title: itemProduct.title)
                    }
                })
                
                return header
            }
        }
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func reusableView_artistDetailHeaderCell(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        
        if kind == UICollectionElementKindSectionHeader{
            if let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: ArtistDetailReusableView.nameOfClass, for: indexPath) as? ArtistDetailReusableView{
                
                self.queueConcurrent.sync(execute: {
                    if let itemProduct = self.productInfo?.itemAtIndex(index: indexPath.section) as? ProductList{
                        if let _artist = itemProduct.header_object as? Artist{
                            
                            header.delegate = self
                            header.configureCellWith(_artist: _artist, enableFollow: true)
                        }
                    }
                })
                return header
            }
        }
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func reusableView_songInfoHeaderCell(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        
        if kind == UICollectionElementKindSectionHeader{
            if let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SongInfoResuableView.nameOfClass, for: indexPath) as? SongInfoResuableView{
                self.queueConcurrent.sync(execute: {
                    
                    if let _song = self.productInfo?.getFilter().requestObject as? Song{
                        
                        header.delegate_songInfo = self
                        header.configureWithSong(itemSong: _song)
                        
                        header.cell_btnSwitchAutoPlay?.isSelected = SharePlayer.isAutoPlayVideo
                    }
                })
                
                return header
            }
        }
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func reusableView_playlistDetail(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        
        if kind == UICollectionElementKindSectionHeader{
            let playlistHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: PlaylistDetailReusableView.nameOfClass, for: indexPath) as! PlaylistDetailReusableView
            
            if let _playlist = self.productInfo?.getFilter().requestObject as? Playlist{
                playlistHeader.configure_playlist(itemPlaylist: _playlist)
                playlistHeader.cell_btnShuffle?.addTarget(self, action: #selector(self.shouldShufflePlaylistDetail), for: .touchUpInside)
            }
            
            return playlistHeader
        }else{
            
            let loadingView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: LoadingCollectionReusableCell.nameOfClass, for: indexPath) as! LoadingCollectionReusableCell
            
            self.requestWithProductListInfo(_productInfo: self.productInfo!, newRequest: false)
            
            return loadingView
        }
    }
    
    func reusableView_chart(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        
        if kind == UICollectionElementKindSectionHeader{
            let slideMenuHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SlideMenuWithCoverImageCollectionReusableView.nameOfClass, for: indexPath) as! SlideMenuWithCoverImageCollectionReusableView
            
            if let _song = self.productInfo?.data.first as? Song{
                slideMenuHeader.itemSong = _song
                slideMenuHeader.paggingTypes = self.productInfo?.paggingTypes
                slideMenuHeader.currentSection = indexPath.section
                if slideMenuHeader.delegate == nil {
                    slideMenuHeader.setSlideMenuBottomConstraint(value: 6.0)
                }
                slideMenuHeader.delegate = self
            }
            
            return slideMenuHeader
        }else{
            
            let loadingView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: LoadingCollectionReusableCell.nameOfClass, for: indexPath) as! LoadingCollectionReusableCell
            
            self.requestWithProductListInfo(_productInfo: self.productInfo!, newRequest: false)
            
            return loadingView
        }
    }
    
    func reusableView_station(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        
        if kind == UICollectionElementKindSectionHeader{
            let new_feed = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: StationCollectionReusableView.nameOfClass, for: indexPath) as! StationCollectionReusableView
            
            new_feed.delegate = self
            if let _station = self.productInfo?.itemAtIndex(index: indexPath.section) as? Station{
                new_feed.configureCell(itemStation: _station,indexPath: indexPath)
            }
            
            return new_feed
            
        }else{
            
            let loadingView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: LoadingCollectionReusableCell.nameOfClass, for: indexPath) as! LoadingCollectionReusableCell
            
            self.requestWithProductListInfo(_productInfo: self.productInfo!, newRequest: false)
            
            return loadingView
        }
    }
    
    //MARK: Header Delegate
    func titleCollectionHeader_DidTapBtnMore(sender: UIButton, headerSection: Int?) {
        //
        
        if headerSection == nil {
            return
        }
        
        var info: ProductListInfo?
        
        if let itemProductList = self.productInfo?.itemAtIndex(index: headerSection!) as? ProductList{
            switch itemProductList.type {
            case .mixset_vertical, .mixset_horizontal, .mixset_vertical_delete:
                //
                info = ProductListInfo.init(_proxy: self.productInfo!.proxyType(), _type: .mixset, filter: self.productInfo?.getFilter(), _data: nil, _paggingTypes: nil, pullRefresh: true)
                
            case .track_vertical, .track_horizontal, .track_player_vertical:
                //
                info = ProductListInfo.init(_proxy: self.productInfo!.proxyType(), _type: .track, filter: self.productInfo?.getFilter(), _data: nil, _paggingTypes: nil, pullRefresh: true)
                
            case .video_vertical, .video_horizontal, .video_vertical_delete:
                //
                info = ProductListInfo.init(_proxy: self.productInfo!.proxyType(), _type: .video, filter: self.productInfo?.getFilter(), _data: nil, _paggingTypes: nil, pullRefresh: true)
                
            case .deejay_horizontal, .deejay_vertical, .deejay_station_horizontal:
                info = ProductListInfo.init(_proxy: self.productInfo!.proxyType(), _type: .deejay_vertical, filter: self.productInfo?.getFilter(), _data: nil, _paggingTypes: nil, pullRefresh: true)
                
            default:
                //
                return
            }
        }
        
        if info != nil {
            self.delegate?.productListView_HeaderTitleDidTapButtonMore(productInfo: info!)
        }
    }
    
    func SongInfo_DidSelectedArtist(_artist: Artist) {
        //
        let customIndexPath = IndexPath.init(row: 0, section: 0)
        self.delegate?.productListView_didSelectedAtIndexPath(indexPath: customIndexPath, withData: [_artist], type: .Artist)
    }
    
    func SongInfo_DidTapBtnAutoPlay(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.delegate?.productListView_BtnAutoPlayVideoDidChange(isOn: sender.isSelected)
    }
    
    func SongInfo_DidBtnLike(sender: UIButton, objects: AnyObject) {
        self.delegate?.productListView_BtnLikeDidTap(sender: sender, object: objects)
    }
    
    func SongInfo_DidBtnShare(sender: UIButton, objects: AnyObject) {
        self.delegate?.productListView_BtnShareDidTap(sender: sender, object: objects)
    }
    
    func stationReusableViewDelegate_BtnMoreDidTap(sender: UIButton, objects: Station) {
        self.delegate?.productListview_BtnMoreDidTap(sender: sender, object: objects)
    }
    
    func stationReusableViewDelegate_DidTapArtist(artist: Artist?) {
        if artist != nil {
            self.SongInfo_DidSelectedArtist(_artist: artist!)
        }
    }
    
    func stationReusableViewDelegate_DidTapAtIndexPath(indexPath: IndexPath) {
        if self.myCollectionView != nil {
            self.collectionView(self.myCollectionView!, shouldSelectItemAt: indexPath)
        }
    }
    
    func stationReusableViewDelegate_LikeBtnDidTap(sender: UIButton, objects: Station) {
        self.delegate?.productListView_BtnLikeDidTap(sender: sender, object: objects)
    }
    
    //MARK: - SlideMenuView Delegate
    func slideMenu_DidSelectedItemAtIndex(index: NSInteger) {
        //
        if self.productInfo == nil {
            return
        }
        
        if self.productInfo!.proxyType() == .chart{
            self.productInfo!.setProductType(paggingIndex: index)
            
            self.requestWithProductListInfo(_productInfo: self.productInfo!, newRequest: true)
        }
    }
    
    
    //MARK: - Song Cell Delegate
    func songCellDelegate_BtnDeleteTap(sender: UIButton?, indexPath: IndexPath) {
        if self.delegate?.productListView_shouldDeleteItemAtIndexPath(indexPath: indexPath, currentDatas: self.productInfo?.data) ?? false{
            // delete song
            // delete itemSong in array datasrouce success
            
            if let item_objects = self.productInfo?.itemsProductAt(index: indexPath.section){
                self.delegate?.productListView_didDeleteItemAtIndexPath(indexPath: indexPath, currentDatas: item_objects)
                
            }else{
                self.delegate?.productListView_didDeleteItemAtIndexPath(indexPath: indexPath, currentDatas: self.productInfo?.data)
            }
            
            // update layout
            if self.productInfo?.removeSongItemAt(indexPath: indexPath) ?? false{
                
                if (self.productInfo?.dataEmpty() ?? true){
                    self.myCollectionView?.reloadData()
                }else{
                    self.myCollectionView?.collectionViewLayout.invalidateLayout()
                    self.myCollectionView?.performBatchUpdates({
                        self.myCollectionView?.deleteItems(at: [indexPath])
                        
                    }, completion: { (finish) in
                        //
                        if finish{
                            if self.productInfo?.data.count ?? 0 > 0 {
                                UIView.setAnimationsEnabled(false)
                                self.myCollectionView?.collectionViewLayout.invalidateLayout()
                                self.myCollectionView?.performBatchUpdates({
                                    //fixme
                                    self.myCollectionView?.reloadSections(IndexSet(integer: indexPath.section))
                                }, completion: { (finish) in
                                    UIView.setAnimationsEnabled(true)
                                    print("remove cell success")
                                })
                            }
                        }
                    })
                }
            }
        }
    }
    
    func songCellDelegate_BtnMoreTap(sender: UIButton, indexPath: IndexPath) {
        switch self.productInfo!.proxyType() {
            
        case .station:
            self.btnMoreTapWithStation(indexPath: indexPath)
            break
            
            
        default:
            self.btnMoreTapWithNormalCell(indexPath: indexPath)
        }
    }
    
    func btnMoreTapWithStation(indexPath: IndexPath){
        if let itemStation = self.productInfo?.itemAtIndex(index: indexPath.section) as? Station, itemStation.object != nil{
            
            switch self.productInfo!.type {
            case .mixset:
                self.delegate?.productListView_MoreTapAtIndexPath(index: 0, withData: [itemStation.object!])
                break
                
            case .track:
                self.delegate?.productListView_MoreTapAtIndexPath(index: 0, withData: [itemStation.object!])
                break
                
            case .trackPlayer:
                break
                
            case .trackPlayerMasup:
                break
                
            case .chart:
                break
                
            case .deejay_vertical, .deejay_horizontal:
                //fixme
                break
                
            case .video:
                self.delegate?.productListView_MoreTapAtIndexPath(index: 0, withData: [itemStation.object!])
                break
                
            case .station:
                break
                
            case .all:
                break
                
            case .keyword_trend:
                break
                
            case .keyword_recently:
                break
                
                
            case .unknow:
                break
                
            }
        }
    }
    
    func btnMoreTapWithNormalCell(indexPath: IndexPath){
        if self.productInfo != nil {
            
            if let itemProduct = self.productInfo?.itemAtIndex(index: indexPath.section) as? ProductList {
                if let _objects = itemProduct.object, _objects.count > 0{
                    switch itemProduct.type {
                    case .mixset_vertical, .mixset_horizontal, .mixset_vertical_delete:
                        //
                        self.delegate?.productListView_MoreTapAtIndexPath(index: indexPath.row, withData: _objects)
                        break
                        
                    case .video_horizontal, .video_vertical, .video_vertical_delete:
                        //
                        self.delegate?.productListView_MoreTapAtIndexPath(index: indexPath.row, withData: _objects)
                        break
                        
                    case .track_player_vertical, .track_horizontal, .track_vertical, .track_vertical_delete:
                        self.delegate?.productListView_MoreTapAtIndexPath(index: indexPath.row, withData: _objects)
                        break
                        
                    case .deejay_vertical, .deejay_horizontal, .deejay_station_horizontal, .deejay_vertical_follow:
                        self.delegate?.productListView_MoreTapAtIndexPath(index: indexPath.row, withData: _objects)
                        break
                        
                    default:
                        break
                    }
                }
                return
            }
            
            switch self.productInfo!.type {
            case .mixset:
                //
                self.delegate?.productListView_MoreTapAtIndexPath(index: indexPath.row, withData: self.productInfo?.data)
                break
                
            case .video:
                //
                self.delegate?.productListView_MoreTapAtIndexPath(index: indexPath.row, withData: self.productInfo?.data)
                break
                
            default:
                self.delegate?.productListView_MoreTapAtIndexPath(index: indexPath.row, withData: self.productInfo?.data)
                break
            }
        }
    }
    
    func artistCellDelegate_DidTapFollowBtn(sender: UIButton, object: Artist) {
        self.delegate?.productListView_BtnLikeDidTap(sender: sender, object: object)
    }
    
    //MARK: SongDownload Delegate
    func SongDownload_DidRemoveDownloadSongSuccess(at indexPath: IndexPath) {
        self.songCellDelegate_BtnDeleteTap(sender: nil, indexPath: indexPath)
    }
    
    func SongDownload_CantRemoveDownloadSong(error: NSError) {
        Loader.show(message: error.localizedDescription, view: self)
    }
    
    
    //MARK: - ScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.delegate_scrolling?.productListView_ScrollViewDidScroll(scrollView: scrollView)
        
        if self.isEnablePullRefresh{
            return
        }
        
        queueConcurrent.sync {
            // parallax scrolling zooming
            if self.myCollectionView == nil {
                return
            }
            
            var parallaxFound: Bool = false
            
            for cell in self.myCollectionView!.visibleCells{
                if let parallaxCell = cell as? BannerCollectionCell{
                    if parallaxCell.currentSection == 0{
                        self.myCollectionView?.sendSubview(toBack: parallaxCell)
                        parallaxCell.superScrollViewDidScrolling(scrollView: scrollView)
                    }
                    
                    parallaxFound = true
                }
            }
            
            
            if #available(iOS 9.0, *) {
                
                if parallaxFound{
                    return
                }
                
                for cell in self.myCollectionView!.visibleSupplementaryViews(ofKind: UICollectionElementKindSectionHeader){
                    if let parallaxCell = cell as? SlideMenuWithCoverImageCollectionReusableView{
                        if parallaxCell.currentSection == 0{
                            self.myCollectionView?.sendSubview(toBack: parallaxCell)
                            parallaxCell.superScrollViewDidScrolling(scrollView: scrollView)
                        }
                    }
                }
                
                if parallaxFound{
                    return
                }
                for cell in self.myCollectionView!.visibleSupplementaryViews(ofKind: UICollectionElementKindSectionHeader){
                    if let parallaxCell = cell as? ArtistDetailReusableView{
                        if parallaxCell.currentSection == 0{
                            self.myCollectionView?.sendSubview(toBack: parallaxCell)
                            parallaxCell.superScrollViewDidScrolling(scrollView: scrollView)
                        }
                    }
                }
                
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.delegate_scrolling?.productListView_ScrollViewDidEndDecelerating(scrollView: scrollView)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.isUserDragging = true
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.isUserDragging = false
        
        self.refresh()
        
        self.delegate_scrolling?.productListView_ScrollViewDidEndDragging(scrollView: scrollView, willDecelerate: decelerate)
    }
}


/*******************************************************************************/
// MARK: - Pull To Refresh Setup
/*******************************************************************************/

extension ProductListView {
    internal func setupRefreshControl() {
        
        if !self.isEnablePullRefresh {
            // pull to refresh disable
            return
        }
        
        // Programmatically inserting a UIRefreshControl
        self.refreshControl = UIRefreshControl()
        
        // Setup the loading view, which will hold the moving graphics
        //        self.refreshLoadingView = UIView(frame: self.refreshControl!.bounds)
        //        self.refreshLoadingView.backgroundColor = UIColor.clearColor()
        
        // Hide the original spinner icon
        self.refreshControl!.tintColor = UIColor.lightGray
        
        // Add the loading and colors views to our refresh control
        //        self.refreshControl!.addSubview(self.refreshColorView)
        //        self.refreshControl!.addSubview(self.refreshLoadingView)
        
        // Initalize flags
        //        self.isRefreshIconsOverlap = false;
        //        self.isRefreshAnimating = false;
        
        // When activated, invoke our refresh function
        self.refreshControl?.addTarget(self, action: #selector(ProductListView.refresh), for: UIControlEvents.valueChanged)
        
        self.myCollectionView?.addSubview(self.refreshControl!)
    }
    
    @objc internal func refresh(){
        if self.refreshControl != nil && ( self.refreshControl?.isRefreshing )! && !self.isUserDragging{
            //
            if self.productInfo != nil {
                self.requestWithProductListInfo(_productInfo: self.productInfo!, newRequest: true)
            }
        }
    }
    
    internal func endRefreshingWithAnimate(animate: Bool){
        
        let delayInSeconds = 0.1
        DispatchQueue.main.asyncAfter(deadline: .now() + delayInSeconds) {
            // When done requesting/reloading/processing invoke endRefreshing, to close the control
            self.refreshControl?.endRefreshing()
            //            LoadingProgress.dissmiss()
            self.myCollectionView?.setContentOffset(CGPoint(), animated: animate)
        }
    }
}


/*******************************************************************************/
// MARK: - SET
/*******************************************************************************/

extension ProductListView{
    
    /**
     set background corlor (it just collectionView background to clear and set superView to your corlor)
     */
    func setProductListBackgroundColor(color: UIColor){
        self.backgroundColor = color
        self.myCollectionView?.backgroundColor = UIColor.clear
    }
    
    /* set bouncing type */
    func setBouncingStyleEnable(enable: Bool){
        self.myCollectionView?.bounces = enable
    }
    
    func setAlwaysBouncingVertical(enable: Bool){
        self.myCollectionView?.alwaysBounceVertical = enable
    }
    
    func setAlwaysBouncingHorizontal(enable: Bool){
        self.myCollectionView?.alwaysBounceVertical = enable
    }
    
    /* set list view contentInsect */
    func setContentInsect(insect: UIEdgeInsets){
        self.myCollectionView?.contentInset = insect
    }
    
    /* set collection view scroll to offset */
    func setOffSet(point: CGPoint, withAnimate enable: Bool){
        self.myCollectionView?.setContentOffset(point, animated: enable)
    }
    
    func setScrollIndicator(hidden: Bool){
        self.myCollectionView?.showsVerticalScrollIndicator = hidden
        self.myCollectionView?.showsHorizontalScrollIndicator = hidden
    }
}

/*******************************************************************************/
// MARK: - GET
/*******************************************************************************/

extension ProductListView{
    
    func getVisibleIndexPathForVisibleSupplementaryElements(kind: String) -> [IndexPath]?{
        if #available(iOS 9.0, *) {
            return self.myCollectionView?.indexPathsForVisibleSupplementaryElements(ofKind: kind)
        } else {
            // Fallback on earlier versions
            return nil
        }
    }
    
    func getDataSource() -> [AnyObject]{
        return self.productInfo?.data ?? []
    }
}
