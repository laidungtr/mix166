//
//  DLTCenterFlowLayout.swift
//  Buyer
//
//  Created by ttiamap on 8/18/16.
//  Copyright © 2016 Trong. All rights reserved.
//

import UIKit

class DLTCenterFlowLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let superAttributes = super.layoutAttributesForElements(in: rect)!
        
        let rowCollections = NSMutableDictionary()
        
        // Collect attributes by their midY coordinate.. i.e. rows!
        for itemAttributes in superAttributes {
            let yCenter = itemAttributes.frame.midY
            
            if (rowCollections[yCenter] == nil){
                rowCollections[yCenter] = NSMutableArray()
            }
            
            if let arrCollection = rowCollections[yCenter] as? NSMutableArray{
                arrCollection.add(itemAttributes)
            }
        }
        
        // Adjust the items in each row
        rowCollections.enumerateKeysAndObjects({ (key, obj, stop) in
            //
            let itemAttributesCollection : [UICollectionViewLayoutAttributes] = obj as! [UICollectionViewLayoutAttributes]
            let itemInRow = itemAttributesCollection.count
            
            
            // this command code for center layout
            // x-x-x-x ... sum up the interim space
            let aggregateInteritemSpacing = self.minimumInteritemSpacing * CGFloat((itemInRow - 1))
            
            // Sum the width of all elements in the row
            var aggregateItemWidths: CGFloat = 0.0
            
            for itemAttributes in itemAttributesCollection{
                aggregateItemWidths += itemAttributes.frame.width
            }
            
            // Build an alignment rect
            // |==|--------|==|
            let alignmentWidth = aggregateItemWidths + aggregateInteritemSpacing
            let alignmentXOffset = ((self.collectionView?.bounds)!.width - alignmentWidth) / 2.0
            
            print(" ------------ \(alignmentXOffset)")
            
            // Adjust each item's position to be centered
            var previousFrame = CGRect.init(x: 0, y: 0, width: 0, height: 0)

            for itemAttributes in itemAttributesCollection{
                
                var itemFrame = itemAttributes.frame
                
//                if previousFrame.equalTo(CGRect()){
//                    itemFrame.origin.x = alignmentXOffset
                    
//                    print(" ------------ \(itemFrame.origin.y)")
//                    itemFrame.origin.y += self.minimumLineSpacing
//                    print(" ++++++++++++ \(itemFrame.origin.y)")
//                }else{
                    itemFrame.origin.x = previousFrame.maxX + self.minimumInteritemSpacing

//                }
                itemAttributes.frame = itemFrame
                previousFrame = itemFrame
                
            }
        })
        
        
        return superAttributes
    }

}
