//
//  DLTVideoPlayerView.swift
//  Mix166
//
//  Created by ttiamap on 7/7/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import AVFoundation

protocol DLTVideoPlayerDelegate {
    func DLTVideoPlayerDelegate_PlayingStatusChange(isPlaying status: Bool)
    func DLTVideoPlayerDelegate_FullScreenModeChange(isFullScreen status: Bool)
    func DLTVideoPlayerDelegate_FullScreenMode(active: Bool)
    func DLTVideoPlayerDelegate_ScaleVideoBackToNormal()
    func DLTVideoPlayerDelegate_DidReachEndPlaying()
    func DLTVideoPlayerDelegate_BtnDissmissDidTap(sender: UIButton)
}

open class CustomSlider : UISlider {
    @IBInspectable open var trackWidth:CGFloat = 1.0 {
        didSet {setNeedsDisplay()}
    }
    
    override open func trackRect(forBounds bounds: CGRect) -> CGRect {
        let defaultBounds = super.trackRect(forBounds: bounds)
        return CGRect(
            x: defaultBounds.origin.x,
            y: defaultBounds.origin.y + defaultBounds.size.height/2 - trackWidth/2,
            width: defaultBounds.size.width,
            height: trackWidth
        )
    }
}

class DLTVideoPlayerView: UIView, DLTPlayerDelegate {
    
    var lb_durationCount: UILabel?
    var lb_duration: UILabel?
    var btnFullScreen: UIButton?
    var btnPlayPause: UIButton?
    var btnDissmiss: UIButton?
    var slider_seekbar: CustomSlider?
    var imageCover: UIImageView?
    var indicator: UIActivityIndicatorView?
    var viewContain: UIView?
    
    var fontTimer = UIFont.fontRegular_small()
    var colorTimer = UIColor.white
    var imageFullScreen:        UIImage? = UIImage.init(named: Image_Name.ic_fullscreen.rawValue)
    var imageScaleScreen:       UIImage? = UIImage.init(named: Image_Name.ic_scalescreen.rawValue)
    var imagePlay:              UIImage? = UIImage.init(named: Image_Name.ic_playCircle.rawValue)
    var imagePause:             UIImage? = UIImage.init(named: Image_Name.ic_pauseCircle.rawValue)
    var imageDismiss:           UIImage? = UIImage.init(named: Image_Name.ic_dismiss.rawValue)
    var imageSeekbarThumnail:   UIImage? = UIImage.init(named: Image_Name.ic_seekbar.rawValue)
    
    var colorSeekbarMinTrack = UIColor.init(hexaString: AppColor.appRedColor.rawValue)
    var colorSeekbarMaxTrack = UIColor.white.withAlphaComponent(0.2)
    
    private var layoutSeekBarBottomConstraint: NSLayoutConstraint?
    private var layoutBtnFullScreenCenterYConstraint: NSLayoutConstraint?
    private var layoutSeekBarLeadingConstraint: NSLayoutConstraint?
    private var layoutSeekBarTrailing: NSLayoutConstraint?
    
    var direction: panDirection = .unknow
    var isScaled = false{
        didSet{
            self.viewContain?.isHidden = self.isScaled
            if self.isScaled{
                self.layer.borderColor = UIColor.white.withAlphaComponent(0.4).cgColor
                self.layer.borderWidth = 1.0
                
            }else{
                self.layer.borderWidth = 0.0
            }
        }
    }
    var delegate: DLTVideoPlayerDelegate?
    
    private var currentSteamingLink = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupFristLoad()
    }
    
    init() {
        super.init(frame: CGRect())
        self.setupFristLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupFristLoad()
    }
    
    //MARK: - DLTPlayer Delegate
    func DLTPlayer_UpdateBackgroundSongInfo() {
        //
    }
    
    func DLTPlayer_PlayingStatusChange(_ isPlaying: Bool){
        if Thread.isMainThread{
            self.btnPlayPause?.isSelected = isPlaying
        }else{
            DispatchQueue.main.async() { () -> Void in
                self.btnPlayPause?.isSelected = isPlaying
            }
        }
    }
    
    func DLTPlayer_CurrentLinkSteamingFail(str_linkUrl: String) {
        //
        self.delegate?.DLTVideoPlayerDelegate_DidReachEndPlaying()
    }
    
    func DLTPlayer_DurationCountDown(str_durationCount: String, str_duration: String, sliderValue: Float) {
        self.lb_durationCount?.text = str_durationCount
        self.lb_duration?.text = str_duration
        self.slider_seekbar?.value = sliderValue
    }
    
    func DLTPlayer_DidFinishPlaying(){
        self.delegate?.DLTVideoPlayerDelegate_DidReachEndPlaying()
    }
    
    func DLTPlayerBuffering(enable: Bool){
        
        if enable{
            // is buffering
            self.indicator?.isHidden = false
            self.indicator?.startAnimating()
            self.show_controlPlayBack()
            self.btnPlayPause?.isHidden = true
            
        }else{
            self.indicator?.isHidden = true
            self.btnPlayPause?.isHidden = false
            self.hide_controlPlayback()
        }
    }
    
    //MARK: - Method
    func playingVideoWithSteamingLink(str_url: String){
        
        DLTPlayer.sharedInstance.delegate = self
        
        if !DLTPlayer.sharedInstance.isPlaying || str_url != DLTPlayer.sharedInstance.currentStreamingLink{
            
            self.currentSteamingLink = str_url
            
            // your code here
            self.layoutIfNeeded()
            DLTPlayer.setupPlayerLayer(frame: self.bounds)
            let playerPlayer =  DLTPlayer.playVideoWithUrl(str_url)
            
            if let sublayer = self.layer.sublayers{
                for layer in sublayer {
                    if layer.isKind(of: AVPlayerLayer.self) {
                        layer.removeFromSuperlayer()
                        break
                    }
                }
            }
            self.layer.addSublayer(playerPlayer)
            
            if self.viewContain != nil {
                self.bringSubview(toFront: self.viewContain!)
            }
        }
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        DLTPlayer.updatePlayerLayer(frame: self.bounds)
        
    }
    
    private func setupFristLoad(){
        
        if self.btnPlayPause == nil{
            // avoid adding subview twice
            
            self.initViewContain()
            self.initLabel()
            self.initButton()
            self.initSeekbar()
            //            self.initImageCover()
            self.initIndicator()
            
            self.backgroundColor = UIColor.black
            
            if self.viewContain         != nil {self.addSubview(self.viewContain!)}
            if self.btnFullScreen       != nil {self.viewContain?.addSubview(self.btnFullScreen!)}
            if self.slider_seekbar      != nil {self.viewContain?.addSubview(self.slider_seekbar!)}
            //            if self.imageCover          != nil {self.addSubview(self.imageCover!); self.bringSubview(toFront: self.imageCover!)}
            if self.btnPlayPause        != nil {self.viewContain?.addSubview(self.btnPlayPause!)}
            if self.btnDissmiss         != nil {self.viewContain?.addSubview(self.btnDissmiss!)}
            if self.indicator           != nil {self.viewContain?.addSubview(self.indicator!)}
            
            if self.lb_duration         != nil {self.viewContain?.addSubview(self.lb_duration!)
                self.viewContain?.bringSubview(toFront: self.lb_duration!)}
            if self.lb_durationCount    != nil {self.viewContain?.addSubview(self.lb_durationCount!)
                self.viewContain?.bringSubview(toFront: self.lb_durationCount!)}
            
            //            // layout imageCover
            //            self.imageCover?.addLeftConstraint(toView: self, attribute: .left, relation: .equal, constant: 0)
            //            self.imageCover?.addRightConstraint(toView: self, attribute: .right, relation: .equal, constant: 0)
            //            self.imageCover?.addBottomConstraint(toView: self, attribute: .bottom, relation: .equal, constant: 0)
            //            self.imageCover?.addTopConstraint(toView: self, attribute: .top, relation: .equal, constant: 0)
            
            // layout viewContain
            self.viewContain?.addLeadingConstraint(toView: self, attribute: .leading, relation: .equal, constant: 0)
            self.viewContain?.addTrailingConstraint(toView: self, attribute: .trailing, relation: .equal, constant: 0)
            self.viewContain?.addTopConstraint(toView: self, attribute: .top, relation: .equal, constant: 0)
            self.viewContain?.addBottomConstraint(toView: self, attribute: .bottom, relation: .equal, constant: 0)
            
            // layout seekbar
            self.layoutSeekBarBottomConstraint = self.slider_seekbar?.addBottomConstraint(toView: self.viewContain, attribute: .bottom, relation: .equal, constant: 7.0)
            self.layoutSeekBarLeadingConstraint = self.slider_seekbar?.addLeadingConstraint(toView: self.viewContain, attribute: .leading, relation: .equal, constant: 0)
            self.layoutSeekBarTrailing = self.slider_seekbar?.addTrailingConstraint(toView: self.viewContain, attribute: .trailing, relation: .equal, constant: 0)
            
            // layout btn full screen
            
            self.layoutBtnFullScreenCenterYConstraint = self.btnFullScreen?.addCenterYConstraint(toView: self.slider_seekbar, relation: .equal, constant: -15.0)
            self.btnFullScreen?.addTrailingConstraint(toView: self.viewContain, attribute: .trailing, relation: .equal, constant: 0.0)
            
            self.btnFullScreen?.addWidthConstraint(toView: nil, relation: .equal, constant: 35.0)
            self.btnFullScreen?.addHeightConstraint(toView: nil, relation: .equal, constant: 35.0)
            
            self.btnDissmiss?.addWidthConstraint(toView: nil, relation: .equal, constant: 35.0)
            self.btnDissmiss?.addHeightConstraint(toView: nil, relation: .equal, constant: 35.0)
            self.btnDissmiss?.addLeadingConstraint(toView: self.viewContain, attribute: .leading, relation: .equal, constant: 4.0)
            self.btnDissmiss?.addTopConstraint(toView: self.viewContain, attribute: .top, relation: .equal, constant: 20.0)
            
            // layout label duration
            self.lb_duration?.addCenterYConstraint(toView: self.btnFullScreen)
            self.lb_duration?.addRightConstraint(toView: self.btnFullScreen, attribute: .left, relation: .equal, constant: 0.0)
            
            // layout label duration count
            self.lb_durationCount?.addCenterYConstraint(toView: self.btnFullScreen)
            self.lb_durationCount?.addLeadingConstraint(toView: self.viewContain, attribute: .leading, relation: .equal, constant: 8.0)
            
            // layout btn play pause
            self.btnPlayPause?.addCenterYConstraint(toView: self.viewContain)
            self.btnPlayPause?.addCenterXConstraint(toView: self.viewContain)
            self.btnPlayPause?.addHeightConstraint(toView: self.btnPlayPause, relation: .equal, constant: self.frame.size.height/4)
            self.btnPlayPause?.addWidthConstraint(toView: self.btnPlayPause, relation: .equal, constant: self.frame.size.height/4)
            self.btnPlayPause?.isHidden = true
            
            // layout indicator
            self.indicator?.addCenterYConstraint(toView: self.viewContain)
            self.indicator?.addCenterXConstraint(toView: self.viewContain)
            
            self.bringSubview(toFront: indicator!)
            
            self.layoutIfNeeded()
            
            
            // Gradient make
            let size = self.frame.size
            self.viewContain?.gradientFromTopToBottom(UIColor.clear.cgColor, bottomColor: UIColor.black.cgColor, gradientFrame: CGRect.init(x: 0, y: size.height*3/4, width: size.width, height: size.height/4))
        }
        
    }
    
    private func setupTapGesture(){
        self.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.viewContainDidTap(gesture:)))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc private func viewContainDidTap(gesture: UIGestureRecognizer){
        
        if self.isScaled == true{
            self.delegate?.DLTVideoPlayerDelegate_ScaleVideoBackToNormal()
        }
        self.hide_showControlPlayback()
    }
    
    @objc private func hide_showControlPlayback(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.hide_showControlPlayback), object: nil)
        
        if self.viewContain!.alpha > 0 {
            self.hide_controlPlayback()
        }else if self.viewContain!.alpha <= 0{
            self.show_controlPlayBack()
        }
    }
    
    private func hide_controlPlayback(){
        UIView.animate(withDuration: 0.5, animations: {
            self.viewContain?.alpha = 0
        })
    }
    
    private func show_controlPlayBack(){
        UIView.animate(withDuration: 0.5, animations: {
            self.viewContain?.alpha = 1
        }) { (finish) in
            // set up a delayed call…
            self.perform(#selector(self.hide_showControlPlayback), with: nil, afterDelay: 6.0)
        }
    }
    
    private func initViewContain(){
        if self.viewContain == nil {
            self.viewContain = UIView.init(frame: CGRect())
            self.viewContain?.backgroundColor = UIColor.clear
            self.viewContain?.clipsToBounds = false
            
            self.setupTapGesture()
        }
    }
    
    private func initLabel(){
        if self.lb_duration == nil {
            self.lb_duration = UILabel.init(frame: CGRect())
            self.lb_duration?.font = self.fontTimer
            self.lb_duration?.textColor = self.colorTimer
            self.lb_duration?.numberOfLines = 1
        }
        
        if self.lb_durationCount == nil {
            self.lb_durationCount = UILabel.init(frame: CGRect())
            self.lb_durationCount?.font = self.fontTimer
            self.lb_durationCount?.textColor = self.colorTimer
            self.lb_durationCount?.numberOfLines = 1
        }
    }
    
    private  func initButton(){
        
        if self.btnFullScreen == nil {
            self.btnFullScreen = UIButton.init(frame: CGRect())
            self.btnFullScreen?.setTitle("", for: .normal)
            self.btnFullScreen?.setImage(self.imageFullScreen, for: .normal)
            self.btnFullScreen?.setImage(self.imageScaleScreen, for: .selected)
            self.btnFullScreen?.addTarget(self, action: #selector(self.btnFullScreenTap(sender:)), for: .touchUpInside)
            self.btnFullScreen?.backgroundColor = UIColor.clear
        }
        
        if self.btnPlayPause == nil {
            self.btnPlayPause?.isHidden = true
            self.btnPlayPause = UIButton.init(frame: CGRect())
            self.btnPlayPause?.setTitle("", for: .normal)
            self.btnPlayPause?.setImage(self.imagePlay, for: .normal)
            self.btnPlayPause?.setImage(self.imagePause, for: .selected)
            self.btnPlayPause?.addTarget(self, action: #selector(self.btnPlayPauseTap(sender:)), for: .touchUpInside)
        }
        
        if self.btnDissmiss == nil {
            self.btnDissmiss = UIButton.init(frame: CGRect())
            self.btnDissmiss?.setTitle("", for: .normal)
            self.btnDissmiss?.setImage(self.imageDismiss, for: .normal)
            self.btnDissmiss?.setImage(UIImage(), for: .selected)
            self.btnDissmiss?.addTarget(self, action: #selector(self.btnDismissTap(sender:)), for: .touchUpInside)
        }
    }
    
    private func initSeekbar(){
        if self.slider_seekbar == nil {
            self.slider_seekbar = CustomSlider.init(frame: CGRect())
            self.slider_seekbar?.minimumValue = 0
            self.slider_seekbar?.maximumValue = 1
            
            self.slider_seekbar?.maximumTrackTintColor = self.colorSeekbarMaxTrack
            self.slider_seekbar?.minimumTrackTintColor = self.colorSeekbarMinTrack
            self.slider_seekbar?.setThumbImage(self.imageSeekbarThumnail, for: .normal)
            self.slider_seekbar?.addTarget(self, action: #selector(self.sliderValueChange(sender:)), for: .touchUpInside)
        }
    }
    
    private func initIndicator(){
        if self.indicator == nil {
            self.indicator = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
            self.indicator?.startAnimating()
            self.indicator?.hidesWhenStopped = true
        }
    }
    
    private  func initImageCover(){
        if self.imageCover == nil{
            self.imageCover = UIImageView.init(frame: CGRect())
        }
    }
    
    //MARK: IBAction
    @objc private func btnDismissTap(sender: UIButton){
        self.delegate?.DLTVideoPlayerDelegate_BtnDissmissDidTap(sender: sender)
    }
    
    
    @objc private  func btnPlayPauseTap(sender: UIButton){
        self.btnPlayPause?.isSelected = !(self.btnPlayPause?.isSelected ?? true)
        
        if !(self.btnPlayPause!.isSelected){
            // pause
            DLTPlayer.pause()
        }else{
            // play
            DLTPlayer.playVideoWithUrl(self.currentSteamingLink)
        }
        
        self.delegate?.DLTVideoPlayerDelegate_PlayingStatusChange(isPlaying: self.btnPlayPause?.isSelected ?? false)
    }
    
    func updatePlayerVideoLayerWithRotateMode(orientation: UIInterfaceOrientation){
        if orientation == .landscapeLeft || orientation == .landscapeRight{
            self.layoutSeekBarBottomConstraint?.constant = -7.0
            self.layoutSeekBarLeadingConstraint?.constant = self.lb_durationCount!.frame.origin.x + self.lb_durationCount!.frame.size.width + 8.0
            self.layoutSeekBarTrailing?.constant =  -(self.lb_duration!.frame.size.width + self.btnFullScreen!.frame.size.width + 24.0)
            self.layoutBtnFullScreenCenterYConstraint?.constant = 0.0
            
            self.btnFullScreen?.isSelected = true
            self.btnDissmiss?.isSelected = true
            
            self.slider_seekbar?.layoutIfNeeded()
        }else{
            self.layoutBtnFullScreenCenterYConstraint?.constant = -18.0
            self.layoutSeekBarBottomConstraint?.constant = 7.0
            self.layoutSeekBarLeadingConstraint?.constant = 0.0
            self.layoutSeekBarTrailing?.constant = 0.0
            self.slider_seekbar?.layoutIfNeeded()
            
            self.btnFullScreen?.isSelected = false
            self.btnDissmiss?.isSelected = false
        }
    }
    
    @objc private  func btnFullScreenTap(sender: UIButton){
        
        self.btnFullScreen?.isSelected = !(self.btnFullScreen?.isSelected ?? true)
        self.btnDissmiss?.isSelected = (self.btnFullScreen?.isSelected ?? false)
        
        self.delegate?.DLTVideoPlayerDelegate_FullScreenMode(active: self.btnFullScreen?.isSelected ?? false)
    }
    
    @objc private  func sliderValueChange(sender: UISlider){
        DLTPlayer.seekTo(value: sender.value)
    }
    
}
