//
//  SlideMenuView.swift
//  Buyer
//
//  Created by ttiamap on 9/14/16.
//  Copyright © 2016 Trong. All rights reserved.
//

import UIKit

@objc protocol SlideMenuViewDelegate {
    func slideMenu_NumberOfItem() -> NSInteger
    func slideMenu_TitleForItem(index: NSInteger) -> String
    func slideMenu_DidSelectedItemAtIndex(index: NSInteger)
    func slideMenu_FristloadSelected() -> Int
}

extension SlideMenuViewDelegate{
    func slideMenu_TitleForItem(index: NSInteger) -> String{
        // leave this empty for optional protocol
        
        return ""
    }
}

class SlideMenuView: UIView,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    enum SlideMenuStyle: String {
        case Title
    }
    
    var myCollectionView : UICollectionView?
    var indicatorBottom: UIView?
    
    private var sizingCell = SlideMenuTitleCollectionCell()
    private var currentSelectedIndexPath: IndexPath = IndexPath.init(row: 0, section: 0)
    
    private var activeWhenSelected = true
    
    var title_textColor: UIColor = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    var title_textColorSelected: UIColor = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    var title_borderColor: UIColor = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    
    var title_backgroundColor: UIColor = UIColor.clear
    var height_cellItem: CGFloat = 38.0
    
    var slideMenuStyle: SlideMenuStyle = .Title
    
    var delegate : SlideMenuViewDelegate?
    
    private var itemSize_dict: Dictionary<String,CGSize> = [:]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupFristLoad()
    }
    
    init() {
        super.init(frame: CGRect())
        self.setupFristLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupFristLoad()
    }
    
    //MARK: set up view
    
    func userInteractionEnable(enable: Bool){
        self.myCollectionView?.isUserInteractionEnabled = enable
    }
    
    func setActiveWhenSelected(enable: Bool){
        self.activeWhenSelected = enable
    }
    
    func scrollToCurrentIndex(index: NSInteger, withAnimate animate: Bool){
        
        let indexPath = IndexPath.init(row: index, section: 0)
        self.myCollectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: animate)
    }
    
    private func setupFristLoad(){
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize.init(width: screenWidth/2, height: 100.0)
        layout.scrollDirection = .horizontal
        
        if self.myCollectionView == nil {
            myCollectionView = UICollectionView.init(frame: CGRect(), collectionViewLayout: layout)
            myCollectionView?.collectionViewLayout = layout
            myCollectionView?.scrollsToTop = false
            
            self.myCollectionView?.delegate = self
            self.myCollectionView?.dataSource = self
            
            self.addSubview(self.myCollectionView!)
            self.bringSubview(toFront: self.myCollectionView!)
            
            myCollectionView?.showsHorizontalScrollIndicator = false
            
            // auto layout for myCollectionView
            self.myCollectionView?.addTopConstraint(toView: self, attribute: .top, relation: .equal, constant: 0)
            self.myCollectionView?.addBottomConstraint(toView: self, attribute: .bottom, relation: .equal, constant: 0)
            self.myCollectionView?.addLeadingConstraint(toView: self, attribute: .leading, relation: .equal, constant: 0)
            self.myCollectionView?.addTrailingConstraint(toView: self, attribute: .trailing, relation: .equal, constant: 0)
            
            // register cell
            self.registerCell()
        }
        
        if self.indicatorBottom == nil {
            self.indicatorBottom = UIView.init(frame: CGRect.init(x: 0, y: self.frame.height - 1, width: 0, height: 1.0))
            self.addSubview(self.indicatorBottom!)
            self.bringSubview(toFront: self.indicatorBottom!)
            self.indicatorBottom?.backgroundColor = self.title_textColorSelected
            
            let hightLightIndex = self.delegate?.slideMenu_FristloadSelected() ?? 0
            self.currentSelectedIndexPath = IndexPath.init(row: hightLightIndex, section: 0)
        }
    }
    
    func reloadSlideMenuData(){
        
        self.myCollectionView?.performBatchUpdates({
            self.myCollectionView?.reloadData()
        }, completion: { (finish) in
            if finish{
                self.moveBottomIndicatorAt(index: self.currentSelectedIndexPath.row)
            }
        })
    }
    
    func moveBottomIndicatorAt(index: Int){
        
        var _index = index
        var movingFrame = CGRect.init(x: 0, y: indicatorBottom?.frame.origin.y ?? 0, width: indicatorBottom?.frame.width ?? 0, height: indicatorBottom?.frame.height ?? 0)
        
        if self.itemSize_dict.count >= 0{
            while _index >= 0{
                if let size = self.itemSize_dict["\(_index)"]{
                    if _index == index{
                        movingFrame.size.width = size.width
                        print(size)
                    }
                    
                    if _index != 0{
                        movingFrame.origin.x += size.width
                    }
                }
                _index -= 1
            }
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.indicatorBottom?.frame = movingFrame
        }) { (finish) in
            //
        }
    }
    
    func moveBottomIndicator(xOffset: CGFloat){
        
        if var frame = self.indicatorBottom?.frame{
            
            let numberItem = self.collectionView(self.myCollectionView!, numberOfItemsInSection: 0)
            let itemWidth = screenWidth/CGFloat(numberItem)
            frame.origin.x = xOffset * itemWidth
            
            UIView.animate(withDuration: 0.1) {
                self.indicatorBottom?.frame = frame
            }
        }
    }
    
    
    //MARK: - Register Menu collectionView
    private func registerCell(){
        
        let cellNib = UINib.init(nibName: SlideMenuTitleCollectionCell.nameOfClass, bundle: nil)
        self.myCollectionView?.register(cellNib, forCellWithReuseIdentifier: SlideMenuTitleCollectionCell.nameOfClass)
        
        sizingCell = cellNib.instantiate(withOwner: nil, options: nil)[0] as! SlideMenuTitleCollectionCell
        //
        //        let cellNib2 = UINib.init(nibName: kCollectoinIdentifierSlideMenuCategories, bundle: nil)
        //        self.myCollectionView?.registerNib(cellNib2, forCellWithReuseIdentifier: kCollectoinIdentifierSlideMenuCategories)
        //
        //        sizingCategoriesCell = cellNib2.instantiateWithOwner(nil, options: nil)[0] as! SlideMenuCategoriesCollectionCell
        //
        //        let layout = DLTCenterFlowLayout()
        //
        //        layout.scrollDirection = .Horizontal
        //        layout.minimumLineSpacing = 6
        //        layout.minimumInteritemSpacing = 6
        //
        //        self.my_collectionViewMenu?.collectionViewLayout = layout
    }
    
    
    /*******************************************************************************/
    
    /*******************************************************************************/
    // MARK: - SET
    
    func setSlideMenuBackgroundColor(color: UIColor){
        
        self.backgroundColor = color
        self.myCollectionView?.backgroundColor = UIColor.clear
    }
    
    /*******************************************************************************/
    
    //MARK: - CollectionView Delegate - Data Source
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.delegate?.slideMenu_NumberOfItem()) ?? 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch self.slideMenuStyle {
        case .Title:
            //
            self._configureCell(cell: sizingCell, indexPath: indexPath)
            var size = sizingCell.intrinsicContentSize
            size.height = self.height_cellItem
            size.width  = screenWidth/CGFloat(self.collectionView(collectionView, numberOfItemsInSection: indexPath.section))//size.width + 40.0
            
            itemSize_dict["\(indexPath.row)"] = size
            
            return size
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch self.slideMenuStyle {
        case .Title:
            return self.configureCellWithCollectionViewMenu(indexPath: indexPath)
        }
    }
    
    // MARK: Layout
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    private func configureCellWithCollectionViewMenu(indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = self.myCollectionView?.dequeueReusableCell(withReuseIdentifier: SlideMenuTitleCollectionCell.nameOfClass, for: indexPath) as! SlideMenuTitleCollectionCell
        
        cell.setupCellItemColor(BorderColor: self.title_borderColor, BackgroundColor: self.title_backgroundColor, TextColor: self.title_textColor)
        
        if self.activeWhenSelected{
            if self.currentSelectedIndexPath.row == indexPath.row {
                cell.isSelected = true
            }else{
                cell.isSelected = false
            }
            cell.setupBtnIsSelected(selected: cell.isSelected)
        }
        self._configureCell(cell: cell, indexPath: indexPath)
        
        return cell
    }
    
    private func _configureCell(cell: SlideMenuTitleCollectionCell, indexPath: IndexPath){
        
        if let stringTitle = self.delegate?.slideMenu_TitleForItem(index: indexPath.row) {
            cell.cell_LabelTitle?.text = stringTitle
        }else{
            cell.cell_LabelTitle?.text = ""
        }
    }
    
    
    //MARK: Did Select
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch self.slideMenuStyle {
        case .Title:
            //
            self.selectItemMenuAtIndexPath(indexPath: indexPath, withDelegateCallBack: true)
            self.moveBottomIndicatorAt(index: indexPath.row)
            
            break
            
        }
    }
    
    private func selectItemMenuAtIndexPath(indexPath: IndexPath, withDelegateCallBack enable: Bool){
        if self.activeWhenSelected{
            self.cleanLastSelectedButton(lastIndexPath: self.currentSelectedIndexPath)
            self.setupCurrentSelectedIndexPath(indexPath: indexPath)
        }
        
        if enable{
            self.delegate?.slideMenu_DidSelectedItemAtIndex(index: indexPath.row)
        }
    }
    
    private func cleanLastSelectedButton(lastIndexPath: IndexPath){
        if let cell = self.myCollectionView?.cellForItem(at: currentSelectedIndexPath) as?  SlideMenuTitleCollectionCell{
            cell.isSelected = false
            
            cell.setupBtnIsSelected(selected: cell.isSelected)
        }
    }
    
    private func setupCurrentSelectedIndexPath(indexPath: IndexPath){
        currentSelectedIndexPath = indexPath
        
        if let cell = self.myCollectionView?.cellForItem(at: currentSelectedIndexPath) as?  SlideMenuTitleCollectionCell{
            cell.isSelected = true
            cell.setupBtnIsSelected(selected: cell.isSelected)
        }
        
        self.myCollectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func updateCurentSelected(index: Int){
        let indexPath = IndexPath.init(row: index, section: 0)
        self.selectItemMenuAtIndexPath(indexPath: indexPath, withDelegateCallBack: true)
    }
}
