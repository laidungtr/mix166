//
//  DLTPlayer.swift
//  AVPlayer Example
//
//  Created by ttiamap on 4/27/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import AVFoundation
import MediaPlayer

protocol DLTPlayerDelegate {
    
    func DLTPlayer_UpdateBackgroundSongInfo()
    func DLTPlayer_PlayingStatusChange(_ isPlaying: Bool)
    func DLTPlayer_DurationCountDown(str_durationCount: String, str_duration: String, sliderValue: Float)
    func DLTPlayer_DidFinishPlaying()
    func DLTPlayer_CurrentLinkSteamingFail(str_linkUrl: String)
    func DLTPlayerBuffering(enable: Bool)
}

private enum LocalizeString: String{
    case unknow_songName,unknow_artist,unknow_genres   = "Đang cập nhật"
}

class DLTPlayer: NSObject {
    
    
    // KVO keyPath
    internal let pathStatus          = "status"
    internal let pathRate            = "rate"
    internal let pathBufferEmpty     = "playbackBufferEmpty"
    internal let pathPlaybackKeepUp  = "currentItem.playbackLikelyToKeepUp"
    internal let pathLoadTimeRange   = "loadedTimeRanges"
    
    //MARK: - Properties
    internal var player: AVPlayer = AVPlayer.init()
    
    internal var timeObsever: Any?
    
    internal var isObserverRegister = false
    internal var isObserverNotificationRegister = false
    
    internal var playerLayer: AVPlayerLayer = AVPlayerLayer.init()
    
    internal var currentStreamingLink = ""
    internal var commingStreamingLink = "none"
    
    internal var storeFailStreamingLink: [String] = []
    
    let queueConcurrent = DispatchQueue(label: "queuename", attributes: .concurrent)
    
    // an item for streaming play
    internal var playerItem: AVPlayerItem?
    
    internal var playingInfo: [String : Any]?
    
    var isPlaying = false {
        didSet{
            self.delegate?.DLTPlayer_PlayingStatusChange(self.isPlaying)
        }
    }
    
    internal var storingIsPlayingFlag = false
    
    var enableLoop: Bool = false
    
    internal var isVideoPlaying: Bool = false

    
    var delegate: DLTPlayerDelegate?
    
    var volume: CGFloat         { return self.getVolume() }
    
    // song duration
    var duration: TimeInterval  { return self.getPlayerItemDuration() }
    
    // current playing time
    var time: TimeInterval      { return self.getPlayerItemCurrentTime() }
    
    class var sharedInstance: DLTPlayer {
        
        struct Singleton {
            static let instance = DLTPlayer()
        }
        return Singleton.instance
    }
    
    override init() {
        super.init()
        self.setupNowPlayingInfoCenter()
    }
}

//MARK: - STREAMING
extension DLTPlayer{
    
    class func setupPlayerLayer(frame: CGRect){
        // An AVPlayerLayer is a CALayer instance to which the AVPlayer can
        // direct its visual output. Without it, the user will see nothing.
        self.sharedInstance.playerLayer = AVPlayerLayer(player: self.sharedInstance.player)
        self.sharedInstance.playerLayer.frame = frame
        self.sharedInstance.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.sharedInstance.playerLayer.needsDisplayOnBoundsChange = true
        
    }
    
    class func updatePlayerLayer(frame: CGRect){
        self.sharedInstance.playerLayer.frame = frame
    }
    
    @discardableResult class func playVideoWithUrl(_ stringUrl: String) -> AVPlayerLayer{
        
        self.playWithUrl(stringUrl, isVideo: true)
        
        return self.sharedInstance.playerLayer
    }
    
    class func playWithUrl(_ stringUrl: String, isVideo: Bool = false){
        
        self.sharedInstance.isVideoPlaying = isVideo
        self.sharedInstance.setupPermisionToPlayInBackground(active: !isVideo)
        
        if self.sharedInstance.storeFailStreamingLink.contains(stringUrl){
            self.sharedInstance.delegate?.DLTPlayer_CurrentLinkSteamingFail(str_linkUrl: stringUrl)
            return
        }
        
        if stringUrl == self.sharedInstance.currentStreamingLink{
            if !self.sharedInstance.isPlaying{
                self.sharedInstance.play_resume_Audio()
            }
            
            return
        }
        
        DLTPlayer.stop {
            print("Playing Link : \(stringUrl)")
            
            if Thread.isMainThread{
                self.sharedInstance.commingStreamingLink = stringUrl
                
                if let url = URL.init(string: stringUrl){
                    
                    let asset = AVAsset.init(url: url)
                    asset.loadValuesAsynchronously(forKeys: ["duration"], completionHandler: {
                        let newItem = AVPlayerItem.init(asset: asset)
                        self.sharedInstance.playerItem = newItem
                        
                        // play
                        self.sharedInstance.play_resume_Audio()
                    })
                    
                }
            }else{
                
                DispatchQueue.main.async() { () -> Void in
                    
                    self.sharedInstance.commingStreamingLink = stringUrl
                    
                    if let url = URL.init(string: stringUrl){
                        
                        let asset = AVAsset.init(url: url)
                        asset.loadValuesAsynchronously(forKeys: ["duration"], completionHandler: {
                            let newItem = AVPlayerItem.init(asset: asset)
                            self.sharedInstance.playerItem = newItem
                            
                            // play
                            self.sharedInstance.play_resume_Audio()
                        })
                    }
                }
            }
        }
    }
    
    @objc class func didFinishPlaying(){
        self.sharedInstance.didFinishPlaying()
    }
    
    @objc private func didFinishPlaying(){
        
        self.delegate?.DLTPlayer_DurationCountDown(str_durationCount: "00:00", str_duration: "00:00", sliderValue: 0)
        if !self.enableLoop || self.isVideoPlaying{
            self.delegate?.DLTPlayer_DidFinishPlaying()
        }else{
            self.seekTo(value: 0)
            self.player.play()
        }
    }
    
    fileprivate func play_resume_Audio(){
        
        self.isPlaying = true
        if !self.detectPlayerResume(){
            // replace audio with new player item
            
            if self.isObserverRegister{
                self.dellocObserver()
            }
            
            self.player.replaceCurrentItem(with: self.playerItem)
            
            self.addObserver()
            
            self.addNotification()
            
            // setting
            self.player.rate = 1.0
            
            self.player.play()
            
        }else{
            self.updateBackgroundViewPlayerPlay_Pause()
            // play
            self.player.play()
            
        }
    }
    
    class func stop(callback:@escaping () -> ()){
        
        if !self.sharedInstance.isPlaying ||
            self.sharedInstance.currentStreamingLink == ""{
            callback()
            return
        }
        
        self.pause()
        
        self.sharedInstance.dellocObserver()
        
        self.sharedInstance.currentStreamingLink = ""
        
        self.sharedInstance.player.replaceCurrentItem(with: nil)
        
        self.sharedInstance.playerLayer.removeFromSuperlayer()
        
        self.sharedInstance.delegate?.DLTPlayer_DurationCountDown(str_durationCount: "00:00", str_duration: "00:00", sliderValue: 0)
        
        callback()
    }
    
    class func pauseForChangeSong(){
        self.sharedInstance.pauseForChangeSong()
    }
    
    func pauseForChangeSong(){
        self.pause()
        self.delegate?.DLTPlayer_DurationCountDown(str_durationCount: "00:00", str_duration: "00:00", sliderValue: 0)
    }
    
    class func pause(){
        
        // set back playing flag to false
        // this line must run before asking player pause
        self.sharedInstance.pause()
    }
    
    func pause(){
        self.isPlaying = false
        self.player.pause()
        self.updateBackgroundViewPlayerPlay_Pause()
    }
    
    class func seekTo(value: Float){
        self.sharedInstance.seekTo(value: value)
    }
    
    func seekTo(value: Float){
        let time = self.getPlayerItemDuration() * Double(value)
        self.player.seek(to: CMTime.init(seconds: time, preferredTimescale: 60)) { (finish) in
            //
        }
    }
    
    
    //MARK: KVO handle
    fileprivate func addObserver(){
        
        if !self.isObserverRegister{
            self.player.currentItem?.addObserver(self, forKeyPath: pathStatus, options: NSKeyValueObservingOptions(), context: nil)
            self.player.addObserver(self, forKeyPath: pathRate, options: NSKeyValueObservingOptions(), context: nil)
            self.player.addObserver(self, forKeyPath: pathBufferEmpty, options: NSKeyValueObservingOptions(), context: nil)
            self.player.addObserver(self, forKeyPath: pathLoadTimeRange, options: NSKeyValueObservingOptions(), context: nil)
            self.player.addObserver(self, forKeyPath: pathPlaybackKeepUp, options: .new, context: nil)
            
            self.addTimeObserver()
            
            self.isObserverRegister = true
        }
        
    }
    
    private func addTimeObserver(){
        let interval = CMTime(value: 1, timescale: 1)
        self.timeObsever = self.player.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) { (progressTime) in
            let secondsProgress = CMTimeGetSeconds(progressTime)
            
            let str_countDownDuration = self.getFormatedTime(FromTime: Int(secondsProgress))
            let str_duration = self.getFormatedTime(FromTime: Int(self.getPlayerItemDuration()))
            
            let duration = self.getPlayerItemDuration()
            if  duration > 0{
                let sliderValue = self.getPlayerItemCurrentTime()/duration
                self.delegate?.DLTPlayer_DurationCountDown(str_durationCount: str_countDownDuration,
                                                           str_duration: str_duration,
                                                           sliderValue: Float(sliderValue))
            }
        }
    }
    
    func getFormatedTime(FromTime timeDuration:Int) -> String {
        
        let minutes = Int(timeDuration) / 60 % 60
        let seconds = Int(timeDuration) % 60
        let strDuration = String(format:"%02d:%02d", minutes, seconds)
        return strDuration
    }
    
    
    /**
     listen user interruption ( phone call ...)
     listen router change (head phone plug/unplug...)
     */
    fileprivate func addNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserEndInterruption(notification:)), name: NSNotification.Name.AVAudioSessionInterruption, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleRouterChange(notification:)), name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.playerItem)
    }
    
    fileprivate func dellocObserver(){
        
        if self.isObserverRegister{
            
            self.player.currentItem?.removeObserver(self, forKeyPath: pathStatus)
            self.player.removeObserver(self, forKeyPath: pathRate)
            self.player.removeObserver(self, forKeyPath: pathBufferEmpty)
            self.player.removeObserver(self, forKeyPath: pathLoadTimeRange)
            self.player.removeObserver(self, forKeyPath: pathPlaybackKeepUp)
            
            if self.timeObsever != nil{
                self.player.removeTimeObserver(self.timeObsever!)
                self.timeObsever = nil
            }
            
            self.isObserverRegister = false
        }
        
        NotificationCenter.default.removeObserver(self)
        
    }
    
    // handle phone interrupe
    
    //
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        print("change at keypath = \(keyPath ?? "nil") for \(change?.description ?? "nil")")
        
        if keyPath == pathStatus {
            self.handlePlayerStatus()
            
        }else if keyPath == pathRate {
            let currentTime = self.getPlayerItemCurrentTime()
            if self.player.rate == 0 && //if player rate dropped to 0
                currentTime > 0 && //if video was started
                currentTime < self.getPlayerItemDuration() && //but not yet finished
                self.isPlaying{
                self.handleStalled()
            }
            
        }else if keyPath == pathBufferEmpty {
            //showAlert("No internet connection!", message: "Mixmatic requires an internet connection to continue streaming.")
            
        }else if keyPath == pathPlaybackKeepUp {
            if self.player.currentItem != nil {
                if self.player.currentItem!.isPlaybackLikelyToKeepUp {
                    self.delegate?.DLTPlayerBuffering(enable: false)
                    self.delegate?.DLTPlayer_UpdateBackgroundSongInfo()
                    
                } else {
                    self.delegate?.DLTPlayerBuffering(enable: true)
                }
            }
        }else if keyPath == pathLoadTimeRange {
            
        }
    }
    
    func handleUserEndInterruption(notification: NSNotification){
        
        if let typeKey = notification.userInfo? [AVAudioSessionInterruptionTypeKey] as? UInt,
            let type = AVAudioSessionInterruptionType(rawValue: typeKey) {
            switch type {
            case .began:
                print("Audio Interruption Began")
                
                self.storingIsPlayingFlag = self.isPlaying
                
                if self.isPlaying{
                    self.pause()
                }
                
            case .ended:
                print("Audio Interuption Ended")
                
                self.isPlaying = self.storingIsPlayingFlag
                
                if self.isPlaying { self.play_resume_Audio() }
            }
        }
    }
    
    func handleRouterChange(notification: NSNotification){
        
        if let typeKey = notification.userInfo?[AVAudioSessionRouteChangeReasonKey] as? UInt,
            let type = AVAudioSessionRouteChangeReason(rawValue: typeKey){
            
            switch type {
            case .unknown:
                //
                print("handleRouterChange unknown")
                break
                
            case .newDeviceAvailable:
                //
                print("handleRouterChange newDeviceAvailable")
                if self.isPlaying { self.play_resume_Audio() }
                break
                
            case .oldDeviceUnavailable:
                //
                print("handleRouterChange oldDeviceUnavailable")
                if self.isPlaying { self.play_resume_Audio() }
                break
                
            case .categoryChange:
                //
                print("handleRouterChange categoryChange")
                break
                
            case .override:
                //
                print("handleRouterChange override")
                break
                
            case .wakeFromSleep:
                //
                print("handleRouterChange wakeFromSleep")
                break
                
            case .noSuitableRouteForCategory:
                //
                print("handleRouterChange noSuitableRouteForCategory")
                break
                
            case .routeConfigurationChange:
                //
                print("handleRouterChange routeConfigurationChange")
                break
            }
        }
    }
    
    func handlePlayerStatus(){
        
        switch self.player.status {
        case .readyToPlay:
            
            print(self.player.currentItem!.loadedTimeRanges)
            
            /*
             [CMTimeRange: {{0/1 = 0.000}, {0/1 = 0.000}}]
             */
            
            if !(self.player.currentItem!.loadedTimeRanges.count > 0){
                
                // update fail link first
                let failLink = self.currentStreamingLink
                if !self.storeFailStreamingLink.contains(failLink){
                    self.storeFailStreamingLink.append(failLink)
                }
                
                print("Ready to play couldnt steaming with currrent link")
                self.delegate?.DLTPlayer_CurrentLinkSteamingFail(str_linkUrl: self.currentStreamingLink)
                
            }else{
                print("Ready to play")
            }
            
            break
            
        case .failed:
            //  Possibly show error message or attempt replay from tart
            //  Description from the docs:
            //  Indicates that the player can no longer play AVPlayerItem instances because of an error. The error is described by
            //  the value of the player's error property.
            print("play fail")
            break
            
        case .unknown:
            print("play fail unknow")
            break
            
        }
    }
    
    func handleStalled() {
        
        if self.player.currentItem == nil {
            print("handleStalled player.currentItem = nil")
            return
        }
        
        if (self.player.currentItem!.isPlaybackLikelyToKeepUp || self.availableDuration() - self.getPlayerItemCurrentTime() > 10.0){
            self.play_resume_Audio()
        }else{
            self.perform(#selector(self.handleStalled), with: nil, afterDelay: 1.0)
        }
    }
}

// MARK: - GET
extension DLTPlayer{
    internal func getPlayerItemCurrentTime() -> TimeInterval{
        
        if self.player.currentItem != nil {
            let durationSecond = CMTimeGetSeconds(self.player.currentItem!.currentTime())
            return durationSecond
        }else{
            print("getPlayerItemCurrentTime currentItem is Nil")
            return 0.0
        }
    }
    
    internal func getPlayerItemWasBufferDuration() -> TimeInterval{
        
        if let loadedTimeRanges = self.player.currentItem?.loadedTimeRanges{
            if let timeRange = loadedTimeRanges.first?.timeRangeValue {
                let durationSecond = CMTimeGetSeconds(timeRange.duration)
                return durationSecond
            }
        }
        
        return 0//TimeInterval.nan
    }
    
    internal func getPlayerItemDuration() -> TimeInterval{
        var durationSecond = CMTimeGetSeconds(self.player.currentItem?.asset.duration ?? CMTime.init())
        if durationSecond.isNaN{
            durationSecond = 0
        }
        return durationSecond < 0 ? 0 : durationSecond
    }
    
    internal func getVolume() -> CGFloat{
        return CGFloat(AVAudioSession.sharedInstance().outputVolume)
    }
}

//MARK: - SET
extension DLTPlayer{
    
    func setVolume(value: Float){
        self.player.volume = value
    }
    
    // set play view at background
    class func setBackgroundViewPlayer(image: UIImage, song_name: String, artist_name: String, genres_name: String){
        
        DispatchQueue.global(qos: .background).sync {
            
            let _song_name = song_name.length > 0 ? song_name : LocalizeString.unknow_songName.rawValue
            let _artist_name = artist_name.length > 0 ? artist_name : LocalizeString.unknow_artist.rawValue
            let _genres_name = genres_name.length > 0 ? genres_name : LocalizeString.unknow_genres.rawValue
            
            let mySize = CGSize(width: image.size.width, height: image.size.height)
            
            var albumArt: MPMediaItemArtwork = MPMediaItemArtwork.init(image: image_songPlaceholder!)
            if #available(iOS 10.0, *) {
                albumArt = MPMediaItemArtwork(boundsSize:mySize) { sz in
                    return image
                }
            } else {
                albumArt = MPMediaItemArtwork.init(image: image)
            }
            
            self.sharedInstance.playingInfo = [MPMediaItemPropertyTitle: _song_name,
                                               MPMediaItemPropertyGenre: _genres_name,
                                               MPMediaItemPropertyArtist: _artist_name,
                                               MPMediaItemPropertyPlaybackDuration: self.sharedInstance.getPlayerItemDuration(),
                                               MPNowPlayingInfoPropertyPlaybackRate: self.sharedInstance.isPlaying ? 1 : 0,
                                               MPNowPlayingInfoPropertyElapsedPlaybackTime: self.sharedInstance.getPlayerItemCurrentTime(),
                                               MPMediaItemPropertyArtwork: albumArt] as [String : Any]
            
            
            let infoCenter = MPNowPlayingInfoCenter.default()
            infoCenter.nowPlayingInfo = self.sharedInstance.playingInfo
            
            print("create background view")
        }
    }
    
    class func updateBackgroundViewPlayerPlay_Pause(){
        self.sharedInstance.updateBackgroundViewPlayerPlay_Pause()
    }
    
    func updateBackgroundViewPlayerPlay_Pause(){
        
        if self.playingInfo == nil {
            return
        }
        DispatchQueue.global(qos: .background).sync {
            
            self.playingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = self.isPlaying ? 1 : 0
            self.playingInfo?[MPNowPlayingInfoPropertyElapsedPlaybackTime] = self.getPlayerItemCurrentTime()
            
            let infoCenter = MPNowPlayingInfoCenter.default()
            infoCenter.nowPlayingInfo = self.playingInfo
            
            print("update background view pause")
        }
    }
    
    func setupNowPlayingInfoCenter() {
        
        MPRemoteCommandCenter.shared().playCommand.addTarget {event in
            if let playerController = SharePlayer.playerController{
                playerController.btnPlayTap(sender: nil)
            }
            
            return MPRemoteCommandHandlerStatus.success
        }
        MPRemoteCommandCenter.shared().pauseCommand.addTarget {event in
            if let playerController = SharePlayer.playerController{
                playerController.btnPlayTap(sender: nil)
            }
            return MPRemoteCommandHandlerStatus.success
        }
        MPRemoteCommandCenter.shared().nextTrackCommand.addTarget {event in
            if let playerController = SharePlayer.playerController{
                playerController.btnNextTap(sender: nil)
            }
            return MPRemoteCommandHandlerStatus.success
        }
        MPRemoteCommandCenter.shared().previousTrackCommand.addTarget {event in
            if let playerController = SharePlayer.playerController{
                playerController.btnPreviousTap(sender: nil)
            }
            return MPRemoteCommandHandlerStatus.success
        }
    }
    
    func setupPermisionToPlayInBackground(active: Bool){
        let application = UIApplication.shared
        application.beginReceivingRemoteControlEvents()
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setMode(AVAudioSessionModeDefault)
                
                do{
                    
                    try AVAudioSession.sharedInstance().setActive(active)
                    
                    application.becomeFirstResponder()
                    print("AVAudioSession is Active")
                }catch{
                    print(error)
                }
                
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
        
        var task:UIBackgroundTaskIdentifier = 0
        
        task = application.beginBackgroundTask(expirationHandler: {
            print("expiration handler called %f \(application.backgroundTimeRemaining)")
            application.endBackgroundTask(task)
            task  = UIBackgroundTaskInvalid
        })
    }
}

//MARK: - HELP
extension DLTPlayer{
    func detectPlayerResume() -> Bool{
        
        if let _playerItem = self.playerItem{
            
            // This condition: CMTimeGetSeconds(self.playerItem.duration) != CMTimeGetSeconds(self.playerItem.currentTime) is to detect the difference between arriving at the end of the video or stopping in the middle
            
            let duration = self.getPlayerItemDuration()
            
            if self.player.rate == 0 &&
                !duration.isNaN &&
                duration != CMTimeGetSeconds(_playerItem.currentTime()) &&
                self.commingStreamingLink == self.currentStreamingLink{
                
                // resume
                return true
            }
        }
        
        self.currentStreamingLink = self.commingStreamingLink
        return false
    }
    
    func availableDuration() -> TimeInterval{
        if let loadedTimeRanges = self.player.currentItem?.loadedTimeRanges{
            if let timeRange = loadedTimeRanges.first?.timeRangeValue {
                
                let startSecond = CMTimeGetSeconds(timeRange.start)
                let durationSecond = CMTimeGetSeconds(timeRange.duration)
                
                return startSecond + durationSecond
            }
        }
        
        print("available duration = 0.0")
        return 0.0
    }
}
