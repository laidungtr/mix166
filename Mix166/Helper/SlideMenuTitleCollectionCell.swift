//
//  SlideMenuTitleCollectionCell.swift
//  Buyer
//
//  Created by ttiamap on 9/14/16.
//  Copyright © 2016 Trong. All rights reserved.
//

import UIKit

class SlideMenuTitleCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var cell_LabelTitle: UILabel?
    @IBOutlet weak var layoutHeightConstraint: NSLayoutConstraint?
    private var _extraMargins = CGSize()
    
    let queueConcurrent = DispatchQueue(label: "SlideMenuTitleCollectionCell", attributes: .concurrent)
    
    private var title_borderColor: UIColor = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    private var title_textColor: UIColor = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    private var title_textColorSelected: UIColor = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    private var title_backgroundColor: UIColor = UIColor.clear
    private var title_fontSelected = UIFont.fontSemiBold_medium()
    private var title_font = UIFont.fontRegular_medium()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.refreshUI()
        self.setupFont()
        self.setupCellTitleColor()
    }
    
    override func prepareForReuse() {
        self.refreshUI()
    }
    
    private func setupFont(){
        self.cell_LabelTitle?.font = title_font
        self.cell_LabelTitle?.textColor = self.title_textColor
        self.cell_LabelTitle?.backgroundColor = self.title_backgroundColor
    }
    
    func setupCellTitleColor(){
        
        let height: CGFloat = self.frame.size.height/1.45
        
        self.layoutHeightConstraint?.constant = height
        
        self.setupFont()
        //        self.cell_LabelTitle?.borderWithRadius(height/2 , borderWidth: 0.5, borderColor: title_borderColor)
        
        if Thread.isMainThread{
            self.layoutIfNeeded()
        }else{
            DispatchQueue.main.async() { () -> Void in
                self.layoutIfNeeded()
            }
        }
        
    }
    
    func setupCellItemColor(BorderColor borderColor: UIColor, BackgroundColor backgroundColor: UIColor, TextColor textColor: UIColor){
        self.title_borderColor = borderColor
        self.title_backgroundColor = backgroundColor
        self.title_textColor = textColor
        
        self.setupCellTitleColor()
    }
    
    
    func refreshUI(){
        
        self.backgroundColor = UIColor.clear
        self.cell_LabelTitle?.backgroundColor = UIColor.clear
        self.cell_LabelTitle?.textColor = self.title_textColor
        self.cell_LabelTitle?.font = self.title_font
    }
    
    func setupBtnIsSelected(selected: Bool){
        
        queueConcurrent.async() { () -> Void in
            DispatchQueue.main.async() { () -> Void in
                if selected{
                    self.cell_LabelTitle?.textColor = self.title_textColorSelected
                    self.cell_LabelTitle?.font = self.title_fontSelected
                    self.layoutIfNeeded()
                    
                }else{
                    self.refreshUI()
                }
            }
        }
    }
    
    override var intrinsicContentSize: CGSize{
        var size = self.cell_LabelTitle?.intrinsicContentSize
        
        size?.height = 40.0
        
        return size!
    }
    
}
