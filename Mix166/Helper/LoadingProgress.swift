//
//  LoadingProgress.swift
//  LoadingProgress
//
//  Created by ttiamap on 8/4/16.
//  Copyright © 2016 ttiamap. All rights reserved.
//

import UIKit


let maskColor_Default = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.7)
let maskColor_Black = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.7)
let maskColor_White = UIColor.white
let maskColor_Transparent = UIColor.clear

let kWidthHeightProgressPullRefresh :CGFloat = 32.0
let kWidthHeightLogoPullRefresh :CGFloat = 24.0

let kWidthHeightProgressDefault :CGFloat = 40.0
let kWidthHeightLogoPullDefault :CGFloat = 28.0

enum maskStyle: String {
    
    case None = "1"             // user can do action with control when loading progress is showing
    case Black = "2"            //
    case White = "3"            //
    case Transparent = "4"      // user can't do action with mask
    case BlurExtraLight = "5"   //
    case BlurLight = "6"        //
    case BlurDark = "7"         //
    case Default = "8"          //
}

class LoadingProgress {
    
    var widthHeightProgress = kWidthHeightProgressDefault
    var widthHeightLogo = kWidthHeightLogoPullDefault
    
    var imgProgress = UIImage.init(named: "ic_circleProgress")
    var imgLogo = UIImage.init(named: "ic_logoProgress")
    
    var viewMask: UIView?
    var viewContain : UIView?
    var imgvProgress: UIImageView?
    var imgvLogo: UIImageView?
    
    let alphaMask: CGFloat = 0.9
    
    class var sharedInstance: LoadingProgress {
        struct Singleton {
            static let instance = LoadingProgress()
        }
        return Singleton.instance
    }
    
    
    //MARK: - Show progress
    static func showFromView(_ view: UIView, _maskStyle: maskStyle){
        self.sharedInstance.dismissLoadingProgressWithoutAnimation()
        self.sharedInstance.showLoadingProgressFromView(view, _maskStyle: _maskStyle)
    }
    
    // do normal progress
   fileprivate func showLoadingProgressFromView(_ view: UIView, _maskStyle: maskStyle){
        
        //update progress size without pull request
        self.updateLoadingSizeWithPullToRefresh(false)
    
        // setup progress loading
        self.setupProgressFromView(view, _maskStyle: _maskStyle)
        
        // do progress
        self.rotate360Degrees()
    }
    
    // do with pull to refresh progress
    static func showFromViewWithPullToRefresh(_ view: UIView, _maskStyle: maskStyle){
        self.sharedInstance.showFromViewWithPullToRefresh(view, _maskStyle: _maskStyle)
    }
    
    fileprivate func showFromViewWithPullToRefresh(_ view: UIView, _maskStyle: maskStyle){
        
        //update progress size without pull request
        self.updateLoadingSizeWithPullToRefresh(true)
        
        // setup progress loading
        self.setupProgressFromView(view, _maskStyle: _maskStyle)
        
        // do progress
        self.rotate360Degrees()
    }
    
    func updateLoadingSizeWithPullToRefresh(_ enable: Bool){
        if enable {
            widthHeightLogo = kWidthHeightLogoPullRefresh
            widthHeightProgress = kWidthHeightProgressPullRefresh
        }else{
            widthHeightLogo = kWidthHeightLogoPullDefault
            widthHeightProgress = kWidthHeightProgressDefault
        }
    }
    
    
    //MARK: - Dissmiss progress
   static func dissmiss(){
        self.sharedInstance.dismissLoadingProgress()
    }
    
    func dismissLoadingProgress(){
        
        UIView.animate(withDuration: 0.15, animations: {
            //
            self.viewContain?.alpha = 0
            self.viewMask?.alpha = 0
            
        }) { (finish) in
            if finish{
                self.viewContain?.removeFromSuperview()
                self.viewMask?.removeFromSuperview()
                
                self.viewContain?.alpha = 0
                self.viewMask?.alpha = 0
            }
        }
    }
    
    fileprivate func dismissLoadingProgressWithoutAnimation(){
        self.viewContain = nil
        self.viewMask = nil
    }
    
    
    //MARK: setup Progress View with _maskType
    fileprivate func setupProgressFromView(_ view: UIView, _maskStyle: maskStyle){
        switch _maskStyle {
        case .None:
            //
            self.setupProgressWithoutMaskFromView(view)
            break
        default:
            //
            self.setupProgressWithMaskFromView(view, _maskStyle: _maskStyle)
            break
        }
    }
    
    fileprivate func setupProgressWithoutMaskFromView(_ view: UIView){
        if viewContain == nil{
            viewContain = UIView.init()
            imgvProgress = UIImageView.init(image: imgProgress)
            imgvLogo = UIImageView.init(image: imgLogo)
            viewContain?.layer.zPosition = 1
        }
        
        // add subview
        viewContain?.addSubview(imgvLogo!)
        viewContain?.addSubview(imgvProgress!)
        
        view.addSubview(viewContain!)
        view.bringSubview(toFront: viewContain!)
        
        // AutoLayout
        // viewContain
        
        viewContain?.addCenterXConstraint(toView: view)
        viewContain?.addCenterYConstraint(toView: view)
        
        // imgvProgress
        imgvProgress?.addLeadingConstraint(toView: viewContain, attribute: .leading, relation: .equal, constant: 2.0)
        imgvProgress?.addTrailingConstraint(toView: viewContain, attribute: .trailing, relation: .equal, constant: 2.0)
        imgvProgress?.addTopConstraint(toView: viewContain, attribute: .top, relation: .equal, constant: 2.0)
        imgvProgress?.addBottomConstraint(toView: viewContain, attribute: .top, relation: .equal, constant: 2.0)
        
        imgvProgress?.addHeightConstraint(toView: nil, relation: .equal, constant: widthHeightProgress)
        imgvProgress?.addWidthConstraint(toView: nil, relation: .equal, constant: widthHeightProgress)
        
        // imgLogo
        imgvLogo?.addCenterXConstraint(toView: viewContain)
        imgvLogo?.addCenterYConstraint(toView: viewContain)
        
        imgvLogo?.addHeightConstraint(toView: nil, relation: .equal, constant: widthHeightLogo)
        imgvLogo?.addWidthConstraint(toView: nil, relation: .equal, constant: widthHeightLogo)
        
        imgvLogo?.contentMode = .scaleAspectFit
        
        viewContain?.layoutIfNeeded()
        view.layoutIfNeeded()
    }
    
    fileprivate func setupProgressWithMaskFromView(_ view: UIView, _maskStyle: maskStyle){
        if viewMask == nil {
            viewMask = UIView.init()
            viewContain = UIView.init()
            imgvProgress = UIImageView.init(image: imgProgress)
            imgvLogo = UIImageView.init(image: imgLogo)
            
            viewContain?.layer.zPosition = 1
        }
        
        
        // set up view mask with maskStyle
        self.setupViewWithMaskStyle(_maskStyle)
        
        // add subview
        view.addSubview(viewMask!)
        view.bringSubview(toFront: viewMask!)
        
        viewContain?.addSubview(imgvLogo!)
        viewContain?.addSubview(imgvProgress!)
        viewMask?.addSubview(viewContain!)
        
        
        // AutoLayout
        
        // viewMask
        viewMask?.addLeftConstraint(toView: view, attribute: .left, relation: .equal, constant: 0)
        viewMask?.addRightConstraint(toView: view, attribute: .right, relation: .equal, constant: 0)
        viewMask?.addTopConstraint(toView: view, attribute: .top, relation: .equal, constant: 0)
        viewMask?.addBottomConstraint(toView: view, attribute: .bottom, relation: .equal, constant: 0)
        
        // viewContain
        viewContain?.addCenterXConstraint(toView: viewMask)
        viewContain?.addCenterYConstraint(toView: viewMask)
        
        // imgvProgress
        imgvProgress?.addLeftConstraint(toView: viewContain, attribute: .left, relation: .equal, constant: 0)
        imgvProgress?.addRightConstraint(toView: viewContain, attribute: .right, relation: .equal, constant: 0)
        imgvProgress?.addTopConstraint(toView: viewContain, attribute: .top, relation: .equal, constant: 0)
        imgvProgress?.addBottomConstraint(toView: viewContain, attribute: .bottom, relation: .equal, constant: 0)
        
        imgvProgress?.addHeightConstraint(toView: nil, relation: .equal, constant: widthHeightProgress)
        imgvProgress?.addWidthConstraint(toView: nil, relation: .equal, constant: widthHeightProgress)
        
        // imgLogo
        imgvLogo?.addCenterXConstraint(toView: viewContain)
        imgvLogo?.addCenterYConstraint(toView: viewContain)
        
        imgvLogo?.addHeightConstraint(toView: nil, relation: .equal, constant: widthHeightLogo)
        imgvLogo?.addWidthConstraint(toView: nil, relation: .equal, constant: widthHeightLogo)
        imgvLogo?.contentMode = .scaleAspectFit
        
        viewMask?.layoutIfNeeded()
        view.layoutIfNeeded()
    }
    
    
    //MARK: viewMask setup with MaskStyle
    @discardableResult
    fileprivate func setupViewWithMaskStyle(_ _maskStyle: maskStyle)->UIView{
        
        if viewMask == nil {
            // detect viewMask was nil
            viewMask = UIView.init()
        }
        
        switch _maskStyle {
        case .White:
            self.setupViewMaskWithColor(maskColor_White)
            break
            
        case .Black:
            self.setupViewMaskWithColor(maskColor_Black)
            break
            
        case .Transparent:
            self.setupViewMaskWithColor(maskColor_Transparent)
            break
            
        case .BlurExtraLight:
            self.setupViewMaskWithBlur(.extraLight)
            break
            
        case .BlurLight:
            self.setupViewMaskWithBlur(.light)
            break
            
        case .BlurDark:
            self.setupViewMaskWithBlur(.dark)
            break
            
        default:
            self.setupViewMaskWithColor(maskColor_Default)
        }
        
        return self.viewMask!
    }
    
    fileprivate func setupViewMaskWithColor(_ color: UIColor){
        self.viewMask = UIView.init()
        self.viewMask?.backgroundColor = color
    }
    
    fileprivate func setupViewMaskWithBlur(_ blurEffect: UIBlurEffectStyle){
        viewMask = UIVisualEffectView(effect: UIBlurEffect(style: blurEffect))
        self.viewMask?.alpha = alphaMask
        
    }
    
    //MARK: Animate
    fileprivate func rotate360Degrees(_ duration: CFTimeInterval = 0.75, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 2.0)
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = HUGE
        rotateAnimation.isCumulative = true
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as? CAAnimationDelegate
        }
        
        imgvProgress!.layer.add(rotateAnimation, forKey: "transform.rotation")
    }
    
}
