//
//  BaseTabbarController.swift
//  Mix166
//
//  Created by ttiamap on 6/16/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class BaseTabbarController: UITabBarController {
    
    var playerControllerExists = false
    var isStatusBarHidden = false
    var colorStatusBar: UIColor = .clear

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setStatusBarBackgroundColor(color: UIColor) {
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        
        statusBar.backgroundColor = color
    }
    
    func getStatusBarBackgroundColor() -> UIColor?{
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return  nil}
        
        return statusBar.backgroundColor
    }

    func showPlayerViewController(isFromListing: Bool){
        if let controller = SharePlayer.playerController{
            
            self.hideVideoControllerCompletely(callBack: {
                if !self.playerControllerExists{
                    self.addChildViewController(controller)
                    controller.view.frame = CGRect.init(x: 0, y: screenHeight, width: screenWidth, height: screenHeight)
                    self.view.addSubview(controller.view)
                    controller.didMove(toParentViewController: self)
                }
                
                let visibleRect = CGRect.init(x: 0, y: -playerControlHeight, width: screenWidth, height: playerViewheight)
                self.view.sendSubview(toBack: self.tabBar)
                
                // have to set isPlayerShow frist for update DLTPlayer delegate
                // do not move it to completion block below
                controller.vmodel.isPlayerShow = true
                
                UIView.animate(withDuration: 0.2, animations: {
                    controller.view.frame = visibleRect
                    UIApplication.shared.isStatusBarHidden = true
                }, completion: { (finish) in
                    if finish{
                        self.playerControllerExists = true
                        
                        controller.setupDelegate()
                        
                        controller.scrollToCurrentPlayingCell(isFromTracklisting: isFromListing)
                    }
                })
            })
        }
    }
    
    
    func hidePlayerViewController(){
        DispatchQueue.global(qos: .background).sync {
            if let controller = SharePlayer.playerController{
                view.bringSubview(toFront: self.tabBar)
                let frame = CGRect.init(x: 0, y: screenHeight - self.tabBar.frame.height - playerControlHeight, width: screenWidth, height: screenHeight)
                
                // have to set isPlayerShow frist for update DLTPlayer delegate
                // do not move it to completion block below
                controller.vmodel.isPlayerShow = false
                
                UIView.animate(withDuration: 0.2, animations: {
                    controller.view.frame = frame
                    UIApplication.shared.isStatusBarHidden = false
                }, completion: { (finish) in
                    if finish{
                        
                    }
                })
            }
        }
    }
    
    func hidePlayerViewControllerCompletely(){
        if let controller = SharePlayer.playerController{
            let frame = CGRect.init(x: 0, y: screenHeight, width: screenWidth, height: screenHeight)
            
            // have to set isPlayerShow frist for update DLTPlayer delegate
            // do not move it to completion block below
            controller.vmodel.isPlayerShow = false
            
            UIView.animate(withDuration: 0.2, animations: {
                controller.view.frame = frame
                UIApplication.shared.isStatusBarHidden = false
            }, completion: { (finish) in
                
            })
        }
    }
    
    
    // Video
    func showVideoViewController(){
        if let controller = SharePlayer.videoPlayerController{
            
            if controller.view.frame.origin.y == 0 && controller.vmodel.isExsitVideoController{
                // is showing
                return
            }
            
            controller.view.frame = CGRect.init(x: 0, y: screenHeight, width: screenWidth, height: screenHeight)
            
            if !controller.vmodel.isExsitVideoController{
                self.addChildViewController(controller)
                controller.view.frame = CGRect.init(x: 0, y: screenHeight, width: screenWidth, height: screenHeight)
                self.view.addSubview(controller.view)
                self.view.bringSubview(toFront: controller.view)
                controller.didMove(toParentViewController: self)
            }
            
            let visibleRect = CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight)
            view.sendSubview(toBack: self.tabBar)
            
            self.hidePlayerViewControllerCompletely()
            
            controller.videoViewScale(isScale: false)
            controller.viewWillAppear(false)
            
            UIView.animate(withDuration: 0.2, animations: {
                controller.view.frame = visibleRect
            }, completion: { (finish) in
                if finish{
                    UIApplication.shared.isStatusBarHidden = false
                    controller.vmodel.isExsitVideoController = true
                }
            })
        }
    }
    
    func hideVideoViewController(){
        DispatchQueue.global(qos: .background).sync {
            if let controller = SharePlayer.videoPlayerController{
                view.bringSubview(toFront: self.tabBar)
                
                let width = screenWidth/2
                let height = width * 9 / 16
                
                let frame = CGRect.init(x: screenWidth - width, y: screenHeight - height, width: width, height: height)
                UIView.animate(withDuration: 0.2) {
                    controller.view.frame = frame
                    UIApplication.shared.isStatusBarHidden = false
                }
            }
        }
    }
    
    func hideVideoControllerCompletely(callBack: @escaping () -> ()){
        if let controller = SharePlayer.videoPlayerController{
            controller.removeVideoController(callBack: callBack)
        }else{
            callBack()
        }
    }
    
    // More
    func showMoreController(){
        
        self.isStatusBarHidden = UIApplication.shared.isStatusBarHidden
        
        if let color = self.getStatusBarBackgroundColor(){
            self.colorStatusBar = color
        }
        
        self.setStatusBarBackgroundColor(color: .clear)
        
        UIApplication.shared.isStatusBarHidden = true
        
        self.showBlurView {
            if let controller = SharePlayer.playerMoreController{
                
                if !controller.vmodel.isExistsMoreController{
                    
                    self.addChildViewController(controller)
                    controller.view.frame = CGRect.init(x: 0, y: self.view.frame.size.height, width: screenWidth, height: screenHeight)
                    self.view.addSubview(controller.view)
                    self.view.bringSubview(toFront: controller.view)
                    controller.didMove(toParentViewController: self)
                }
                
                let visibleRect = CGRect.init(x: 0, y: self.view.frame.size.height - screenHeight, width: screenWidth, height: screenHeight)
                self.view.sendSubview(toBack: self.tabBar)
                
                controller.viewWillAppear(false)
                
                UIView.animate(withDuration: 0.2, animations: {
                    controller.view.frame = visibleRect
                }, completion: { (finish) in
                    if finish{
                        controller.vmodel.isExistsMoreController = true
                    }
                })
            }
        }
    }
    
    func hideMoreController(){
        
        DispatchQueue.global(qos: .background).sync {
            if let controller = SharePlayer.playerMoreController{
                
                let parentFrame = controller.parent
                let frame =  CGRect.init(x: 0, y: parentFrame?.view.frame.size.height ?? screenHeight, width: screenWidth, height: screenHeight)
                
                UIView.animate(withDuration: 0.2, animations: {
                    controller.view.frame = frame
                }, completion: { (finish) in
                    if finish{
                        if let playerController = SharePlayer.playerController{
                            if !playerController.vmodel.isPlayerShow{
                                self.view.bringSubview(toFront: self.tabBar)
                            }
                        }else{
                            self.view.bringSubview(toFront: self.tabBar)
                        }
                        
                        UIApplication.shared.isStatusBarHidden = self.isStatusBarHidden
                        self.setStatusBarBackgroundColor(color: self.colorStatusBar)
                        
                        self.hideBlurView(complete: {
                            
                        })
                    }
                })
            }
        }
    }

    
    // blur view
    func showBlurView(complete:@escaping () -> Void){
        
        var blurView:  UIVisualEffectView?
        if let _blurView = SharePlayer.blurView{
            blurView = _blurView
            
        }else{
            let blurEffect = UIBlurEffect(style: .dark)
            blurView = UIVisualEffectView(effect: blurEffect)
            blurView!.frame = self.view.bounds
            blurView!.alpha = 0
            view.addSubview(blurView!)
            
            SharePlayer.blurView = blurView
        }
        
        self.view.bringSubview(toFront: blurView!)
        
        UIView.animate(withDuration: 0.2, animations: {
            blurView?.alpha = 1
        }, completion: { (finish) in
            if finish{
                complete()
            }
        })
    }
    
    func hideBlurView(complete:@escaping () -> Void){
        if let blurView = SharePlayer.blurView{
            UIView.animate(withDuration: 0.2, animations: {
                blurView.alpha = 0
            }, completion: { (finish) in
                if finish{
                    self.view.sendSubview(toBack: blurView)
                    complete()
                }
            })
        }
    }
}
