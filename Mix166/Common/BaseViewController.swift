//
//  BaseViewController.swift
//  Mix166
//
//  Created by ttiamap on 5/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import MBProgressHUD

enum NavigationBarStyle {
    case Default
    case Playlist
    case Transparent
    case Search
}

struct AppUtility {
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        
        self.lockOrientation(orientation)
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }
    
}

struct BaseLayoutConfig {
    
    let navigation_config = NavigationConfig()
    let leftBarIconWidth: CGFloat = 25.0
    
    //    let textButtonNavWidth: CGFloat = 40.0
    //    let textOfBarButtonWidth: CGFloat = 35.0
    //    let navigationButtonSpace: CGFloat = 5.0
    //
    //    // ---  Color  ---
    //    let colorNavigation
    //
    //    // ---  Search Bar ----
    let searchBarTextDefaut: String = "Searching..."
    let searchBarHeight:CGFloat = 25.0;
    let searchTextFont = UIFont.fontRegular_medium()
    let searchTextColor = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    let searchBarBackgroundColor = UIColor.init(hexaString: AppColor.searchBarColor.rawValue)
    let searchBarPlaceholderTextColor = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    
    let textOfBarButtonWidth: CGFloat = 50.0
}

struct NavigationConfig {
    let titleColor: UIColor = UIColor.white
    let fontTitle = UIFont.fontSemiBold_extraBig()
    let backgroundColor: UIColor = colorNavigationBar
    var title: String = AppViewTitle.default.rawValue
    var style: NavigationBarStyle = .Default
    let defaultSize = CGSize(width: screenWidth, height: 64.0)
    
}

protocol baseViewKeyBoardDelegate{
    
    func keyboardDelegate_WillShowNotification(notification: NSNotification!)
    func keyboardDelegate_WillHideNotification(notification: NSNotification!)
}

protocol ArtistDetailDelegate {
    func artistDetail_didChangeFollowingStatus()
}

class BaseViewController: UIViewController, UITextFieldDelegate, BaseViewModelDelegate, SignInViewDelegate, ArtistDetailDelegate {
    
    func productListView_BtnShareDidTap(sender: UIButton, object: AnyObject) {
        //
        if let item = object as? Song{
            self.shareSocialWithLink(str_link: item.linkShare)
        }else if let item = object as? Station{
            if let _objects = item.object as? Song{
                self.shareSocialWithLink(str_link: _objects.linkShare)
            }/*else if let _objects = item.object as? Artist{
             
             }*/
        }
    }
    
    func productListView_BtnLikeDidTap(sender: UIButton, object: AnyObject) {
        //
        self.baseViewModel_LikeObject(sender: sender, object: object)
    }
    
    func productListview_BtnMoreDidTap(sender: UIButton, object: AnyObject) {
        //
        var song: Song?
        
        if let item = object as? Song{
            song = item
        }else if let item = object as? Station{
            if let _objects = item.object as? Song{
                song = _objects
            }/*else if let _objects = item.object as? Artist{
             
             }*/
        }
        
        if song != nil {
            let customIndexPath = IndexPath.init(row: 0, section: 0)
            self.baseViewModel_moreBtnTapAtIndexPath(indexPath: customIndexPath, datas: [song!])
        }
        
    }
    
    func baseViewModel_moreBtnTapAtIndexPath(indexPath: IndexPath, datas: [AnyObject]?) {
        
        var songType: SongType = .Track
        
        if datas is [Mix]{
            songType = .Mix
        }
        
        if let items = datas as? [Song]{
            let itemViewModel = PlayerViewModel.init(title: AppViewTitle.more.rawValue)
            itemViewModel.songs = items
            itemViewModel.type = songType
            itemViewModel.setPlayingIndex(index: indexPath.row, isFromPlayerTracklisting: false)
            
            self.presentMoreViewController(vmodel: itemViewModel, itemSongSelected: nil)
        }
    }
    
    func baseViewModel_LikeObject(sender: UIButton?, object: AnyObject) {
        
        if sender == nil {
            return
        }
        
        var request = DataRequest(.Network)
        
        
        self.baseViewModel_showLoadingProgress()
        request.onSuccess = {(data,  moreInfo) in
            
            if let error = moreInfo as? NSError, error.code != 0{
                self.baseViewModel_showError(error: error)
            }else{
                
                var number = sender!.titleLabel!.text!.toInt(defaultNumber: -1)
                if number >= 0{
                    if sender!.isSelected{
                        
                        number -= 1
                        
                    }else{
                        number += 1
                    }
                    
                    sender?.setTitle("  " + String(number), for: .normal)
                }
                
                sender!.isSelected = !sender!.isSelected
                self.baseViewModel_hideLoadingProgress()
            }
            
            self.delegateArtistDetail?.artistDetail_didChangeFollowingStatus()
        }
        
        request.onFailure = {(error, moreInfo) in
            
            if error != nil {
                self.baseViewModel_showError(error: error!)
            }
        }
        
        DataProxy.user.activities(is_cancel_status: sender!.isSelected, object: object, dataRequest: request)
    }
    
    func baseViewModel_LikeObject(sender: UIButton?, object: AnyObject, callback:@escaping (_ isSuccess: Bool, _ error: NSError?)->()) {
        
        var request = DataRequest(.Network)
        
        
        self.baseViewModel_showLoadingProgress()
        request.onSuccess = {(data,  moreInfo) in
            
            if let error = moreInfo as? NSError, error.code != 0{
                self.baseViewModel_showError(error: error)
            }else{
                if sender != nil {
                    
                    var number = sender!.titleLabel!.text!.toInt(defaultNumber: -1)
                    if number >= 0{
                        if sender!.isSelected{
                            
                            number -= 1
                            
                        }else{
                            number += 1
                        }
                        
                        sender?.setTitle("  " + String(number), for: .normal)
                    }
                    
                    sender!.isSelected = !sender!.isSelected
                    self.baseViewModel_hideLoadingProgress()
                    
                }else{
                    self.baseViewModel_showMessages(str_messages: AppMixError.addToFavouriteSucess.rawValue)
                }
                
                self.delegateArtistDetail?.artistDetail_didChangeFollowingStatus()
            }
        }
        
        request.onFailure = {(error, moreInfo) in
            
            if error != nil {
                self.baseViewModel_showError(error: error!)
            }
        }
        
        DataProxy.user.activities(is_cancel_status: sender?.isSelected, object: object, dataRequest: request)
    }
    
    // only user this for profile favourite unlike
    func baseViewModel_UnLikeObject(object: AnyObject, callback:@escaping (_ isSuccess: Bool, _ error: NSError?)->()) {
        
        var request = DataRequest(.Network)
        
        self.baseViewModel_showLoadingProgress()
        request.onSuccess = {(data,  moreInfo) in
            
            if let error = moreInfo as? NSError, error.code != 0{
                self.baseViewModel_showError(error: error)
            }else{
                self.baseViewModel_hideLoadingProgress()
            }
        }
        
        request.onFailure = {(error, moreInfo) in
            
            if error != nil {
                self.baseViewModel_showError(error: error!)
            }
        }
        
        DataProxy.user.activities(is_cancel_status: true, object: object, dataRequest: request)
    }
    
    
    func baseViewModel_hideLoadingProgress() {
        //
        self.hideProgress()
    }
    
    func baseViewModel_showLoadingProgress() {
        //
        self.showLoadingProgress()
    }
    
    func baseViewModel_showMessages(str_messages: String){
        self.hideProgress()
        self.showMessages(messages: str_messages)
    }
    
    func baseViewModel_showError(error: NSError) {
        //
        
        if Thread.isMainThread{
            if error.code == ErrorCode.userInfoNotFound.rawValue{
                self.hideProgress()
                self.presentSignInViewController()
            }else{
                self.hideProgress()
                self.showMessages(messages: error.localizedDescription)
            }
        }else{
            DispatchQueue.main.async() { () -> Void in
                if error.code == ErrorCode.userInfoNotFound.rawValue{
                    self.hideProgress()
                    self.presentSignInViewController()
                }else{
                    self.hideProgress()
                    self.showMessages(messages: error.localizedDescription)
                }
            }
        }
        
    }
    
    
    // Delegate
    var delegateKeyBoard: baseViewKeyBoardDelegate?
    var delegateArtistDetail: ArtistDetailDelegate?
    
    var keyboardSize = CGSize.init(width: screenWidth, height: 256.0)
    var keyboardAnimationDuration: TimeInterval = 0
    
    var colorStatusBar: UIColor = .clear
    
    // Base Model
    var layout = BaseLayoutConfig();
    
    var searchBar: UITextField?
    
    var isNavigationBarHidden: Bool{
        return false
    }
    
    var isStatusBarTransparent: Bool{
        return false
    }
    
    var isNavigationBarHiddenWhenSwipe: Bool{
        return true
    }
    
    var isLockOrientation: Bool{
        return true
    }
    
    var scrollLastOffset: CGPoint = CGPoint.init(x: 0, y: 0)
    
    override var shouldAutorotate: Bool{
        return false
    }
    
    // avoid suggest search request extremply fast
    private var numberRequestSuggestCalling = 0
    private var numberTextFieldDelegateCalling = 0
    
    //property to change navigation bar UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lockOrientation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.rotateNotificationHandle), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    func rotateNotificationHandle(){
        if !SharePlayer.isVideoScaling{
            return
        }
        
        self.rotated()
    }
    
    
    @discardableResult func rotated() -> Bool {
        return self.lockOrientation()
    }
    
    @discardableResult func lockOrientation() -> Bool{
        if self.isLockOrientation{
            AppUtility.lockOrientation(.portrait)
            return true
        }else{
            AppUtility.lockOrientation(.all)
            return false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupNavigationBar()
        self.hideNavigationBar()
        self.hideNavigationBarBottomLine()
        self.notificationCenterKeyBoardInit()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AppUtility.lockOrientation(.portrait)
        self.notificationCenterRemove()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        debugPrint("deinit \(type(of: self))")
    }
    
    /*******************************************************************************/
    //MARK: Navigation bar
    /*******************************************************************************/
    // add keyboard notification Observer
    func notificationCenterKeyBoardInit(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(nofitication:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(nofitication:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func notificationCenterRemove(){
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func keyboardWillShow(nofitication: NSNotification){
        
        // keyboard frame is in window coordinates
        if let kbSize = nofitication.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue{
            self.keyboardSize = kbSize.cgRectValue.size
            
            // keyboard animation duration
            let info = nofitication.userInfo!
            let value = info[UIKeyboardAnimationDurationUserInfoKey]
            (value as AnyObject).getValue(&keyboardAnimationDuration)
            
            //
            delegateKeyBoard?.keyboardDelegate_WillShowNotification(notification: nofitication)
        }
    }
    
    func scrollWithKeyboardShow(scrollView: UIScrollView, viewSelected: UIView?){
        
        if viewSelected == nil {
            return
        }
        
        scrollView.isScrollEnabled = true
        var contentInset = scrollView.contentInset
        contentInset.bottom = keyboardSize.height + 60.0
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
        
        // if active text filed is hidden by keyboard, scroll it so its visible
        // your app might not need or want this behavior
        
        var aRect = self.view.frame
        aRect.size.height -= (keyboardSize.height)
        
        if(!aRect.contains((viewSelected!.frame.origin))){
            scrollView.scrollRectToVisible((viewSelected!.frame),animated: true)
        }
    }
    
    func keyboardWillHide(nofitication: NSNotification){
        
        delegateKeyBoard?.keyboardDelegate_WillHideNotification(notification: nofitication)
    }
    
    func scrollBack(scrollView: UIScrollView){
        
        var contentInset = scrollView.contentInset
        contentInset.bottom -= (keyboardSize.height + 60.0)
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
        
        //        scrollView.setContentOffset(CGPointMake(0.0,-scrollView.contentInset.top), animated: true)
    }
    
    
    
    /*******************************************************************************/
    //MARK: Status Bar
    /*******************************************************************************/
    func setStatusBarBackgroundColor(color: UIColor) {
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        
        statusBar.backgroundColor = color
    }
    
    func getStatusBarBackgroundColor() -> UIColor?{
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return  nil}
        
        return statusBar.backgroundColor
    }
    
    func getStatusBarHiddenStatus() -> Bool{
        return UIApplication.shared.isStatusBarHidden
    }
    
    func setStatusBarHidden(hidden: Bool){
        UIApplication.shared.isStatusBarHidden = hidden
    }
    
    /*******************************************************************************/
    //MARK: BaseView Init
    /*******************************************************************************/
    //    internal func backgroundColor() -> (AppColor, alpha: CGFloat){
    //        return (AppColor.backgroundColor, 1.0)
    //    }
    
    /*******************************************************************************/
    //MARK: Navigation bar
    /*******************************************************************************/
    
    // Override this func to set Title for View
    internal func setViewTitle() -> String {
        return self.title ?? layout.navigation_config.title;
    }
    
    // Override this func to set setAppBaseNavigationStyle
    internal func setNavigationBarStyle() -> NavigationBarStyle {
        return NavigationBarStyle.Default;
    }
    
    // Override this func to set navigation background
    internal func navigation_background() -> UIColor {
        return layout.navigation_config.backgroundColor
    }
    
    // Override this func to set title Color
    internal func setNavigationTitleColor() -> UIColor {
        return UIColor.white
    }
    
    // Override this func to hide navigation bar bottom line
    internal func isNavigationBarLineBottomHidden() -> Bool {
        return false
    }
    
    private func setupNavigationBar(){
        
        // setup background
        self.setNavigationBarBackground()
        
        switch self.setNavigationBarStyle() {
        case .Default:
            // setup title
            self.setNavigationBarTitle()
            break
            
        case .Transparent:
            self.setNavigationBarTitle()
            
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.view.backgroundColor = .clear
            break
            
        case .Playlist:
            // setup title
            self.setNavigationBarTitle()
            self.navigationItem.leftBarButtonItem = self.leftBarDismissButton()
            self.navigationController?.navigationBar.tintColor = UIColor.clear
            break
            
        case .Search:
            self.setNavigationWithSearchBar()
            break
        }
        
        
        
        if self.navigationController?.viewControllers.count ?? 0 > 1 {
            // this is a Back button
            self.navigationItem.leftBarButtonItem = self.leftBarBackButton();
            self.navigationController?.navigationBar.tintColor = UIColor.clear
        }
        
        if self.isNavigationBarHidden{
            self.setStatusBarBackgroundColor(color: .black)
        }else{
            self.setStatusBarBackgroundColor(color: .clear)
        }
    }
    
    func navigationBarHideOnSwipe(scrollView: UIScrollView){
        
        DispatchQueue.global(qos: .background).sync {
            
            if self.navigationController != nil{
                
                if  self.setNavigationBarStyle() == .Transparent{
                    
                    let offset = scrollView.contentOffset.y
                    let alpha = offset/bannerSize.height
                    
                    DispatchQueue.main.async() { () -> Void in
                        self.setStatusBarBackgroundColor(color: UIColor.black.withAlphaComponent(alpha))
                        if alpha < 1 {
                            if self.navigationController!.isNavigationBarHidden{
                                self.navigationController?.setNavigationBarHidden(false, animated: true)
                            }
                        }else{
                            if !self.navigationController!.isNavigationBarHidden{
                                self.navigationController?.setNavigationBarHidden(true, animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func updateLastScrollingOffset(scrollView: UIScrollView){
        self.scrollLastOffset = scrollView.contentOffset
    }
    
    private func hideNavigationBar(){
        self.navigationController?.setNavigationBarHidden(self.isNavigationBarHidden, animated: true)
    }
    
    private func hideNavigationBarBottomLine(){
        self.navigationController?.navigationBar.shadowImage = self.isNavigationBarLineBottomHidden() ? UIImage() : self.navigationController?.navigationBar.shadowImage
    }
    
    private func setNavigationWithSearchBar(){
        if self.navigationController == nil { return; }
        
        
        if(searchBar != nil) { return; }
        
        // init search Bar
        
        searchBar = UITextField(frame: CGRect.init(x: 10, y: 0, width: screenWidth - 20, height: layout.searchBarHeight))
        
        searchBar?.delegate = self
        searchBar?.font = layout.searchTextFont
        searchBar?.textColor = layout.searchTextColor
        searchBar?.backgroundColor = layout.searchBarBackgroundColor
        self.searchBarPlaceHolder(title: layout.searchBarTextDefaut)
        
        searchBar?.tintColor = UIColor.white
        searchBar?.modifyClearButton(with: UIImage.init(named: Image_Name.ic_clearText.rawValue)!)
        //        searchBar?.layer.borderColor = UIColor.clearColor().CGColor
        searchBar?.clearButtonMode = .whileEditing
        searchBar?.returnKeyType = .search
        searchBar?.borderWithRadius(5.0)
        
        let heightSearchBar = self.searchBar?.frame.height ?? layout.searchBarHeight
        
        let view = UIView.init(frame: CGRect.init(x: 4, y: 0, width: 28.0, height: heightSearchBar))
        let paddingView = UIImageView.init(frame:CGRect.init(x: 8, y: (heightSearchBar - 13.0)/2, width: 13.0, height: 13.0) )
        paddingView.image = UIImage(named: Image_Name.ic_search.rawValue)
        paddingView.contentMode = .center
        
        view.addSubview(paddingView)
        
        searchBar?.leftView = view
        searchBar?.leftViewMode = .always
        
        self.navigationItem.titleView = searchBar;
    }
    
    ///Set placeHolder for SearchBar
    private func searchBarPlaceHolder(title: String) {
        if(searchBar == nil) { return; }
        searchBar?.attributedPlaceholder = NSAttributedString(string: title,
                                                              attributes:[NSForegroundColorAttributeName: layout.searchBarPlaceholderTextColor])
    }
    
    private func setNavigationBarBackground(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(color: self.navigation_background()), for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.subviews.first?.alpha = 1
    }
    
    private func setNavigationBarTitle(){
        if let lable = self.navigationItem.titleView  as? UILabel {
            lable.text = self.setViewTitle()
        } else {
            self.navigationItem.titleView = self.makeNavigationTitle();
        }
    }
    
    private func makeNavigationTitle() -> UILabel {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        label.textAlignment = .center
        
        let str_title = self.setViewTitle()
        
        if str_title.contains("playlist"){
            label.text = str_title
        }else{
            label.text = str_title.capitalized
        }
        
        label.font = layout.navigation_config.fontTitle
        label.textColor = self.setNavigationTitleColor()
        return label
    }
    
    
    //------------------------
    //MARK: Search Bar
    //click return keyboard => hiden keyboard
    
    func searchBarHideKeyboard(){
        self.searchBar?.resignFirstResponder()
    }
    
    func setSearchBarWith(keyword: String){
        self.searchBar?.text = keyword
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.searchBar != textField {
            self.searchBar?.resignFirstResponder()
            return true
        }
        
        let stringSearch = textField.text ?? ""
        
        if stringSearch.length > 0 {
            self.searchBar?.resignFirstResponder()
            //fixme
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if self.searchBar == textField{
            self.textFieldDidClearKeyword(keyword: "")
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if self.searchBar != textField { return; }
        
        self.navigationItem.rightBarButtonItem = self.rightBarButtonCancel();
        
        let stringSearch = self.searchBar?.text ?? ""
        
        if !(stringSearch.length > 0) {
            //fixme
        }else{
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if self.searchBar != textField { return true; }
        
        
        // avoid suggest search request extremply fast
        numberTextFieldDelegateCalling += 1
        self.perform(#selector(BaseViewController.requestSuggestSearch), with: nil, afterDelay: 0.5)
        //
        
        return true
    }
    
    func requestSuggestSearch(){
        
        numberRequestSuggestCalling += 1
        
        if self.numberRequestSuggestCalling == self.numberTextFieldDelegateCalling{
            let stringSearch = self.searchBar?.text ?? ""
            
            if !(stringSearch.length > 0) {
                //fixme
            } else {
                self.textFieldDidSearchWithKeyword(keyword: stringSearch)
            }
        }
    }
    
    // override this method to handle request search
    
    func textFieldDidSearchWithKeyword(keyword: String){
        print("override textFieldDidSearchWithKeyword method to handle request search")
    }
    
    func textFieldDidClearKeyword(keyword: String){
        print("override textFieldDidClearKeyword method to handle request search")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //
        print("override textFieldDidEndEditing method to enter search result value")
        textField.endEditing(true)
    }
    
    //------------------------
    // MARK: Right button setup
    func rightBarButtonCancel() -> UIBarButtonItem{
        return self.createButtonBar(title: "Cancel", iconName: nil, action: #selector(self.rightBarCancelTap(sender:)), width: self.layout.textOfBarButtonWidth, isleftBarItem: false)
    }
    
    func rightBarCancelTap(sender: AnyObject){
        self.hidenRightButton()
        
        self.searchBar?.text = ""
        self.textFieldDidClearKeyword(keyword: self.searchBar?.text ?? "")
        self.searchBar?.endEditing(true)
    }
    
    //------------------------
    // MARK: Left button setup
    
    /// A ButtonBar Back
    func leftBarBackButton() -> UIBarButtonItem {
        let iconName = "ic_back"
        return self.createButtonBar(title: "",
                                    iconName: iconName,
                                    action: #selector(self.touchBack),
                                    width: self.layout.leftBarIconWidth, isleftBarItem: true)
    }
    
    func leftBarDismissButton() -> UIBarButtonItem {
        let iconName = "ic_dissmiss"
        return self.createButtonBar(title: "",
                                    iconName: iconName,
                                    action: #selector(self.leftBarDismissTap),
                                    width: self.layout.leftBarIconWidth, isleftBarItem: true)
    }
    
    ///No Button
    func barButtonZero() -> UIBarButtonItem {
        self.navigationItem.hidesBackButton = true;
        let itemButton: UIBarButtonItem = UIBarButtonItem()
        return itemButton
    }
    
    func hidenRightButton() {
        // hiden leftBarbutton
        self.navigationItem.rightBarButtonItem = self.barButtonZero()
        //        self.searchBarResetFrame();
    }
    
    func touchBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func leftBarDismissTap(){
        self.dismissViewWithStatusBar(isHidden: false)
    }
    
    ///Create a button bar for navigation
    func createButtonBar(title: String?, iconName: String?, action: Selector, width: CGFloat, isleftBarItem: Bool) -> UIBarButtonItem {
        let itemButton: UIBarButtonItem = self.createButtonBar(title: title, iconName: iconName, textcolor: UIColor.white, action: action, width: width, height: 25, isleftBarItem: isleftBarItem)
        return itemButton
    }
    
    func createButtonBar(title: String?, iconName: String?,textcolor: UIColor ,action: Selector, width: CGFloat, isleftBarItem: Bool) -> UIBarButtonItem {
        let itemButton: UIBarButtonItem = self.createButtonBar(title: title, iconName: iconName, textcolor: textcolor, action: action, width: width, height: 25, isleftBarItem: isleftBarItem)
        return itemButton
    }
    
    func createButtonBar(title: String?, iconName: String?,textcolor: UIColor ,action: Selector, width: CGFloat, height: CGFloat, isleftBarItem: Bool) -> UIBarButtonItem {
        
        let image = UIImage(named: iconName ?? "")
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        button.setImage(image, for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        
        if !isleftBarItem{
            button.imageEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 0)
            button.titleEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 0)
        }else{
            button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 6)
            button.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 6)
        }
        
        button.setTitle(title ?? "", for: .normal)
        button.addTarget(self, action: action, for: .touchUpInside)
        button.titleLabel?.font = UIFont.fontRegular_big()
        button.setTitleColor(textcolor, for: .normal)
        
        let itemButton: UIBarButtonItem = UIBarButtonItem(customView: button)
        
        return itemButton
    }
    
    //MARK: - Rotate
    
    
    //MARK: - SignInView Delegate
    func SignInView_willDissmissView(logginSuccess status: Bool) {
        print("override SignInView_willDissmissView method to handle signIn View dismiss")
    }
    
    func SignInView_didDissmissView(logginSuccess status: Bool) {
        print("override SignInView_didDissmissView method to handle signIn View dismiss")
    }
    
    
    //MARK: - BaseViewModel Delegate
    func baseViewModel_titleCollectionHeader_DidTapBtnMore(entry: FeatureEntries){
        switch entry.type {
            
        case .mixset:
            let info = ProductListInfo.init(_proxy: .feature, _type: .mixset, filter: nil, _data: nil, _paggingTypes: nil, pullRefresh: true)
            let vmodel = BaseViewModel.init(title: entry.title)
            self.pushToProductListViewController(info: info,vmodel: vmodel)
            break
            
        case .track:
            let info = ProductListInfo.init(_proxy: .feature, _type: .track, filter: nil, _data: nil, _paggingTypes: nil, pullRefresh: true)
            let vmodel = BaseViewModel.init(title: entry.title)
            self.pushToProductListViewController(info: info,vmodel: vmodel)
            break
            
        case .chart:
            let info = ProductListInfo.init(_proxy: .chart, _type: .mixset, filter: nil, _data: nil, _paggingTypes: [.mixset, .track, .video], pullRefresh: false)
            self.pushToChartListViewController(info: info)
            break
            
        case .video:
            let info = ProductListInfo.init(_proxy: .feature, _type: .video, filter: nil, _data: nil, _paggingTypes: nil, pullRefresh: true)
            let vmodel = BaseViewModel.init(title: entry.title)
            self.pushToProductListViewController(info: info,vmodel: vmodel)
            break
            
        case .deejay:
            let info = ProductListInfo.init(_proxy: .artist, _type: .deejay_vertical, filter: nil, _data: nil, _paggingTypes: nil, pullRefresh: true)
            self.pushToArtistListViewController(info: info)
            
        default:
            break
        }
        
    }
    
    func baseViewModel_didChangeDataSource(datas: AnyObject, playingIndex: Int, type: SongType) {
        if let videos = datas as? [Video]{
            self.presentVideoViewController(_videos: videos, playingIndex: playingIndex)
            
        }else if let songs = datas as? [Song]{
            self.presentToPlayerViewController(_songs: songs, playingIndex: playingIndex, type: type, shuffleEnable: false, isFromViewTracklisting: true)
            
        }else if let artist = datas as? [Artist], artist.count > playingIndex{
            self.pushToArtistDetailViewController(artist_info: artist[playingIndex])
        }
    }
    
    
    func baseViewModel_didSelectedKeyword(keyword: String) {
        self.setSearchBarWith(keyword: keyword)
        self.textFieldDidSearchWithKeyword(keyword: keyword)
    }
    
    //MARK: - BaseController Delegate
    func artistDetail_didChangeFollowingStatus() {
        print("override this function to handle your code")
    }
}

/*******************************************************************************/
//MARK:- Social
/*******************************************************************************/
extension BaseViewController{
    func shareSocialWithLink(str_link: String){
        // Check if Twitter is available
        //        if(SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)) {
        //            // Create the tweet
        //            let tweet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        //            tweet.setInitialText("I want to share this App: ")
        //            tweet.addImage(UIImage(named: "shareImage"))
        //            self.presentViewController(tweet, animated: true, completion: nil)
        //        } else {
        //            // Twitter not available. Show a warning
        //            let alert = UIAlertController(title: "Twitter", message: "Twitter not available", preferredStyle: UIAlertControllerStyle.Alert)
        //            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        //            self.presentViewController(alert, animated: true, completion: nil)
        //        }
        
        
        var itemToShare: [Any] = []
        
        if !(str_link.length > 0){
            return
        }
        
        itemToShare.append(str_link)
        
        let activityVC = UIActivityViewController.init(activityItems: itemToShare, applicationActivities: nil)
        
        
        activityVC.excludedActivityTypes = [.print, .copyToPasteboard, .assignToContact, .saveToCameraRoll]
        
        activityVC.modalPresentationStyle = .popover
        
        self.present(activityVC, animated: true, completion: nil)
        
        activityVC.popoverPresentationController?.sourceView = self.view
    }
}

/*******************************************************************************/
//MARK: Tabbar
/*******************************************************************************/
extension BaseViewController{
    
    func tabbar_bringToFont(){
        if let tabbar = SharePlayer.tabbarController?.tabBar{
            SharePlayer.tabbarController?.view.bringSubview(toFront: tabbar)
        }
    }
    
    func tabbar_sendToBack(){
        if let tabbar = SharePlayer.tabbarController?.tabBar{
            SharePlayer.tabbarController?.view.sendSubview(toBack: tabbar)
        }
    }
    
}

/*******************************************************************************/
//MARK: Rotate
/*******************************************************************************/
extension BaseViewController{
    
    func rotateWithLandScape(){
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    func rotateWithPortrait(){
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
}

/*******************************************************************************/
//MARK: Loading Progress
/*******************************************************************************/

class Loader: NSObject {
    
    class func show(message:String, delegate: UIViewController) {
        
        var hud : MBProgressHUD = MBProgressHUD()
        
        /*
         Changes "contentColor" if you want to change the spinner's color
         Change"bezelView.color" if you wan to change the background color of the hub. You need to set "bezelView.style" as MBProgressHUDBackgroundStyleSolidColor to remove the blur effect.
         */
        hud = MBProgressHUD.showAdded(to: delegate.view, animated: true)
        
        hud.contentColor = UIColor.white
        hud.label.textColor = UIColor.white
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.font = UIFont.fontRegular_big()
        hud.label.text = "Loading..."
        hud.label.numberOfLines = 0
        hud.bezelView.color = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5) // Your backgroundcolor
        hud.bezelView.style = .solidColor // you should change the bezelview style to solid color.
        
        if message.length > 0 {
            hud.hide(animated: true, afterDelay: 1.5)
            hud.mode = MBProgressHUDMode.text
            hud.label.text = message
        }
        
        //        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
    }
    
    class func show(message:String, view: UIView){
        
        Loader.hide(view: view)
        var hud : MBProgressHUD = MBProgressHUD()
        
        /*
         Changes "contentColor" if you want to change the spinner's color
         Change"bezelView.color" if you wan to change the background color of the hub. You need to set "bezelView.style" as MBProgressHUDBackgroundStyleSolidColor to remove the blur effect.
         */
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        
        hud.contentColor = UIColor.white
        hud.label.textColor = UIColor.white
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.font = UIFont.fontRegular_big()
        hud.label.text = "Loading..."
        hud.label.numberOfLines = 0
        hud.bezelView.color = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.7) // Your backgroundcolor
        hud.bezelView.style = .solidColor // you should change the bezelview style to solid color.
        
        
        if message.length > 0 {
            hud.hide(animated: true, afterDelay: 2.0)
            hud.mode = MBProgressHUDMode.text
            hud.label.text = message
        }
    }
    
    
    class func hide(delegate:UIViewController) {
        MBProgressHUD.hide(for: delegate.view, animated: true)
        //        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    class func hide(view:UIView) {
        MBProgressHUD.hide(for: view, animated: true)
        //        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension BaseViewController{
    /*
     /*
     @Method: showLoadingProgressFromView
     @Description: show loading progress with view you want to make a loading progress (it only once progress pertime, if you have 2 view and only the last view you call can working)
     @Input: view : a view you wanna make a loading progres
     _maskTyle : it a view below progress loading to block user active with controller
     */
     func showLoadingProgressFromView(view : UIView, _maskStyle: maskStyle){
     LoadingProgress.showFromView(view, _maskStyle: _maskStyle)
     }
     
     /*
     @Method: showLoadingProgress
     @Description: show loading progress with default view (main view)
     @Input: _maskTyle : it a view below progress loading to block user active with controller
     */
     func showLoadingProgress(_maskStyle: maskStyle){
     LoadingProgress.showFromView(self.view, _maskStyle: _maskStyle)
     }
     
     /*
     @Method: hideLoadingProgress
     @Description: hide all loading progress
     */
     func hideLoadingProgress(){
     LoadingProgress.dissmiss()
     }
     
     func showErrorMessages(messages: String){
     self.hideLoadingProgress()
     }
     
     */
    
    func showLoadingProgress(){
        Loader.show(message: "", delegate: self)
    }
    
    func showMessages(messages: String){
        Loader.show(message: messages, delegate: self)
    }
    
    func hideProgress(){
        Loader.hide(delegate: self)
    }
}

/*******************************************************************************/
//MARK: Presenter
/*******************************************************************************/
extension BaseViewController{
    
    ///************************
    // SignIn
    ///************************
    @discardableResult func presentSignInViewController() -> SignInViewController?{
        if let vc = StoryBoardManager.SignInController() as? SignInViewController{
            vc.delegate = self
            
            if self.navigationController != nil {
                self.navigationController?.present(vc, animated: true, completion: {
                    //
                })
            }else{
                self.present(vc, animated: true, completion: {
                    //
                })
            }
            
            return vc
        }
        return nil
    }
    
    ///************************
    // ProductList
    ///************************
    func pushToProductListViewController(info: ProductListInfo, vmodel: BaseViewModel?){
        if let vc = StoryBoardManager.ProductListViewController() as? ProductListViewController{
            vc.productlist_info = info
            if vmodel != nil {
                vc.vmodel = vmodel!
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    ///************************
    // Artist
    ///************************
    func pushToArtistListViewController(info: ProductListInfo){
        if let vc = StoryBoardManager.ArtistListingController() as? ArtistListingController{
            vc.productlist_info = info
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func pushToArtistDetailViewController(artist_info: Artist){
        if let vc = StoryBoardManager.ArtistDetailController() as? ArtistDetailController{
            
            vc.delegateArtistDetail = self
            let artistVmodel = ArtistViewModel.init(title: AppViewTitle.artist.rawValue)
            artistVmodel.filterBase.requestObject = artist_info
            let info = ProductListInfo.init(_proxy: .artistDetail, _type: .unknow, filter: artistVmodel.filterBase, _data: nil, _paggingTypes: nil, pullRefresh: false)
            
            vc.productlist_info = info
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func presentToArtistDetailViewController(artist_info: Artist){
        if let vc = StoryBoardManager.ArtistDetailController() as? ArtistDetailController{
            
            let artistVmodel = ArtistViewModel.init(title: AppViewTitle.artist.rawValue)
            artistVmodel.filterBase.requestObject = artist_info
            let info = ProductListInfo.init(_proxy: .artistDetail, _type: .unknow, filter: artistVmodel.filterBase, _data: nil, _paggingTypes: nil, pullRefresh: false)
            
            vc.productlist_info = info
            
            self.present(vc, animated: true, completion: {
                //
            })
        }
    }
    
    
    ///************************
    // Profile Download Song
    ///************************
    func pushToProfileSongDownloadController(){
        if let vc = StoryBoardManager.ProfileSongDownloadController() as? ProfileSongDownloadController{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    ///************************
    // ChartList
    ///************************
    func pushToChartListViewController(info: ProductListInfo){
        if let vc = StoryBoardManager.ChartListViewController() as? ChartListViewController{
            vc.vmodel.productlist_info = info
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    ///************************
    // User
    ///************************
    func pushToProfileFavouriteController(){
        if let vc = StoryBoardManager.ProfileFavouriteController() as? ProfileFavouriteController{
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    ///************************
    // More Controller
    ///************************
    func presentMoreViewController(vmodel: PlayerViewModel, itemSongSelected: Song?){
        
        if let vc = SharePlayer.playerMoreController{
            if itemSongSelected != nil {
                vc.selectedSong = itemSongSelected
            }
            vc.vmodel = vmodel
            
        }else{
            if let controller = StoryBoardManager.MoreController() as? MoreViewController{
                
                if itemSongSelected != nil {
                    controller.selectedSong = itemSongSelected
                }
                controller.vmodel = vmodel
                SharePlayer.playerMoreController = controller
            }
        }
        
        self.showMoreController()
    }
    
    func showMoreController(){
        if let tabbar = self.tabBarController as? BaseTabbarController{
            tabbar.showMoreController()
        }
    }
    
    func hideMoreController(){
        if let tabbar = self.tabBarController as? BaseTabbarController{
            tabbar.hideMoreController()
        }
    }
    
    ///************************
    // Player View Controller
    ///************************
    func presentToPlayerViewController(_songs: [Song], playingIndex: Int, type: SongType, shuffleEnable: Bool, isFromViewTracklisting: Bool = false){
        // shuffleEnable is using for user tap btn shuffle to play all song
        
        if let vc = SharePlayer.playerController{
            vc.vmodel.type = type
            if shuffleEnable{
                vc.isEnableShuffleMode(enable: shuffleEnable)
            }
            
            // handle songs_masup from mix
            if _songs is [Song_Masup]{
                if _songs.count > playingIndex{
                    let songsTmp = _songs[playingIndex]
                    vc.vmodel.songs = [songsTmp]
                }
                
            }else{
                if _songs is [Mix]{
                    vc.vmodel.songs = [_songs[playingIndex]]
                    vc.vmodel.setPlayingIndex(index: 0, isFromPlayerTracklisting: isFromViewTracklisting)
                }else{
                    vc.vmodel.songs = _songs
                    vc.vmodel.setPlayingIndex(index: playingIndex, isFromPlayerTracklisting: isFromViewTracklisting)
                }
            }
            
            
        }else{
            if let vc = StoryBoardManager.PlayerViewController() as? PlayerViewController{
                vc.vmodel.type = type
                if shuffleEnable{
                    vc.isEnableShuffleMode(enable: shuffleEnable)
                }
                
                // handle songs_masup from mix
                if _songs is [Song_Masup]{
                    if _songs.count > playingIndex{
                        let songsTmp = _songs[playingIndex]
                        vc.vmodel.songs = [songsTmp]
                    }
                    
                }else{
                    if _songs is [Mix]{
                        vc.vmodel.songs = [_songs[playingIndex]]
                        vc.vmodel.setPlayingIndex(index: 0, isFromPlayerTracklisting: isFromViewTracklisting)
                    }else{
                        vc.vmodel.songs = _songs
                        vc.vmodel.setPlayingIndex(index: playingIndex, isFromPlayerTracklisting: isFromViewTracklisting)
                    }
                }
                
                SharePlayer.playerController = vc
            }
        }
        
        self.showPlayerViewController(isFromListing: isFromViewTracklisting)
    }
    
    func showPlayerViewController(isFromListing: Bool){
        if let tabbar = self.tabBarController as? BaseTabbarController{
            tabbar.showPlayerViewController(isFromListing: isFromListing)
        }
    }
    
    func hidePlayerViewController(){
        if let tabbar = self.tabBarController as? BaseTabbarController{
            tabbar.hidePlayerViewController()
        }
    }
    
    ///************************
    // Video View Controller
    ///************************
    func presentVideoViewController(_videos: [Video], playingIndex: Int){
        if let vc = SharePlayer.videoPlayerController{
            vc.vmodel.videos = _videos
            vc.vmodel.setPlayingIndex(index: playingIndex)
            
        }else{
            if let controller = StoryBoardManager.VieoViewController() as? VideoViewController{
                controller.vmodel.videos = _videos
                controller.vmodel.setPlayingIndex(index: playingIndex)
                SharePlayer.videoPlayerController = controller
            }
        }
        
        self.showVideoViewController()
    }
    
    func showVideoViewController(){
        if let tabbar = self.tabBarController as? BaseTabbarController{
            tabbar.showVideoViewController()
        }
    }
    
    func hideVideoViewController(){
        if let tabbar = self.tabBarController as? BaseTabbarController{
            tabbar.hideVideoViewController()
        }
    }
    
    ///************************
    // Tracklisting
    ///************************
    func pushToPlayerViewTracklisting(vmodel: PlayerViewModel){
        if let vc = SharePlayer.playerTracklist{
            vc.vmodel = vmodel
        }else{
            if let controller = StoryBoardManager.PlayerTracklistingController() as? PlayerViewTrackListing{
                controller.vmodel = vmodel
                SharePlayer.playerTracklist = controller
            }
        }
        
        self.showPlayerViewTrackListingController()
    }
    
    ///************************
    // Playlist
    ///************************
    func presentToPlaylistViewController(itemSong: [Song]?){
        if let controller = StoryBoardManager.PlaylistController() as? PlaylistViewController{
            
            controller.vmodel.songs = itemSong
            
            let nav = UINavigationController.init(rootViewController: controller)
            if let tabbar = self.tabBarController as? BaseTabbarController{
                UIApplication.shared.isStatusBarHidden = false
                tabbar.present(nav, animated: true, completion: {
                    //
                })
            }
        }
    }
    
    func pushToPlaylistViewController(itemSong: [Song]?){
        if let controller = StoryBoardManager.PlaylistController() as? PlaylistViewController{
            
            controller.vmodel.songs = itemSong
            
            self.navigationController?.pushViewController(controller, animated: true)
            
            //            let nav = UINavigationController.init(rootViewController: controller)
            //            if let tabbar = self.tabBarController as? BaseTabbarController{
            //                UIApplication.shared.isStatusBarHidden = false
            //                tabbar.present(nav, animated: true, completion: {
            //                    //
            //                })
            //            }
        }
    }
    
    func pushToPlaylistDetailViewController(info: ProductListInfo, vmodel: PlaylistViewModel){
        if let controller = StoryBoardManager.PlaylistDetailViewController() as? PlaylistDetailController{
            controller.productlist_info = info
            controller.vmodel = vmodel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    func dismissViewWithStatusBar(isHidden: Bool){
        self.dismiss(animated: true) {
            UIApplication.shared.isStatusBarHidden = isHidden
        }
    }
    
    ///************************
    // Player Info Controller
    ///************************
    func presentInfoController(vmodel: PlayerViewModel){
        if let vc = SharePlayer.playerInfoController{
            vc.vmodel = vmodel
            
        }else{
            if let controller = StoryBoardManager.PlayerSongInfoController() as? PlayerSongInfoController{
                
                controller.vmodel = vmodel
                SharePlayer.playerInfoController = controller
            }
        }
        
        self.showPlayerInfoController()
    }
    
    func showPlayerInfoController(){
        self.showBlurViewTrackList {
            if let controller = SharePlayer.playerInfoController{
                
                if !controller.vmodel.isExistsPlayerSongInfo{
                    self.addChildViewController(controller)
                    controller.view.frame = CGRect.init(x: 0, y: self.view.frame.size.height, width: screenWidth, height: screenHeight)
                    self.view.addSubview(controller.view)
                    controller.didMove(toParentViewController: self)
                }
                
                
                self.view.bringSubview(toFront: controller.view)
                let visibleRect = CGRect.init(x: 0, y: self.view.frame.size.height - screenHeight, width: screenWidth, height: screenHeight)
                
                UIView.animate(withDuration: 0.225, animations: {
                    controller.view.frame = visibleRect
                    
                }, completion: { (finish) in
                    if finish{
                        controller.vmodel.isExistsPlayerSongInfo = true
                    }
                })
            }
        }
    }
    
    func hidePlayerInfoController(){
        
        DispatchQueue.global(qos: .background).sync {
            if let controller = SharePlayer.playerInfoController{
                
                let parentFrame = controller.parent
                let frame =  CGRect.init(x: 0, y: parentFrame?.view.frame.size.height ?? screenHeight, width: screenWidth, height: screenHeight)
                
                UIView.animate(withDuration: 0.225, animations: {
                    controller.view.frame = frame
                }, completion: { (finish) in
                    if finish{
                        self.hideBlurViewTrackList()
                    }
                })
            }
        }
    }
    
    
    
    // blur view
    func showBlurViewTrackList(complete:@escaping () -> Void){
        
        var blurView:  UIVisualEffectView?
        if let _blurView = SharePlayer.blurViewTrackListing{
            blurView = _blurView
            
        }else{
            let blurEffect = UIBlurEffect(style: .dark)
            blurView = UIVisualEffectView(effect: blurEffect)
            blurView!.frame = self.view.bounds
            blurView!.alpha = 0
            view.addSubview(blurView!)
            view.bringSubview(toFront: blurView!)
            
            SharePlayer.blurViewTrackListing = blurView
        }
        
        
        self.view.bringSubview(toFront: blurView!)
        
        UIView.animate(withDuration: 0.225, animations: {
            blurView?.alpha = 1
        }, completion: { (finish) in
            if finish{
                complete()
            }
        })
    }
    
    func hideBlurViewTrackList(){
        if let blurView = SharePlayer.blurViewTrackListing{
            UIView.animate(withDuration: 0.225, animations: {
                blurView.alpha = 0
            }, completion: { (finish) in
            })
        }
    }
    
    ///************************
    // Player Tracklist Controller
    ///************************
    func showPlayerViewTrackListingController(){
        
        self.showBlurViewTrackList {
            
            var color = UIColor.clear
            if let _color = self.getStatusBarBackgroundColor(){
                color = _color
            }
            if let controller = SharePlayer.playerTracklist{
                
                if !controller.vmodel.isExistsPlayerTracklisting{
                    self.addChildViewController(controller)
                    controller.view.frame = CGRect.init(x: screenWidth, y: playerControlHeight, width: screenWidth, height: screenHeight)
                    self.view.addSubview(controller.view)
                    controller.didMove(toParentViewController: self)
                }
                
                self.view.bringSubview(toFront: controller.view)
                let visibleRect = CGRect.init(x: 0, y: playerControlHeight, width: screenWidth, height: screenHeight)
                
                UIView.animate(withDuration: 0.225, animations: {
                    controller.view.frame = visibleRect
                }, completion: { (finish) in
                    if finish{
                        controller.colorStatusBar = color
                        self.setStatusBarBackgroundColor(color: .clear)
                        UIApplication.shared.isStatusBarHidden = false
                        controller.vmodel.isExistsPlayerTracklisting = true
                        controller.scrollToCurrentPlayingCell()
                    }
                })
            }
        }
    }
    
    func hidePlayerViewTrackListingController(){
        
        DispatchQueue.global(qos: .background).sync {
            if let controller = SharePlayer.playerTracklist{
                let frame = CGRect.init(x: screenWidth, y: playerControlHeight, width: screenWidth, height: screenHeight)
                
                UIView.animate(withDuration: 0.225, animations: {
                    controller.view.frame = frame
                }, completion: { (finish) in
                    if finish{
                        self.setStatusBarBackgroundColor(color: controller.colorStatusBar)
                        self.hideBlurViewTrackList()
                        UIApplication.shared.isStatusBarHidden = true
                    }
                })
            }
        }
    }
    
    ///************************
    // Search Result Controller
    ///************************
    func pushToSearchResultController(search_vmodel: SearchViewModel){
        if let controller = StoryBoardManager.SearchResultController() as? SearchResultViewController{
            controller.vmodel = search_vmodel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
