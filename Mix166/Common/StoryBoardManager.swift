//
//  StoryBoardManager.swift
//  Karaoke_Plus_Remote
//
//  Created by ttiamap on 5/9/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import UIKit

class StoryBoardManager{
    
//    class func SignInController() -> UIViewController?{
//        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "SignInController") as? SignInController{
//            
//            vc.modalTransitionStyle = .crossDissolve
//            return vc
//        }
//        return nil
//    }
//
    class func ProductListViewController() ->  UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "ProductListViewController") as? ProductListViewController{
            
//            vc.modalTransitionStyle = .crossDissolve
            return vc
        }
        return nil
    }
    
    class func ArtistListingController() ->  UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "ArtistListingController") as? ArtistListingController{
            
            return vc
        }
        return nil
    }
    
    class func ArtistDetailController() ->  UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "ArtistDetailController") as? ArtistDetailController{
            return vc
        }
        return nil
    }
    
    class func ProfileFavouriteController() ->  UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "ProfileFavouriteController") as? ProfileFavouriteController{
            return vc
        }
        return nil
    }
    
    class func PlaylistDetailViewController() ->  UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "PlaylistDetailController") as? PlaylistDetailController{
            
            //            vc.modalTransitionStyle = .crossDissolve
            return vc
        }
        return nil
    }
    
    class func PlayerViewController() ->  UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "PlayerViewController") as? PlayerViewController{
            
            vc.modalTransitionStyle = .crossDissolve
            return vc
        }
        return nil
    }
    
    class func PlayerSongInfoController() ->  UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "PlayerSongInfoController") as? PlayerSongInfoController{

            return vc
        }
        return nil
    }
    
    class func PlayerTracklistingController() -> UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "PlayerViewTrackListing") as? PlayerViewTrackListing{
            
            vc.modalTransitionStyle = .coverVertical
            return vc
        }
        return nil
    }
    
    class func VieoViewController() -> UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "VideoViewController") as? VideoViewController{
            
            vc.modalTransitionStyle = .coverVertical
            return vc
        }
        return nil
    }
    
    class func PlaylistController() -> UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "PlaylistViewController") as? PlaylistViewController{

            return vc
        }
        return nil
    }
    
    class func MoreController() -> UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "MoreViewController") as? MoreViewController{
            
            vc.modalTransitionStyle = .crossDissolve
            return vc
        }
        return nil
    }
    
    class func ProfileSongDownloadController() -> UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "ProfileSongDownloadController") as? ProfileSongDownloadController{
            return vc
        }
        return nil
    }
    
    class func SearchResultController() -> UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "SearchResultViewController") as? SearchResultViewController{
            return vc
        }
        return nil
    }
    
    class func ChartListViewController() -> UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "ChartListViewController") as? ChartListViewController{
            return vc
        }
        return nil
    }
    
    
    
    class func SignInController() -> UIViewController?{
        if let vc = self.storyBoard().instantiateViewController(withIdentifier: "SignInViewController") as? SignInViewController{
            vc.modalTransitionStyle = .crossDissolve
            return vc
        }
        return nil
    }
    
    class func storyBoard() -> UIStoryboard{
        return UIStoryboard(name: "Main", bundle: nil)
    }

}
