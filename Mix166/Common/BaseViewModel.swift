//
//  BaseViewModel.swift
//  Mix166
//
//  Created by ttiamap on 6/14/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

typealias paggingLoadingClosual = () -> Void

protocol BaseViewModelDelegate {
    func baseViewModel_titleCollectionHeader_DidTapBtnMore(entry: FeatureEntries)
    func baseViewModel_didSelectedKeyword(keyword: String)
    func baseViewModel_didChangeDataSource(datas: AnyObject, playingIndex: Int, type: SongType)
    func baseViewModel_showError(error: NSError)
    func baseViewModel_showLoadingProgress()
    func baseViewModel_hideLoadingProgress()
    func baseViewModel_moreBtnTapAtIndexPath(indexPath: IndexPath, datas: [AnyObject]?)
    func baseViewModel_LikeObject(sender: UIButton?, object: AnyObject)
    
}

class BaseViewModel: NSObject, TitleCollectionHeaderDelegate, HorizontalCollectionDelegate, SongCellDelegate, ArtistCollectionCellDelegate {
    
    // override for home view model
    // override this method from parent class
    func getEntriesAtIndex(index: Int?) -> AnyObject?{fatalError("still not override from child viewmodel class")}
    //    func getSongAtIndexPath(indexPath: IndexPath) -> Song?{fatalError("still not override from child viewmodel class") }
    func getItemAtIndexPath(indexPath: IndexPath) -> AnyObject? {fatalError("still not override from child viewmodel class")}
    func getItemsAtIndexPath(indexPath: IndexPath) -> (AnyObject?, ProductType?) {fatalError("still not override from child viewmodel class")}
    func getItemAt(index: Int) -> AnyObject? {fatalError("still not override from child viewmodel class")}
    func getItemsAtHorizontalIndexPath(indexPath: IndexPath, parentSection section: Int) -> (AnyObject?, FeatureType?){
        fatalError("still not override from child viewmodel class")}
    
    func songCellDelegate_BtnDeleteTap(sender: UIButton?, indexPath: IndexPath) {
        //
        fatalError("still not override from child viewmodel class")
    }
    
    func songCellDelegate_BtnMoreTap(sender: UIButton, indexPath: IndexPath) {
        fatalError("still not override from child viewmodel class")
    }
    
    // override this method in playlist view model
    func getPlaylistAt(index: Int) -> Playlist? {return nil}
    
    // override this method for listen from pagging request needed
    internal func paggingRequest(){}
    
    var filterBase: Filter = Filter.init(_keyword: "")
    
    var sizingKeywordCell = SearchKeywordRoundCell()
    var sizingDescriptionCell = DescriptionCollectionCell()
    
    private var viewTitle: String
    
    var titleNavigationBar: String?{
        return self.viewTitle
    }
    
    var delegate: BaseViewModelDelegate?
    var paggingCallback: paggingLoadingClosual?
    var queueConcurrent: DispatchQueue {
//        let concurrentQueue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
        let queue = DispatchQueue(label: self.viewTitle, attributes: .concurrent)
        return queue
    }
    
    override convenience init() {
        self.init()
    }
    
    init(title: String){
        self.viewTitle = title
    }
    
    // MARK: - Horizontal Delegate
    
    func numberOfItemInSection(type: HorizontalType, parentSection: Int?) -> Int? {
        switch type {
        case .banner:
            return self.getBannerTotalCount(section: parentSection)
            
        case .mixset, .video, .deejayOverlay, .deejay, .deejayHome:
            if let _entries = self.getEntriesAtIndex(index: parentSection) as? FeatureEntries{
                return _entries.entries?.count ?? 0
            }else if let _entries = self.getEntriesAtIndex(index: parentSection) as? Search_Entries{
                return _entries.entries?.count ?? 0
            }
            
        case .unknow:
            return 0
        }
        
        return 0
    }
    
    func horizontal_objectItem(type: HorizontalType, indexPath: IndexPath, parentSection: Int?) -> AnyObject? {
        switch type {
        case .banner:
            return self.horizontal_bannerSongItem(type:type, indexPath:indexPath, parentSection:parentSection)
            
        case .mixset, .video:
            return self.horizontal_songItem(type:type, indexPath:indexPath, parentSection:parentSection)
            
        case .deejayOverlay, .deejay, .deejayHome:
            return self.horizontal_deejayItem(type:type, indexPath:indexPath, parentSection:parentSection)
            
        default:
            return nil
        }
    }
    
    func horizontal_bannerSongItem(type: HorizontalType, indexPath: IndexPath, parentSection: Int?) -> Song? {
        let (song, _) = self.bannerSongItem(indexPath: indexPath, parentSection: parentSection)
        return song
    }
    
    func horizontal_songItem(type: HorizontalType, indexPath: IndexPath, parentSection: Int?) -> Song? {
        
        switch type {
        case .mixset, .video:
            if let _entries = self.getEntriesAtIndex(index: parentSection) as? FeatureEntries{
                if _entries.entries?.count ?? 0 > indexPath.row{
                    if let _song = _entries.entries?[indexPath.row] as? Song{
                        return _song
                    }
                }
            }else if let _entries = self.getEntriesAtIndex(index: parentSection) as? Search_Entries{
                if _entries.entries?.count ?? 0 > indexPath.row{
                    if let _song = _entries.entries?[indexPath.row] as? Song{
                        return _song
                    }
                }
            }
        default:
            return nil
        }
        return nil
    }
    
    func horizontal_deejayItem(type: HorizontalType, indexPath: IndexPath, parentSection: Int?) -> Deejay? {
        switch type {
        case .deejay, .deejayOverlay, .deejayHome:
            if let _entries = self.getEntriesAtIndex(index: parentSection) as? FeatureEntries{
                if _entries.entries?.count ?? 0 > indexPath.row{
                    if let dj = _entries.entries?[indexPath.row] as? Deejay{
                        return dj
                    }
                }
            }else if let _entries = self.getEntriesAtIndex(index: parentSection) as? Search_Entries{
                if _entries.entries?.count ?? 0 > indexPath.row{
                    if let dj = _entries.entries?[indexPath.row] as? Deejay{
                        return dj
                    }
                }
            }
            
        default:
            return nil
        }
        return nil
    }
    
    func horizontal_shoudSelectedItem(type: HorizontalType, indexPath: IndexPath, parentSection: Int?) -> Bool {
        if parentSection != nil {
            
            if type == .banner{
                let (_songs,type) = self.bannerSongItem(indexPath: indexPath, parentSection: parentSection)
                
                if _songs != nil && type != nil {
                    let _items = [_songs]
                    
                    switch type! {
                    case .mixset:
                        self.delegate?.baseViewModel_didChangeDataSource(datas: _items as AnyObject, playingIndex: 0, type: .Mix)
                        break
                        
                    case .video:
                        self.delegate?.baseViewModel_didChangeDataSource(datas: _items as AnyObject, playingIndex: 0, type: .Video)
                        break
                    default:
                        self.delegate?.baseViewModel_didChangeDataSource(datas: _items as AnyObject, playingIndex: 0, type: .Track)
                        break
                    }
                }
                
            }else if type == .deejay || type == .deejayOverlay || type == .deejayHome{
                let (artist, _) = self.getItemsAtHorizontalIndexPath(indexPath: indexPath, parentSection: parentSection!)
                if let _artist = artist as? [Artist]{
                    self.delegate?.baseViewModel_didChangeDataSource(datas: _artist as AnyObject, playingIndex: indexPath.row,type: .Track)
                }
                
            }else{
                let (songs, type) = self.getItemsAtHorizontalIndexPath(indexPath: indexPath, parentSection: parentSection!)
                if type != nil {
                    if let _songs = songs as? [Song]{
                        switch type! {
                        case .mixset:
                            self.delegate?.baseViewModel_didChangeDataSource(datas: _songs as AnyObject, playingIndex: indexPath.row,type: .Mix)
                            break
                            
                        case .video:
                            self.delegate?.baseViewModel_didChangeDataSource(datas: _songs as AnyObject, playingIndex: indexPath.row,type: .Video)
                            break
                        default:
                            self.delegate?.baseViewModel_didChangeDataSource(datas: _songs as AnyObject, playingIndex: indexPath.row,type: .Track)
                        }
                    }
                }
            }
        }
        return true
    }
    
    func horizontal_shouldReloadData(type: HorizontalType, parentSection: Int?) -> Bool {
        return false
    }
    
    //MARK: - register Cell
    func registerCellHome(collectionView: UICollectionView?,callback:@escaping () ->()){
        
        let cellNib = UINib.init(nibName: SongListCollectionCell.nameOfClass, bundle: nil)
        let cellNib2 = UINib.init(nibName: ChartListCollectionCell.nameOfClass, bundle: nil)
        let cellNib4 = UINib.init(nibName: LoadingListCollectionCell.nameOfClass, bundle: nil)
        let cellNib6 = UINib.init(nibName: HorizontalCollectionViewCell.nameOfClass, bundle: nil)
        let cellNib7 = UINib.init(nibName: HorizontalCollectionViewCell.nameOfClass, bundle: nil)
        let cellNib8 = UINib.init(nibName: HorizontalCollectionViewCell.nameOfClass, bundle: nil)
        let cellNib10 = UINib.init(nibName: HorizontalCollectionViewCell.nameOfClass, bundle: nil)
        let cellNib14 = UINib.init(nibName: SongListCollectionCell2.nameOfClass, bundle: nil)
        
        collectionView?.register(cellNib, forCellWithReuseIdentifier:  SongListCollectionCell.nameOfClass)
        collectionView?.register(cellNib2, forCellWithReuseIdentifier: ChartListCollectionCell.nameOfClass)
        collectionView?.register(cellNib4, forCellWithReuseIdentifier: LoadingListCollectionCell.nameOfClass)
        
        collectionView?.register(cellNib6, forCellWithReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.mixset.rawValue)
        collectionView?.register(cellNib7, forCellWithReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.video.rawValue)
        collectionView?.register(cellNib8, forCellWithReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.banner.rawValue)
        collectionView?.register(cellNib10, forCellWithReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.deejayOverlay.rawValue)
        
        collectionView?.register(cellNib14, forCellWithReuseIdentifier: SongListCollectionCell2.nameOfClass)
        
        let nibHeader = UINib(nibName: TitleCollectionReusableView.nameOfClass, bundle: nil)
        collectionView?.register(nibHeader, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: TitleCollectionReusableView.nameOfClass)
        
        
        let nibfooter = UINib(nibName: LoadingCollectionReusableCell.nameOfClass, bundle: nil)
        collectionView?.register(nibfooter, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: LoadingCollectionReusableCell.nameOfClass)
        
        callback()
    }
    
    func registerCell(collectionView: UICollectionView?,callback:@escaping () ->()){
        
        let cellNib = UINib.init(nibName: SongListCollectionCell.nameOfClass, bundle: nil)
        let cellNib2 = UINib.init(nibName: ChartListCollectionCell.nameOfClass, bundle: nil)
        let cellNib3 = UINib.init(nibName: SearchKeywordCollectionCell.nameOfClass, bundle: nil)
        let cellNib4 = UINib.init(nibName: LoadingListCollectionCell.nameOfClass, bundle: nil)
        let cellNib5 = UINib.init(nibName: ArtistListCollectionCell.nameOfClass, bundle: nil)
        let cellNib6 = UINib.init(nibName: HorizontalCollectionViewCell.nameOfClass, bundle: nil)
        let cellNib7 = UINib.init(nibName: HorizontalCollectionViewCell.nameOfClass, bundle: nil)
        let cellNib8 = UINib.init(nibName: HorizontalCollectionViewCell.nameOfClass, bundle: nil)
        let cellNib9 = UINib.init(nibName: HorizontalCollectionViewCell.nameOfClass, bundle: nil)
        let cellNib10 = UINib.init(nibName: HorizontalCollectionViewCell.nameOfClass, bundle: nil)
        let cellNib11 = UINib.init(nibName: HorizontalCollectionViewCell.nameOfClass, bundle: nil)
        let cellNib12 = UINib.init(nibName: ProfileCollectionCell.nameOfClass, bundle: nil)
        let cellNib13 = UINib.init(nibName: MoreItemCollectionCell.nameOfClass, bundle: nil)
        let cellNib14 = UINib.init(nibName: SearchKeywordRoundCell.nameOfClass, bundle: nil)
        let cellNib15 = UINib.init(nibName: SongListCollectionCell2.nameOfClass, bundle: nil)
        let cellNib16 = UINib.init(nibName: DescriptionCollectionCell.nameOfClass, bundle: nil)
        let cellNib17 = UINib.init(nibName: ProfileMoreItemCell.nameOfClass, bundle: nil)
        
        
        self.sizingKeywordCell = cellNib14.instantiate(withOwner: nil, options: nil)[0] as! SearchKeywordRoundCell
        self.sizingDescriptionCell = cellNib16.instantiate(withOwner: nil, options: nil)[0] as! DescriptionCollectionCell
        
        collectionView?.register(cellNib, forCellWithReuseIdentifier:  SongListCollectionCell.nameOfClass)
        collectionView?.register(cellNib2, forCellWithReuseIdentifier: ChartListCollectionCell.nameOfClass)
        collectionView?.register(cellNib3, forCellWithReuseIdentifier: SearchKeywordCollectionCell.nameOfClass)
        collectionView?.register(cellNib4, forCellWithReuseIdentifier: LoadingListCollectionCell.nameOfClass)
        collectionView?.register(cellNib5, forCellWithReuseIdentifier: ArtistListCollectionCell.nameOfClass)
        
        collectionView?.register(cellNib6, forCellWithReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.mixset.rawValue)
        collectionView?.register(cellNib7, forCellWithReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.video.rawValue)
        collectionView?.register(cellNib8, forCellWithReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.banner.rawValue)
        collectionView?.register(cellNib9, forCellWithReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.deejay.rawValue)
        collectionView?.register(cellNib10, forCellWithReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.deejayOverlay.rawValue)
        collectionView?.register(cellNib11, forCellWithReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.deejayHome.rawValue)
        
        collectionView?.register(cellNib12, forCellWithReuseIdentifier: ProfileCollectionCell.nameOfClass)
        collectionView?.register(cellNib13, forCellWithReuseIdentifier: MoreItemCollectionCell.nameOfClass)
        collectionView?.register(cellNib14, forCellWithReuseIdentifier: SearchKeywordRoundCell.nameOfClass)
        collectionView?.register(cellNib15, forCellWithReuseIdentifier: SongListCollectionCell2.nameOfClass)
        collectionView?.register(cellNib16, forCellWithReuseIdentifier: DescriptionCollectionCell.nameOfClass)
        collectionView?.register(cellNib17, forCellWithReuseIdentifier: ProfileMoreItemCell.nameOfClass)
        
        let nibHeader = UINib(nibName: TitleCollectionReusableView.nameOfClass, bundle: nil)
        collectionView?.register(nibHeader, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: TitleCollectionReusableView.nameOfClass)
        
        
        
        let nibHeader2 = UINib(nibName: SlideMenuWithCoverImageCollectionReusableView.nameOfClass, bundle: nil)
        collectionView?.register(nibHeader2, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: SlideMenuWithCoverImageCollectionReusableView.nameOfClass)
        
        let nibHeader3 = UINib(nibName: TitleHeaderProfileReusableView.nameOfClass, bundle: nil)
        collectionView?.register(nibHeader3, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: TitleHeaderProfileReusableView.nameOfClass)
        
        let nibfooter = UINib(nibName: LoadingCollectionReusableCell.nameOfClass, bundle: nil)
        collectionView?.register(nibfooter, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: LoadingCollectionReusableCell.nameOfClass)
        
        callback()
    }
    
    // MARK: - Configure Cell
    
    // MARK: - HeaderTitle
    func configureHeaderTitleCell(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, indexPath: IndexPath, colorBlack_Enable: Bool = false) -> UICollectionReusableView{
        
        if kind == UICollectionElementKindSectionHeader{
            if let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: TitleCollectionReusableView.nameOfClass, for: indexPath) as? TitleCollectionReusableView{
                header.delegate = self
                header.headerSection = indexPath.section
                
                if colorBlack_Enable{
                    header.backgroundColor = UIColor.black
                }
                
                self.queueConcurrent.async(execute: {
                    if let entry = self.getEntriesAtIndex(index: indexPath.section){
                        header.configureHeader(str_title: entry.title)
                    }
                })
                
                return header
            }
        }
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureHeaderTitleProfileCell(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, indexPath: IndexPath) -> UICollectionReusableView{
        
        if kind == UICollectionElementKindSectionHeader{
            if let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: TitleHeaderProfileReusableView.nameOfClass, for: indexPath) as? TitleHeaderProfileReusableView{
                
                
                self.queueConcurrent.async(execute: {
                    
                    if let _itemMore = self.getItemAt(index: indexPath.section) as? ProfileViewModel.ItemMore{
                        header.configureHeader(str_title: _itemMore.type.rawValue.uppercased())
                    }
                })
                
                return header
            }
        }
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Banner
    func configureBannerCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.banner.rawValue, for: indexPath) as? HorizontalCollectionViewCell{
            
            queueConcurrent.sync(execute: {
                cell.currentSection = indexPath.section
                cell.type = .banner
                if cell.delegate == nil {
                    cell.delegate = self
                }
            })
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Mixset
    func configureMixSetCell(collectionView: UICollectionView, indexPath: IndexPath, colorBlack_Enable: Bool = false) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.mixset.rawValue, for: indexPath) as? HorizontalCollectionViewCell{
            
            if colorBlack_Enable{
                cell.backgroundColor = UIColor.black
            }
            
            queueConcurrent.sync(execute: {
                cell.type = .mixset
                cell.currentSection = indexPath.section
                if cell.delegate == nil {
                    cell.delegate = self
                }
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Track
    func configureTrackCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:SongListCollectionCell.nameOfClass, for: indexPath) as? SongListCollectionCell{
            
            queueConcurrent.async(execute: {
                if let _song = self.getItemAtIndexPath(indexPath: indexPath) as? Song{
                    cell.currentIndexPath = indexPath
                    if cell.delegate == nil {
                        cell.delegate = self
                    }
                    cell.configureCellWith(_song: _song)
                }
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Chart
    func configureChartCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChartListCollectionCell.nameOfClass, for: indexPath) as? ChartListCollectionCell{
            
            queueConcurrent.async(execute: {
                if let _song = self.getSongChartAtIndexPath(indexPath: indexPath){
                    cell.currentIndexPath = indexPath
                    if cell.delegate == nil {
                        cell.delegate = self
                    }
                    cell.configureCellWith(_song: _song, rankIndex: indexPath.row)
                }
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Deejay
    func configureDeejayCell(collectionView: UICollectionView, indexPath: IndexPath, hightLightWhenSelected: Bool) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.deejay.rawValue, for: indexPath) as? HorizontalCollectionViewCell{
            
            queueConcurrent.sync(execute: {
                
                cell.currentSection = indexPath.section
                cell.type = .deejay
//                if cell.delegate == nil {
                    cell.delegate = self
                    cell.isHightlightWhenSelected = hightLightWhenSelected
//                }
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureDeejayHomeCell(collectionView: UICollectionView, indexPath: IndexPath, hightLightWhenSelected: Bool) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.deejayHome.rawValue, for: indexPath) as? HorizontalCollectionViewCell{
            
            queueConcurrent.sync(execute: {
                cell.currentSection = indexPath.section
                cell.type = .deejayHome
                if cell.delegate == nil {
                    cell.isHightlightWhenSelected = hightLightWhenSelected
                    cell.delegate = self
                }
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureDeejayOverlayCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.deejayOverlay.rawValue, for: indexPath) as? HorizontalCollectionViewCell{
            
            queueConcurrent.sync(execute: {
                cell.currentSection = indexPath.section
                cell.type = .deejayOverlay
                if cell.delegate == nil {
                    cell.delegate = self
                }
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureDeejayListCell(collectionView: UICollectionView, indexPath: IndexPath, enableFollow: Bool) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ArtistListCollectionCell.nameOfClass, for: indexPath) as? ArtistListCollectionCell{
            
            queueConcurrent.sync(execute: {
                if let _artist = self.horizontal_deejayItem(type: .deejay, indexPath: indexPath, parentSection: indexPath.section){
                    
                    if cell.delegate == nil {
                        cell.delegate = self
                    }
                    cell.configureCellWith(_artist: _artist, enableFollow: enableFollow)
                }
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func artistCellDelegate_DidTapFollowBtn(sender: UIButton, object: Artist) {
        self.delegate?.baseViewModel_LikeObject(sender: sender, object: object)
    }
    
    
    // MARK: Video
    func configureVideoCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.video.rawValue, for: indexPath) as? HorizontalCollectionViewCell{
            
            queueConcurrent.sync(execute: {
                cell.currentSection = indexPath.section
                cell.type = .video
                if cell.delegate == nil {
                    cell.delegate = self
                }
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Player Song
//    func configureSongPlayerCell(collectionView: UICollectionView, indexPath: IndexPath, currentPlayingIndex: Int) -> UICollectionViewCell{
//        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PlayerSongCollectionCell.nameOfClass, for: indexPath) as? PlayerSongCollectionCell{
//            
//            if let _song = self.getItemAtIndexPath(indexPath: indexPath) as? Song{
//                cell.configureCellWith(_song: _song, smallImage: indexPath.row != currentPlayingIndex)
//            }
//            
//            return cell
//        }
//        
//        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
//    }
    
    func configureSongPlayerCell(collectionView: UICollectionView, indexPath: IndexPath, currentSongPlaying: Song?) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PlayerSongCollectionCell.nameOfClass, for: indexPath) as? PlayerSongCollectionCell{
            
            if let _song = self.getItemAtIndexPath(indexPath: indexPath) as? Song{
                cell.configureCellWith(_song: _song, smallImage: _song.id != currentSongPlaying?.id ?? "0")
            }
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Profile Item Cell
    func configureMoreItemCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let _cell = collectionView.dequeueReusableCell(withReuseIdentifier:MoreItemCollectionCell.nameOfClass, for: indexPath) as? MoreItemCollectionCell{
            
            if let _itemMore = self.getItemAt(index: indexPath.section) as? ProfileViewModel.ItemMore{
                if indexPath.row < _itemMore.items.count{
                    let more_type = _itemMore.items[indexPath.row]
                    _cell.configureCell(itemMore: more_type)
                }
            }
            
            return _cell
        }
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureProfileMoreItemCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let _cell = collectionView.dequeueReusableCell(withReuseIdentifier:ProfileMoreItemCell.nameOfClass, for: indexPath) as? ProfileMoreItemCell{
            
            if let _itemMore = self.getItemAt(index: indexPath.section) as? ProfileViewModel.ItemMore{
                if indexPath.row < _itemMore.items.count{
                    let more_type = _itemMore.items[indexPath.row]
                    _cell.configureCell(itemMore: more_type)
                }
            }
            
            return _cell
        }
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureProfileUserCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let _cell = collectionView.dequeueReusableCell(withReuseIdentifier:ProfileCollectionCell.nameOfClass, for: indexPath) as? ProfileCollectionCell{
            
            _cell.configure_userProfile()
            
            return _cell
        }
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    
    //MARK: Playlist
    func configurePlaylistCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let _cell = collectionView.dequeueReusableCell(withReuseIdentifier:PlayListCollectionCell.nameOfClass, for: indexPath) as? PlayListCollectionCell{
            
            if let _playlist = self.getPlaylistAt(index: indexPath.row), !_playlist.isEmpty{
                queueConcurrent.async(execute: {
                    
                    _cell.configureCellWithPlaylist(_playlist: _playlist, at: indexPath)
                    
                    if _cell.delegate == nil {
                        _cell.isEnableSwipeToDelete = true
                        _cell.delegate = self
                    }
                })
                return _cell
            }
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureAddPlaylistCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let _cell = collectionView.dequeueReusableCell(withReuseIdentifier:PlayListCollectionCell.nameOfClass, for: indexPath) as? PlayListCollectionCell{
            
            queueConcurrent.async(execute: {
                _cell.configureCellWithAddPlaylist()
                _cell.isEnableSwipeToDelete = false
            })
            return _cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Loading
    func configureLoadingListCollectionCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LoadingListCollectionCell.nameOfClass, for: indexPath) as!LoadingListCollectionCell
        
        return cell
    }
    
    func configureLoadingFooterCell(collectionView: UICollectionView, kind: String, indexPath: IndexPath) -> UICollectionReusableView{
        //        if kind == UICollectionElementKindSectionFooter {
        let loadingView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: LoadingCollectionReusableCell.nameOfClass, for: indexPath) as! LoadingCollectionReusableCell
        
        self.paggingRequest()
        
        return loadingView
        //        }
    }
    
    //MARK: - Action Cell
    
    func titleCollectionHeader_DidTapBtnMore(sender: UIButton, headerSection: Int?) {
        if let entry = self.getEntriesAtIndex(index: headerSection) as? FeatureEntries{
            self.delegate?.baseViewModel_titleCollectionHeader_DidTapBtnMore(entry: entry)
        }
    }
    
    func selectedItemAtIndexPath(indexPath: IndexPath){
        
        let (data, type) = self.getItemsAtIndexPath(indexPath: indexPath)
        var _type: SongType?
        
        if type == nil{
            return
        }
        
        switch type! {
        case .mixset:
            //
            _type = .Mix
            break
            
        case .video:
            //
            _type = .Video
            break
            
        case .track:
            //
            _type = .Track
            break
            
        case .deejay_vertical, .deejay_horizontal:
            //
            _type = .Artist
            break
            
        default:
            break
        }
        
        if _type == nil {
            // keyword
            if let keyword = data as? String{
                self.delegate?.baseViewModel_didSelectedKeyword(keyword: keyword)
            }
        }else{
            self.delegate?.baseViewModel_didChangeDataSource(datas: data as AnyObject, playingIndex: indexPath.row, type: _type!)
        }
    }
    
    
    // MARK: - Help method
    func bannerSongItem(indexPath: IndexPath, parentSection: Int?) -> (Song?, FeatureType?) {
        let (type, index) = self.detectBannerType(indexPath: indexPath, parentSection: parentSection)
        return (self.getBannerSongItem(type: type, index: index, parentSection: parentSection),type)
    }
    
    func getBannerSongItem(type: FeatureType, index: Int, parentSection: Int?) -> Song?{
        
        if let _banner = self.getBannerObject(section: parentSection){
            switch type {
            case .track:
                return _banner.songs[index]
                
            case .mixset:
                return _banner.mixs[index]
                
            case .video:
                return _banner.videos[index]
                
            default:
                //
                return nil
            }
        }
        return nil
    }
    
    func getBannerObject(section: Int?) -> Banner?{
        if section == nil{
            return nil
        }
        
        if let entries = self.getEntriesAtIndex(index: section!) as? FeatureEntries{
            if let banners = entries.entries as? [Banner]{
                if banners.count == 1{
                    return banners[0]
                }
            }
        }
        return nil
    }
    
    func getBannerTotalCount(section: Int?) -> Int?{
        
        if let _banner = self.getBannerObject(section: section){
            let totalSong = _banner.songs.count
            let totalMix = _banner.mixs.count
            let totalVideo = _banner.videos.count
            
            return totalSong + totalMix + totalVideo
        }
        return 0
    }
    
    func detectBannerType(indexPath: IndexPath, parentSection: Int?) -> (FeatureType, Int){
        
        if let _banner = self.getBannerObject(section: parentSection){
            let totalSong = _banner.songs.count
            let totalMix = _banner.mixs.count
            //            let totalVideo = _banner.videos.count
            
            if indexPath.row < totalSong{
                return (.track, indexPath.row)
            }else if indexPath.row < (totalSong + totalMix){
                return (.mixset, (indexPath.row - totalSong))
            }else{
                return (.video, (indexPath.row - (totalSong + totalMix)))
            }
        }
        
        return (.unknow,0)
    }
    
    func getSongChartAtIndexPath(indexPath: IndexPath) -> Song?{
        if let featureEntries = self.getEntriesAtIndex(index: indexPath.section) as? FeatureEntries{
            if featureEntries.entries?.count ?? 0 > 0 {
                if let _chart = featureEntries.entries?[0] as? Chart, _chart.tracks.count > indexPath.row{
                    return _chart.tracks[indexPath.row]
                }
            }
        }
        return nil
    }
    
    func getSongChartTrackAtIndexPath(indexPath: IndexPath) -> [Song]?{
        if let featureEntries = self.getEntriesAtIndex(index: indexPath.section) as? FeatureEntries{
            if featureEntries.entries?.count ?? 0 > 0 {
                if let _chart = featureEntries.entries?[0] as? Chart{
                    return _chart.tracks
                }
            }
        }
        return nil
    }
    
}
