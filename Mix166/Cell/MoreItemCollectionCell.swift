//
//  MoreItemCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/23/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class MoreItemCollectionCell: UICollectionViewCell {
    
    var font_title: UIFont{
        return UIFont.fontRegular_medium()
    }
    
    @IBOutlet weak var cell_imageIcon: UIImageView?
    @IBOutlet weak var cell_title: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupFont()
    }
    
    func setupFont(){
        self.cell_title?.font = self.font_title
        self.cell_title?.textColor = UIColor.white
    }
    
    func configureCell(str_iconName: String, str_title: String){
        self.cell_title?.text = str_title
        self.cell_imageIcon?.image = UIImage.init(named: str_iconName)
    }
    
    func configureCell(itemMore: ProfileViewModel.MoreType){
        
        if itemMore == ProfileViewModel.MoreType.my_music{
            
            let strings = itemMore.title().components(separatedBy: " ")
            let frist = strings.first?.capitalized ?? ""
            let second = strings.last?.uppercased() ?? ""
            let attributeString = NSMutableAttributedString.init(string: (frist+" "+second))
            
            attributeString.addAttributes([NSForegroundColorAttributeName : UIColor.init(hexaString: AppColor.appRedColor.rawValue)], range: NSRange.init(location: 0, length: frist.length))
            
            self.cell_title?.attributedText = attributeString
        }else{
            self.cell_title?.text = itemMore.title().uppercased()
        }
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }

}
