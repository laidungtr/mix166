//
//  ProfileMoreItemCell.swift
//  Mix166
//
//  Created by ttiamap on 8/10/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ProfileMoreItemCell: MoreItemCollectionCell {
    
    override var font_title: UIFont{
        return UIFont.fontRegular_extraLargeXX()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
