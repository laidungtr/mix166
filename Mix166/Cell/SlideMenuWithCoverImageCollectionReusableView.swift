//
//  SlideMenuCollectionReusableView.swift
//  Mix166
//
//  Created by ttiamap on 7/19/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import SDWebImage

protocol SlideMenuWithCoverImageCollectionReusableViewDelegate {
    func slideMenu_DidSelectedItemAtIndex(index: NSInteger)
}


class SlideMenuWithCoverImageCollectionReusableView: UICollectionReusableView, SlideMenuViewDelegate {
    
    @IBOutlet private weak var viewSlideMenu: SlideMenuView?
    @IBOutlet weak var cell_imageView: UIImageView!
    @IBOutlet weak var cell_blurView: UIView!
    
    @IBOutlet weak var layoutTopConstraint: NSLayoutConstraint?
    @IBOutlet weak var layoutBottomConstraint: NSLayoutConstraint?
    @IBOutlet weak var layoutLeadingConstraint: NSLayoutConstraint?
    @IBOutlet weak var layoutTrailingConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var layoutBottomSlideMenu: NSLayoutConstraint?
    
    var currentSection: Int = 0
    var delegate: SlideMenuWithCoverImageCollectionReusableViewDelegate?
    
    var paggingTypes: [ProductType]?{
        didSet{
            self.setupSliderMenu()
        }
    }
    var itemSong: Song?{
        didSet{
            self.setImageWith(str_url: self.itemSong?.artists.first?.image_cover ?? "")
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        
        DispatchQueue.main.async() { () -> Void in
            
            self.layoutIfNeeded()
            self.cell_blurView.gradientFromTopToBottom(UIColor.black.withAlphaComponent(0.0).cgColor, bottomColor: UIColor.black.withAlphaComponent(1).cgColor, gradientFrame: self.cell_blurView.bounds)
        }

    }
    
    func setSlideMenuBottomConstraint(value: CGFloat){
        self.layoutBottomSlideMenu?.constant = value
        
        if Thread.isMainThread{
            self.layoutIfNeeded()
        }else{
            DispatchQueue.main.async() { () -> Void in
                self.layoutIfNeeded()
            }
        }
        
    }
    
    func setupSliderMenu(){
        self.viewSlideMenu?.slideMenuStyle = .Title
        self.viewSlideMenu?.delegate = self
        self.viewSlideMenu?.setSlideMenuBackgroundColor(color: UIColor.clear)
        self.viewSlideMenu?.reloadSlideMenuData()
        
        if Thread.isMainThread{
            self.layoutIfNeeded()
        }else{
            DispatchQueue.main.async() { () -> Void in
                self.layoutIfNeeded()
            }
        }
    }
    
    
    internal func setImageWith(str_url: String){
        
        if let url = URL.init(string: str_url.urlConvertWith(width: bannerSize.width, height: bannerSize.height)){
            self.cell_imageView?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
        }
    }
    
    //MARK: - SlideMenu Delegate
    func slideMenu_NumberOfItem() -> NSInteger{
        return self.paggingTypes?.count ?? 0
    }
    
    func slideMenu_TitleForItem(index: NSInteger) -> String{
        if self.paggingTypes?.count ?? 0 > index{
            return self.paggingTypes?[index].rawValue.uppercased() ?? ""
        }
        return ""
    }
    
    func slideMenu_DidSelectedItemAtIndex(index: NSInteger){
        
//        if !self.isUserDragging{
//            self.isUserDragging = false
//            self.isScrollingManually = true
//        }
//        
//        let xOffset = CGFloat(index) * screenWidth
//        self.myScrollView?.setContentOffset(CGPoint.init(x: xOffset, y: 0), animated: true)
        
        self.delegate?.slideMenu_DidSelectedItemAtIndex(index: index)
    }
    
    func slideMenu_FristloadSelected() -> Int {
        return 0
    }
    
    func superScrollViewDidScrolling(scrollView: UIScrollView){
        let offsetY = scrollView.contentOffset.y
        
        if offsetY >= 0{
            self.layoutTopConstraint?.constant = offsetY/2
            self.layoutTrailingConstraint?.constant = 0
            self.layoutLeadingConstraint?.constant = 0
            self.layoutBottomConstraint?.constant = -offsetY/2
            
        }else{
            self.layoutTopConstraint?.constant = offsetY
            self.layoutTrailingConstraint?.constant = offsetY
            self.layoutLeadingConstraint?.constant = offsetY
            self.layoutBottomConstraint?.constant = offsetY
        }
    }
    
}
