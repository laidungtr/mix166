//
//  ArtistListCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/30/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ArtistListCollectionCell: BaseArtistCollectionCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.borderImageLayout()
    }
    
    private func borderImageLayout(){
        self.cell_image?.borderWithRadius(deejayListSize.height/2)
    }
    
    override func configureCellWith(_artist: Artist, enableFollow: Bool){
        
        self.itemArtist = _artist
        self.isEnableFollowBtn = enableFollow
        
        if let url = URL.init(string: _artist.image_cover.urlConvertWith(width: deejayListSize.height, height: deejayListSize.height)){
            self.cell_image?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
        }
        
        if Thread.isMainThread{
            self.cell_name?.text = _artist.alias
            self.cell_followersNumber?.text = _artist.like_count + " Followers"
            
            if let isLike =  DataProxy.user.checkLikeWithoutRequest(object: _artist){
                self.cell_btnFollowing?.isSelected = isLike
            }else{
                self.cell_btnFollowing?.isSelected = _artist.isFollow
            }
        }else{
            
            DispatchQueue.main.async() { () -> Void in
                
                self.cell_name?.text = _artist.alias
                self.cell_followersNumber?.text = _artist.like_count + " Followers"
                
                if let isLike =  DataProxy.user.checkLikeWithoutRequest(object: _artist){
                    self.cell_btnFollowing?.isSelected = isLike
                }else{
                    self.cell_btnFollowing?.isSelected = _artist.isFollow
                }
            }
        }
    }
}
