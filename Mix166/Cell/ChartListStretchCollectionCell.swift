//
//  ChartListCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/8/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ChartListStretchCollectionCell: BaseSongCollectionCell {
    
    override func borderImageCornerStyle() -> UIView.BorderCornerStyle {
        return .default
    }
}
