//
//  VideoGridCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/7/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class VideoGridCollectionCell: BaseSongCollectionCell {
    
    override var font_duration: UIFont{
        return UIFont.fontLight_small()
    }
    
    override var color_duration: UIColor{
        return .white
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization codes
    }
}
