//
//  ArtistBannerCell.swift
//  Mix166
//
//  Created by ttiamap on 8/9/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ArtistBannerCell: BannerCollectionCell {
    
    override var font_title: UIFont{
        return UIFont.fontBold_large()
    }
    
    
    func configureWith(itemArtist: Artist){
        
        if let url = URL.init(string: itemArtist.image_cover.urlConvertWith(width: screenWidth, height: screenWidth)){
            self.cell_imageView?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
        }
        
        if Thread.isMainThread{
            self.cell_title?.text = itemArtist.alias
        }else{
            DispatchQueue.main.async() { () -> Void in
                self.cell_title?.text = itemArtist.alias
            }
        }
    }
    
    override func awakeFromNib() {
        // Initialization code
        self.cell_title?.font = self.font_title
        self.cell_title?.textColor = self.color_primary
        
        DispatchQueue.main.async() { () -> Void in
            self.cell_imageView?.layoutIfNeeded()
            self.cell_imageDescription?.layoutIfNeeded()
            self.cell_blurView?.gradientFromTopToBottom(UIColor.black.withAlphaComponent(0.0).cgColor, bottomColor: UIColor.black.withAlphaComponent(1).cgColor, gradientFrame: CGRect.init(x: 0, y: bannerSize.height * 2/3 - 5.0, width: screenWidth, height: bannerSize.height/3 + 5.0))
        }
    }
    
}
