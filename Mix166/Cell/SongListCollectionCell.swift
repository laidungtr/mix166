//
//  SongListCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/7/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class SongListCollectionCell: BaseSongCollectionCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func borderImageCornerStyle() -> UIView.BorderCornerStyle {
        return .default
    }
}
