//
//  SearchKeywordRoundCell.swift
//  Mix166
//
//  Created by ttiamap on 8/2/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class SearchKeywordRoundCell: SearchKeywordCollectionCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setupFont() {
        self.cell_lbKeyword?.font = font_title
        self.cell_lbKeyword?.textColor = UIColor.init(hexaString: AppColor.appRedColor.rawValue).withAlphaComponent(0.9)
    }
    
    func configureCellTrend(keyword: String){
        
        if Thread.isMainThread{
            self.borderWithRadius(self.frame.size.height/2, borderWidth: 1.0, borderColor: UIColor.init(hexaString: AppColor.appRedColor.rawValue).withAlphaComponent(0.9))
            self.cell_lbKeyword?.text = keyword
        }else{
            DispatchQueue.main.async() { () -> Void in
                
                self.borderWithRadius(self.frame.size.height/2, borderWidth: 1.0, borderColor: UIColor.init(hexaString: AppColor.appRedColor.rawValue).withAlphaComponent(0.9))
                self.cell_lbKeyword?.text = keyword
            }
        }
        
    }
    
    func configureSizingCell(keyword: String){
        self.cell_lbKeyword?.text = keyword
    }
    
    override var intrinsicContentSize: CGSize{
        var size = self.cell_lbKeyword?.intrinsicContentSize
        
        // add to intrisic content size of label
        size?.width  +=  32.0
        size?.height +=  16.0
        
        return size!
    }

}
