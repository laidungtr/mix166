//
//  SongGridCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/7/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class SongGridCollectionCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    internal var font_title: UIFont{
        return UIFont.fontSemiBold_medium()
    }
    internal var font_artist    = UIFont.fontRegular_medium()
    internal var font_category  = UIFont.fontRegular_medium()
    internal var font_duration  = UIFont.fontRegular_medium()
    
    internal var color_primary     = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    internal var color_secondary: UIColor{
        return UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    }
    

    @IBOutlet weak var cell_image: UIImageView?
    @IBOutlet weak var cell_title: UILabel?
    @IBOutlet weak var cell_artist: UILabel?
    @IBOutlet weak var cell_description: UILabel?
    @IBOutlet weak var cell_category: UILabel?
    @IBOutlet weak var cell_duration: UILabel?
    @IBOutlet weak var cell_btnMore: UIButton?
    @IBOutlet weak var cell_viewContain: UIView?
    @IBOutlet weak var cell_btnDelete: UIButton?
    @IBOutlet weak var cell_lbRank: UILabel?
    @IBOutlet weak var cell_imageRank: UIImageView?
    
    @IBOutlet weak var layoutCellLeadingConstraint: NSLayoutConstraint?
    
    private var direction: panDirection = .unknow
    private var storeMovingPoint = CGPoint.init(x: 0, y: 0)
    internal var isDownloading = false
    
    var delegate: SongCellDelegate?
    var currentIndexPath: IndexPath?
    
    var isEnableSwipeToDelete: Bool = false{
        didSet{
            if self.isEnableSwipeToDelete == true{
                self.setupSwipeToDelete()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
        self.setupColor()
        self.borderImageLayout(radius: self.borderImageRadius())
        self.clearLayout()
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.refreshConstraint()
        self.clearLayout()
    }
    
    internal func borderImageCornerStyle() -> BorderCornerStyle{
        return .top
    }
    
    internal func borderImageRadius() -> CGFloat{
        return 2.0
    }
    
    private func setupSwipeToDelete(){
        if !self.isEnableSwipeToDelete{
            return
        }
        
        self.cell_viewContain?.isUserInteractionEnabled = true
        let panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(self.panGestureRecognizer(gesture:)))
        panGesture.delegate = self
        self.cell_viewContain?.addGestureRecognizer(panGesture)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc func panGestureRecognizer(gesture: UIPanGestureRecognizer){
        
        
        if self.cell_btnDelete == nil || self.isDownloading {
            return
        }
        
        switch gesture.state {
        case .began:
            //
            let velocity = gesture.velocity(in: gesture.view)
            
            let isVerticalGesture = fabs(velocity.y) > fabs(velocity.x)
            
            if isVerticalGesture{
                // vertical
                
                
            }else{
                if gesture.view == self.cell_viewContain{
                    // horizontal
                    if velocity.x > 0{
                        // right
                        self.direction = .right
                    }else{
                        //left
                        self.direction = .left
                    }
                }
            }
            
        case .changed:
            //
            if self.direction == .right || self.direction == .left{
                
                let translation = gesture.translation(in: self.cell_viewContain)
                
                var movingPoint = CGPoint.init(x: self.layoutCellLeadingConstraint!.constant, y: 0)
                movingPoint.x += translation.x
                
                if movingPoint.x > 0{
                    movingPoint.x = 0
                }else if movingPoint.x < -self.cell_btnDelete!.bounds.width{
                    movingPoint.x = -self.cell_btnDelete!.bounds.width
                }
                
                self.layoutCellLeadingConstraint?.constant = movingPoint.x
                
                if Thread.isMainThread{
                    self.layoutIfNeeded()
                }else{
                    DispatchQueue.main.async() { () -> Void in
                        self.layoutIfNeeded()
                    }
                }
                
                self.storeMovingPoint = movingPoint
                gesture.setTranslation(CGPoint.zero, in: self.cell_viewContain)
            }
            break
            
            
        case .ended:
            //
            switch self.direction {
            case .left:
                //
                UIView.animate(withDuration: 0.2, animations: {
                    //
                    self.layoutCellLeadingConstraint?.constant = -self.cell_btnDelete!.bounds.width
                    self.layoutIfNeeded()
                    
                }, completion: { (finish) in
                    //
                })
                break
            case .right:
                //
                UIView.animate(withDuration: 0.2, animations: {
                    //
                    self.refreshConstraint()
                    
                }, completion: { (finish) in
                    //
                })
                break
                
                
            default:
                break
            }
            
            self.storeMovingPoint = CGPoint.init(x: 0, y: 0)
            self.direction = .unknow
            break
            
        default:
            
            self.direction = .unknow
        }
    }
    
    private func refreshConstraint(){
        if self.layoutCellLeadingConstraint?.constant != 0 {
            self.layoutCellLeadingConstraint?.constant = 0
            if Thread.isMainThread{
                self.layoutIfNeeded()
            }else{
                DispatchQueue.main.async() { () -> Void in
                    self.layoutIfNeeded()
                }
            }
        }
    }
    
    private func clearLayout(){
        self.cell_title?.text = ""
        self.cell_artist?.text = ""
        self.cell_category?.text = ""
        self.cell_duration?.text = ""
        self.cell_description?.text = ""
        self.cell_image?.image = nil
    }
    
    internal func setupFont(){
        
        self.cell_description?.font = self.font_artist
        self.cell_title?.font = self.font_title
        self.cell_artist?.font = self.font_artist
        self.cell_category?.font = self.font_category
        self.cell_duration?.font = self.font_duration
    }
    
    private func setupColor(){
        self.cell_title?.textColor = self.color_primary
        self.cell_artist?.textColor = self.color_secondary
        self.cell_description?.textColor = self.color_secondary
        self.cell_category?.textColor = self.color_secondary
        self.cell_duration?.textColor = self.color_secondary
    }
    
    private func borderImageLayout(radius: CGFloat){
        DispatchQueue.main.async() { () -> Void in
            self.cell_image?.borderRadius(radius, frame: self.cell_image?.bounds, borderStyle: self.borderImageCornerStyle())
        }
    }
    
    internal func configureCellWith(_song: Song){
        DispatchQueue.global(qos: .background).sync {
            
            let allArtist = _song.all_artistName
            let allGenres = _song.all_genresName
            
            if Thread.isMainThread{
                self.setImageWith(str_url: _song.imageCover)
                self.cell_title?.text = _song.name
                self.cell_artist?.text = allArtist
                self.cell_duration?.text = _song.mp3.duration
                self.cell_category?.text = allGenres
                
                
            }else{
                DispatchQueue.main.async() { () -> Void in
                    self.setImageWith(str_url: _song.imageCover)
                    self.cell_title?.text = _song.name
                    self.cell_artist?.text = allArtist
                    self.cell_duration?.text = _song.mp3.duration
                    self.cell_category?.text = allGenres
                }
            }
        }
    }
    
    internal func setImageWith(str_url: String){
        
        if let url = URL.init(string: str_url.urlConvertWith(width: mixSize.width, height: mixSize.width)){
            print(url)
            self.cell_image?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnDeleteTap(sender : UIButton){
        if self.currentIndexPath != nil{
            self.delegate?.songCellDelegate_BtnDeleteTap(sender: sender,indexPath: self.currentIndexPath!)
        }
    }
    
    @IBAction func btnMoreTap(sender : UIButton){
        if self.currentIndexPath != nil {
            self.delegate?.songCellDelegate_BtnMoreTap(sender: sender, indexPath: self.currentIndexPath!)
        }
    }

}
