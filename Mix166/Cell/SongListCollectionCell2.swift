//
//  SongListCollectionCell2.swift
//  Mix166
//
//  Created by ttiamap on 6/23/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class SongListCollectionCell2: BaseSongCollectionCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setupFont() {
        self.cell_title?.font = UIFont.fontRegular_big()
        self.cell_artist?.font = UIFont.fontRegular_small()
        
        self.cell_title?.textColor = self.color_primary
        self.cell_artist?.textColor = self.color_secondary
    }
}
