//
//  SongListDownloadCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 7/26/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import Alamofire

protocol SongDownloadDelegate {
    func SongDownload_DidRemoveDownloadSongSuccess(at indexPath: IndexPath)
    func SongDownload_CantRemoveDownloadSong(error: NSError)
}

class SongListDownloadCollectionCell: BaseSongCollectionCell {
    
    @IBOutlet weak var cell_btnDownload: UIButton?
    @IBOutlet weak var circularProgressView: KDCircularProgress!
    
    private var downloadInfo: DownloadInfo?
    private var progressView: UIProgressView?
    
    var delegate_songDownload: SongDownloadDelegate?
    
    var tmpSong: Song?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if self.cell_btnDownload != nil {
            self.cell_btnDownload?.borderWithRadius(self.cell_btnDownload!.frame.size.height/2, borderWidth: 1.25, borderColor: color_primary)
        }
        
        self.circularProgressView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.circleViewDidTap))
        self.circularProgressView.addGestureRecognizer(tapGesture)
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    override func setupFont() {
        self.cell_title?.font = UIFont.fontRegular_big()
        self.cell_artist?.font = UIFont.fontRegular_small()
        
        self.cell_title?.textColor = self.color_primary
        self.cell_artist?.textColor = self.color_secondary
    }
    
    override func setupIndicator() {
        //
    }
    
    func circleViewDidTap(){
        if self.cell_btnDownload != nil {
            self.btnDownloadtap(sender: self.cell_btnDownload!)
        }
    }
    
    func configureCellWith(song: Song, type: SongType?, downloadInfo: DownloadInfo?) {
        //
        super.configureCellWith(_song: song)
        
        self.tmpSong = song
        
        if self.downloadInfo == nil {
            
            if let info = DataProxy.download.getDownloadInfo(itemSong: song, type: type){
                self.hiddenDownloadProgress(is_hidden: false)
                self.downloadInfo = info
                self.tmpSong = song
                self.progressHandler()
                
            }else{
                CoreData.getDownloadSessionData(itemSong: song, type: type, successBlock: { (downloadInfo) in
                    //
                    if let info = downloadInfo as? DownloadInfo{
                        self.hiddenDownloadProgress(is_hidden: false)
                        self.downloadInfo = info
                        self.tmpSong = song
                        self.progressHandler()
                        
                    }else{
                        self.hiddenDownloadProgress(is_hidden: true)
                    }
                    
                }) { (str_error) in
                    //fixme
                    
                }
            }
        }else{
            self.progressHandler()
        }
    }
    
    private func progressHandler(){
        
        if self.downloadInfo != nil && self.downloadInfo?.song != nil && self.downloadInfo?.type != nil {
            
            DataProxy.download.completeBlock = { (str_location, downloadInfo) in
                // download success
                let _song = self.downloadInfo?.song
                let _type = self.downloadInfo?.type
                
                
                _song?.updatePlayingOfflineLink(str_location: str_location)
                
                DataProxy.download.updateSongDownloadToDB(itemSong: _song, type: _type, successBlock: {
                    //fixme
                    print("song download success absolutly")
                    self.hiddenDownloadProgress(is_hidden: true)
                    self.downloadInfo = nil

                }, errorBlock: { (str_error) in
                    //fixme
                    print("song download update error \(str_error)")
                })
            }
            
            DataProxy.download.failBlock = { (str_error, moreInfo) in
                //fixme
                print("song download failBlock error \(str_error)")

            }
            
            DataProxy.download.progressDownloadBlock = { (currentDownloadValue) in
                
                print("current thread \(Thread.current) ------- progressValue \(currentDownloadValue)")
            
                var isAnimate = false
                
                if !isAnimate{
                    isAnimate = true
                    
                    self.circularProgressView.animate(toAngle: currentDownloadValue, duration: 0.2, completion: { (finish) in
                        //
                        if finish{
                            isAnimate = false
                        }
                    })
                }
            }
        }
    }
    

    @IBAction func btnDownloadtap(sender : AnyObject){
        
        if let info = DataProxy.download.getDownloadInfo(itemSong: self.tmpSong, type: self.tmpSong?.type){
            
            if info.song != nil && info.type != nil {
                DataProxy.download.cancelDownload(info.song!)
                CoreData.deleteDownloadSongInDB(type: info.type!, itemSong: info.song!, successBlock: {
                    //
                    print("delete song \(info.song!.name) in DB success")
                    if self.currentIndexPath != nil {
                        self.delegate_songDownload?.SongDownload_DidRemoveDownloadSongSuccess(at: self.currentIndexPath!)
                    }
                }, errorBlock: { (str_error) in
                    //
                    print("delete song \(info.song!.name) in DB error : \(str_error)")
                })
            }
        }
    }
    
    func hiddenDownloadProgress(is_hidden: Bool){
        if is_hidden{
            self.cell_duration?.isHidden = false
            self.cell_btnDownload?.isHidden = true
            self.circularProgressView.isHidden = true
            self.isDownloading = false
            
        }else{
            self.cell_duration?.isHidden = true
            self.cell_btnDownload?.isHidden = false
            self.circularProgressView.isHidden = false
            self.isDownloading = true
        }
    }
    
    @IBAction override func btnDeleteTap(sender : UIButton){
        
        if self.tmpSong != nil {
            
            if let playingSong = SharePlayer.playerController?.vmodel.getCurrentPlayingSong(), playingSong.id == self.tmpSong?.id && playingSong.type == self.tmpSong?.type{
                
                if let error = HttpUtility.makeNSerror(errorCode: -1, message: AppMixError.delete_songPlayingError.rawValue){
                    self.delegate_songDownload?.SongDownload_CantRemoveDownloadSong(error: error)
                }
                
                return
            }
            
            DataProxy.download.deleteFileFromDirectory(filePath: self.tmpSong?.mp3.playing_linkOffline ?? "", successBlock: {
                // if file delete success
                // delete song from DB
                CoreData.deleteDownloadSongInDB(type: self.tmpSong!.type, itemSong: self.tmpSong!, successBlock: {
                    //
                    print("delete song \(self.tmpSong!.name) in DB success")
                    if self.currentIndexPath != nil {
                        self.delegate_songDownload?.SongDownload_DidRemoveDownloadSongSuccess(at: self.currentIndexPath!)
                    }
                }, errorBlock: { (str_error) in
                    //
                    if let error = HttpUtility.makeNSerror(errorCode: -1, message: "delete song \(self.tmpSong!.name) in DB error : \(str_error)"){
                        self.delegate_songDownload?.SongDownload_CantRemoveDownloadSong(error: error)
                    }
                })
            }, errorBlock: { (str_error) in
                if let error = HttpUtility.makeNSerror(errorCode: -1, message: str_error){
                    self.delegate_songDownload?.SongDownload_CantRemoveDownloadSong(error: error)
                }
            })
            
        }
        

    }
}

