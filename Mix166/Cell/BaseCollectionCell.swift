//
//  BaseCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/7/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class BaseCollectionCell: UICollectionViewCell {
    
    
    internal var font_title: UIFont{
        return UIFont.fontSemiBold_medium()
    }
    internal var font_artist    = UIFont.fontRegular_medium()
    internal var font_category  = UIFont.fontRegular_medium()
    internal var font_duration  = UIFont.fontRegular_medium()
    
    internal var color_primary     = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    internal var color_secondary: UIColor{
        return UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
