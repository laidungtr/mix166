//
//  HorizontalCollectionViewCell.swift
//  Mix166
//
//  Created by ttiamap on 6/8/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

enum HorizontalType: String {
    case banner = "banner"
    case mixset = "mixset"
    case deejayOverlay = "deejayOverlay"
    case deejay = "deejay"
    case deejayHome = "deejayHome"
    case video = "video"
    case unknow = "uknow"
}

protocol HorizontalCollectionDelegate{
    func numberOfItemInSection(type: HorizontalType, parentSection: Int?) -> Int?
    func horizontal_objectItem(type: HorizontalType, indexPath: IndexPath, parentSection: Int?) -> AnyObject?
    func horizontal_shoudSelectedItem(type: HorizontalType, indexPath: IndexPath, parentSection: Int?) -> Bool
    func horizontal_shouldReloadData(type: HorizontalType, parentSection: Int?) -> Bool
}

class HorizontalCollectionViewCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var cell_collectionView: UICollectionView?
    
    let queueConcurrent = DispatchQueue(label: "HorizontalCollectionViewCell", attributes: .concurrent)
    
    var type: HorizontalType = .unknow{
        didSet{
            switch self.type {
            case .banner:
                self.cell_collectionView?.isPagingEnabled = true
                
            default:
                self.cell_collectionView?.isPagingEnabled = false
            }
            
        }
    }
    
    var isHightlightWhenSelected = false
    
    var delegate: HorizontalCollectionDelegate?{
        willSet{
            
            DispatchQueue.global(qos: .background).sync {
                if newValue == nil{
                    self.currentSelectedIndex = 0
                    self.delegate = newValue
                    self.cell_collectionView?.delegate = nil
                    self.cell_collectionView?.dataSource = nil
                    return
                }
                
                if self.cell_collectionView?.delegate != nil {
                    if self.delegate?.horizontal_shouldReloadData(type: self.type, parentSection: self.currentSection) ?? false{
                        
                        self.currentSelectedIndex = 0
                        self.cell_collectionView?.collectionViewLayout.invalidateLayout()
                        self.cell_collectionView?.performBatchUpdates({
                            self.cell_collectionView?.reloadSections(IndexSet(integer: 0))
                        }, completion: { (finish) in
                            //
                        })
                    }
                    
                }else{
                    self.cell_collectionView?.delegate = self
                    self.cell_collectionView?.dataSource = self
                }
            }
        }
    }
    
    var currentSection: Int?
    var currentSelectedIndex: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.registerNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func registerNib(){
        let cellNib = UINib.init(nibName: BannerCollectionCell.nameOfClass, bundle: nil)
        let cellNib2 = UINib.init(nibName: SongGridCollectionCell.nameOfClass, bundle: nil)
        let cellNib3 = UINib.init(nibName: ArtistGridCollectionCell.nameOfClass, bundle: nil)
        let cellNib4 = UINib.init(nibName: VideoGridCollectionCell.nameOfClass, bundle: nil)
        let cellNib5 = UINib.init(nibName: ArtistGridWithoutOverlayCollectionCell.nameOfClass, bundle: nil)
        
        
        self.cell_collectionView?.register(cellNib, forCellWithReuseIdentifier: BannerCollectionCell.nameOfClass)
        self.cell_collectionView?.register(cellNib2, forCellWithReuseIdentifier: SongGridCollectionCell.nameOfClass)
        self.cell_collectionView?.register(cellNib3, forCellWithReuseIdentifier:  ArtistGridCollectionCell.nameOfClass)
        self.cell_collectionView?.register(cellNib4, forCellWithReuseIdentifier: VideoGridCollectionCell.nameOfClass)
        self.cell_collectionView?.register(cellNib5, forCellWithReuseIdentifier: ArtistGridWithoutOverlayCollectionCell.nameOfClass)
    }
    
    //MARK - CollectionView Datasource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.delegate?.numberOfItemInSection(type: type, parentSection: self.currentSection) ?? 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch type {
        case .banner:
            return self.configureBannerCell(collectionView:collectionView, atIndexPath:indexPath)
            
        case .mixset:
            return self.configureMixsetCell(collectionView:collectionView, atIndexPath:indexPath)
            
        case .deejayOverlay:
            return self.configureArtistOverlayCell(collectionView:collectionView, atIndexPath:indexPath)
            
        case .deejay:
            return self.configureArtistCell(collectionView:collectionView, atIndexPath:indexPath)
            
        case .deejayHome:
            return self.configureArtistHomeCell(collectionView:collectionView, atIndexPath:indexPath)
            
        case .video:
            return self.configureVideoCell(collectionView:collectionView, atIndexPath:indexPath)
            
        default:
            return UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 0))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        if (self.delegate?.horizontal_shoudSelectedItem(type: self.type, indexPath: indexPath, parentSection: self.currentSection) ?? false){
            if self.isHightlightWhenSelected{
                
                let previousIndexPath = IndexPath.init(row: self.currentSelectedIndex, section: indexPath.section)
                self.currentSelectedIndex = indexPath.row
                
                self.refreshPrevious_SelectedIndexPath(indexPath: [previousIndexPath,indexPath])
            }
            return true
            
        }else{
            return false
        }
    }
    
    func refreshPrevious_SelectedIndexPath(indexPath: [IndexPath]){
        
        self.cell_collectionView?.collectionViewLayout.invalidateLayout()
        self.cell_collectionView?.performBatchUpdates({
            self.cell_collectionView?.reloadItems(at: indexPath)
        }, completion: { (finish) in
            //
        })
    }
    
    
    // Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch type {
        case .banner:
            return bannerHomeSize
            
        case .mixset:
            return CGSize.init(width: mixSize.width - 10, height: mixSize.height - 10)
            
        case .deejayOverlay:
            return deejayOverlaySize
            
        case .deejayHome:
            return deejayCircleHomeSize
            
        case .deejay:
            return CGSize.init(width: deejaySize.width - 10, height: deejaySize.height)
            
        case .video:
            return CGSize.init(width: videoSize.width - 10, height: videoSize.height)
            
        default:
            return CGSize.init(width: 0, height: 0)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch self.type {
        case .banner:
            return UIEdgeInsetsMake(0, 0, 0, 0)
        default:
            return UIEdgeInsetsMake(0, 10, 0, 10)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        switch type {
        case .banner:
            return 0
            
        default:
            return 10.0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        switch type {
        case .banner:
            return 0
            
        default:
            return 10.0
        }
    }
    
    
    // MARK: Banner
    func configureBannerCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:BannerCollectionCell.nameOfClass, for: indexPath) as? BannerCollectionCell{
            
            if Thread.isMainThread{
                queueConcurrent.async {
                    if let _song = self.delegate?.horizontal_objectItem(type: .banner, indexPath: indexPath, parentSection: self.currentSection) as? Song{
                        cell.configureWith(itemSong: _song)
                    }
                }
            }else{
                if let _song = self.delegate?.horizontal_objectItem(type: .banner, indexPath: indexPath, parentSection: self.currentSection) as? Song{
                    cell.configureWith(itemSong: _song)
                }
            }
            
            return cell
        }
        
        return UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 0))
    }
    
    // MARK: Mixset
    func configureMixsetCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SongGridCollectionCell.nameOfClass, for: indexPath) as? SongGridCollectionCell{
            
            if Thread.isMainThread{
                queueConcurrent.async {
                    if let _song = self.delegate?.horizontal_objectItem(type: .mixset, indexPath: indexPath, parentSection: self.currentSection) as? Song{
                        cell.configureCellWith(_song: _song)
                    }
                }
            }else{
                if let _song = self.delegate?.horizontal_objectItem(type: .mixset, indexPath: indexPath, parentSection: self.currentSection) as? Song{
                    cell.configureCellWith(_song: _song)
                }
            }
            return cell
        }
        
        return UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 0))
    }
    
    // MARK: Artist
    func configureArtistOverlayCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:ArtistGridCollectionCell.nameOfClass, for: indexPath) as? ArtistGridCollectionCell{
            
            if Thread.isMainThread{
                queueConcurrent.async {
                    if let dj = self.delegate?.horizontal_objectItem(type: .deejayOverlay, indexPath: indexPath, parentSection: self.currentSection) as? Artist{
                        cell.configureCellWith(_artist: dj, enableFollow: false)
                    }
                }
            }else{
                if let dj = self.delegate?.horizontal_objectItem(type: .deejayOverlay, indexPath: indexPath, parentSection: self.currentSection) as? Artist{
                    cell.configureCellWith(_artist: dj, enableFollow: false)
                }
            }
            
            return cell
        }
        
        return UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 0))
    }
    
    func configureArtistCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:ArtistGridWithoutOverlayCollectionCell.nameOfClass, for: indexPath) as? ArtistGridWithoutOverlayCollectionCell{
            
            if Thread.isMainThread{
                queueConcurrent.async {
                    if let dj = self.delegate?.horizontal_objectItem(type: .deejay, indexPath: indexPath, parentSection: self.currentSection) as? Artist{
                        cell.isHightLightWhenSelected = self.isHightlightWhenSelected
                        cell.configureCellWith(_artist: dj, enableFollow: false)
                        
                        if self.isHightlightWhenSelected{
                            cell.isSelected = indexPath.row == self.currentSelectedIndex
                        }
                    }
                }
            }else{
                if let dj = self.delegate?.horizontal_objectItem(type: .deejay, indexPath: indexPath, parentSection: self.currentSection) as? Artist{
                    cell.isHightLightWhenSelected = self.isHightlightWhenSelected
                    cell.configureCellWith(_artist: dj, enableFollow: false)
                    if self.isHightlightWhenSelected{
                        cell.isSelected = indexPath.row == self.currentSelectedIndex
                    }
                }
            }
            
            return cell
        }
        
        return UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 0))
    }
    
    func configureArtistHomeCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:ArtistGridWithoutOverlayCollectionCell.nameOfClass, for: indexPath) as? ArtistGridWithoutOverlayCollectionCell{
            
            if Thread.isMainThread{
                cell.cell_name?.font = UIFont.fontSemiBold_medium()
                
                queueConcurrent.async {
                    if let dj = self.delegate?.horizontal_objectItem(type: .deejay, indexPath: indexPath, parentSection: self.currentSection) as? Artist{
                        
                        cell.isHightLightWhenSelected = self.isHightlightWhenSelected
                        cell.configureCellWith(_artist: dj, enableFollow: false)
                        if self.isHightlightWhenSelected{
                            cell.isSelected = indexPath.row == self.currentSelectedIndex
                        }
                    }
                }
            }else{
                if let dj = self.delegate?.horizontal_objectItem(type: .deejay, indexPath: indexPath, parentSection: self.currentSection) as? Artist{
                    
                    cell.isHightLightWhenSelected = self.isHightlightWhenSelected
                    cell.configureCellWith(_artist: dj, enableFollow: false)
                    DispatchQueue.main.async() { () -> Void in
                        cell.cell_name?.font = UIFont.fontSemiBold_medium()
                    }
                    if self.isHightlightWhenSelected{
                        cell.isSelected = indexPath.row == self.currentSelectedIndex
                    }
                }
            }
            
            return cell
        }
        
        return UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 0))
    }
    
    // MARK: Video
    func configureVideoCell(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:VideoGridCollectionCell.nameOfClass, for: indexPath) as? VideoGridCollectionCell{
            
            if Thread.isMainThread{
                queueConcurrent.async {
                    if let _song = self.delegate?.horizontal_objectItem(type: .video, indexPath: indexPath, parentSection: self.currentSection) as? Song{
                        cell.configureCellWith(_song: _song)
                    }
                }
            }else{
                if let _song = self.delegate?.horizontal_objectItem(type: .video, indexPath: indexPath, parentSection: self.currentSection) as? Song{
                    cell.configureCellWith(_song: _song)
                }
            }
            
            return cell
        }
        
        return UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 0))
    }
    
    func superScrollViewDidScrolling(scrollView: UIScrollView){
        
        if self.cell_collectionView == nil {
            return
        }
        
        self.queueConcurrent.sync {
            // parallax scrolling zooming
            
            for cell in self.cell_collectionView!.visibleCells{
                if let parallaxCell = cell as? BannerCollectionCell{
                    if parallaxCell.currentSection == 0{
                        self.cell_collectionView?.sendSubview(toBack: parallaxCell)
                        parallaxCell.superScrollViewDidScrolling(scrollView: scrollView)
                    }
                }
            }
        }
    }
}
