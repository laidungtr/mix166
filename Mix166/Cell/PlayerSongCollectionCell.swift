//
//  PlayerSongCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/14/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class PlayerSongCollectionCell: BaseSongCollectionCell {
    
    @IBOutlet weak var layoutTopConsraint: NSLayoutConstraint?
    @IBOutlet weak var layoutLeftConsraint: NSLayoutConstraint?
    @IBOutlet weak var layoutBottomConsraint: NSLayoutConstraint?
    @IBOutlet weak var layoutRightConsraint: NSLayoutConstraint?
    
    @IBOutlet weak var cell_viewGradient: UIView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cell_viewGradient?.borderWithRadius(self.borderImageRadius())
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.layoutTopConsraint?.constant = 10.0
        self.layoutLeftConsraint?.constant = 10.0
        self.layoutBottomConsraint?.constant = 10.0
        self.layoutRightConsraint?.constant = 10.0
        
        if Thread.isMainThread{
            self.cell_image?.layoutIfNeeded()
        }else{
            DispatchQueue.main.async() { () -> Void in
                self.cell_image?.layoutIfNeeded()
            }
        }
    }
    
    override func borderImageCornerStyle() -> UIView.BorderCornerStyle {
        return .default
    }
    
    override func borderImageRadius() -> CGFloat {
        return 5.0
    }
    
    override func configureCellWith(_song: Song) {
        
        let size = self.cell_image?.frame.size
        
        if let url = URL.init(string: _song.imageCover.urlConvertWith(width: size?.width, height: size?.height)){
            self.cell_image?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
        }
    }
    
    func configureCellWith(_song: Song, smallImage enable: Bool){
        self.configureCellWith(_song: _song)
        self.layoutUpdateWithSmallImage(enable: enable)
    }
    
    func layoutUpdateWithSmallImage(enable: Bool){
        
        let constraint: CGFloat = enable ? 10.0 : 0.0
        let alpha: CGFloat = enable ? 0.7 : 0.0
        
        UIView.animate(withDuration: 0.2) {
            self.layoutTopConsraint?.constant = constraint
            self.layoutLeftConsraint?.constant = constraint
            self.layoutBottomConsraint?.constant = constraint
            self.layoutRightConsraint?.constant = constraint
            self.cell_image?.layoutIfNeeded()
            
            self.cell_viewGradient?.alpha = alpha
        }
    }


}
