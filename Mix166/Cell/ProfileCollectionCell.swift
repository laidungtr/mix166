//
//  ProfileCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 7/25/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ProfileCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var cell_lbName: UILabel?
    @IBOutlet weak var cell_imgAvatar: UIImageView?
    @IBOutlet weak var cell_imgCover: UIImageView?
    
    var font_title: UIFont{
        return UIFont.fontSemiBold_big()
    }
    
    internal var color_primary     = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.setupFont()
        self.setupImage()

    }
    
    func setupFont(){
        self.cell_lbName?.font = self.font_title
        self.cell_lbName?.textColor = self.color_primary
    }
    
    func setupImage(){
        DispatchQueue.main.async() { () -> Void in
            if self.cell_imgAvatar != nil {
                self.layoutIfNeeded()
                self.cell_imgAvatar?.borderWithRadius(self.cell_imgAvatar!.bounds.size.height/2)
            }
            
            if self.cell_imgCover != nil {
                self.cell_imgCover?.gradientFromTopToBottom(UIColor.clear.cgColor, bottomColor: UIColor.black.withAlphaComponent(0.6).cgColor, gradientFrame: self.cell_imgCover!.bounds)
            }
        }
    }
    
    func configure_userProfile(){
        if let info = DataProxy.user.userInfo{
            
            let size = self.cell_imgAvatar?.frame.size
            
            if let url = URL.init(string: info.avatar.urlConvertWith(width: size?.width, height: size?.height)){
                self.cell_imgAvatar?.sd_setImage(with: url, placeholderImage: image_userPlaceholder)
            }
            
            self.cell_lbName?.text = info.name
        }
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
