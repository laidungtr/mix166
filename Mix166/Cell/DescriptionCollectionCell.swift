//
//  ArtistDescriptionCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 8/8/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class DescriptionCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var cell_lbDescription: UILabel?
    
    private let colorText = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    private let fontText = UIFont.fontRegular_medium()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        self.cell_lbDescription?.preferredMaxLayoutWidth = screenWidth - 32.0
        self.cell_lbDescription?.font = fontText
        self.cell_lbDescription?.textColor = colorText
    }
    
    override func prepareForReuse() {
        self.cell_lbDescription?.text = ""
    }
    
    func configureCell(str_descriptionHTML: String, mainQueueEnable: Bool){
        
        if mainQueueEnable {
            DispatchQueue.main.async() { () -> Void in
                self.setDescriptionWithStrHTML(str_html: str_descriptionHTML)
            }
            
        }else{
            self.setDescriptionWithStrHTML(str_html: str_descriptionHTML)
        }
        
    }
    
    func setDescriptionWithStrHTML(str_html: String){
        
        if str_html.contains("<br>"){
            if let htmlData = str_html.data(using: String.Encoding.unicode) {
                do {
                    let attributedText = try NSMutableAttributedString(data: htmlData, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                    
                    attributedText.addAttributes([NSForegroundColorAttributeName : self.colorText], range: NSRange.init(location: 0, length: attributedText.length))
                    attributedText.addAttributes([NSFontAttributeName : fontText], range: NSRange.init(location: 0, length: attributedText.length))
                    
                    self.cell_lbDescription?.attributedText = attributedText
                    
                } catch let e as NSError {
                    print("Couldn't translate \(str_html): \(e.localizedDescription) ")
                }
            }
        }else{
            self.cell_lbDescription?.text = str_html
        }
    }
    
    override var intrinsicContentSize: CGSize{
        var size = self.cell_lbDescription?.intrinsicContentSize
        
        // add to intrisic content size of label
        size?.height +=  32.0
        
        return size!
    }
    
}
