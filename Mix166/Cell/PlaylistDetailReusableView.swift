//
//  PlaylistDetailReusableView.swift
//  Mix166
//
//  Created by ttiamap on 8/8/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class PlaylistDetailReusableView: UICollectionReusableView {
    
    @IBOutlet weak var cell_lbTitle: UILabel?
    @IBOutlet weak var cell_btnShuffle: UIButton?
    @IBOutlet weak var cell_imgCover: UIImageView?
    
    internal var font_primary: UIFont{
        return UIFont.fontRegular_big()
    }
    
    internal var font_secondary: UIFont{
        return UIFont.fontRegular_medium()
    }

    
    internal var color_primary     = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    internal var color_secondary    = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.setupFont()
        self.setupColor()
        self.setupBtnShuffle()
        
        if Thread.isMainThread{
            self.cell_imgCover?.layoutIfNeeded()
        }else{
            DispatchQueue.main.async() { () -> Void in
                self.cell_imgCover?.layoutIfNeeded()
            }
        }
        
    }
    
    private func setupFont(){
        self.cell_lbTitle?.font = self.font_primary
        self.cell_btnShuffle?.titleLabel?.font = self.font_primary
    }
    
    private func setupColor(){
        self.cell_lbTitle?.textColor = self.color_primary
        self.cell_btnShuffle?.setTitleColor(self.color_primary, for: .normal)
    }
    
    private func setupBtnShuffle(){
        let topLeft = "4023C5"
        let bottomRight = "C61551"
        if self.cell_btnShuffle == nil {
            return
        }

        DispatchQueue.main.async() { () -> Void in
            self.layoutIfNeeded()
            self.cell_imgCover?.borderWithRadius(2.0)
            self.cell_btnShuffle?.borderWithRadius(self.cell_btnShuffle!.frame.size.height/2)
            self.cell_btnShuffle?.gradientFromLeftToRight(UIColor.init(hexaString: topLeft).cgColor, bottomColor: UIColor.init(hexaString: bottomRight).cgColor, gradientFrame: self.cell_btnShuffle!.bounds)
        }
    }
    
    func configure_playlist(itemPlaylist: Playlist){
        
//        self.setImageWith(str_url: itemPlaylist.imageCover)
        
        if self.cell_imgCover != nil {
            itemPlaylist.mergesCoverImage(mainImageSize: self.cell_imgCover!.frame.size, highQuality: true) { (image) in
                self.cell_imgCover?.image = image
            }
        }
        
        if Thread.isMainThread{
            self.cell_lbTitle?.text = itemPlaylist.name
        }else{
            DispatchQueue.main.async() { () -> Void in
                self.cell_lbTitle?.text = itemPlaylist.name
            }
        }
    }
    
    internal func setImageWith(str_url: String){
        
        let size = self.cell_imgCover?.frame.size
        
        if let url = URL.init(string: str_url.urlConvertWith(width: size?.width, height: size?.height)){
            self.cell_imgCover?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
        }
    }
    
}
