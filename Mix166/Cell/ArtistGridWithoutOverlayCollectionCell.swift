//
//  ArtistGridWithoutOverlayCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 8/8/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ArtistGridWithoutOverlayCollectionCell: BaseArtistCollectionCell {
    
    override var isSelected: Bool{
        didSet{
            if self.isHightLightWhenSelected{
                if Thread.isMainThread{
                    if self.isSelected{
                        self.alpha = 1
                    }else{
                        self.alpha = 0.3
                    }
                }else{
                    if self.isSelected{
                        DispatchQueue.main.async() { () -> Void in
                            self.alpha = 1
                        }
                    }else{
                        DispatchQueue.main.async() { () -> Void in
                            self.alpha = 0.3
                        }
                    }
                }
            }
        }
    }
    
    var isHightLightWhenSelected = false
    
    override var font_title: UIFont{
        return UIFont.fontRegular_small()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        DispatchQueue.main.async() { () -> Void in
            if self.cell_image != nil {
                self.cell_image?.borderWithRadius(self.cell_image!.bounds.height/2)
            }
        }
    }
    
    

}
