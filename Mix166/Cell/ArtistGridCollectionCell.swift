//
//  ArtistGridCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/7/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ArtistGridCollectionCell: BaseArtistCollectionCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutArtistImage()
        
    }
    
    internal func layoutArtistImage(){
        DispatchQueue.main.async() { () -> Void in
            self.cell_image?.addOverlay(xOffset: deejayOverlaySize.width/2, yOffset: deejayOverlaySize.width/2, radius: deejayOverlaySize.width/2)
        }
    }

}
