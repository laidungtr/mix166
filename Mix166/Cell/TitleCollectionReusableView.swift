//
//  TitleCollectionReusableView.swift
//  Mix166
//
//  Created by ttiamap on 6/7/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

protocol TitleCollectionHeaderDelegate {
    func titleCollectionHeader_DidTapBtnMore(sender: UIButton, headerSection: Int?)
}

class TitleCollectionReusableView: UICollectionReusableView {
    
    var font_title: UIFont{
        return UIFont.fontBold_extraLargeXX()
    }
    
    var font_btnMore: UIFont{
        return UIFont.fontRegular_medium()
    }
    
    var color_title: UIColor{
        return UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    }
    
    var color_btnMore: UIColor{
        return UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    }
    
    
    @IBOutlet weak var cell_title: UILabel?
    @IBOutlet weak var cell_btnMore: UIButton?
    
    @IBOutlet weak var layoutLeadingContraint: NSLayoutConstraint?
    
    var headerSection: Int?
    var delegate: TitleCollectionHeaderDelegate?
    
    var leadingValue: CGFloat = 10.0{
        didSet{
            self.layoutLeadingContraint?.constant = self.leadingValue
            if Thread.isMainThread{
                self.layoutIfNeeded()
            }else{
                DispatchQueue.main.async() { () -> Void in
                    self.layoutIfNeeded()
                }
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.setupFont()
        self.setupColor()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.cell_title?.text = ""
    }
    
    private func setupFont(){
        self.cell_title?.font = self.font_title
        self.cell_btnMore?.titleLabel?.font = font_btnMore
    }
    
    private func setupColor(){
        self.cell_title?.textColor = self.color_title
        self.cell_btnMore?.setTitleColor(color_btnMore, for: .normal)
    }
    
    func configureHeader(str_title: String){
        
        if Thread.isMainThread{
            self.cell_btnMore?.isHidden = ( self.headerSection == nil )
            self.cell_title?.text = str_title
        }else{
            DispatchQueue.main.async() { () -> Void in
                self.cell_btnMore?.isHidden = ( self.headerSection == nil )
                self.cell_title?.text = str_title
            }
        }
    }
    
    @IBAction func btnSeeMore(sender : UIButton){
        self.delegate?.titleCollectionHeader_DidTapBtnMore(sender: sender, headerSection: self.headerSection)
    }
    
}
