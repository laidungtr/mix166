//
//  BannerCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/8/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import SDWebImage

class BannerCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var layoutTopConstraint: NSLayoutConstraint?
    @IBOutlet weak var layoutBottomConstraint: NSLayoutConstraint?
    @IBOutlet weak var layoutLeadingConstraint: NSLayoutConstraint?
    @IBOutlet weak var layoutTrailingConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var cell_imageView: UIImageView?
    @IBOutlet weak var cell_title: UILabel?
    @IBOutlet weak var cell_imageDescription: UIImageView?
    @IBOutlet weak var cell_shadowView: UIView?
    @IBOutlet weak var cell_blurView: UIView!
    
    var currentSection: Int = 0
    
    var font_title: UIFont{
        return UIFont.fontRegular_big()
    }
    
    internal var color_primary     = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.cell_title?.font = self.font_title
        self.cell_title?.textColor = self.color_primary
        
        self.cell_blurView?.gradientFromTopToBottom(UIColor.black.withAlphaComponent(0.0).cgColor, bottomColor: UIColor.black.withAlphaComponent(0.8).cgColor, gradientFrame: CGRect.init(x: 0, y: 0, width: screenWidth, height: bannerHomeSize.height/3))
        
        self.cell_imageDescription?.borderWithRadius(4.0)
        self.cell_shadowView?.borderRadius(radius: 4.0, pathFillColor: .clear, shadownColor: UIColor.white.withAlphaComponent(0.1), shadownHeight: 1.0, shadownWidth: 1.0)

        DispatchQueue.main.async() { () -> Void in
            self.cell_imageView?.layoutIfNeeded()
            self.cell_imageDescription?.layoutIfNeeded()
        }
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func configureWith(itemSong: Song){
        
        if Thread.isMainThread{
            let size = self.cell_imageView?.frame.size
            
            if let url = URL.init(string: itemSong.image_coverBanner.urlConvertWith(width: size?.width, height: size?.height)){
                self.cell_imageView?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
            }
            
            let sizeDescription = self.cell_imageDescription?.frame.size
            
            if let url = URL.init(string: itemSong.artists.first?.image_cover.urlConvertWith(width: sizeDescription?.width, height: sizeDescription?.height) ?? ""){
                self.cell_imageDescription?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
            }
            
            self.cell_title?.text = itemSong.description_detail
        }else{
            DispatchQueue.main.async() { () -> Void in
                let size = self.cell_imageView?.frame.size
                
                if let url = URL.init(string: itemSong.image_coverBanner.urlConvertWith(width: size?.width, height: size?.height)){
                    self.cell_imageView?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
                }
                
                let sizeDescription = self.cell_imageDescription?.frame.size
                
                if let url = URL.init(string: itemSong.artists.first?.image_cover.urlConvertWith(width: sizeDescription?.width, height: sizeDescription?.height) ?? ""){
                    self.cell_imageDescription?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
                }
                
                self.cell_title?.text = itemSong.description_detail
            }
        }
    }
    
    func configureImageWithUrl(str_url: String){
        
        let size = self.cell_imageView?.frame.size
        
        if let url = URL.init(string: str_url.urlConvertWith(width: size?.width, height: size?.height)){
            self.cell_imageView?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
        }
    }
    
    func superScrollViewDidScrolling(scrollView: UIScrollView){
        let offsetY = scrollView.contentOffset.y
        
        if offsetY >= 0{
            self.layoutTopConstraint?.constant = offsetY/2
            self.layoutTrailingConstraint?.constant = 0
            self.layoutLeadingConstraint?.constant = 0
            self.layoutBottomConstraint?.constant = -offsetY/2
            
        }else{
            self.layoutTopConstraint?.constant = offsetY
            self.layoutTrailingConstraint?.constant = offsetY
            self.layoutLeadingConstraint?.constant = offsetY
            self.layoutBottomConstraint?.constant = offsetY
        }
    }
}
