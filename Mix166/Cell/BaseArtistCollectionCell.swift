//
//  BaseArtistCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/7/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

protocol ArtistCollectionCellDelegate {
    func artistCellDelegate_DidTapFollowBtn(sender: UIButton, object: Artist)
}

class BaseArtistCollectionCell: UICollectionViewCell {
    
    internal var font_title: UIFont{
        return UIFont.fontSemiBold_medium()
    }
    internal var font_artist    = UIFont.fontRegular_medium()
    internal var font_category  = UIFont.fontRegular_medium()
    internal var font_duration  = UIFont.fontRegular_medium()
    
    internal var color_primary     = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    internal var color_secondary: UIColor{
        return UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    }
    
    
    @IBOutlet weak var cell_image: UIImageView?
    @IBOutlet weak var cell_name: UILabel?
    @IBOutlet weak var cell_followersNumber: UILabel?
    @IBOutlet weak var cell_followingNumber: UILabel?
    @IBOutlet weak var cell_playingNumber: UILabel?
    @IBOutlet weak var cell_btnFollowing: UIButton?
    @IBOutlet weak var layoutBtnFollowTrailingConstraint: NSLayoutConstraint?
    
    let font_follow: UIFont = UIFont.fontSemiBold_medium()
    
    var delegate: ArtistCollectionCellDelegate?
    internal var itemArtist: Artist?
    
    var isEnableFollowBtn: Bool = false {
        didSet{
            if Thread.isMainThread{
                if self.isEnableFollowBtn {
                    self.cell_btnFollowing?.isHidden = false
                }else{
                    self.cell_btnFollowing?.isHidden = true
                }
            }else{
                DispatchQueue.main.async() { () -> Void in
                    
                    if self.isEnableFollowBtn {
                        self.cell_btnFollowing?.isHidden = false
                    }else{
                        self.cell_btnFollowing?.isHidden = true
                    }
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
        self.setupColor()

        DispatchQueue.main.async() { () -> Void in
            self.layoutIfNeeded()
            self.cell_btnFollowing?.setupBtnFollow()
        }
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.clearLayout()
    }
    
    private func clearLayout(){
        self.cell_image?.image = nil
        self.cell_name?.text = ""
        self.cell_followersNumber?.text = ""
    }
    
    internal func setupFont(){
        self.cell_name?.font = self.font_title
        self.cell_followersNumber?.font = self.font_artist
        self.cell_followingNumber?.font = self.font_artist
        self.cell_btnFollowing?.titleLabel?.font = self.font_follow
    }
    
    internal func setupColor(){
        self.cell_name?.textColor = self.color_primary
        self.cell_followersNumber?.textColor = self.color_secondary
        self.cell_followingNumber?.textColor = self.color_secondary
        self.cell_btnFollowing?.setTitleColor(UIColor.init(hexaString: AppColor.appRedColor.rawValue), for: .normal)
    }
    
    func configureCellWith(_artist: Artist, enableFollow: Bool){
        
        self.isEnableFollowBtn = enableFollow
        self.itemArtist = _artist
        if Thread.isMainThread{
            let size = self.cell_image?.frame.size
            
            if let url = URL.init(string: _artist.image_cover.urlConvertWith(width: size?.width, height: size?.height)){
                self.cell_image?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
            }
            
            self.cell_name?.text = _artist.alias
            self.cell_btnFollowing?.isSelected = _artist.isFollow
        }else{
            DispatchQueue.main.async() { () -> Void in
                let size = self.cell_image?.frame.size
                
                if let url = URL.init(string: _artist.image_cover.urlConvertWith(width: size?.width, height: size?.height)){
                    self.cell_image?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
                }
                self.cell_name?.text = _artist.alias
                self.cell_btnFollowing?.isSelected = _artist.isFollow
            }
        }
        
    }
    
    @IBAction func btnFollowTap(sender : UIButton){
        if self.itemArtist != nil{
            self.delegate?.artistCellDelegate_DidTapFollowBtn(sender: sender, object: self.itemArtist!)
        }
    }
}
