//
//  BaseCollectionViewCell.swift
//  Mix166
//
//  Created by ttiamap on 6/6/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import SDWebImage
import NVActivityIndicatorView

enum panDirection {
    case left
    case right
    case up
    case down
    case unknow
}

protocol SongCellDelegate {
    func songCellDelegate_BtnDeleteTap(sender: UIButton?, indexPath: IndexPath)
    func songCellDelegate_BtnMoreTap(sender: UIButton, indexPath: IndexPath)
}

class BaseSongCollectionCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    internal var font_title: UIFont{
        return UIFont.fontSemiBold_medium()
    }
    internal var font_artist    = UIFont.fontRegular_medium()
    internal var font_category  = UIFont.fontRegular_medium()
    internal var font_duration: UIFont{
        return UIFont.fontRegular_medium()
    }
    
    internal var color_primary     = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    internal var color_secondary: UIColor{
        return UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    }
    
    internal var color_duration: UIColor{
        return UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    }
    

    @IBOutlet weak var cell_image: UIImageView?
    @IBOutlet weak var cell_title: UILabel?
    @IBOutlet weak var cell_artist: UILabel?
    @IBOutlet weak var cell_description: UILabel?
    @IBOutlet weak var cell_category: UILabel?
    @IBOutlet weak var cell_duration: UILabel?
    @IBOutlet weak var cell_btnMore: UIButton?
    @IBOutlet weak var cell_viewContain: UIView?
    @IBOutlet weak var cell_btnDelete: UIButton?
    @IBOutlet weak var cell_lbRank: UILabel?
    @IBOutlet weak var cell_imageRank: UIImageView?
    
    @IBOutlet weak var layoutCellLeadingConstraint: NSLayoutConstraint?
    @IBOutlet weak var layoutImageLeadingConstraint: NSLayoutConstraint?
    
    private var direction: panDirection = .unknow
    private var storeMovingPoint = CGPoint.init(x: 0, y: 0)
    internal var isDownloading = false
    internal var indicator_songPlaying: NVActivityIndicatorView?
    
    var delegate: SongCellDelegate?
    var currentIndexPath: IndexPath?
    
    var isEnableSwipeToDelete: Bool = false{
        didSet{
            if self.isEnableSwipeToDelete == true{
                self.setupSwipeToDelete()
                
            }else{
                self.layoutCellLeadingConstraint = nil
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
        self.setupColor()
        self.borderImageLayout(radius: self.borderImageRadius())
        self.clearLayout()
        self.setupIndicator()
    }
    
    func setupIndicator(){
        self.indicator_songPlaying = NVActivityIndicatorView(frame: CGRect(x: trackSize.width - 30.0, y: (trackSize.height - 10) / 2, width: 15, height: 15), type: NVActivityIndicatorType.lineScalePulseOut, color: UIColor.init(hexaString: AppColor.appRedColor.rawValue), padding: 0)
        
        self.cell_viewContain?.addSubview(self.indicator_songPlaying!)
        self.cell_viewContain?.bringSubview(toFront: self.indicator_songPlaying!)
        
        self.indicator_songPlaying?.startAnimating()
        
        self.indicator_songPlaying?.isHidden = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.refreshConstraint()
        self.clearLayout()
    }
    
    internal func borderImageCornerStyle() -> BorderCornerStyle{
        return .default
    }
    
    internal func borderImageRadius() -> CGFloat{
        return 2.0
    }
    
    private func setupSwipeToDelete(){
        if !self.isEnableSwipeToDelete{
            return
        }
        
        self.cell_viewContain?.isUserInteractionEnabled = true
        let panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(self.panGestureRecognizer(gesture:)))
        panGesture.delegate = self
        self.cell_viewContain?.addGestureRecognizer(panGesture)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc func panGestureRecognizer(gesture: UIPanGestureRecognizer){
        
        
        if self.cell_btnDelete == nil || self.isDownloading || !self.isEnableSwipeToDelete {
            return
        }
        
        switch gesture.state {
        case .began:
            //
            let velocity = gesture.velocity(in: gesture.view)
            
            let isVerticalGesture = fabs(velocity.y) > fabs(velocity.x)
            
            if isVerticalGesture{
                // vertical
                
                
            }else{
                if gesture.view == self.cell_viewContain{
                    // horizontal
                    if velocity.x > 0{
                        // right
                        self.direction = .right
                    }else{
                        //left
                        self.direction = .left
                    }
                }
            }
            
        case .changed:
            //
            if self.direction == .right || self.direction == .left{
                
                let translation = gesture.translation(in: self.cell_viewContain)
                
                var movingPoint = CGPoint.init(x: self.layoutCellLeadingConstraint!.constant, y: 0)
                movingPoint.x += translation.x
                
                if movingPoint.x > 0{
                    movingPoint.x = 0
                }else if movingPoint.x < -self.cell_btnDelete!.bounds.width{
                    movingPoint.x = -self.cell_btnDelete!.bounds.width
                }
                
                self.layoutCellLeadingConstraint?.constant = movingPoint.x
                
                if Thread.isMainThread{
                    self.layoutIfNeeded()
                }
                
                self.storeMovingPoint = movingPoint
                gesture.setTranslation(CGPoint.zero, in: self.cell_viewContain)
            }
            break
            
            
        case .ended:
            //
            switch self.direction {
            case .left:
                //
                UIView.animate(withDuration: 0.2, animations: {
                    //
                    self.layoutCellLeadingConstraint?.constant = -self.cell_btnDelete!.bounds.width
                    self.layoutIfNeeded()
                    
                }, completion: { (finish) in
                    //
                })
                break
            case .right:
                //
                UIView.animate(withDuration: 0.2, animations: {
                    //
                    self.refreshConstraint()
                    
                }, completion: { (finish) in
                    //
                })
                break
                
                
            default:
                break
            }
            
            self.storeMovingPoint = CGPoint.init(x: 0, y: 0)
            self.direction = .unknow
            break
            
        default:
            
            self.direction = .unknow
        }
    }
    
    private func refreshConstraint(){
        if self.layoutCellLeadingConstraint?.constant != 0 {
            self.layoutCellLeadingConstraint?.constant = 0
            if Thread.isMainThread{
                self.layoutIfNeeded()
            }else{
                DispatchQueue.main.async() { () -> Void in
                    self.layoutIfNeeded()
                }
            }
        }
    }
    
    private func clearLayout(){
        self.cell_title?.text = ""
        self.cell_artist?.text = ""
        self.cell_category?.text = ""
        self.cell_duration?.text = ""
        self.cell_description?.text = ""
        self.cell_image?.image = nil
    }
    
    internal func setupFont(){
        
        self.cell_lbRank?.font = UIFont.fontSemiBold_big()
        self.cell_description?.font = self.font_artist
        self.cell_title?.font = self.font_title
        self.cell_artist?.font = self.font_artist
        self.cell_category?.font = self.font_category
        self.cell_duration?.font = self.font_duration
    }
    
    private func setupColor(){
        self.cell_title?.textColor = self.color_primary
        self.cell_artist?.textColor = self.color_secondary
        self.cell_description?.textColor = self.color_secondary
        self.cell_category?.textColor = self.color_secondary
        self.cell_duration?.textColor = self.color_duration
    }
    
    private func borderImageLayout(radius: CGFloat){
        DispatchQueue.main.async() { () -> Void in
            self.layoutIfNeeded()
            self.cell_image?.borderRadius(radius, frame: self.cell_image?.bounds, borderStyle: self.borderImageCornerStyle())
        }
    }
    
    internal func configureCellWith(_song: Song){
        DispatchQueue.global(qos: .background).sync {
            
            let allArtist = _song.all_artistName
            let allGenres = _song.all_genresName
            
            self.setImageWith(str_url: _song.imageCover)
            
            if Thread.isMainThread{
                self.cell_title?.text = _song.name
                self.cell_artist?.text = allArtist
                self.cell_duration?.text = _song.mp3.duration.length > 0 ? _song.mp3.duration : _song.qualities.duration
                self.cell_category?.text = allGenres
                
                self.layer.shouldRasterize = true
                self.layer.rasterizationScale = UIScreen.main.scale
                
            }else{
                DispatchQueue.main.async() { () -> Void in
                    self.cell_title?.text = _song.name
                    self.cell_artist?.text = allArtist
                    self.cell_duration?.text = _song.mp3.duration.length > 0 ? _song.mp3.duration : _song.qualities.duration
                    self.cell_category?.text = allGenres
                    
                    self.layer.shouldRasterize = true
                    self.layer.rasterizationScale = UIScreen.main.scale
                }
            }
        }
    }
    
    internal func configureCellTrackMasupWith(_song: Song_Masup, enableHightLight: Bool){
        DispatchQueue.global(qos: .background).sync {
            
            if Thread.isMainThread{
                self.cell_title?.text = _song.name
                self.cell_artist?.text = _song.name_artist
                self.cell_duration?.text = _song.location_duration
                self.cell_btnMore?.isHidden = true
                self.indicator_songPlaying?.isHidden = enableHightLight
                
                self.layoutIfNeeded()
                self.layoutImageLeadingConstraint?.constant = -(self.cell_image!.frame.size.width)
                self.layoutIfNeeded()
                
            }else{
                DispatchQueue.main.async() { () -> Void in
                    self.cell_title?.text = _song.name
                    self.cell_artist?.text = _song.name_artist
                    self.cell_duration?.text = _song.location_duration
                    self.cell_btnMore?.isHidden = true
                    self.indicator_songPlaying?.isHidden = enableHightLight
                    
                    self.layoutIfNeeded()
                    self.layoutImageLeadingConstraint?.constant = -(self.cell_image!.frame.size.width)
                    self.layoutIfNeeded()
                }
            }
            
            self.layer.shouldRasterize = true
            self.layer.rasterizationScale = UIScreen.main.scale
        }
    }
    
    internal func configureCellWithHighLightEnable(_song: Song){
        DispatchQueue.global(qos: .background).sync {
            
            let allArtist = _song.all_artistName
            let allGenres = _song.all_genresName
            
            self.setImageWith(str_url: _song.imageCover)
            
            if Thread.isMainThread{
                self.cell_title?.text = _song.name
                self.cell_artist?.text = allArtist
                self.cell_duration?.text = _song.mp3.duration.length > 0 ? _song.mp3.duration : _song.qualities.duration
                self.cell_category?.text = allGenres
                
                if SharePlayer.currentPlayingSong?.id == _song.id &&
                    SharePlayer.currentPlayingSong?.name == _song.name &&
                    self.indicator_songPlaying != nil{
                    
                    self.cell_btnMore?.isHidden = true
                    self.cell_category?.isHidden = true
                    self.indicator_songPlaying?.isHidden = false
                    
                }else{
                    self.cell_btnMore?.isHidden = false
                    self.cell_category?.isHidden = false
                    self.indicator_songPlaying?.isHidden = true
                }
                
            }else{
                DispatchQueue.main.async() { () -> Void in
                    self.cell_title?.text = _song.name
                    self.cell_artist?.text = allArtist
                    self.cell_duration?.text = _song.mp3.duration.length > 0 ? _song.mp3.duration : _song.qualities.duration
                    self.cell_category?.text = allGenres
                    
                    if SharePlayer.currentPlayingSong?.id == _song.id &&
                        SharePlayer.currentPlayingSong?.name == _song.name &&
                        self.indicator_songPlaying != nil{
                        
                        self.cell_btnMore?.isHidden = true
                        self.cell_category?.isHidden = true
                        self.indicator_songPlaying?.isHidden = false
                        
                    }else{
                        self.cell_btnMore?.isHidden = false
                        self.cell_category?.isHidden = false
                        self.indicator_songPlaying?.isHidden = true
                    }
                }
            }
            
            self.layer.shouldRasterize = true
            self.layer.rasterizationScale = UIScreen.main.scale
        }
    }
    
    func configureCellWith(_song: Song, rankIndex: Int) {
        DispatchQueue.global(qos: .background).sync {
            
            
            let allArtist = _song.all_artistName
            let allGenres = _song.all_genresName
            
            self.setImageWith(str_url: _song.imageCover)
            
            if Thread.isMainThread{
                self.cell_title?.text = _song.name
                self.cell_artist?.text = allArtist
                self.cell_duration?.text = _song.mp3.duration.length > 0 ? _song.mp3.duration : _song.qualities.duration
                self.cell_category?.text = allGenres
                self.cell_lbRank?.text = String(rankIndex + 1)

                
                switch _song.status {
                case 0,2:
                    //
                    self.setRankStatus(colorType: .rankUp, imgType: .imgRankUp)
                    break
                    
                case 3:
                    //
                    self.setRankStatus(colorType: .rankDown, imgType: .imgRankDown)
                    break
                    
                default:
                    //
                    self.setRankStatus(colorType: .rankKeep, imgType: .imgRankKeep)
                    break
                }
                
                self.layer.shouldRasterize = true
                self.layer.rasterizationScale = UIScreen.main.scale
                
            }else{
                DispatchQueue.main.async() { () -> Void in
                    self.cell_title?.text = _song.name
                    self.cell_artist?.text = allArtist
                    self.cell_duration?.text = _song.mp3.duration.length > 0 ? _song.mp3.duration : _song.qualities.duration
                    self.cell_category?.text = allGenres
                    self.cell_lbRank?.text = String(rankIndex + 1)
                    
                    switch _song.status {
                    case 0,2:
                        //
                        self.setRankStatus(colorType: .rankUp, imgType: .imgRankUp)
                        break
                        
                    case 3:
                        //
                        self.setRankStatus(colorType: .rankDown, imgType: .imgRankDown)
                        break
                        
                    default:
                        //
                        self.setRankStatus(colorType: .rankKeep, imgType: .imgRankKeep)
                        break
                    }
                    
                    self.layer.shouldRasterize = true
                    self.layer.rasterizationScale = UIScreen.main.scale
                }
            }
        }
    }
    
    private func setRankStatus(colorType: ColorText, imgType: Image_Name){
        self.cell_lbRank?.textColor = UIColor.init(hexaString: colorType.rawValue)
        self.cell_imageRank?.image = UIImage.init(named: imgType.rawValue)
    }
    
    internal func setImageWith(str_url: String){
        if Thread.isMainThread{
            let size = self.cell_image?.frame.size
            
            if let url = URL.init(string: str_url.urlConvertWith(width: size?.width, height: size?.height)){
                self.cell_image?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
            }
        }else{
            DispatchQueue.main.async() { () -> Void in
                let size = self.cell_image?.frame.size
                
                if let url = URL.init(string: str_url.urlConvertWith(width: size?.width, height: size?.height)){
                    self.cell_image?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
                }
            }
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnDeleteTap(sender : UIButton){
        if self.currentIndexPath != nil{
            self.delegate?.songCellDelegate_BtnDeleteTap(sender: sender,indexPath: self.currentIndexPath!)
        }
    }
    
    @IBAction func btnMoreTap(sender : UIButton){
        if self.currentIndexPath != nil {
            self.delegate?.songCellDelegate_BtnMoreTap(sender: sender, indexPath: self.currentIndexPath!)
        }
    }
    
}
