//
//  PlayListCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/26/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import SDWebImage

class PlayListCollectionCell: BaseSongCollectionCell {
    
    @IBOutlet weak var cell_viewGradient: UIView?
    
    override var font_title: UIFont{
        return UIFont.fontRegular_big()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setLayout()
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.cell_title?.textColor = color_primary
    }
    
    func setLayout(){
        
        let topLeft = "c50c2f"
        let bottomRight = "5913a1"
        self.cell_image?.image = UIImage.init(named: Image_Name.ic_add.rawValue)
        DispatchQueue.main.async() { () -> Void in
            self.cell_viewGradient?.borderWithRadius(self.borderImageRadius())
            self.cell_viewGradient?.gradientFromTopRightToBottomLeft(UIColor.init(hexaString: topLeft).cgColor, bottomColor: UIColor.init(hexaString: bottomRight).cgColor, gradientFrame: self.cell_image!.bounds)
        }
    }
    
    func configureCellWithPlaylist(_playlist: Playlist, at indexPath: IndexPath){
        //        self.setImageWith(str_url: _playlist.imageCover)
        
        _playlist.mergesCoverImage(mainImageSize: CGSize.init(width: playlistSize.height, height: playlistSize.height), highQuality: true) { (image) in
            self.cell_image?.image = image
        }
        
        self.currentIndexPath = indexPath
        
        if Thread.isMainThread{
            self.cell_title?.text = _playlist.name
            self.cell_description?.text = _playlist.description_playlist
        }else{
            DispatchQueue.main.async() { () -> Void in
                self.cell_title?.text = _playlist.name
                self.cell_description?.text = _playlist.description_playlist
            }
        }
    }
    
    func configureCellWithAddPlaylist(){
        
        if Thread.isMainThread{
            self.cell_title?.textColor = UIColor.init(hexaString: AppColor.appRedColor.rawValue)
            self.cell_title?.text = "New Playlist..."
            self.cell_image?.contentMode = .center
            self.cell_image?.image = UIImage.init(named: Image_Name.ic_addBig.rawValue)
        }else{
            DispatchQueue.main.async() { () -> Void in
                
                self.cell_title?.textColor = UIColor.init(hexaString: AppColor.appRedColor.rawValue)
                self.cell_title?.text = "New Playlist..."
                self.cell_image?.contentMode = .center
                self.cell_image?.image = UIImage.init(named: Image_Name.ic_addBig.rawValue)
            }
        }
        
        
    }
    
    
    
}
