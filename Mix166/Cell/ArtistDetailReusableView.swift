//
//  ArtistDetailReusableView.swift
//  Mix166
//
//  Created by ttiamap on 8/11/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ArtistDetailReusableView: BaseArtistCollectionReusableView {
    
    @IBOutlet weak var layoutTopConstraint: NSLayoutConstraint?
    @IBOutlet weak var layoutBottomConstraint: NSLayoutConstraint?
    @IBOutlet weak var layoutLeadingConstraint: NSLayoutConstraint?
    @IBOutlet weak var layoutTrailingConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var cell_lbTitlePlay: UILabel?
    @IBOutlet weak var cell_lbTitleFollower: UILabel?
    @IBOutlet weak var cell_lbTitleFollowing: UILabel?
    
    @IBOutlet weak var cell_blurView: UIView!
    
    override var font_title: UIFont{
        return UIFont.fontBold_extraLargeXXXX()
    }
    
    var font_secondary = UIFont.fontSemiBold_medium()
    var font_titleSecondary = UIFont.fontSemiBold_medium()
    var currentSection: Int = 0
    
    override var color_secondary: UIColor{
        return UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue).withAlphaComponent(0.6)
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        DispatchQueue.main.async() { () -> Void in
            
            self.cell_blurView?.gradientFromTopToBottom(UIColor.black.withAlphaComponent(0.0).cgColor, bottomColor: UIColor.black.withAlphaComponent(1).cgColor, gradientFrame: CGRect.init(x: 0, y: 0, width: screenWidth, height: bannerSize.height))
        }
    }
    
    override internal func setupFont(){
        
        self.cell_lbTitlePlay?.font = self.font_titleSecondary
        self.cell_lbTitleFollower?.font = self.font_titleSecondary
        self.cell_lbTitleFollowing?.font = self.font_titleSecondary
        
        self.cell_name?.font = self.font_title
        
        self.cell_followersNumber?.font = self.font_secondary
        self.cell_followingNumber?.font = self.font_secondary
        self.cell_playingNumber?.font = self.font_secondary
        self.cell_btnFollowing?.titleLabel?.font = self.font_follow
    }
    
    override internal func setupColor(){
        
        self.cell_lbTitlePlay?.textColor = self.color_secondary
        self.cell_lbTitleFollower?.textColor = self.color_secondary
        self.cell_lbTitleFollowing?.textColor = self.color_secondary
        
        self.cell_name?.textColor = self.color_primary
        self.cell_followersNumber?.textColor = self.color_primary
        self.cell_followingNumber?.textColor = self.color_primary
        self.cell_playingNumber?.textColor = self.color_primary
    }
    
    override func configureCellWith(_artist: Artist, enableFollow: Bool){
        
        self.isEnableFollowBtn = enableFollow
        self.itemArtist = _artist

        if let url = URL.init(string: _artist.image_cover.urlConvertWith(width: screenWidth, height: screenWidth) ){
            self.cell_image?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
        }
        
        if Thread.isMainThread{
            self.cell_name?.text = _artist.alias
            self.cell_followersNumber?.text = _artist.like_count
            self.cell_followingNumber?.text =  _artist.followings
            self.cell_playingNumber?.text = _artist.listen_count
        }else{
        DispatchQueue.main.async() { () -> Void in
            self.cell_name?.text = _artist.alias
            self.cell_followersNumber?.text = _artist.like_count
            self.cell_followingNumber?.text =  _artist.followings
            self.cell_playingNumber?.text = _artist.listen_count
        }
        }
        self.checkArtistFollow(_artist: _artist)
    }
    
    func checkArtistFollow(_artist: Artist){
        var request = DataRequest(.Network)
        
        request.onSuccess = {(isLike,  moreInfo) in
            
            if let error = moreInfo as? NSError, error.code != 0{
                self.cell_btnFollowing?.isSelected = false
            }else{
                if let _bool = isLike as? Bool{
                    self.cell_btnFollowing?.isSelected = _bool
                }
            }
        }
        
        request.onFailure = {(error, moreInfo) in
            self.cell_btnFollowing?.isSelected = false
        };
        
        DataProxy.user.checkLike(object: _artist, dataRequest: request)
    }
    
    func superScrollViewDidScrolling(scrollView: UIScrollView){
        let offsetY = scrollView.contentOffset.y
        
        if offsetY >= 0{
            self.layoutTopConstraint?.constant = offsetY/2
            self.layoutTrailingConstraint?.constant = 0
            self.layoutLeadingConstraint?.constant = 0
            self.layoutBottomConstraint?.constant = -offsetY/2
            
        }else{
            self.layoutTopConstraint?.constant = offsetY
            self.layoutTrailingConstraint?.constant = offsetY
            self.layoutLeadingConstraint?.constant = offsetY
            self.layoutBottomConstraint?.constant = offsetY
        }
    }
}

class BaseArtistCollectionReusableView: BaseCollectionReusableView{
    @IBOutlet weak var cell_image: UIImageView?
    @IBOutlet weak var cell_name: UILabel?
    @IBOutlet weak var cell_followersNumber: UILabel?
    @IBOutlet weak var cell_followingNumber: UILabel?
    @IBOutlet weak var cell_playingNumber: UILabel?
    @IBOutlet weak var cell_btnFollowing: UIButton?
    @IBOutlet weak var layoutBtnFollowTrailingConstraint: NSLayoutConstraint?
    
    let font_follow: UIFont = UIFont.fontSemiBold_medium()
    
    var delegate: ArtistCollectionCellDelegate?
    internal var itemArtist: Artist?
    
    var isEnableFollowBtn: Bool = false {
        didSet{
            
            if Thread.isMainThread{
                if self.isEnableFollowBtn {
                    self.cell_btnFollowing?.isHidden = false
                }else{
                    self.cell_btnFollowing?.isHidden = true
                }
            }else{
            DispatchQueue.main.async() { () -> Void in
                if self.isEnableFollowBtn {
                    self.cell_btnFollowing?.isHidden = false
                }else{
                    self.cell_btnFollowing?.isHidden = true
                }
            }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
        self.setupColor()
        
        
        DispatchQueue.main.async() { () -> Void in
            self.layoutIfNeeded()
            self.cell_btnFollowing?.setupBtnFollow()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.clearLayout()
    }
    
    private func clearLayout(){
        self.cell_image?.image = nil
        self.cell_name?.text = ""
        self.cell_followersNumber?.text = ""
    }
    
    internal func setupFont(){
        self.cell_name?.font = self.font_title
        self.cell_followersNumber?.font = self.font_artist
        self.cell_followingNumber?.font = self.font_artist
        self.cell_btnFollowing?.titleLabel?.font = self.font_follow
    }
    
    internal func setupColor(){
        self.cell_name?.textColor = self.color_primary
        self.cell_followersNumber?.textColor = self.color_secondary
        self.cell_followingNumber?.textColor = self.color_secondary
        self.cell_btnFollowing?.setTitleColor(UIColor.init(hexaString: AppColor.appRedColor.rawValue), for: .normal)
    }
    
    func configureCellWith(_artist: Artist, enableFollow: Bool){
        
        self.isEnableFollowBtn = enableFollow
        self.itemArtist = _artist
        
        if let url = URL.init(string: _artist.image_cover.urlConvertWith(width: screenWidth, height: screenWidth)){
            self.cell_image?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
        }
        
        
        if Thread.isMainThread{
            self.cell_name?.text = _artist.alias
            
            if let isLike =  DataProxy.user.checkLikeWithoutRequest(object: _artist){
                self.cell_btnFollowing?.isSelected = isLike
            }else{
                self.cell_btnFollowing?.isSelected = _artist.isFollow
            }
        }else{
            DispatchQueue.main.async() { () -> Void in
                self.cell_name?.text = _artist.alias
                
                if let isLike =  DataProxy.user.checkLikeWithoutRequest(object: _artist){
                    self.cell_btnFollowing?.isSelected = isLike
                }else{
                    self.cell_btnFollowing?.isSelected = _artist.isFollow
                }
            }
        }
    }
    
    @IBAction func btnFollowTap(sender : UIButton){
        if self.itemArtist != nil{
            self.delegate?.artistCellDelegate_DidTapFollowBtn(sender: sender, object: self.itemArtist!)
        }
    }
}

class BaseCollectionReusableView: UICollectionReusableView {
    
    internal var font_title: UIFont{
        return UIFont.fontSemiBold_medium()
    }
    internal var font_artist    = UIFont.fontRegular_medium()
    internal var font_category  = UIFont.fontRegular_medium()
    internal var font_duration  = UIFont.fontRegular_medium()
    
    internal var color_primary     = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    internal var color_secondary: UIColor{
        return UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}

