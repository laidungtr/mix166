//
//  SongInfoResuableView.swift
//  Mix166
//
//  Created by ttiamap on 8/14/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

protocol SongInfoReusableViewDelegate {
    func SongInfo_DidSelectedArtist(_artist: Artist)
    func SongInfo_DidTapBtnAutoPlay(sender: UIButton)
    func SongInfo_DidBtnShare(sender: UIButton, objects: AnyObject)
    func SongInfo_DidBtnLike(sender: UIButton, objects: AnyObject)
}

class SongInfoResuableView: StationCollectionReusableView {
    
    @IBOutlet weak var cell_btnSwitchAutoPlay: UIButton?
    
    var delegate_songInfo: SongInfoReusableViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setupGesture(){
        self.cell_viewContainArtist?.isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(artistDidSelected(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        
        self.cell_viewContainArtist?.addGestureRecognizer(tapGesture)
        
    }
    
    func artistDidSelected(gesture: UITapGestureRecognizer){
        if let _artist = self.itemSong?.artists.first{
            self.delegate_songInfo?.SongInfo_DidSelectedArtist(_artist: _artist)
        }
    }
    
    @IBAction func btnAutoPlayTap(sender : UIButton){

        self.delegate_songInfo?.SongInfo_DidTapBtnAutoPlay(sender: sender)
    }
    
    override func btnLikeDidTap(sender: UIButton) {
        if self.itemSong != nil {
            self.delegate_songInfo?.SongInfo_DidBtnLike(sender: sender, objects: self.itemSong!)
        }
    }
    
    @IBAction func btnFollowDidTap(sender: UIButton){
        if self.itemSong != nil {
            if let artist = self.itemSong?.artists.first{
                self.delegate_songInfo?.SongInfo_DidBtnLike(sender: sender, objects: artist)
            }
        }
    }
    
    override func btnShareDidTap(sender: UIButton) {
        if self.itemSong != nil {
            self.delegate_songInfo?.SongInfo_DidBtnShare(sender: sender, objects: self.itemSong!)
        }
    }
    
    override func configureWithSong(itemSong: Song) {
        super.configureWithSong(itemSong: itemSong)
        
        self.checkSongLike(_itemSong: itemSong)
        
        if let _artist = itemSong.artists.first{
            self.checkArtistFollow(_artist: _artist)
        }
    }
    
    func checkSongLike(_itemSong: Song){
        var request = DataRequest(.Network)
        
        request.onSuccess = {(isLike,  moreInfo) in
            
            if let error = moreInfo as? NSError, error.code != 0{
                self.cell_btnLike?.isSelected = false
            }else{
                if let _bool = isLike as? Bool{
                    self.cell_btnLike?.isSelected = _bool
                }
            }
        }
        
        request.onFailure = {(error, moreInfo) in
            self.cell_btnLike?.isSelected = false
        };
        
        DataProxy.user.checkLike(object: _itemSong, dataRequest: request)
    }
    
    func checkArtistFollow(_artist: Artist){
        var request = DataRequest(.Network)
        
        request.onSuccess = {(isLike,  moreInfo) in
            
            if let error = moreInfo as? NSError, error.code != 0{
                self.cell_btnFollowing?.isSelected = false
            }else{
                if let _bool = isLike as? Bool{
                    self.cell_btnFollowing?.isSelected = _bool
                }
            }
        }
        
        request.onFailure = {(error, moreInfo) in
            self.cell_btnFollowing?.isSelected = false
        };
        
        DataProxy.user.checkLike(object: _artist, dataRequest: request)
    }
}
