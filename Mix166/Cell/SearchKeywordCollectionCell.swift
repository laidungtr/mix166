//
//  SearchKeywordCollectionCell.swift
//  Mix166
//
//  Created by ttiamap on 6/28/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class SearchKeywordCollectionCell: UICollectionViewCell {
    
    private var _extraMargins = sizeZero
    
    internal var color_secondary: UIColor{
        return UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    }
    
    var font_title: UIFont{
        return UIFont.fontRegular_big()
    }
    
    @IBOutlet weak var cell_lbKeyword: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.setupFont()
    }
    
    func setupFont(){
        self.cell_lbKeyword?.font = font_title
        self.cell_lbKeyword?.textColor = color_secondary
    }
    
    override func prepareForReuse() {
        self.cell_lbKeyword?.text = ""
    }
    
    func configureCell(keyword: String){
        
        if Thread.isMainThread{
            self.cell_lbKeyword?.text = keyword
        }else{
            DispatchQueue.main.async() { () -> Void in
                self.cell_lbKeyword?.text = keyword
            }
        }
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }

}
