//
//  StationCollectionReusableView.swift
//  Mix166
//
//  Created by ttiamap on 8/1/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

protocol  StationCollectionReusableViewDelegate{
    
    func stationReusableViewDelegate_DidTapArtist(artist: Artist?)
    func stationReusableViewDelegate_DidTapAtIndexPath(indexPath: IndexPath)
    func stationReusableViewDelegate_BtnMoreDidTap(sender: UIButton, objects: Station)
    func stationReusableViewDelegate_LikeBtnDidTap(sender: UIButton, objects: Station)
}

class StationCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var cell_lbDescription: UILabel?
    @IBOutlet weak var cell_lbTimePost: UILabel?
    @IBOutlet weak var cell_lbTitle: UILabel?
    @IBOutlet weak var cell_lbArtist: UILabel?
    @IBOutlet weak var cell_lbDuration: UILabel?
    @IBOutlet weak var cell_lbHashTag: UILabel?
    
    @IBOutlet weak var cell_btnLike: UIButton?
    @IBOutlet weak var cell_btnMore: UIButton?
    @IBOutlet weak var cell_btnShare: UIButton?
    
    @IBOutlet weak var cell_imgCover: UIImageView?
    @IBOutlet weak var cell_imgAvatar: UIImageView?
    
    @IBOutlet weak var cell_followNumber: UILabel?
    @IBOutlet weak var cell_btnFollowing: UIButton?
    
    @IBOutlet weak var cell_titleRelated: UILabel?
    @IBOutlet weak var cell_titleAutoplay: UILabel?
    
    @IBOutlet weak var cell_viewContainArtist: UIView?
    
    var delegate: StationCollectionReusableViewDelegate?
    private var currentIndexPath: IndexPath?
    
    internal var itemSong: Song?
    private var itemStation: Station?
    
    internal var font_primary: UIFont{
        return UIFont.fontSemiBold_big()
    }
    
    internal var font_secondary: UIFont{
        return UIFont.fontRegular_medium()
    }
    
    internal var font_thridly: UIFont{
        return UIFont.fontRegular_small()
    }
    
    internal var font_titleSecondary: UIFont{
        return UIFont.fontSemiBold_medium()
    }
    
    let font_follow: UIFont = UIFont.fontSemiBold_medium()
    
    internal var color_primary     = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
    internal var color_secondary    = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async() { () -> Void in
            self.layoutIfNeeded()
            self.cell_btnFollowing?.setupBtnFollow()
        }
        
        self.setupFont()
        self.setupColor()
        self.setupLayer()
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
        self.setupGesture()
    }
    
    func setupGesture(){
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.cellDidTap(gesture:)))
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tapGesture)
        
        let tapGesture2 = UITapGestureRecognizer.init(target: self, action: #selector(self.cellArtistProfileDidTap(gesture:)))
        self.cell_viewContainArtist?.isUserInteractionEnabled = true
        self.cell_viewContainArtist?.addGestureRecognizer(tapGesture2)
    }
    
    override func prepareForReuse() {
        self.cell_imgCover?.image = nil
    }
    
    @objc private func cellDidTap(gesture: UIGestureRecognizer){
        if self.currentIndexPath != nil {
            self.delegate?.stationReusableViewDelegate_DidTapAtIndexPath(indexPath: self.currentIndexPath!)
        }
    }
    
    @objc private func cellArtistProfileDidTap(gesture: UIGestureRecognizer){
        if self.currentIndexPath != nil {
            self.delegate?.stationReusableViewDelegate_DidTapArtist(artist: self.itemStation?.artists)
        }
    }
    
    private func setupFont(){
        self.cell_lbDescription?.font = self.font_thridly
        self.cell_lbTimePost?.font = self.font_thridly
        self.cell_lbDuration?.font = self.font_thridly
        self.cell_lbTitle?.font = self.font_primary
        self.cell_lbArtist?.font = self.font_secondary
        self.cell_lbHashTag?.font = self.font_secondary
        self.cell_followNumber?.font = self.font_secondary
        
        self.cell_titleRelated?.font = self.font_titleSecondary
        self.cell_titleAutoplay?.font = self.font_titleSecondary
        
        self.cell_btnLike?.titleLabel?.font = self.font_secondary
        self.cell_btnShare?.titleLabel?.font = self.font_secondary
        self.cell_btnFollowing?.titleLabel?.font = self.font_follow
    }
    
    private func setupColor(){
        self.cell_lbDescription?.textColor = self.color_secondary
        self.cell_lbTimePost?.textColor = self.color_secondary
        self.cell_lbDuration?.textColor = self.color_primary
        self.cell_lbTitle?.textColor = self.color_primary
        self.cell_lbArtist?.textColor = self.color_secondary
        self.cell_lbHashTag?.textColor = self.color_secondary
        self.cell_titleAutoplay?.textColor = self.color_secondary
        self.cell_titleRelated?.textColor = self.color_secondary
        self.cell_followNumber?.textColor = self.color_secondary
        
        self.cell_btnLike?.setTitleColor(self.color_secondary, for: .normal)
        self.cell_btnShare?.setTitleColor(self.color_secondary, for: .normal)
        self.cell_btnFollowing?.setTitleColor(self.color_primary, for: .normal)
    }
    
    private func setupLayer(){
        self.cell_btnShare?.borderWithRadius(2.0, borderWidth: 0.5, borderColor: UIColor.darkGray.withAlphaComponent(0.3))
        self.cell_btnLike?.borderWithRadius(2.0, borderWidth: 0.5, borderColor: UIColor.darkGray.withAlphaComponent(0.3))
        self.cell_btnMore?.borderWithRadius(2.0, borderWidth: 0.5, borderColor: UIColor.darkGray.withAlphaComponent(0.3))
        
        self.cell_imgCover?.borderWithRadius(5.0)
        self.cell_imgAvatar?.borderWithRadius(self.cell_imgAvatar!.frame.size.height/2)
    }
    
    func configureCell(itemStation: Station, indexPath: IndexPath){
        
        self.currentIndexPath = indexPath
        self.itemStation = itemStation
        
        let size = self.cell_imgAvatar?.frame.size
        
        if let url = URL.init(string: itemStation.artists.image_cover.urlConvertWith(width: size?.height, height: size?.width)){
            self.cell_imgAvatar?.sd_setImage(with: url, placeholderImage: image_userPlaceholder)
        }
        
        let userName = itemStation.artists.alias
        let actionString = "  uploaded "
        var objectName =  " a Song"
        
        switch itemStation.type {
        case .video_vertical, .video_horizontal, .video_vertical_delete:
            //
            objectName = " a Video"
            break
            
        case .mixset_horizontal, .mixset_vertical_delete, .mixset_vertical:
            //
            objectName = " a Mixset"
            break
        default:
            //
            break
        }
        
        let wholeStr = userName+actionString+objectName
        
        let att_string = NSMutableAttributedString(string: wholeStr)
        att_string.addAttribute(NSFontAttributeName, value: self.font_primary, range: (wholeStr as NSString).range(of: userName))
        att_string.addAttribute(NSForegroundColorAttributeName, value: self.color_primary, range: (wholeStr as NSString).range(of: userName))
        att_string.addAttribute(NSFontAttributeName, value: self.font_primary, range: (wholeStr as NSString).range(of: objectName))
        att_string.addAttribute(NSForegroundColorAttributeName, value: self.color_primary, range: (wholeStr as NSString).range(of: objectName))
        
        self.cell_lbDescription?.attributedText = att_string
        
        if let itemSong = itemStation.object as? Song{
            
            let sizeCover = self.cell_imgCover?.frame.size
            
            if let url = URL.init(string: itemSong.imageCover.urlConvertWith(width: sizeCover?.width, height: sizeCover?.height)){
                self.cell_imgCover?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
            }
            
            
            if Thread.isMainThread{
                self.cell_btnLike?.setTitle("  "+String(itemSong.like_count), for: .normal)
                self.cell_lbTimePost?.text = itemStation.dateModified
                self.cell_lbTitle?.text = itemSong.name
                self.cell_lbArtist?.text = itemSong.all_artistName
                self.cell_lbHashTag?.text = itemSong.all_genresNameHashTag
                self.cell_lbDuration?.text = itemSong.mp3.duration.length > 0 ? itemSong.mp3.duration : itemSong.qualities.duration
                
                if let isLike =  DataProxy.user.checkLikeWithoutRequest(object: itemSong){
                    self.cell_btnLike?.isSelected = isLike
                }else{
                    self.cell_btnLike?.isSelected = itemStation.is_liked
                }
            }else{
                
                DispatchQueue.main.async() { () -> Void in
                    
                    self.cell_btnLike?.setTitle("  "+String(itemSong.like_count), for: .normal)
                    self.cell_lbTimePost?.text = itemStation.dateModified
                    self.cell_lbTitle?.text = itemSong.name
                    self.cell_lbArtist?.text = itemSong.all_artistName
                    self.cell_lbHashTag?.text = itemSong.all_genresNameHashTag
                    self.cell_lbDuration?.text = itemSong.mp3.duration.length > 0 ? itemSong.mp3.duration : itemSong.qualities.duration
                    
                    if let isLike =  DataProxy.user.checkLikeWithoutRequest(object: itemSong){
                        self.cell_btnLike?.isSelected = isLike
                    }else{
                        self.cell_btnLike?.isSelected = itemStation.is_liked
                    }
                }
            }
        }
    }
    
    func configureWithSong(itemSong: Song){
        
        self.itemSong = itemSong
        
        let size = self.cell_imgAvatar?.frame.size
        
        if let url = URL.init(string: itemSong.artists.first?.image_cover.urlConvertWith(width: size?.width, height: size?.height) ?? ""){
            self.cell_imgAvatar?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
        }
        
        
        if Thread.isMainThread{
            self.cell_btnLike?.setTitle("  "+String(itemSong.like_count), for: .normal)
            self.cell_lbTitle?.text = itemSong.name
            self.cell_lbArtist?.text = itemSong.all_artistName
            self.cell_lbHashTag?.text = itemSong.all_genresNameHashTag
            self.cell_lbDescription?.text = itemSong.artists.first?.alias ?? ""
            self.cell_followNumber?.text = String(itemSong.artists.first?.like_count ?? "0") + " Follows"
            self.cell_lbDuration?.text = itemSong.mp3.duration.length > 0 ? itemSong.mp3.duration : itemSong.qualities.duration
            
            if let _artist = itemSong.artists.first{
                if let isLike =  DataProxy.user.checkLikeWithoutRequest(object: _artist){
                    self.cell_btnLike?.isSelected = isLike
                }else{
                    self.cell_btnLike?.isSelected = _artist.isFollow
                }
            }
        }else{
            
            DispatchQueue.main.async() { () -> Void in
                
                self.cell_btnLike?.setTitle("  "+String(itemSong.like_count), for: .normal)
                self.cell_lbTitle?.text = itemSong.name
                self.cell_lbArtist?.text = itemSong.all_artistName
                self.cell_lbHashTag?.text = itemSong.all_genresNameHashTag
                self.cell_lbDescription?.text = itemSong.artists.first?.alias ?? ""
                self.cell_followNumber?.text = String(itemSong.artists.first?.like_count ?? "0") + " Follows"
                self.cell_lbDuration?.text = itemSong.mp3.duration.length > 0 ? itemSong.mp3.duration : itemSong.qualities.duration
                
                if let _artist = itemSong.artists.first{
                    if let isLike =  DataProxy.user.checkLikeWithoutRequest(object: _artist){
                        self.cell_btnLike?.isSelected = isLike
                    }else{
                        self.cell_btnLike?.isSelected = _artist.isFollow
                    }
                }
            }
        }
    }
    
    //MARK: IBAction
    @IBAction func btnShareDidTap(sender: UIButton){
        
    }
    
    @IBAction func btnMoreDidTap(sender: UIButton){
        if self.itemStation != nil{
            self.delegate?.stationReusableViewDelegate_BtnMoreDidTap(sender: sender, objects: self.itemStation!)
        }
    }
    
    @IBAction func btnLikeDidTap(sender: UIButton){
        if self.itemStation != nil {
            self.delegate?.stationReusableViewDelegate_LikeBtnDidTap(sender: sender, objects: self.itemStation!)
        }
    }
    
}
