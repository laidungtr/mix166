//
//  TitleHeaderProfileReusableView.swift
//  Mix166
//
//  Created by ttiamap on 8/10/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class TitleHeaderProfileReusableView: TitleCollectionReusableView {
    
    override var font_title: UIFont{
        return UIFont.fontRegular_small()
    }
    
    override var color_title: UIColor{
        return UIColor.init(hexaString: AppColor.appRedColor.rawValue)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
