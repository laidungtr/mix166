//
//  HttpUtility.swift
//  Mix166
//
//  Created by ttiamap on 5/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import Alamofire

// Request upload successful
typealias OnUploadSuccess = (_ data:Any?) ->Void

// Request upload failed
typealias OnUploadFailure = (_ error:NSError?) ->Void

// Request upload successful
typealias OnUploadProgress = (_ byte:Double?, _ totalByte:Double?) ->Void

typealias NetworkSuccessClosual = (_ result: AnyObject?) -> Void
typealias NetworkFailureClosual = (_ _error: NSError?) -> Void

enum LocalError: Int {
    case LocalErroParseJson = -1
}

struct Request {
    
    var method: HTTPMethod;
    var url: String;
    var parameters: [String : AnyObject]?;
    
    var onSuccess : (NetworkSuccessClosual)?;
    var onFailure : (NetworkFailureClosual)?;
    var onCached  : (NetworkSuccessClosual)?;
    
    var forceLoadCached: Bool? = false;
    
    var is_multipart: Bool? = false;
    
    init(_ _method: HTTPMethod, _ _url: String) {
        method = _method;
        url = _url;
    }
}

public enum ErrorCode: Int{
    case userInfoNotFound = -2
}

class HttpUtility: Alamofire.SessionManager {
    
    static var requestCounting: Int = 0;
    
    let clientId = "e5e01e70bd14f7512ac25cfed557ff77716b41bb"
    let secretKey = "bd9177a1696e6186fbf04249990a1239d6bb8b6b"
    
    var manager: SessionManager = {
        
        //create default header;
        var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        
        //        defaultHeaders["User-Agent"]    = Config.userAgent;
        defaultHeaders["Content-Type"]  = "application/json";
        //defaultHeaders["Content-Type"]  = "application/x-www-form-urlencoded; charset=utf-8";
        //defaultHeaders["Cache-Control"] = "private";
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 8.0
        configuration.timeoutIntervalForResource = 8.0
        configuration.httpAdditionalHeaders = defaultHeaders
        
        return Alamofire.SessionManager(configuration: configuration, delegate: SessionDelegate.init(), serverTrustPolicyManager: nil)
    }()
    
    /*
     func getAuthorizationCertificate() -> String{
     
     var _signature = clientId+secretKey+String().stringTimeStamp
     return _signature.MD5()
     
     //        int startTime = [[NSDate date] timeIntervalSince1970];
     //        _timeTamps = StringFormat(@"%d",startTime);
     //
     //        @try {
     //            _signature = StringFormat(@"%@%@%@",CLIENTID,SECRETKEY,_timeTamps);
     //            _signature = [ShareData hashToSHA1WithString:_signature];
     //        }
     //        @catch (NSException *exception) {
     //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:[exception description] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
     //            [alert show];
     //        }
     //        @finally {
     //
     //        }
     }
     */
    
    /**
     * Call a URL with method GET, POST, PUT ...
     * - parameter Input a Request Struct
     * Return Request.OnSuccessed or Failure, or cache
     */
    func request(sender: Request!) {
        
        //UI... Show loading in Status Bar
        UIApplication.shared.isNetworkActivityIndicatorVisible = true;
        
        let url = sender.url;
        
        //Setup request Config
        let configuration = manager.session.configuration;
        let defaultHeaders  = configuration.httpAdditionalHeaders ?? [:];
        //        defaultHeaders["Authorization"] = DataProxy.currentUser.access_token ?? "";
        
        configuration.httpAdditionalHeaders = defaultHeaders;
        
        // Check network, if offline then return Cache
        let isReachability  =  NetworkReachabilityManager(host: Config.host);
        if(isReachability?.isReachable == true) {
            configuration.requestCachePolicy = .useProtocolCachePolicy
        } else {
            configuration.requestCachePolicy = .returnCacheDataElseLoad
        }
        
        if sender.forceLoadCached == true {
            configuration.requestCachePolicy = .returnCacheDataDontLoad
        }
        
        //setup config
        manager = Alamofire.SessionManager(configuration: configuration, delegate: SessionDelegate(), serverTrustPolicyManager: nil)
        self.sessionDidReceiveChallenge(manager: manager)
        
        
        // Endcoding url
        let encoding = URLEncoding.queryString
        var request = manager.request(url, method: sender.method,
                                      parameters: sender.parameters,
                                      encoding: encoding,
                                      headers: nil)
        
        if sender.method == .post {
            request = manager.request(url, method: sender.method,
                                      parameters: sender.parameters,
                                      encoding: sender.parameters?.toStringJson() ?? "",
                                      headers: nil)
        }
        
        request.responseJSON(completionHandler: { response in
            
            //UI... hiden loading in Status Bar
            UIApplication.shared.isNetworkActivityIndicatorVisible = false;
            
            switch response.result {
                
            case .success(let JSON): sender.onSuccess?(JSON as AnyObject)
            case .failure(let error): sender.onFailure?(error as NSError);
                
            }
            
            #if DEBUG
                NSLog("\n---------------[ X -> %@ ->%d ]---------------\n",#function, #line)
                print("\(String(describing: response.response?.url))")
                print("\(response)")
            #endif
        });
        
        ////debug
        HttpUtility.requestCounting += 1;
        let isCached = sender.forceLoadCached == true ? "Yes": "No";
        //#if DEBUG
        print("= Reqest = Cached \(isCached)) = \(HttpUtility.requestCounting) \(String(describing: request.request?.url))")
        //#endif
        
    }
    
    //    func uploadImage(url: String, image: UIImage, imageKey: String, suceess:OnUploadSuccess,failure:OnUploadFailure, progress: OnUploadProgress) {
    //
    //        let parameters: Dictionary<String,AnyObject> = [:]
    //
    //        let imageData = image.JPGRepresentationLow()
    //
    //        Alamofire.upload(.POST, url, multipartFormData: {
    //            multipartFormData in
    //            multipartFormData.appendBodyPart(data: imageData, name: imageKey, fileName: "file.jpg", mimeType: "image/jpg")
    //
    //            for (key, value) in parameters {
    //                multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key as! String )
    //            }
    //        }, encodingCompletion: {
    //            encodingResult in
    //            switch encodingResult {
    //
    //            case .Success(let upload, _, _):
    //
    //                upload.responseJSON {
    //
    //                    response in
    //
    //                    if let JSON = response.result.value {
    //                        suceess(data: JSON)
    //                    } else {
    //                        failure(error: response.result.error)
    //                    }
    //                }
    //
    //                upload.progress { tranferbyte, totalBytesRead, totalBytesExpectedToRead in
    //                    progress(byte: Double(totalBytesRead), totalByte: Double(totalBytesExpectedToRead))
    //                }
    //
    //            case .Failure(_):
    //                failure(error: HttpUtility.makeNSerror(-1, message: "error"))
    //            }
    //        })
    //
    //
    //    }
    
    
    func sessionDidReceiveChallenge(manager: Alamofire.SessionManager){
        
        manager.delegate.sessionDidReceiveChallenge = { session, challenge in
            var disposition: URLSession.AuthChallengeDisposition = .performDefaultHandling
            var credential: URLCredential?
            
            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
                disposition = URLSession.AuthChallengeDisposition.useCredential
                credential =  URLCredential.init(trust: challenge.protectionSpace.serverTrust!)
            } else {
                if challenge.previousFailureCount > 0 {
                    disposition = .cancelAuthenticationChallenge
                } else {
                    credential = manager.session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
                    
                    if credential != nil {
                        disposition = .useCredential
                    }
                }
            }
            return (disposition, credential)
        }
    }
    
    //Default a Error if Parse Json Fail
    class func localParseError() -> NSError {
        let stringMessage = "Không tìm thấy dữ liệu!"
        let userInfo: [NSObject : AnyObject] = [
            NSLocalizedDescriptionKey as NSObject :  NSLocalizedString("local.parse_json", value: stringMessage, comment: "") as AnyObject,
            NSLocalizedFailureReasonErrorKey as NSObject : NSLocalizedString("local.parse_json", value: "Are you Json", comment: "") as AnyObject
        ]
        return NSError(domain: "local", code: LocalError.LocalErroParseJson.rawValue , userInfo: userInfo)
    }
    
    class func makeNSError(result: Dictionary<String, AnyObject>) -> NSError?{
        if let errorCode = result["error_code"] as? Int{
            if let description = result["error_description"] as? String{
                return self.makeNSerror(errorCode: errorCode, message: description)
            }else{
                return self.makeNSerror(errorCode: errorCode, message: AppMixError.unknow.rawValue)
            }
        }
        return self.localParseError()
    }
    
    class func makeNSerror(errorCode: Int, message: String ) -> NSError? {
        
        let userInfo: [NSObject : AnyObject] = [
            NSLocalizedDescriptionKey as NSObject :  NSLocalizedString(message , value:message , comment: "") as AnyObject,
            ]
        return NSError(domain: "api.error", code: errorCode , userInfo: userInfo)
    }
    
    /// setup url return a url string
    class func makeURL(uri:String) -> String{
        return self.makeURL(uri: uri, parameter: "")
    }
    
    
    /// setup url return a url string
    class func makeURL(uri:String, parameter: String) -> String {
        
        var _uri = "";
        
        if(parameter == "") {
            _uri = uri;
        } else {
            _uri = uri + "?" + parameter.URLEscapedString;
        }
        
        let urlString = String(domainApi + _uri)
        
        return urlString ?? ""
    }
    
    
    /// setup url width Domain return a url string
    class func makeURL(domain: String, uri:String) -> String{
        return self.makeURL(domain: domainApi, uri: uri, parameter: "")
    }
    
    /// setup url return a url string
    class func makeURL(domain:String, uri:String, parameter: String) -> String {
        
        var _uri = ""
        
        if(parameter == "") {
            _uri = uri
        } else {
            _uri = uri + "?" + parameter.URLEscapedString
        }
        
        let urlString = String(domain + _uri)
        
        return urlString ?? ""
    }
}

//extension Alamofire.manager
//{
//    private func cancelTasksByUrl(tasks: [NSURLSessionTask], url: String)
//    {
//        for task in tasks
//        {
//            print("1======")
//            print(task.description)
//            if ((task.currentRequest?.URL?.description.contains(url)) != nil &&
//                task.currentRequest!.URL!.description.contains(url))
//            {
//
//                print("Cancel success")
//                task.cancel()
//            }
//        }
//    }
//
//    func cancelRequests(uri: String, complete: completeBlock)
//    {
//        self.session.getTasksWithCompletionHandler
//            {
//                (dataTasks, uploadTasks, downloadTasks) -> Void in
//
//
//                print("2======")
//                print(dataTasks.description)
//                print(uploadTasks.description)
//                print(downloadTasks.description)
//
//                self.cancelTasksByUrl(dataTasks     as [NSURLSessionTask], url: uri)
//                self.cancelTasksByUrl(uploadTasks   as [NSURLSessionTask], url: uri)
//                self.cancelTasksByUrl(downloadTasks as [NSURLSessionTask], url: uri)
//                
//                complete()
//        }
//        
//    }
//}
