//
//  SearchProxy.swift
//  Mix166
//
//  Created by ttiamap on 6/29/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

internal enum Search_Type: String{
    case all = "all"
    case mixset = "mixset"
    case track = "track"
    case video = "video"
    case deejay = "artist"
}

private enum SearchUriType: String {
    
    case search           = "edms/esSearch"
    case trending         = "edms/esTrending"
    
    
    var uri : String {
        switch self {
        default : return self.rawValue
        }
    }
    
}

class SearchProxy: BaseProxy{
    
    func searchTrending(dataRequest: DataRequest){
        let uri = SearchUriType.trending.uri
        
        let url = self.makeURL(uri: uri)
        
        var request = Request(.get, url)
        
        request.onSuccess = { (result) in
            
            print(result ?? "")
            if let results = result as? Dictionary<String, AnyObject> {
                if let data = results["data"] as? [Dictionary<String, AnyObject>]{
                    
                    let output = Search(dict: [:])
                    var arrItem : [String] = []
                    for _itemDict in data {
                        if let item = _itemDict["keyword"] as? String{
                            arrItem.append(item)
                        }
                    }
                    
                    if arrItem.count > 0 {
                        self.setTrendSearchKeyword(keyword: arrItem)
                    }
                    
                    output._datas = arrItem as [AnyObject]
                    
                    dataRequest.onSuccess?(output as AnyObject, "" as AnyObject)
                    
                    return
                }
            }
            let error = HttpUtility.localParseError()
            dataRequest.onFailure?(error, "" as AnyObject)
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject)
        }
        
        httpUtillty.request(sender: request)
    }
    
    /** Method  call API Search with searchType and keyword
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return [Playlist] if success
     ex: : api.mix166.com/api/v1.1_a/playlists/getUserPlaylist?user_id=2008072&page=1&limit=20
     */
    func searchWith(type: Search_Type, filter: Filter, dataRequest: DataRequest){
        
        let uri = SearchUriType.search.uri
        
        var url = ""
        var param: [String: AnyObject] = [:]
        
        if type != .all{
            
            if let _id = DataProxy.user.userInfo?.id{
                param = [
                    FilterType.Page.rawValue : (filter.offset/limitDefault + 1) as AnyObject,
                    FilterType.Limit.rawValue : limitDefault as AnyObject,
                    "user_id": _id as AnyObject,
                    "type": type.rawValue,
                    "keyword": filter.keyword
                    ] as [String: AnyObject]
            }else{
                param = [
                    "type": type.rawValue,
                    "keyword": filter.keyword
                ] as [String: AnyObject]
            }
            
            
            url = self.makeURL(uri: uri, parameter: param.description)
            
            self.setRecentSearchKeyword(keyword: filter.keyword)
            
        }else{
            if let _id = DataProxy.user.userInfo?.id{
                param = [
                    "user_id": _id as AnyObject,
                    "type": type.rawValue,
                    "keyword": filter.keyword
                ] as [String: AnyObject]
            }else{
                param = [
                    "type": type.rawValue,
                    "keyword": filter.keyword
                    ] as [String: AnyObject]
            }
            url = self.makeURL(uri: uri)
            
        }
        
        var request = Request(.get, url)
        request.parameters = param 
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                switch type {
                case .all:
                    if let data = results["data"] as? Dictionary<String, AnyObject>{
                        
                        let output = Search(dict: data)
                        
                        dataRequest.onSuccess?(output as AnyObject, "" as AnyObject)
                        
                        return
                    }else{
                        
                        // do this for request with keyword == ""
                        let output = Search(dict: [:])
                        
                        dataRequest.onSuccess?(output as AnyObject, "" as AnyObject)
                        
                        return
                    }
                    
                case .mixset:
                    if let data = results["data"] as? [Dictionary<String, AnyObject>]{
                        
                        let output = Search(dict: [:])
                        var arrItem : [Mix] = []
                        for _itemDict in data {
                            let item = Mix.init(dict: _itemDict)
                            item.parsingSongMasup()
                            arrItem.append(item)
                        }
                        output._datas = arrItem
                        
                        dataRequest.onSuccess?(output as AnyObject, "" as AnyObject)
                        
                        return
                    }
                    break
                    
                case .track:
                    if let data = results["data"] as? [Dictionary<String, AnyObject>]{
                        
                        let output = Search(dict: [:])
                        var arrItem : [Track] = []
                        for _itemDict in data {
                            let item = Track.init(dict: _itemDict)
                            arrItem.append(item)
                        }
                        output._datas = arrItem
                        
                        dataRequest.onSuccess?(output as AnyObject, "" as AnyObject)
                        
                        return
                    }
                    break
                    
                case .video:
                    if let data = results["data"] as? [Dictionary<String, AnyObject>]{
                        
                        let output = Search(dict: [:])
                        var arrItem : [Video] = []
                        for _itemDict in data {
                            let item = Video.init(dict: _itemDict)
                            arrItem.append(item)
                        }
                        output._datas = arrItem
                        
                        dataRequest.onSuccess?(output as AnyObject, "" as AnyObject)
                        
                        return
                    }
                    break
                    
                case .deejay:
                    if let data = results["data"] as? [Dictionary<String, AnyObject>]{
                        
                        let output = Search(dict: [:])
                        var arrItem : [Deejay] = []
                        for _itemDict in data {
                            let item = Deejay.init(dict: _itemDict)
                            arrItem.append(item)
                        }
                        output._datas = arrItem
                        
                        dataRequest.onSuccess?(output as AnyObject, "" as AnyObject)
                        
                        return
                    }
                    break
                }
            }
            
            let error = HttpUtility.localParseError()
            dataRequest.onFailure?(error, "" as AnyObject)
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject)
        }
        
        httpUtillty.request(sender: request)
    }
    
    func getRecentSearchKeywords() -> [String]?{
        let userDefault = UserDefaults.standard
        if let _arrKeyword = userDefault.object(forKey: "recentlySearch") as? [String]{
            return _arrKeyword
        }
        return  nil
    }
    
    func setRecentSearchKeyword(keyword: String){
        
        DispatchQueue.global(qos: .background).sync {
            let userDefault = UserDefaults.standard
            
            var arrKeywords : [String] = []
            if keyword.length > 0{
                if let _arrKeyword = self.getRecentSearchKeywords(){
                    
                    arrKeywords = _arrKeyword
                    
                    if !arrKeywords.contains(keyword){
                        arrKeywords.insert(keyword, at: 0)
                    }
                }else{
                        arrKeywords.append(keyword)
                }
            }
            
            if arrKeywords.count > 5{
                arrKeywords.removeLast()
            }
            
            userDefault.set(arrKeywords, forKey: "recentlySearch")
        }
    }
    
    func setTrendSearchKeyword(keyword: [String]){
        
        DispatchQueue.global(qos: .background).sync {
            let userDefault = UserDefaults.standard
            userDefault.set(keyword, forKey: "trendSearch")
        }
    }
    
    func getTrendSearchKeywords() -> [String]?{
        let userDefault = UserDefaults.standard
        if let _arrKeyword = userDefault.object(forKey: "trendSearch") as? [String]{
            return _arrKeyword
        }
        return  nil
    }
}

