//
//  BaseProxy.swift
//  Mix166
//
//  Created by ttiamap on 5/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import Alamofire

class BaseProxy: NSObject {

    let httpUtillty:HttpUtility = HttpUtility();
    let localError: String? = "-1"
    
    /// remove to NSUserDefault
    func removeToNsUserDefaut(withKey: String) {
        UserDefaults().removeObject(forKey: withKey)
    }
    
    /// save to NSUserDefault
    func saveToNsUserDefaut(anyObject: AnyObject?, withKey: String){
        
        if(anyObject == nil) { return; }
        
        if let _anyObject: AnyObject = anyObject {
            let data =  NSKeyedArchiver.archivedData(withRootObject: _anyObject)
            UserDefaults().set(data, forKey: withKey)
        }
    }
    
    /// read from NSUserDefault
    func readNsUserDefaut(withKey: String) -> AnyObject{
        if let loaded = UserDefaults().object(forKey: withKey) as? Data {
            if let data = NSKeyedUnarchiver.unarchiveObject(with: loaded) {
                return data as AnyObject
            } else {
                return "" as AnyObject
            }
            
        } else {
            
//            if withKey == DataProxy.user.userNSUserDefaultKey {
//                guard let data:String = NSUserDefaults.standardUserDefaults().objectForKey(DataProxy.user.userTokenOldNSUserDefaultKey) as? String ?? "" where data.length > 0 else { return "" }
//                
//                self.removeToNsUserDefaut(DataProxy.user.userTokenOldNSUserDefaultKey)
//                
//                let currentUser = UserModel(dict:[:])
//                currentUser.make(data)
//                
//                return currentUser;
//            }
            
            return  "" as AnyObject;
        }
        
    }
    
    /// save String to NSUserDefault
    func saveStrToNsUserDefaut(keyword: String?, withKey: String, limit: Int){
        
        let keyword = keyword?.trim();
        
        if(keyword == nil || keyword == "") { return; }
        
        var currentStrings: [String] = self.readStrNsUserDefaut(withKey: withKey);
        
        if limit > 0 && currentStrings.count >= limit{
            if currentStrings.count > 1 { currentStrings.removeLast() }
        }
        
        if currentStrings.contains(keyword!) {
            guard let index = currentStrings.index(of: keyword!) else {return}
            currentStrings.remove(at: index)
        }
        
        currentStrings.insert(keyword!, at: 0);
        
        UserDefaults().set(currentStrings, forKey: withKey)
    }
    
    /// read String from NSUserDefault
    func readStrNsUserDefaut(withKey: String) -> [String] {
        if let loaded = UserDefaults().array(forKey: withKey) as? [String] {
            return loaded;
        } else {
            return [];
        }
    }
    
    /// setup url return a url string
    func makeURL(uri:String) -> String{
        return self.makeURL(uri: uri, parameter: "")
    }
    
    
    /// setup url return a url string
    func makeURL(uri:String, parameter: String) -> String {
        
        var urlString = String(domainApi + uri) ?? ""
        
        let timeExpire = self.getStaticTimeExpireFromLocalDevice()
        let secureLink = self.getSecureLink(_expireTime: String(timeExpire), uri: uri)
        
        if urlString.contains("?"){
            urlString = urlString + "&st=" + secureLink.URLEscapedString + "&e=" + String(timeExpire)
        }else{
            urlString = urlString + "?st=" + secureLink.URLEscapedString + "&e=" + String(timeExpire)
        }
        
        return urlString
    }
    
    
    /// setup url width Domain return a url string
    func makeURL(domain: String, uri:String) -> String{
        return self.makeURL(domain: domain, uri: uri, parameter: "")
    }
    
    /// setup url return a url string
    func makeURL(domain:String, uri:String, parameter: String) -> String {
        
        var _uri = "";
        
        if(parameter == "") {
            _uri = uri;
        } else {
            _uri = uri + "?" + parameter.URLEscapedString;
        }
        
        let urlString = String(domain + "/" + _uri);
        
        return urlString ?? ""
    }
    
    func getStaticTimeExpireFromLocalDevice() -> Int {
        let dateNow = Date().addingTimeInterval(60*60*12)
        let timeInterval = Int(dateNow.timeIntervalSince1970)
        return timeInterval
    }
    
    
    func getSecureLink(_expireTime: String, uri: String) -> String{

        let secretKey = "JDSDsaaaDSLJDdsada"
        let expireTime = _expireTime
        
        var secureLink = secretKey+expireTime+"/api/v1.1_a/"+uri
        secureLink = secureLink.md5Data_toBase64()
        secureLink = secureLink.replacingOccurrences(of: "+", with: "-")
        secureLink = secureLink.replacingOccurrences(of: "/", with: "_")
        secureLink = secureLink.replacingOccurrences(of: "=", with: "")
        
        return secureLink
    }
    
    func getSecureLinkAppVersion(_expireTime: String) -> String{
        
        let uri = "version/index.php"
        let secretKey = "SMN238023HD3DHDDD77KHSS"
        let expireTime = _expireTime
        
        var secureLink = secretKey+expireTime+"/api/"+uri
        secureLink = secureLink.md5Data_toBase64()
        secureLink = secureLink.replacingOccurrences(of: "+", with: "-")
        secureLink = secureLink.replacingOccurrences(of: "/", with: "_")
        secureLink = secureLink.replacingOccurrences(of: "=", with: "")
        
        return secureLink
    }
    
    
    //MARK: parsing
    func parsingResponseDataWithProductList(responseData: [Dictionary<String,AnyObject>]) -> [ProductList]{
        
        var arrProductList: [ProductList] = []
        
        for dict in responseData {
            
            let itemProductList = ProductList.init(dict: dict)
            
            if let dictObject = dict["objects"] as? [Dictionary<String,AnyObject>], dictObject.count > 0{
                
                switch itemProductList.type {
                case .mixset_vertical, .mixset_horizontal, .mixset_vertical_delete:
                    //
                    let arrMix = self.parsingMixsWithDicts(dict: dictObject)
                    itemProductList.object = arrMix as [AnyObject]
                    break
                    
                case .track_vertical, .track_horizontal, .track_player_vertical, .track_vertical_delete:
                    //
                    let arrTracks = self.parsingTracksWithDicts(dict: dictObject)
                    itemProductList.object = arrTracks as [AnyObject]
                    break
                    
                case .video_vertical, .video_horizontal, .video_vertical_delete:
                    //
                    let arrVideo = self.parsingVideosWithDicts(dict: dictObject)
                    itemProductList.object = arrVideo as [AnyObject]
                    break
                    
                case .deejay_vertical, .deejay_horizontal, .deejay_station_horizontal, .deejay_vertical_follow:
                    //
                    let arrArtist = self.parsingArtistsWithDicts(dict: dictObject)
                    itemProductList.object = arrArtist as [AnyObject]
                    break
                    
                default:
                    break
                }
            }
            
            if itemProductList.object?.count ?? 0 > 0{
                arrProductList.append(itemProductList)
            }
        }
        
        return arrProductList
    }
    
    func parsingMixsWithDicts(dict: [Dictionary<String,AnyObject>]) -> [Mix]{
        
        var arrItem: [Mix] = []
        for item in dict {
            let itemMix = Mix.init(dict: item)
            arrItem.append(itemMix)
        }
        
        return arrItem
    }
    
    func parsingTracksWithDicts(dict: [Dictionary<String,AnyObject>]) -> [Track]{
        
        var arrItem: [Track] = []
        for item in dict {
            let itemTrack = Track.init(dict: item)
            arrItem.append(itemTrack)
        }
        
        return arrItem
    }
    
    func parsingVideosWithDicts(dict: [Dictionary<String,AnyObject>]) -> [Video]{
        
        var arrItem: [Video] = []
        for item in dict {
            let itemVideo = Video.init(dict: item)
            arrItem.append(itemVideo)
        }
        
        return arrItem
    }
    
    func parsingArtistsWithDicts(dict: [Dictionary<String,AnyObject>]) -> [Artist]{
        
        var arrItem: [Artist] = []
        for item in dict {
            let itemArtist = Artist.init(dict: item)
            arrItem.append(itemArtist)
        }
        
        return arrItem
    }
}
