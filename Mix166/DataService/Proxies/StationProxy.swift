//
//  StationProxy.swift
//  Mix166
//
//  Created by ttiamap on 8/1/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

private enum StationUriType: String {
    
    case new_feeds            = "timelines/index"
    
    var uri : String {
        switch self {
        default : return self.rawValue
        }
    }
}


class StationProxy: BaseProxy {
    
    func fetch_stationNewfeed(filter: Filter, user_id: String?, dataRequest: DataRequest){
        
        
//        let dicts: [Dictionary<String,Any>] = [[ "artist":["artist_id": 2000028,
//                                                           "artist_alias": "Kygo",
//                                                           "artist_name": "",
//                                                           "image_cover": "https://st-cdn.mix166.com/images/edm_artists/2016/06/24/1466760950-Kygo.jpg",
//                                                           "view_count": 4810,
//                                                           "is_favorited": 0],
//                                                 "type":"mixset_vertical",
//                                                 "object": ["ranking_id":2002200,
//                                                            "point":925,
//                                                            "object_id":2000407,
//                                                            "object_name":"Best Deep House & Future Music Mix 2017",
//                                                            "link_url":"https:/st-cdn.mix166.com",
//                                                            "location":"images/edm_mixs/2017/07/07/1499421375-maxresdefault.jpg",
//                                                            "singers":["artist_id":2000864,
//                                                                       "artist_name":"",
//                                                                       "artist_alias":"V.A",
//                                                                       "link_url":"https:/st-cdn.mix166.com",
//                                                                       "location":"images/edm_artists/2016/07/20/1469001488-73688444a73a76169d03b689a7e785cf1404904575.jpg"],
//                                                            "genres":[["genre_id":2000000,"genre_name":"Deep House"],
//                                                                      ["genre_id":2000008,"genre_name":"Future House"]],
//                                                            "status":4,"rank":0]]  as [String : Any],
//                                               [ "artist":["artist_id": 2000028,
//                                                           "artist_alias": "Kygo",
//                                                           "artist_name": "",
//                                                           "image_cover": "https://st-cdn.mix166.com/images/edm_artists/2016/06/24/1466760950-Kygo.jpg",
//                                                           "view_count": 4810,
//                                                           "is_favorited": 0],
//                                                 "type":"mixset_vertical",
//                                                 "object": ["ranking_id":2002200,
//                                                            "point":925,
//                                                            "object_id":2000407,
//                                                            "object_name":"Best Deep House & Future Music Mix 2017",
//                                                            "link_url":"https:/st-cdn.mix166.com",
//                                                            "location":"images/edm_mixs/2017/07/07/1499421375-maxresdefault.jpg",
//                                                            "singers":["artist_id":2000864,
//                                                                       "artist_name":"",
//                                                                       "artist_alias":"V.A",
//                                                                       "link_url":"https:/st-cdn.mix166.com",
//                                                                       "location":"images/edm_artists/2016/07/20/1469001488-73688444a73a76169d03b689a7e785cf1404904575.jpg"],
//                                                            "genres":[["genre_id":2000000,"genre_name":"Deep House"],
//                                                                      ["genre_id":2000008,"genre_name":"Future House"]],
//                                                            "status":4,"rank":0]]  as [String : Any],
//                                               [ "artist":["artist_id": 2000028,
//                                                           "artist_alias": "Kygo",
//                                                           "artist_name": "",
//                                                           "image_cover": "https://st-cdn.mix166.com/images/edm_artists/2016/06/24/1466760950-Kygo.jpg",
//                                                           "view_count": 4810,
//                                                           "is_favorited": 0],
//                                                 "type":"mixset_vertical",
//                                                 "object": ["ranking_id":2002200,
//                                                            "point":925,
//                                                            "object_id":2000407,
//                                                            "object_name":"Best Deep House & Future Music Mix 2017",
//                                                            "link_url":"https:/st-cdn.mix166.com",
//                                                            "location":"images/edm_mixs/2017/07/07/1499421375-maxresdefault.jpg",
//                                                            "singers":["artist_id":2000864,
//                                                                       "artist_name":"",
//                                                                       "artist_alias":"V.A",
//                                                                       "link_url":"https:/st-cdn.mix166.com",
//                                                                       "location":"images/edm_artists/2016/07/20/1469001488-73688444a73a76169d03b689a7e785cf1404904575.jpg"],
//                                                            "genres":[["genre_id":2000000,"genre_name":"Deep House"],
//                                                                      ["genre_id":2000008,"genre_name":"Future House"]],
//                                                            "status":4,"rank":0]]  as [String : Any],
//                                               ["type":"artist_station_vertical",
//                                                "object": [["artist_id": 2000573,
//                                                            "artist_alias": "Don Diablo",
//                                                            "artist_name": "",
//                                                            "link_url": "http://st.cdn.mix166.com",
//                                                            "location": "images/edm_artists/2016/05/04/1462333612-26th-june-2015-don-diablo-ministry-of-sound-club-square-banner.png"
//                                                    ],
//                                                           ["artist_id": 2000633,
//                                                            "artist_alias": "Craig David",
//                                                            "artist_name": "",
//                                                            "link_url": "http://st.cdn.mix166.com",
//                                                            "location": "images/edm_artists/2016/05/24/1464063084-image.jpg"
//                                                    ],
//                                                           ["artist_id":2000864,
//                                                            "artist_name":"",
//                                                            "artist_alias":"V.A",
//                                                            "link_url":"https:/st-cdn.mix166.com",
//                                                            "location":"images/edm_artists/2016/07/20/1469001488-73688444a73a76169d03b689a7e785cf1404904575.jpg"
//                                                    ]
//                                                ]
//            ]]
//        
//        var _output: [Station] = []
//        
//        for item_dict in dicts {
//            
//            let itemStation = Station.init(dict: item_dict as Dictionary<String, AnyObject>)
//            itemStation.dictionaryDidSet()
//            _output.append(itemStation)
//        }
        
        
//        dataRequest.onSuccess?(_output as AnyObject, "" as AnyObject);
        
        
        let param = [
            FilterType.Page.rawValue : (filter.offset/limitDefault + 1) as AnyObject,
            FilterType.Limit.rawValue : limitDefault as AnyObject,
            "user_id": (user_id ?? "") as AnyObject
        ]
         
         let uri = StationUriType.new_feeds.uri
         let url = self.makeURL(uri: uri, parameter: param.description)
         
         var request = Request(.get, url)
         request.parameters = param
         
         request.onSuccess = { (result) in
         
         var _output: [Station] = []
         if let results = result as? Dictionary<String, AnyObject> {
         
            if let data = results["data"] as? [Dictionary<String, AnyObject>]{
                
                for item_dict in data {
                    let itemStation = Station.init(dict: item_dict)
                    itemStation.dictionaryDidSet()
                    _output.append(itemStation)
                }
            }
         
         dataRequest.onSuccess?(_output as AnyObject, "" as AnyObject);
         
         } else {
         let error = HttpUtility.localParseError()
         dataRequest.onFailure?(error, "" as AnyObject);
         }
         
         }
         
         request.onFailure = { (_error) in
         dataRequest.onFailure?(_error, "" as AnyObject);
         }
         
         httpUtillty.request(sender: request)
        
    }
}
