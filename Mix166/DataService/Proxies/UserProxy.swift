//
//  UserProxy.swift
//  Mix166
//
//  Created by ttiamap on 7/18/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import Alamofire

typealias completeBlock = (_ data: AnyObject?,  _ moreInfo: AnyObject?) -> Void
typealias errorBlock = (_ error: AnyObject?, _ moreInfo: AnyObject?) -> Void

private enum UserUriType: String {
    
    case LoginFacebook           = "auth/loginFacebook"
    case LoginGoogle             = "auth/loginGoogle"
    case LogginNormal            = "auth/login"
    case Activities              = "edms/userActivities"
    case CheckLike               = "users/checkLikeObject"
    case GetObjectUser           = "edms/getObjectUser"

    
    var uri : String {
        switch self {
        default : return self.rawValue
        }
    }
}

struct LoginInfo {
    enum LoginType {
        case facebook
        case google
        case normal
    }
    
    var user_name: String
    var password: String
    var token: String
    var loginType: LoginType = .normal
}

class UserProxy: BaseProxy {
    
    enum Object_Type: String {
        case artist = "artist"
        case mix    = "mixset"
        case track  = "track"
        case video  = "video"
        
        
//        func str_objectType_user() -> String{
//            switch self {
//            case .artist:
//                //
//                return "edm_artist"
//                
//            case .mix:
//                return "edm_mix"
//                
//            case .track:
//                //
//                return "edm_song"
//                
//            case .video:
//                //
//                return "edm_video"
//            }
//        }
    }
    
    
    
    private let k_userinfo = "userloginInfo"
    
    var userInfo: UserInfo?{
        didSet{
            self.saveToNsUserDefaut(anyObject: self.userInfo?.dictionary as AnyObject, withKey: k_userinfo)
        }
    }
    
    var dictLike_Follow: Dictionary<String,Bool> = [:]
    
    var isLogout: Bool {
        return userInfo == nil
    }
    
    func logout(){
        self.userInfo = nil
    }
    
    func userLogginRestore(){
        if let dict = self.readNsUserDefaut(withKey: k_userinfo) as? Dictionary<String,AnyObject>{
            let _userInfo = UserInfo.init(dict: dict)
            self.userInfo = _userInfo
        }
    }
    
    func login(dataRequest: DataRequest, info: LoginInfo){
        
        var uri = ""
        var parameter: [String:String] = [:]
        
        switch info.loginType {
        case .facebook:
            //
            uri = UserUriType.LoginFacebook.rawValue
            parameter = ["access_token":info.token]
            break
        case .google:
            //
            uri = UserUriType.LoginGoogle.rawValue
            parameter = ["authorization_code":info.token]
            break
        default:
            //
            uri = UserUriType.LogginNormal.rawValue
            parameter = ["email":info.user_name,
                        "password": info.password,
                        "is_remember_me":"0"]
            break
        }
        
        let url = self.makeURL(uri: uri)
        
        var request = Request.init(.post, url)
        request.parameters = parameter as [String : AnyObject]
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                if let dict = results["data"] as? Dictionary<String,AnyObject>{
                    let output = UserInfo(dict: dict)
                    
                    self.userInfo = output
                    
                    dataRequest.onSuccess?(output, "" as AnyObject);
                }
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    func getObjectsUser(object_type: Object_Type, filter: Filter, dataRequest: DataRequest){
        
        if self.userInfo == nil {
            return
        }
        
        let param = [
            FilterType.Page.rawValue : (filter.offset/limitDefault + 1) as AnyObject,
            FilterType.Limit.rawValue : limitDefault as AnyObject,
            "user_id": self.userInfo!.id as AnyObject,
            "object_type": object_type.rawValue as AnyObject
        ]
        
        let uri = UserUriType.GetObjectUser.uri
        
        let url = self.makeURL(uri: uri, parameter: param.description)
        
        var request = Request(.get, url)
        request.parameters = param
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                
                var output: [ProductList] = []
                if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{
                    
                    var customDict: Dictionary<String, AnyObject> = [:]
                    
                    switch object_type {
                    case .mix:
                        //
                        customDict = ["objects":dicts as AnyObject,
                                    "type": Product_UI_Type.mixset_vertical_delete.rawValue as AnyObject]
                        break
                        
                    case .track:
                        //
                        customDict = ["objects":dicts as AnyObject,
                                      "type": Product_UI_Type.track_vertical_delete.rawValue as AnyObject]
                        break
                        
                    case .video:
                        //
                        customDict = ["objects":dicts as AnyObject,
                                      "type": Product_UI_Type.video_vertical_delete.rawValue as AnyObject]
                        break
                        
                    case .artist:
                        //
                        customDict = ["objects":dicts as AnyObject,
                                      "type": Product_UI_Type.deejay_vertical_follow.rawValue as AnyObject]
                        break
                        
                    }
                    
                    output = self.parsingResponseDataWithProductList(responseData: [customDict])
                }
                
                dataRequest.onSuccess?(output as AnyObject, "" as AnyObject);
                
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    func checkLikeWithoutRequest(object: AnyObject) -> Bool?{
        var currentObject: AnyObject?
        var id = ""
        var objectType = Object_Type.track
        
        // check station
        if let item = object as? Station{
            currentObject = item.object
            
            
        }else{
            currentObject = object
        }
        
        //
        if let _objects = currentObject as? Video{
            id = _objects.id
            objectType = .video
            
        }else if let _objects = currentObject as? Mix{
            id = _objects.id
            objectType = .mix
            
        }else if let _objects = currentObject as? Track{
            id = _objects.id
            objectType = .track
            
        }else if let _objects = currentObject as? Artist{
            id = _objects.id
            objectType = .artist
        }
        
        if !(id.length > 0){
            return nil
        }
        
        let str_checkFollowLocal = id+objectType.rawValue
        if let is_like = self.checkUserLike_Follow(str_id: str_checkFollowLocal){
            return is_like
        }
        
        return nil
    }
    
    func checkLike(object: AnyObject, dataRequest: DataRequest){
        if self.userInfo == nil {
            return
        }
        
        let uri = UserUriType.CheckLike.uri
        
        var currentObject: AnyObject?
        var id = ""
        var objectType = Object_Type.track
        
        // check station
        if let item = object as? Station{
            currentObject = item.object
            
        }else{
            currentObject = object
        }
        
        //
        if let _objects = currentObject as? Video{
            id = _objects.id
            objectType = .video
            
        }else if let _objects = currentObject as? Mix{
            id = _objects.id
            objectType = .mix
            
        }else if let _objects = currentObject as? Track{
            id = _objects.id
            objectType = .track
            
        }else if let _objects = currentObject as? Artist{
            id = _objects.id
            objectType = .artist
        }
        
        if !(id.length > 0){
            return
        }
        
        let str_checkFollowLocal = id+objectType.rawValue
        if let is_like = self.checkUserLike_Follow(str_id: str_checkFollowLocal){
            if let error = HttpUtility.makeNSerror(errorCode: 0, message: ""){
                dataRequest.onSuccess?(is_like as AnyObject, error as AnyObject)
                return
            }
        }
        
        let param = ["object_id": id as AnyObject,
                     "object_type": objectType.rawValue as AnyObject,
                     "user_id": self.userInfo!.id as AnyObject
                    ]
        
        let url = self.makeURL(uri: uri)
        
        var request = Request(.get, url)
        
        request.parameters = param as [String : AnyObject]
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                if let error = HttpUtility.makeNSError(result: results){
                    
                    if error.code == 0{
                        // success
                        
                        if let isLike = results["data"] as? Bool{
                            self.dictLike_Follow[str_checkFollowLocal] = isLike
                            dataRequest.onSuccess?(isLike as AnyObject, error as AnyObject)
                        
                            return
                        }else{
                            dataRequest.onSuccess?(false as AnyObject, error as AnyObject)
                        }
                    }else{
                        // fail
                        dataRequest.onFailure?(error, "" as AnyObject);
                        return
                    }
                    
                } else {
                    let error = HttpUtility.localParseError()
                    dataRequest.onFailure?(error, "" as AnyObject);
                    return
                }
            }
            let error = HttpUtility.localParseError()
            dataRequest.onFailure?(error, "" as AnyObject);
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)

    }
    
    func activities(is_cancel_status: Bool?, object: AnyObject, dataRequest: DataRequest){
        
        if self.userInfo == nil {
            let error = HttpUtility.makeNSerror(errorCode: ErrorCode.userInfoNotFound.rawValue, message: AppMixError.userinfo_notfound.rawValue)
            dataRequest.onFailure?(error, "" as AnyObject);
            return
        }
        
        let uri = UserUriType.Activities.rawValue
        
        var currentObject: AnyObject?
        var id = ""
        var objectType = Object_Type.track
        
        // check station
        if let item = object as? Station{
            currentObject = item.object
            
        }else{
            currentObject = object
        }
            
        //
        if let _objects = currentObject as? Video{
            id = _objects.id
            objectType = .video
            
        }else if let _objects = currentObject as? Mix{
            id = _objects.id
            objectType = .mix
            
        }else if let _objects = currentObject as? Track{
            id = _objects.id
            objectType = .track
            
        }else if let _objects = currentObject as? Artist{
            id = _objects.id
            objectType = .artist
            
        }
        
        var param: Dictionary<String, AnyObject> = [:]
        
        if is_cancel_status ?? false{
            param = [
                "object_id": id as AnyObject,
                "object_type": objectType.rawValue as AnyObject,
                "user_id": self.userInfo!.id as AnyObject,
                "action_type": "unlike" as AnyObject
            ]
        }else{
            param = [
                "object_id": id as AnyObject,
                "object_type": objectType.rawValue as AnyObject,
                "user_id": self.userInfo!.id as AnyObject
            ]
        }
        
        let str_checkFollowLocal = id+objectType.rawValue
        
        let url = self.makeURL(uri: uri)
        
        var request = Request(.post, url)
        
        request.parameters = param as [String : AnyObject]
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                
                if let error = HttpUtility.makeNSError(result: results){
                    
                    if error.code == 0{
                        // success
                        
                        if let isLike = results["status_code"] as? Int{
                            self.dictLike_Follow[str_checkFollowLocal] = (isLike == 1)
                            dataRequest.onSuccess?(isLike as AnyObject, error as AnyObject)
                        }
                        return
                    }else{
                        // fail
                        dataRequest.onFailure?(error, "" as AnyObject);
                        return
                    }
                    
                } else {
                    let error = HttpUtility.localParseError()
                    dataRequest.onFailure?(error, "" as AnyObject);
                    return
                }
            }
            let error = HttpUtility.localParseError()
            dataRequest.onFailure?(error, "" as AnyObject);
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    func checkUserLike_Follow(str_id: String) -> Bool?{
        if let isLike = self.dictLike_Follow[str_id]{
            return isLike
        }
        return nil
    }
    
    //MARK: Get Version
    func apiGetVersion(complete: @escaping completeBlock, error: @escaping errorBlock){
        
        
        //https://api.fptplay.net.vn/api/v1.0_a/tv?st=yksGsgaAKsS16PQ7OARWLQ&e=1373592973
        
        let timeExpire = self.getStaticTimeExpireFromLocalDevice()
        let urlString = "http://api-cms.fptplay.tv/api/version/index.php?st=" + self.getSecureLinkAppVersion(_expireTime: String(timeExpire)) + "&e=\(timeExpire)"
        
        
        let parameter = ["app_name":"mix166_ios"]
        
        Alamofire.request(urlString, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: nil).responseJSON{
            response in
            
            if let JSON = response.result.value as? Dictionary<String, AnyObject> {
                
                print(JSON)
                
                if let _dict = JSON["data"] as? Dictionary<String, AnyObject>{
                    
                    if let versionCode = _dict["version_code"]{
                        if String(describing: versionCode) == "0"{
                            // apple in review finish
                            if let str_version = _dict["version_number"] as? String{
                                complete(str_version as AnyObject,nil)
                            }
                        }
                    }
                    return
                    
                }else{
                    error("version not found" as AnyObject, nil)
                }
                
            }else{
                print(response.error.debugDescription)
            }
        }
    }
}
