//
//  PlaylistProxy.swift
//  Mix166
//
//  Created by ttiamap on 6/26/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

private enum PlaylistUriType: String {
    
    case playlist                   = "playlists/getUserPlaylist"
    case playlistDetail             = "playlists/detail"
    case playlist_create            = "playlists/add"
    case playlist_addSong           = "playlists/addSongToPlaylist"
    case playlist_removeSong        = "playlists/removeSongFromPlaylist"
    case playlist_removePlaylist    = "playlists/delete"
    
    
    var uri : String {
        switch self {
        default : return self.rawValue
        }
    }
    
}

class PlaylistProxy: BaseProxy{
    
    /** Method  call API Playlist with user id
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return [Playlist] if success
     ex: : api.mix166.com/api/v1.1_a/playlists/getUserPlaylist?user_id=2008072&page=1&limit=20
     */
    func playlist(offset: Int, user_id: String, dataRequest: DataRequest){
        
        let uri = PlaylistUriType.playlist.rawValue
        
        let param = [
            FilterType.Page.rawValue : (offset/limitDefault + 1) as AnyObject,
            FilterType.Limit.rawValue : limitDefault as AnyObject,
            "user_id": user_id
        ] as [String : AnyObject]
        
        let url = self.makeURL(uri: uri, parameter: param.description)
        
        var request = Request(.get, url)
        request.parameters = param
        
        request.onSuccess = { (result) in
            
            var arrPlaylist : [Playlist] = []
            if let results = result as? Dictionary<String, AnyObject> {
                if let data = results["data"] as? [Dictionary<String, AnyObject>]{
                    
                    for item in data{
                        let _playlist = Playlist.init(dict: item)
                        _playlist.dictionaryDidSet()
                        
                        arrPlaylist.append(_playlist)
                    }
                }
                
                let output = arrPlaylist
                
                dataRequest.onSuccess?(output as AnyObject, "" as AnyObject);
                
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
            
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    
    /** Method  call API get song líst in Playlist with playlistId
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return playlistDetail if success
     ex: : api.mix166.com/api/v1.1_a/playlists/detail?id=2002615
     */
    func playlistDetail(filter: Filter, dataRequest: DataRequest){
        
        if let itemPlaylist = filter.requestObject as? Playlist{
            let uri = PlaylistUriType.playlistDetail.rawValue
            
            let param = [
                FilterType.Page.rawValue : (filter.offset/limitDefault + 1) as AnyObject,
                FilterType.Limit.rawValue : limitDefault as AnyObject,
                "id": itemPlaylist.id
            ] as [String: AnyObject]
            
            let url = self.makeURL(uri: uri, parameter: param.description)
            
            var request = Request(.get, url)
            request.parameters = param
            
            request.onSuccess = { (result) in
                
                if let results = result as? Dictionary<String, AnyObject> {
                    if let dict = results["data"] as? Dictionary<String, AnyObject>{
                        let _playlist_Detail = Playlist_Detail.init(dict: dict)
                        _playlist_Detail.dictionaryDidSet()
                        dataRequest.onSuccess?(_playlist_Detail as AnyObject, "" as AnyObject);
                    }
                    let error = HttpUtility.makeNSerror(errorCode: -1, message: AppMixError.data_notfound.rawValue)
                    dataRequest.onFailure?(error, "" as AnyObject)
                } else {
                    let error = HttpUtility.localParseError()
                    dataRequest.onFailure?(error, "" as AnyObject)
                }
                
            }
            
            request.onFailure = { (_error) in
                dataRequest.onFailure?(_error, "" as AnyObject);
            }
            
            httpUtillty.request(sender: request)
        }else{
            let error = HttpUtility.makeNSerror(errorCode: -1, message: "Filter RequestData Not Found")
            dataRequest.onFailure?(error, "" as AnyObject);
        }
    }
    
    /** Method  call API get song líst in Playlist with playlistId
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return playlistDetail if success
     ex: : api.mix166.com/api/v1.1_a/playlists/detail?id=2002615
     */
    func playlistCreate(itemCreate: PlaylistCreate, user_id: String , dataRequest: DataRequest){
        
        let uri = PlaylistUriType.playlist_create.rawValue
        
        let param = [
            "playlist_name": itemCreate.name,
            "description": itemCreate.description,
            "user_id": user_id
        ]
        
        let url = self.makeURL(uri: uri)
        
        var request = Request(.post, url)
        
        request.parameters = param as [String : AnyObject]
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                
                print(results)
                
                if let error = HttpUtility.makeNSError(result: results){
                    
                    if error.code == 0{
                        // success
                        if let dict = results["data"] as? Dictionary<String, AnyObject>{
                            let _playlist = Playlist(dict: dict)
                            dataRequest.onSuccess?(_playlist as AnyObject, error as AnyObject);
                            return
                        }
                    }else{
                        // fail
                        dataRequest.onFailure?(error, "" as AnyObject);
                        return
                    }
                    
                } else {
                    let error = HttpUtility.localParseError()
                    dataRequest.onFailure?(error, "" as AnyObject);
                    return
                }
            }
            let error = HttpUtility.localParseError()
            dataRequest.onFailure?(error, "" as AnyObject);
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    /** Method  call API get song líst in Playlist with playlistId
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return playlistDetail if success
     ex: : api.mix166.com/api/v1.1_a/playlists/detail?id=2002615
     */
    func playlistRemove(_playlist: Playlist, user_id: String , dataRequest: DataRequest){
        
        
        let uri = PlaylistUriType.playlist_removePlaylist.rawValue
        
        let param = [
            "playlist_id": _playlist.id,
            "user_id": user_id
        ]
        
        let url = self.makeURL(uri: uri)
        
        var request = Request(.delete, url)
        
        request.parameters = param as [String : AnyObject]
        
        request.onSuccess = { (result) in

            if let results = result as? Dictionary<String, AnyObject> {
                if let error = HttpUtility.makeNSError(result: results){
                    
                    if error.code == 0{
                        // success
                        dataRequest.onSuccess?(nil, error)
                    }else{
                        // fail
                        dataRequest.onFailure?(error, "" as AnyObject);
                    }
                    
                } else {
                    let error = HttpUtility.localParseError()
                    dataRequest.onFailure?(error, "" as AnyObject);
                }
            }
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    
    
    /** Method  call API get song líst in Playlist with playlistId
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return playlistDetail if success
     ex: : api.mix166.com/api/v1.1_a/playlists/detail?id=2002615
     */
    func playlistAddSong(_playlist: Playlist, itemSong: [Song], user_id: String , dataRequest: DataRequest){
        
        var songIds: [String] = []
        
        for item in itemSong {
            songIds.append(item.id)
        }
        
        let _stringIds = songIds.joined(separator: ",")
        
        let uri = PlaylistUriType.playlist_addSong.rawValue
        
        let param = [
            "song_id": _stringIds,
            "playlist_id": _playlist.id,
            "user_id": user_id
        ]
        
        let url = self.makeURL(uri: uri)
        
        var request = Request(.post, url)
        
        request.parameters = param as [String : AnyObject]
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                if let error = HttpUtility.makeNSError(result: results){
                    
                    if error.code == 0{
                        // success
                        dataRequest.onSuccess?(nil, error)
                    }else{
                        // fail
                        dataRequest.onFailure?(error, "" as AnyObject);
                    }
                    
                } else {
                    let error = HttpUtility.localParseError()
                    dataRequest.onFailure?(error, "" as AnyObject);
                }
            }
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    
    /*
     remove song
     method: post
     url: api.mix166.com/api/v1.1_a/playlists/removeSongFromPlaylist
     body:{"song_ids":"2003755,2000380","user_id":"3000041","playlist_id":"2002616"}
 */
    func playlistRemoveSong(_playlist: Playlist, itemSong: [Song], user_id: String , dataRequest: DataRequest){
        
        var songIds: [String] = []
        
        for item in itemSong {
            songIds.append(item.id)
        }
        
        let _stringIds = songIds.joined(separator: ",")
        
        let uri = PlaylistUriType.playlist_removeSong.rawValue
        
        let param = [
            "song_ids": _stringIds,
            "playlist_id": _playlist.id,
            "user_id": user_id
        ]
        
        let url = self.makeURL(uri: uri)
        
        var request = Request(.post, url)
        
        request.parameters = param as [String : AnyObject]
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                if let error = HttpUtility.makeNSError(result: results){
                    
                    if error.code == 0{
                        // success
                        dataRequest.onSuccess?(nil, error)
                    }else{
                        // fail
                        dataRequest.onFailure?(error, "" as AnyObject);
                    }
                    
                } else {
                    let error = HttpUtility.localParseError()
                    dataRequest.onFailure?(error, "" as AnyObject);
                }
            }
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
}
