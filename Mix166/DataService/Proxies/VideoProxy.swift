//
//  VideoProxy.swift
//  Mix166
//
//  Created by ttiamap on 8/14/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

private enum VideoUriType: String {
    
    case related            = "videos/videoRelated"                      //get top artist list

    
    var uri : String {
        switch self {
        default : return self.rawValue
        }
    }
}

class VideoProxy: BaseProxy{
    
    func videoRelated(filter: Filter, dataRequest: DataRequest){
        
        if let _video = filter.requestObject as? Video{
            
            let param = [
                FilterType.Page.rawValue : (filter.offset/limitDefault + 1) as AnyObject,
                FilterType.Limit.rawValue : limitDefault as AnyObject,
                "video_id": _video.id
            ] as [String : AnyObject]
            
            let uri = VideoUriType.related.uri
            let url = self.makeURL(uri: uri, parameter: param.description)
            
            var request = Request(.get, url)
            request.parameters = param
            
            request.onSuccess = { (result) in
                
                if let results = result as? Dictionary<String, AnyObject> {
                    
                    var output: [ProductList] = []
                    if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{
                        
                        let customDict: Dictionary<String, AnyObject> = ["objects":dicts as AnyObject,
                                                                         "type": Product_UI_Type.video_vertical.rawValue as AnyObject,
                                                                         "type_header": Product_UI_Header_Type.song_infoHeader.rawValue as AnyObject,
                                                                         "object_header": _video as AnyObject]
                        output = self.parsingResponseDataWithProductList(responseData: [customDict])
                    }
                    
                    dataRequest.onSuccess?(output as AnyObject, "" as AnyObject);
                    
                } else {
                    let error = HttpUtility.localParseError()
                    dataRequest.onFailure?(error, "" as AnyObject);
                }
                
            }
            
            request.onFailure = { (_error) in
                dataRequest.onFailure?(_error, "" as AnyObject);
            }
            
            httpUtillty.request(sender: request)
        }
    }
}
