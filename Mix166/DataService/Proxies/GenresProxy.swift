//
//  GenresProxy.swift
//  Mix166
//
//  Created by ttiamap on 5/24/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

private enum GenresUriType: String {
    
    case GenresMixList          = "mixs/genresMixList"
    case GenresTrackList        = "songs/genresTrackList"
    case GenresVideoList        = "videos/genresVideoList"
    
    var uri : String {
        switch self {
        default : return self.rawValue
        }
    }
    
}

class GenresProxy: BaseProxy {

    /** Method  call API genresMixList
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return [Mix] if success
     ex: : api.mix166.com/api/v1.1_a/mixs/genresMixList?genres_id=2000000&page=1&limit=2
     */
    func getMixList(offset: Int, dataRequest: DataRequest, genres_id: String){
        
        let param = [
            FilterType.Page.rawValue : (offset/limitDefault + 1) as AnyObject,
            FilterType.Limit.rawValue : limitDefault as AnyObject,
            "genres_id": genres_id
        ] as [String: AnyObject]
        
        
        
        let uri = GenresUriType.GenresMixList.rawValue
        let url = self.makeURL(uri: uri, parameter: param.description)
        
        var request = Request(.get, url)
        request.parameters = param
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                
                let output = Feature(dict: results)
                
                dataRequest.onSuccess?(output, "" as AnyObject);
                
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
            
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    /** Method  call API genresTrackList
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return [Track] if success
     ex: : api.mix166.com/api/v1.1_a/songs/genresTrackList?genres_id=2000000&page=1&limit=2
     */
    func getTrackList(offset: Int, dataRequest: DataRequest, genres_id: String){
        
        let param = [
            FilterType.Page.rawValue : (offset/limitDefault + 1) as AnyObject,
            FilterType.Limit.rawValue : limitDefault as AnyObject,
            "genres_id": genres_id
        ] as [String: AnyObject]
        
        let uri = GenresUriType.GenresTrackList.rawValue
        let url = self.makeURL(uri: uri, parameter: param.description)
        
        var request = Request(.get, url)
        request.parameters = param
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                
                let output = Feature(dict: results)
                
                dataRequest.onSuccess?(output, "" as AnyObject);
                
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
            
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    /** Method  call API genresTrackList
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return [Track] if success
     ex: : api.mix166.com/api/v1.1_a/songs/genresTrackList?genres_id=2000000&page=1&limit=2
     */
    func getVideoList(offset: Int, dataRequest: DataRequest, genres_id: String){
        
        let param = [
            FilterType.Page.rawValue : (offset/limitDefault + 1) as AnyObject,
            FilterType.Limit.rawValue : limitDefault as AnyObject,
            "genres_id": genres_id
        ] as [String: AnyObject]
        
        let uri = GenresUriType.GenresVideoList.rawValue
        let url = self.makeURL(uri: uri, parameter: param.description)
        
        var request = Request(.get, url)
        request.parameters = param
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                
                let output = Feature(dict: results)
                
                dataRequest.onSuccess?(output, "" as AnyObject);
                
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
            
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
}
