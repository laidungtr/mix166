//
//  ArtistProxy.swift
//  Mix166
//
//  Created by ttiamap on 5/29/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation


private enum ArtistUriType: String {
    
    case artists            = "artists/getListTopArtist"                      //get top artist list
    case detail             = "artists/detail"
    case mixMore            = "artists/mixmore"
    case trackMore          = "artists/trackmore"
    case videoMore          = "artists/videomore"
    
    var uri : String {
        switch self {
        default : return self.rawValue
        }
    }
}


class ArtistProxy: BaseProxy{
    
    func artistsTopDJ(userId: String?, filter: Filter, dataRequest: DataRequest){
        
        var param : Dictionary<String,AnyObject> = [:]
        
        if userId == nil {
            param = [
                FilterType.Page.rawValue : (filter.offset/limitDefault + 1) as AnyObject,
                FilterType.Limit.rawValue : limitDefault as AnyObject
            ]
        }else{
            param = [
                FilterType.Page.rawValue : (filter.offset/limitDefault + 1) as AnyObject,
                FilterType.Limit.rawValue : limitDefault as AnyObject,
                "user_id": userId! as AnyObject
            ]
        }
        
        let uri = ArtistUriType.artists.uri
        let url = self.makeURL(uri: uri, parameter: param.description)
        
        var request = Request(.get, url)
        request.parameters = param
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                
                var arrItem : [Artist] = []
                
                if let dicts = results["items"] as? [Dictionary<String, AnyObject>]{
                    for _itemDict in dicts {
                        let item = Artist.init(dict: _itemDict)
                        arrItem.append(item)
                    }
                }
                
                dataRequest.onSuccess?(arrItem as AnyObject, "" as AnyObject);
                
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
            
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    func artistsDetail(user_id: String?, filter: Filter, dataRequest: DataRequest){
        
        if let itemArtist = filter.requestObject as? Artist{
            let param = [
                FilterType.Page.rawValue : (filter.offset/limitDefault + 1) as AnyObject,
                FilterType.Limit.rawValue : limitDefault as AnyObject,
                "id": itemArtist.id as AnyObject
            ]
            
            let uri = ArtistUriType.detail.uri
            let url = self.makeURL(uri: uri, parameter: param.description)
            
            var request = Request(.get, url)
            request.parameters = param
            
            request.onSuccess = { (result) in

                if let results = result as? Dictionary<String, AnyObject> {
                    
                    var output: [ProductList] = []
                    if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{
                        output = self.parsingResponseDataWithProductList(responseData: dicts)
                    }
                    
                    dataRequest.onSuccess?(output as AnyObject, "" as AnyObject);
                    
                } else {
                    let error = HttpUtility.localParseError()
                    dataRequest.onFailure?(error, "" as AnyObject);
                }
            }
            
            request.onFailure = { (_error) in
                dataRequest.onFailure?(_error, "" as AnyObject);
            }
            
            httpUtillty.request(sender: request)
        }
    }
    
    func artistsMixMore(filter: Filter, dataRequest: DataRequest){
        if let itemArtist = filter.requestObject as? Artist{
            let param = [
                FilterType.Page.rawValue : (filter.offset/limitDefault + 1) as AnyObject,
                FilterType.Limit.rawValue : limitDefault as AnyObject,
                "id": itemArtist.id as AnyObject
            ]
            
            let uri = ArtistUriType.mixMore.uri
            let url = self.makeURL(uri: uri, parameter: param.description)
            
            var request = Request(.get, url)
            request.parameters = param
            
            request.onSuccess = { (result) in
                
                if let results = result as? Dictionary<String, AnyObject> {
                    
                    var output: [ProductList] = []
                    if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{
                        
                        let customDict: Dictionary<String, AnyObject> = ["objects":dicts as AnyObject,
                                                                         "type": Product_UI_Type.mixset_vertical.rawValue as AnyObject]
                        output = self.parsingResponseDataWithProductList(responseData: [customDict])
                    }
                    
                    dataRequest.onSuccess?(output as AnyObject, "" as AnyObject);
                    
                } else {
                    let error = HttpUtility.localParseError()
                    dataRequest.onFailure?(error, "" as AnyObject);
                }
            }
            
            request.onFailure = { (_error) in
                dataRequest.onFailure?(_error, "" as AnyObject);
            }
            
            httpUtillty.request(sender: request)
        }
    }
    
    func artistsTrackMore(filter: Filter, dataRequest: DataRequest){
        if let itemArtist = filter.requestObject as? Artist{
            let param = [
                FilterType.Page.rawValue : (filter.offset/limitDefault + 1) as AnyObject,
                FilterType.Limit.rawValue : limitDefault as AnyObject,
                "id": itemArtist.id as AnyObject
            ]
            
            let uri = ArtistUriType.trackMore.uri
            let url = self.makeURL(uri: uri, parameter: param.description)
            
            var request = Request(.get, url)
            request.parameters = param
            
            request.onSuccess = { (result) in
                
                if let results = result as? Dictionary<String, AnyObject> {
                    
                    var output: [ProductList] = []
                    if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{
                        
                        let customDict: Dictionary<String, AnyObject> = ["objects":dicts as AnyObject,
                                                                         "type": Product_UI_Type.track_vertical.rawValue as AnyObject]
                        output = self.parsingResponseDataWithProductList(responseData: [customDict])
                    }
                    
                    dataRequest.onSuccess?(output as AnyObject, "" as AnyObject);
                    
                } else {
                    let error = HttpUtility.localParseError()
                    dataRequest.onFailure?(error, "" as AnyObject);
                }
            }
            
            request.onFailure = { (_error) in
                dataRequest.onFailure?(_error, "" as AnyObject);
            }
            
            httpUtillty.request(sender: request)
        }
    }
    
    func artistsVideoMore(filter: Filter, dataRequest: DataRequest){
        if let itemArtist = filter.requestObject as? Artist{
            let param = [
                FilterType.Page.rawValue : (filter.offset/limitDefault + 1) as AnyObject,
                FilterType.Limit.rawValue : limitDefault as AnyObject,
                "id": itemArtist.id as AnyObject
            ]
            
            let uri = ArtistUriType.videoMore.uri
            let url = self.makeURL(uri: uri, parameter: param.description)
            
            var request = Request(.get, url)
            request.parameters = param
            
            request.onSuccess = { (result) in
                
                if let results = result as? Dictionary<String, AnyObject> {
                    
                    var output: [ProductList] = []
                    if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{
                        
                        let customDict: Dictionary<String, AnyObject> = ["objects":dicts as AnyObject,
                                                                         "type": Product_UI_Type.video_vertical.rawValue as AnyObject]
                        output = self.parsingResponseDataWithProductList(responseData: [customDict])
                    }
                    
                    dataRequest.onSuccess?(output as AnyObject, "" as AnyObject);
                    
                } else {
                    let error = HttpUtility.localParseError()
                    dataRequest.onFailure?(error, "" as AnyObject);
                }
            }
            
            request.onFailure = { (_error) in
                dataRequest.onFailure?(_error, "" as AnyObject);
            }
            
            httpUtillty.request(sender: request)
        }
    }
    
}
