//
//  ChartProxy.swift
//  Mix166
//
//  Created by ttiamap on 5/26/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

private enum ChartUriType: String {
    
    case ChartMixset        = "mixs/chartMixList"
    case ChartTracks        = "songs/chartTrackList"
    case ChartVideos        = "videos/chartVideoList"
    
    var uri : String {
        switch self {
        default : return self.rawValue
        }
    }
}

class ChartProxy: BaseProxy {
    
    /** Method  call API Home Feature without user loggin
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return Feature if success
     ex: : api.mix166.com/api/v1.1_a/index?user_id=3000020
     */
    func chartGet(type: SongType, filter: Filter, dataRequest: DataRequest){
        
        var uri = ""
        switch type {
        case .Mix:
            //
            uri = ChartUriType.ChartMixset.rawValue
            break
            
        case .Track:
            //
            uri = ChartUriType.ChartTracks.rawValue
            break
            
        case .Video:
            //
            uri = ChartUriType.ChartVideos.rawValue
            break
            
        case .Artist:
            //
            break
            
        }
        let url = self.makeURL(uri: uri)
        
        var request = Request(.get, url)
        
        let param = [
            FilterType.Page.rawValue : (filter.offset/limitDefault + 1) as AnyObject,
            FilterType.Limit.rawValue : limitDefault as AnyObject
        ]
        
        request.parameters = param
        
        request.onSuccess = { (result) in

            if let results = result as? Dictionary<String, AnyObject> {
                var arrItem : [Song] = []
                
                if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{

                    for _itemDict in dicts {
                        switch type {
                        case .Mix:
                            //
                            let item = Mix.init(dict: _itemDict)
                            item.parsingSongMasup()
                            arrItem.append(item)
                            break
                            
                        case .Track:
                            //
                            let item = Track.init(dict: _itemDict)
                            arrItem.append(item)
                            break
                            
                        case .Video:
                            //
                            let item = Video.init(dict: _itemDict)
                            arrItem.append(item)
                            break
                            
                        case .Artist:
                            //
                            break
                            
                        }
                    }
                }
                
                dataRequest.onSuccess?(arrItem as AnyObject, "" as AnyObject);
                
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
            
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
}
