//
//  DownloadProxy.swift
//  Mix166
//
//  Created by ttiamap on 7/24/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import Alamofire

extension FileManager {
    func clearTmpDirectory() {
        do {
            let tmpDirectory = try contentsOfDirectory(atPath: NSTemporaryDirectory())
            try tmpDirectory.forEach {[unowned self] file in
                let path = String.init(format: "%@%@", NSTemporaryDirectory(), file)
                try self.removeItem(atPath: path)
            }
        } catch {
            print(error)
        }
    }
}

typealias progressDownloadBlock = (_ value: Double) -> Void
typealias completeDownloadBlock = (_ str_location: String,_ info: DownloadInfo? ) -> Void
typealias failDownloadBlock = (_ str_error: String, _ moreInfo: AnyObject?) -> Void


class DownloadProxy: BaseProxy, URLSessionDelegate, URLSessionDownloadDelegate {
    
    enum ProgressType {
        case percent // from 0 - 100
        case value // from 0 to 1
        case circle // from 0 - 360
    }
    
    var progressDownloadBlock: (progressDownloadBlock)?
    var completeBlock: (completeDownloadBlock)?
    var failBlock: (failDownloadBlock)?
    
    // SearchViewController creates downloadsSession
    var activeDownloads: [URL: DownloadInfo] = [:]
    var type_progress: ProgressType = .circle
    
    var totalSizeDownload: Int64 = 0
    
    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: String) -> String {
        if let _url = URL.init(string: url){
            return documentsPath.appendingPathComponent(_url.lastPathComponent).absoluteString
        }
        return ""
    }
    
    func localFilePathURL( url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    
    lazy var downloadsSession: URLSession = {
        //    let configuration = URLSessionConfiguration.default
        let configuration = URLSessionConfiguration.background(withIdentifier: "bgSessionConfiguration")
        return URLSession(configuration: configuration, delegate: self, delegateQueue: .main)
        
    }()
    
    // MARK: - Delete file
    func deleteFileFromDirectory(filePath: String,
                                successBlock:@escaping ()->(),
                                    errorBlock: @escaping (_ str_error: String) -> ()){
        let fileManager = FileManager.default
        do {
            
            try fileManager.removeItem(atPath: "\(self.documentsPath.absoluteURL.path)/\(filePath)")
            
            successBlock()
            
        } catch {
            errorBlock("file doesn't exist")
        }
        
        
    }
    
    // MARK: - Download methods called by TrackCell delegate methods
    
    func startDownload(itemSong: Song?, type: SongType) {

        if itemSong != nil {
            
            let freeSpace = Utils.freeDiskSpaceInBytes() - self.totalSizeDownload
            
            if freeSpace < (1024*50){
                // if disk space not enough for download
                self.failBlock?("Syncing Fail \n Not enough space to Sync, please try again later", nil)
                return
            }
            // 1
            self.setItemSongDownloadToDB(itemSong: itemSong!, type: type, successBlock: {
                
                // show alert for syncing music
                self.failBlock?("Syncing \(itemSong!.name)", nil)
                
                let download = DownloadInfo(_song: itemSong!, _type: type)
                // 2
                
                if let url = URL.init(string: itemSong!.mp3.playing_link){
                    download.task = self.downloadsSession.downloadTask(with: url)
                    // 3
                    download.task!.resume()
                    // 4
                    download.isDownloading = true
                    // 5
                    self.activeDownloads[url] = download
                    
                    self.totalSizeDownload += download.song?.mp3.file_size ?? 0
                    
                }else{
                    
                    self.failBlock?(AppMixError.song_notfound.rawValue, nil)
                }
            }, errorBlock: { (str_error) in
                self.failBlock?(str_error, nil)
            })
        }
    }
    
    func pauseDownload(_ song: Song) {
        if let url = URL.init(string: song.mp3.playing_link){
            guard let download = activeDownloads[url] else { return }
            if download.isDownloading {
                download.task?.cancel(byProducingResumeData: { data in
                    download.resumeData = data
                })
                download.isDownloading = false
            }
        }
    }
    
    func cancelDownload(_ song: Song) {
        if let url = URL.init(string: song.mp3.playing_link){
            if let download = activeDownloads[url] {
                download.task?.cancel()
                activeDownloads[url] = nil
            }
        }
    }
    
    func resumeDownload(_ song: Song) {
        if let url = URL.init(string: song.mp3.playing_link){
            guard let download = activeDownloads[url] else { return }
            if let resumeData = download.resumeData {
                download.task = downloadsSession.downloadTask(withResumeData: resumeData)
            } else {
                download.task = downloadsSession.downloadTask(with: url)
            }
            download.task!.resume()
            download.isDownloading = true
        }
    }
    
    func fetchDownloadSongData(type: SongType?, dataRequest: DataRequest){
        
        CoreData.getDownloadSongInDBWith(type: type, successBlock: { (result) in
            //
            dataRequest.onSuccess?(result as AnyObject, "" as AnyObject);
        }) { (str_error) in
            //
            dataRequest.onFailure?(nil, str_error as AnyObject);
        }
    }
    
    // MARK: Database
    func setItemSongDownloadToDB(itemSong: Song, type: SongType,
                                 successBlock:@escaping ()->(),
                                 errorBlock: @escaping (_ str_error: String) -> ()){
        CoreData.setDownloadSongToDBWith(type: type, itemSong: itemSong, successBlock: successBlock, errorBlock: errorBlock)
    }
    
    func updateSongDownloadToDB(itemSong: Song?, type: SongType?,
                successBlock:@escaping ()->(),
                errorBlock: @escaping (_ str_error: String) -> ()){
        CoreData.updateDownloadSong(type: type, itemSong: itemSong, successBlock: successBlock, errorBlock: errorBlock)
    }
    
    //MARK: - Delegate
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        if totalBytesExpectedToWrite > 0 {
            
            guard let sourceURL = downloadTask.originalRequest?.url else { return }
            
            if let _downloadInfo = self.activeDownloads[sourceURL]{
                let progress = Double(totalBytesWritten) / Double(totalBytesExpectedToWrite)
                switch self.type_progress {
                    
                case .percent:
                    _downloadInfo.progress = progress*100
                    self.activeDownloads[sourceURL] = _downloadInfo
                    self.progressDownloadBlock?(progress*100)
                    break
                    
                case .circle:
                    //
                    _downloadInfo.progress = Double(360 * progress)
                    self.activeDownloads[sourceURL] = _downloadInfo
                    self.progressDownloadBlock?(Double(360 * progress))
                    break
                    
                default:
                    _downloadInfo.progress = progress
                    self.activeDownloads[sourceURL] = _downloadInfo
                    self.progressDownloadBlock?(progress)
                    break
                }
            }
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        debugPrint("Download finished: \(location)")

        guard let sourceURL = downloadTask.originalRequest?.url else { return }
        
        // 2
        let destinationURL = localFilePathURL(url: sourceURL)
        // repace if it exists
        let fileManager = FileManager.default
        
        try? fileManager.removeItem(at: destinationURL)
        
        do {
            try fileManager.copyItem(at: location, to: destinationURL)
            
            if let _downloadInfo = self.activeDownloads[sourceURL]{
                self.completeBlock?(destinationURL.lastPathComponent, _downloadInfo)
            }
            
        } catch let error {
            self.failBlock?(error.localizedDescription ,nil)
        }
        
        // remove download tmp file from tmp folder
        try? fileManager.removeItem(at: location)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        debugPrint("Task completed: \(task), error: \(String(describing: error))")
        if error != nil {
            self.failBlock?(error!.localizedDescription ,nil)
        }
    }
    
    func getDownloadInfo(itemSong: Song?, type: SongType?) -> DownloadInfo?{
        if itemSong == nil || type == nil {
            return nil
        }
        
        if let url = URL.init(string: itemSong!.mp3.playing_link){
            return self.activeDownloads[url]
        }
        
        return nil
    }
    
    
   /* var requests: Dictionary<String,DownloadInfo> = [:]
    

    
    func downloadSong(itemSong: Song?, type: SongType,
                      successBlock:@escaping ()->(),
                      errorBlock: @escaping (_ str_error: String) -> ()){
        
        var _itemSong = itemSong
        if _itemSong != nil {
            let destination = DownloadRequest.suggestedDownloadDestination()
            Alamofire.download( itemSong!.mp3.playing_link, to: destination)
                
                .downloadProgress { progress in
                    
                }
                
                .response { response in
                    
                    var a = ""
                    a = response.destinationURL!.lastPathComponent
                    print(a)
                    
                    _itemSong?.mp3.playing_linkOffline = response.destinationURL!.lastPathComponent
                    print(_itemSong?.mp3.playing_linkOffline)
                
                    _itemSong?.mp3.playing_linkOffline = a
                    print(_itemSong?.mp3.playing_linkOffline)
            }
        }
        
        /*
         
            self.setItemSongDownloadToDB(itemSong: itemSong!, type: type, successBlock: {
//                let destination = DownloadRequest.suggestedDownloadDestination()
                
                var request: DownloadRequest?
                
                request = Alamofire.download(itemSong!.mp3.playing_link, to: destination).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { (progress) in
                    
                    print("Progress: \(progress.fractionCompleted) ----- totalCount: \(progress.totalUnitCount)")
                    
                    } .validate().responseData { ( response ) in
                        do {
                            if response.resumeData != nil {
                                //                            try FileManager.default.removeItem(at: response.destinationURL!)
                            }else{
                                
                                if let error = response.error{
                                    errorBlock(error.localizedDescription)
                                }else{
                                    
                                    if response.destinationURL != nil {
                                        
                                        var a = ""
                                        a = response.destinationURL!.lastPathComponent
                                        print(a)
                                        
                                        itemSong?.mp3.playing_linkOffline = response.destinationURL!.lastPathComponent
                                        CoreData.updateDownloadSong(type: type, itemSong: itemSong!, successBlock: successBlock, errorBlock: errorBlock)
                                        
                                        
                                        
//                                        let qosUtility = DispatchQoS.QoSClass.utility
//                                        DispatchQueue.global(qos: qosUtility).async {
//                                            
//                                            DispatchQueue.main.async() { () -> Void in
//                                                var a = ""
//                                                let _song = itemSong
//                                                a = response.destinationURL!.lastPathComponent
//                                                _song?.mp3.playing_linkOffline = a
//                                                print(a)
//                                            }
//
//                                        }
                                    }
                                }
                                
                                
                                
                                //                            if let index = self.requests.enumerated().filter( { $0.element === request }).map({ $0.offset }).first {
                                //                                print("download success and finish remove request")
                                //                                self.requests.remove(at: index)
                                //                            }
                                
                                self.requests.removeValue(forKey: (type.rawValue+itemSong!.id))
                            }
                        } catch {
                            errorBlock(error.localizedDescription)
                        }
                }
                
                
                
                if request != nil {
                    let info = DownloadInfo.init(_id: itemSong?.id, _itemSong: itemSong, _request: request, _type: type)
                    
                    CoreData.setDownloadSessionToDBWith(info: info, successBlock: {
                        //
                    }, errorBlock: { (str_error) in
                        //
                    })
                    
                    self.requests["\(type.rawValue+itemSong!.id)"] = info
                }
            }, errorBlock: errorBlock)
        }
 */
    }
    

 */
    
}
