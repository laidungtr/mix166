//
//  FeatureProxy.swift
//  Mix166
//
//  Created by ttiamap on 5/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

private enum FeatureUriType: String {
    
    case Feature            = "index"                                           //get home without user login
    case MixMore            = "mixs"                                            //mixs?type=highlight&page=1&limit=20
    case TrackMore          = "songs"                                           //songs?type=highlight&page=1&limit=20
    case VideoMore          = "videos"                                          //videos?page=1&limit=20
//    case LikeMixMore        = "mixs/likedMixList?user_id={user_id}"             //mixs/likedMixList?user_id=3000020
    case PlaylistMore       = "playlists"                                       //playlists?page=1&limit=20
//    case GenresMore         = "genres"                                          //genres?page=1&limit=20
//    case GenresDetail       = "genres/detail?id={genres_id}"                    //genres/detail?id=2000000
    
    var uri : String {
        switch self {
        default : return self.rawValue
        }
    }
    
}

class FeatureProxy: BaseProxy {
    
    /** Method  call API Home Feature without user loggin
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return Feature if success
     ex: : api.mix166.com/api/v1.1_a/index?user_id=3000020
     */
    func feature(dataRequest: DataRequest, user_id: String){
        
        let uri = FeatureUriType.Feature.rawValue
        let url = self.makeURL(uri: uri)
        
        var request = Request(.get, url)
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {

                let output = Feature(dict: results)
                output.parsingData()
                
                dataRequest.onSuccess?(output, "" as AnyObject);
                
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
            
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    //http://api.mix166.com/api/v1.1_a/mixs?type=highlight&st=DHRMDiJpC0gd6T2cFHQUMg&e=1504046769&limit=30&page=1
    
    //http://api.mix166.com/api/v1.1_a/index?st=DHRMDiJpC0gd6T2cFHQUMg&e=1504046769
    
    /** Method  call API Home Feature without user loggin
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return [Mix] if success
     ex: : api.mix166.com/api/v1.1_a/index?user_id=3000020
     */
    func featureMixMore(offset: Int,dataRequest: DataRequest){
        
        let param = [
            FilterType.Page.rawValue : (offset/limitDefault + 1) as AnyObject,
            FilterType.Limit.rawValue : limitDefault as AnyObject,
            "type":"highlight"
        ] as [String : AnyObject]
        
        let uri = FeatureUriType.MixMore.uri
        let url = self.makeURL(uri: uri, parameter: param.description)
        
        var request = Request(.get, url)
        request.parameters = param
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                
                var arrItem : [Mix] = []
                
                if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{
                    for _itemDict in dicts {
                        let item = Mix.init(dict: _itemDict)
                        item.parsingSongMasup()
                        
                        arrItem.append(item)
                    }
                }
                
                dataRequest.onSuccess?(arrItem as AnyObject, "" as AnyObject);
                
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
            
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    /** Method  call API Home Feature without user loggin
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return [Track] if success
     ex: : api.mix166.com/api/v1.1_a/songs?type=highlight&page=1&limit=20
     */
    func featureTrackMore(offset: Int,dataRequest: DataRequest){
        
        let param = [
            FilterType.Page.rawValue : (offset/limitDefault + 1) as AnyObject,
            FilterType.Limit.rawValue : limitDefault as AnyObject,
            "type":"highlight"
        ] as [String : AnyObject]
        
        let uri = FeatureUriType.TrackMore.uri
        let url = self.makeURL(uri: uri, parameter: param.description)
        
        var request = Request(.get, url)
        request.parameters = param
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                
                var arrItem : [Track] = []
                
                if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{
                    for _itemDict in dicts {
                        let item = Track.init(dict: _itemDict)
                        arrItem.append(item)
                    }
                }
                
                dataRequest.onSuccess?(arrItem as AnyObject, "" as AnyObject);
                
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
            
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    /** Method  call API Home Feature without user loggin
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return [Video] if success
     ex: : api.mix166.com/api/v1.1_a/videos?page=1&limit=20
     */
    func featureVideoMore(offset: Int,dataRequest: DataRequest){
        
        let param = [
            FilterType.Page.rawValue : (offset/limitDefault + 1) as AnyObject,
            FilterType.Limit.rawValue : limitDefault as AnyObject
        ]
        
        let uri = FeatureUriType.VideoMore.uri
        let url = self.makeURL(uri: uri, parameter: param.description)
        
        var request = Request(.get, url)
        request.parameters = param
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                
                var arrItem : [Video] = []
                
                if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{
                    for _itemDict in dicts {
                        let item = Video.init(dict: _itemDict)
                        arrItem.append(item)
                    }
                }
                
                dataRequest.onSuccess?(arrItem as AnyObject, "" as AnyObject);
                
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
            
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
    
    /** Method  call API Home Feature without user loggin
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return [Genres] if success
     ex: : api.mix166.com/api/v1.1_a/genres?page=1&limit=20
     */
//    func featureGenresMore(offset: Int,dataRequest: DataRequest){
//        
//        let param = [
//            FilterType.Page.rawValue : (offset/limitDefault + 1) as AnyObject,
//            FilterType.Limit.rawValue : limitDefault as AnyObject
//        ]
//        
//        let uri = FeatureUriType.GenresMore.uri
//        let url = self.makeURL(uri: uri, parameter: param.description)
//        
//        var request = Request(.get, url)
//        request.parameters = param
//        
//        request.onSuccess = { (result) in
//            
//            if let results = result as? Dictionary<String, AnyObject> {
//                
//                var arrItem : [Genres] = []
//                
//                if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{
//                    for _itemDict in dicts {
//                        let item = Genres.init(dict: _itemDict)
//                        arrItem.append(item)
//                    }
//                }
//                
//                dataRequest.onSuccess?(arrItem as AnyObject, "" as AnyObject);
//                
//            } else {
//                let error = HttpUtility.localParseError()
//                dataRequest.onFailure?(error, "" as AnyObject);
//            }
//            
//        }
//        
//        request.onFailure = { (_error) in
//            dataRequest.onFailure?(_error, "" as AnyObject);
//        }
//        
//        httpUtillty.request(sender: request)
//    }
    
    /** Method  call API Home Feature without user loggin
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return [Mix] if success
     ex: : api.mix166.com/api/v1.1_a/mixs/likedMixList?user_id=3000020&page=1&limit=2
     */
//    func featureLikeMixMore(str_userid: String,offset: Int,dataRequest: DataRequest){
//        
//        let param = [
//            FilterType.Page.rawValue : (offset/limitDefault + 1) as AnyObject,
//            FilterType.Limit.rawValue : limitDefault as AnyObject
//        ]
//        
//        let uri = FeatureUriType.LikeMixMore.withUserId(user_id: str_userid)
//        let url = self.makeURL(uri: uri, parameter: param.description)
//        
//        var request = Request(.get, url)
//        request.parameters = param
//        
//        request.onSuccess = { (result) in
//            
//            if let results = result as? Dictionary<String, AnyObject> {
//                
//                var arrItem : [Mix] = []
//                
//                if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{
//                    for _itemDict in dicts {
//                        let item = Mix.init(dict: _itemDict)
//                        arrItem.append(item)
//                    }
//                }
//                
//                dataRequest.onSuccess?(arrItem as AnyObject, "" as AnyObject);
//                
//            } else {
//                let error = HttpUtility.localParseError()
//                dataRequest.onFailure?(error, "" as AnyObject);
//            }
//            
//        }
//        
//        request.onFailure = { (_error) in
//            dataRequest.onFailure?(_error, "" as AnyObject);
//        }
//        
//        httpUtillty.request(sender: request)
//    }
    
    /** Method  call API Home Feature without user loggin
     * make new a DataRequest
     * - Handler with  DataRequest object
     * Return [Playlist] if success
     ex: : api.mix166.com/api/v1.1_a/playlists?page=1&limit=20
     */
    func featurePlaylistMore(str_userid: String,offset: Int,dataRequest: DataRequest){
        
        let param = [
            FilterType.Page.rawValue : (offset/limitDefault + 1) as AnyObject,
            FilterType.Limit.rawValue : limitDefault as AnyObject
        ]
        
        let uri = FeatureUriType.PlaylistMore.rawValue
        let url = self.makeURL(uri: uri, parameter: param.description)
        
        var request = Request(.get, url)
        request.parameters = param
        
        request.onSuccess = { (result) in
            
            if let results = result as? Dictionary<String, AnyObject> {
                
                var arrItem : [Playlist] = []
                
                if let dicts = results["data"] as? [Dictionary<String, AnyObject>]{
                    for _itemDict in dicts {
                        let item = Playlist.init(dict: _itemDict)
                        arrItem.append(item)
                    }
                }
                
                dataRequest.onSuccess?(arrItem as AnyObject, "" as AnyObject);
                
            } else {
                let error = HttpUtility.localParseError()
                dataRequest.onFailure?(error, "" as AnyObject);
            }
            
        }
        
        request.onFailure = { (_error) in
            dataRequest.onFailure?(_error, "" as AnyObject);
        }
        
        httpUtillty.request(sender: request)
    }
}
