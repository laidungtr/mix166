//
//  DataProxy.swift
//  Mix166
//
//  Created by ttiamap on 5/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

enum DataRequestOption {
    case Network            // FORCE call api, DO NOT load CACHE
    case Default            // Call API  and  maybe Load Cache
}

/// Closual API for app
typealias OnSuccessClosual = (_ data: AnyObject?,  _ moreInfo: AnyObject?) -> Void
typealias OnFailureClosual = (_ error: NSError?, _ moreInfo: AnyObject?) -> Void
typealias OnLoadingClosual = (_ info: AnyObject?) -> Void

struct DataRequest {
    
    var option    : DataRequestOption
    
    var onSuccess : (OnSuccessClosual)?
    var onUpdate  : (OnSuccessClosual)?
    var onFailure : (OnFailureClosual)?
    var onLoading : (OnLoadingClosual)?
    
    init(_ aOption: DataRequestOption) {
        option = aOption;
    }
}


class DataProxy: NSObject {
    
    //MARK: - Share Data
    static let      feature     = FeatureProxy()
    static let      genres      = GenresProxy()
    static let      chart       = ChartProxy()
    static let      playlist    = PlaylistProxy()
    static let      search      = SearchProxy()
    static let      user        = UserProxy()
    static let      artist      = ArtistProxy()
    static let      download    = DownloadProxy()
    static let      station     = StationProxy()
    static let      video       = VideoProxy()
    
    private static let memoryCache       = 8*1024*1024;  //8Mb
    private static let diskCapacityCache = 20*1024*1024; //20Mb
    
    ///Get default config
    class func fetchAppConfigure(){
//        let request = DataRequest(.Default)
        
        //DataProxy.product.categories(request)
//        DataProxy.app.cities(request)
//        DataProxy.app.setupProductToSearchSpotlight()
//        DataProxy.checkout.getCartList(request)
    }
    
    class func setupUrlCache (){
        
        let urlCache = URLCache.init(memoryCapacity: memoryCache, diskCapacity: diskCapacityCache, diskPath: nil)
        
        URLCache.shared = urlCache
    }
}
