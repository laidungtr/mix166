//
//  ProductList.swift
//  Mix166
//
//  Created by ttiamap on 8/10/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit


class ProductList: BaseModel {
    
    var type: Product_UI_Type{
        if let str_type = self.dictionary["type"] as? String{
            let _type = Product_UI_Type.init(str_type: str_type)
            return _type
        }
        return .unknow
    }
    
    var title: String{
        if let _strTitle = self.dictionary["title"] as? String{
            return _strTitle
        }
        return ""
    }
    
    var header_type: Product_UI_Header_Type{
        if let str_type = self.dictionary["type_header"] as? String{
            let _type = Product_UI_Header_Type.init(str_type: str_type)
            return _type
        }
        return .unknow
    }
    
    var header_object: AnyObject?{
        if let object_header = self.dictionary["object_header"]{
            return object_header
        }
        return nil
    }
    
    var object: [AnyObject]?
    
//    var object: AnyObject? {
//        
//        switch self.type {
//        case .mixset_vertical, .mixset_horizontal:
//            //
//            if let _dicts = self.dictionary["objects"] as? [Dictionary<String, AnyObject>]{
//                
//                if _dicts.count > 0 {
//                    let itemMix = Mix.init(dict: _dicts[0])
//                    return itemMix
//                }
//            }
//            break
//            
//        case .track_vertical, .track_horizontal:
//            //
//            if let _dicts = self.dictionary["objects"] as? [Dictionary<String, AnyObject>]{
//                if _dicts.count > 0 {
//                    let itemTrack = Track.init(dict: _dicts[0])
//                    return itemTrack
//                }
//            }
//            break
//            
//        case .video_vertical:
//            //
//            if let _dicts = self.dictionary["objects"] as? [Dictionary<String, AnyObject>]{
//                if _dicts.count > 0 {
//                    let itemVideo = Video.init(dict: _dicts[0])
//                    return itemVideo
//                }
//            }
//            break
//            
//        case .deejay_vertical, .deejay_horizontal:
//            //
//            if let _dicts = self.dictionary["objects"] as? [Dictionary<String, AnyObject>]{
//                if _dicts.count > 0 {
//                    let itemArtist = Artist.init(dict: _dicts[0])
//                    return itemArtist
//                }
//            }
//            break
//            
//        case .deejay_horizontal, .mixset_horizontal, .track_horizontal, .video_horizontal:
//            //
//            return self.recommend_object as AnyObject
//            
//        default:
//            return nil
//        }
//        
//        return nil
//    }

}
