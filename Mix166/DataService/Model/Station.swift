//
//  Station.swift
//  Mix166
//
//  Created by ttiamap on 8/1/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

class Station: BaseModel {

    override func dictionaryDidSet() {
        let queueConcurrent = DispatchQueue(label: "StationQueue", attributes: .concurrent)
        
        if self.type == .deejay_horizontal || self.type == .deejay_station_horizontal{
            if let _dicts = self.dictionary["object_data"] as? [Dictionary<String, AnyObject>]{
                for itemDict in _dicts {
                    queueConcurrent.sync {
                        let itemArtist = Artist.init(dict: itemDict)
                        self.recommend_object.append(itemArtist)
                    }
                }
            }
        }
    }
    
    var type: Product_UI_Type{
        if let str_type = self.dictionary["object_type"] as? String{
            let _type = Product_UI_Type.init(str_type: str_type)
            return _type
        }
        return .unknow
    }
    
    var id: String {
        if let str_id = self.dictionary["timelines_id"] as? Int{
            return String(str_id)
        }
        return ""
    }
    
    var dateCreate: String {
        if let _str_createDate = self.dictionary["date_created"] as? String{
            return _str_createDate
        }
        return ""
    }
    
    var dateModified: String {
        if let _str_dateModified = self.dictionary["date_modified"] as? String{
            return _str_dateModified
        }
        return ""
    }
    
    var is_liked: Bool{
        if let _is_liked = self.dictionary["is_liked"] as? Int{
            return _is_liked > 0
        }
        return false
    }
    
    var object: AnyObject? {
        
        switch self.type {
        case .mixset_vertical, .mixset_horizontal:
            //
            if let _dicts = self.dictionary["object_data"] as? [Dictionary<String, AnyObject>]{
                
                if _dicts.count > 0 {
                    let itemMix = Mix.init(dict: _dicts[0])
                    return itemMix
                }
            }
            break
            
        case .track_vertical:
            //
            if let _dicts = self.dictionary["object_data"] as? [Dictionary<String, AnyObject>]{
                if _dicts.count > 0 {
                    let itemTrack = Track.init(dict: _dicts[0])
                    return itemTrack
                }
            }
            break
            
        case .video_vertical, .video_horizontal:
            //
            if let _dicts = self.dictionary["object_data"] as? [Dictionary<String, AnyObject>]{
                if _dicts.count > 0 {
                    let itemVideo = Video.init(dict: _dicts[0])
                    return itemVideo
                }
            }
            break
            
        case .deejay_vertical:
            //
            if let _dicts = self.dictionary["object_data"] as? [Dictionary<String, AnyObject>]{
                if _dicts.count > 0 {
                    let itemArtist = Artist.init(dict: _dicts[0])
                    return itemArtist
                }
            }
            break
            
        case .deejay_horizontal, .track_horizontal:
            //
            return self.recommend_object as AnyObject
            
        default:
            return nil
        }
        
        return nil
    }
    
    var artists: Artist{
        if let _dicts = self.dictionary["user_created"] as? [Dictionary<String, AnyObject>]{
            if _dicts.count > 0 {
                let itemArtist = Artist.init(dict: _dicts[0])
                return itemArtist
            }
        }
        return Artist.init(dict: [:])
    }
    
    var recommend_object: [AnyObject] = []
}
