//
//  DownloadInfo.swift
//  Mix166
//
//  Created by ttiamap on 7/24/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import Alamofire

protocol DownloadInfoDelegate {
    func downloadInfo_progressUpdate(progress: Double)
}

class DownloadInfo: NSObject {
    
    // Download service sets these values:
    var task: URLSessionDownloadTask?
    var isDownloading = false
    var resumeData: Data?
    
    // Download delegate sets this value:
    var progress: Double = 0{
        didSet{
            self.delegate?.downloadInfo_progressUpdate(progress: self.progress)
        }
    }
    
    var delegate: DownloadInfoDelegate?
    
    var song: Song?
    var type: SongType? = .Track
    var id: String? = ""
    
    init(_song: Song, _type: SongType?) {
        self.song = _song
        self.type = _type
        self.id = _song.id
    }
    
    init(_song: Song?){
        self.song = _song
    }
    
    /// Encode Object to Archive
    func encodeWithCoder(_ aCoder: NSCoder) {
        aCoder.encode(self.task, forKey:"task")
        aCoder.encode(self.isDownloading, forKey:"isDownloading")
        aCoder.encode(self.resumeData, forKey:"resumeData")
        aCoder.encode(self.type?.rawValue, forKey:"type")
    }
    
    /// Decode Object when Unarchive
    required init(coder aDecoder: NSCoder) {
        if let _task = aDecoder.decodeObject(forKey: "task") as? URLSessionDownloadTask{
            self.task = _task
        }
        
        if let _isDownloading = aDecoder.decodeObject(forKey: "task") as? Bool{
            self.isDownloading = _isDownloading
        }
        
        if let _data = aDecoder.decodeObject(forKey: "resumeData") as? Data{
            self.resumeData = _data
        }
        
        if let _type = aDecoder.decodeObject(forKey: "type") as? String{
            self.type = SongType.init(type: _type)
        }
    }

    
//    var id: String? = ""
//    var song: Song?
//    var request: DownloadRequest?
//    var type: SongType? = .Track
//    
//    init(_id: String?, _itemSong: Song?, _request: DownloadRequest?, _type: SongType?) {
//        
//        self.id = _id
//        self.song = _itemSong
//        self.request = _request
//        self.type = _type
//    }
}
