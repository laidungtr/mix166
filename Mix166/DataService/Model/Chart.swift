//
//  Chart.swift
//  Mix166
//
//  Created by ttiamap on 5/18/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class Chart: BaseModel {
    
    var tracks: [Song] = []
    var mixsets: [Mix] = []
    var videos: [Video] = []
    
    var _tracks: [Track]{
        var arrItem : [Track] = []
        if let dicts = self.dictionary["tracks"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Track.init(dict: _itemDict)
                arrItem.append(item)
            }
        }else if let dicts = self.dictionary["chart_tracks"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Track.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    var _mixsets: [Mix]{
        var arrItem : [Mix] = []
        if let dicts = self.dictionary["chart_mixsets"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Mix.init(dict: _itemDict)
                item.parsingSongMasup()
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    var _videos: [Video]{
        var arrItem : [Video] = []
        if let dicts = self.dictionary["chart_videos"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Video.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    func parsingData(){
        self.tracks = self._tracks
        self.mixsets = self._mixsets
        self.videos = self._videos
    }
}
