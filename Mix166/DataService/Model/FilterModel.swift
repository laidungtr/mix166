//
//  FilterModel.swift
//  Mix166
//
//  Created by ttiamap on 5/18/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

enum FilterType: String{
    
    case Page = "page"
    case Limit = "limit"
    case UserId = "user_id"
}

