//
//  Banner.swift
//  Mix166
//
//  Created by ttiamap on 5/18/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class Banner: BaseModel {
    
    var songs: [Song] = []
    var mixs: [Mix] = []
    var videos: [Video] = []

    private var _songs: [Song]{
        var arrItem : [Song] = []
        
        if let dicts = self.dictionary["song"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Song.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    private var _mixs: [Mix]{
        var arrItem : [Mix] = []
        
        if let dicts = self.dictionary["mix"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Mix.init(dict: _itemDict)
                item.parsingSongMasup()
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    private var _videos: [Video]{
        var arrItem : [Video] = []
        
        if let dicts = self.dictionary["video"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Video.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    
    func parsingBanner(){
        self.songs = self._songs
        self.mixs = self._mixs
        self.videos = self._videos
    }
}
