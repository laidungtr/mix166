//
//  Genres.swift
//  Mix166
//
//  Created by ttiamap on 5/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class Genres: BaseModel {

    var id: String{
        if let str_id = self.dictionary["genre_id"] as? Int{
            return String(str_id)
        }
        return ""
    }
    
    var name: String{
        if let _name = self.dictionary["genre_name"] as? String{
            return _name
        }
        return ""
    }
    
}
