//
//  Video.swift
//  Mix166
//
//  Created by ttiamap on 5/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class Video: Song {

    override var id: String{
        if let str_id = self.dictionary["video_id"] as? Int{
            return String(str_id)
        }else if let str_id = self.dictionary["object_id"] as? Int{
            return String(str_id)
        }else if let str_id = self.dictionary["id"] as? Int{
            return String(str_id)
        }

        return ""
    }
    
    
    override var name: String{
        if let _name = self.dictionary["video_name"] as? String, _name.length > 0{
            if _name.length > 0{    return _name    }
        }
        
        if let _name = self.dictionary["object_name"] as? String, _name.length > 0{
            return _name
        }
        
        if let _name = self.dictionary["name"] as? String, _name.length > 0{
            return _name
        }
        
        return ""
    }
    
    var song_id: String{
        if let str_id = self.dictionary["song_id"] as? String{
            return str_id
        }
        return ""
    }
    
    var mix_id: String{
        if let str_id = self.dictionary["mix_id"] as? String{
            return str_id
        }
        return ""
    }
}
