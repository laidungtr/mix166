//
//  Quality.swift
//  Mix166
//
//  Created by ttiamap on 5/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class Qualities: BaseModel {

    private var location: String{
        if let _location = self.dictionary["location"] as? String{
            return _location
        }
        return ""
    }
    
    private var url: String{
        if let _url = self.dictionary["link_url"] as? String{
            return _url
        }
        return ""
    }
    
    var quality: Int{
        if let _quality = self.dictionary["quality"] as? Int{
            return _quality
        }
        return 0
    }
    
    var file_size: Int{
        if let _file_size = self.dictionary["file_size"] as? Int{
            return _file_size
        }
        return 0
    }
    
    var duration: String{
        if let _duration = self.dictionary["duration"] as? String{
            return _duration
        }
        return "--:--"
    }
    
    var bitrate: Int{
        if let _bitrate = self.dictionary["bitrate"] as? Int{
            return _bitrate
        }
        return 0
    }
    
    var link_streaming: String{
        return self.url + "/" + self.location
    }
    
}
