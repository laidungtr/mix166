//
//  Mix.swift
//  Mix166
//
//  Created by ttiamap on 5/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class Mix: Song {

    override var id: String{
        if let str_id = self.dictionary["mix_id"] as? Int{
            return String(str_id)
        }else if let str_id = self.dictionary["object_id"] as? Int{
            return String(str_id)
        }else if let str_id = self.dictionary["id"] as? Int{
            return String(str_id)
        }
        return ""
    }

   override var name: String{
    if let _name = self.dictionary["mix_name"] as? String{
        if _name.length > 0 {   return _name    }
    }
    
    if let _name = self.dictionary["object_name"] as? String{
        return _name
    }
    
    if let _name = self.dictionary["song_name"] as? String{
        if _name.length > 0 {   return _name    }
    }
    
    if let _name = self.dictionary["name"] as? String{
        return _name
    }
    
        return ""
    }
    
    var songs_masup: [Song_Masup] = []
    
    func parsingSongMasup(){
        var arrItem: [Song_Masup] = []
        if let dicts = self.dictionary["songs"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Song_Masup.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        self.songs_masup = arrItem
    }
}

class Song_Masup: Song{
    
   override var name: String{
        if let _name = self.dictionary["song_name"] as? String{
            return _name
        }
        return ""
    }
    
    var name_artist: String{
        if let _name = self.dictionary["song_artist"] as? String{
            return _name
        }
        return ""
    }
    
    var location_duration: String{
        if let _duration = self.dictionary["song_duration"] as? String{
            return _duration
        }
        return ""
    }
    
    var location_second: Int{
        if let _second = self.dictionary["time_second"] as? Int{
            return _second
        }
        return 0
    }
}
