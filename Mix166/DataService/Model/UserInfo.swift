//
//  UserInfo.swift
//  Mix166
//
//  Created by ttiamap on 7/25/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class UserInfo: BaseModel {
    
    var about: String{
        if let _about = self.dictionary["about"] as? String{
            return _about
        }
        return ""
    }
    
    var address: String{
        if let _address = self.dictionary["address"] as? String{
            return _address
        }
        return ""
    }
    
    var birthday: String{
        if let _birthday = self.dictionary["birthday"] as? String{
            return _birthday
        }
        return ""
    }
    
    var follows_count: Int{
        if let _follows_count = self.dictionary["count_follow"] as? Int{
            return _follows_count
        }
        return 0
    }
    
    var description_user: String{
        if let _description = self.dictionary["description"] as? String{
            return _description
        }
        return ""
    }
    
    var email: String{
        if let _email = self.dictionary["email"] as? String{
            return _email
        }
        return ""
    }
    
    var gender: String{
        if let _gender = self.dictionary["gender"] as? String{
            return _gender
        }
        return ""
    }
    
    var id: String{
        if let str_id = self.dictionary["id"] as? Int{
            return String(str_id)
        }
        return ""
    }
    
    var avatar: String{
        if self.imageAvatar.length > 0 {
            var _tmp = self.imageAvatar
            _tmp = _tmp.replacingOccurrences(of: "http://st.nhacso.net/", with: self.link_url)
            
            if !_tmp.contains(self.link_url){
                _tmp = self.link_url + "/" + _tmp
            }
            return _tmp
        }
        
        return self.link_url + "/" + self.imageAvatar_nolink
    }
    
    var coverImage: String{
        if self.imageCover.length > 0 {
            var _tmp = self.imageCover
            _tmp = _tmp.replacingOccurrences(of: "http://st.nhacso.net/", with: self.link_url)
            
            if !_tmp.contains(self.link_url){
                _tmp = self.link_url + "/" + _tmp
            }
            return _tmp
        }
        
        return self.link_url + "/" + self.imageAvatar_nolink
    }
    
    private var imageCover: String{
        if let _imageCover = self.dictionary["imageCover"] as? String{
            return _imageCover
        }
        return ""
    }
    
    private var imageCover_nolink: String{
        if let _imageCover_nolink = self.dictionary["imageCover_nolink"] as? String{
            return _imageCover_nolink
        }
        return ""
    }
    
    private var imageAvatar: String{
        if let _imgAvatar = self.dictionary["imgAvatar"] as? String{
            return _imgAvatar
        }
        return ""
    }
    
    private var imageAvatar_nolink: String{
        if let _imgAvatar_nolink = self.dictionary["imgAvatar_nolink"] as? String{
            return _imgAvatar_nolink
        }
        return ""
    }
    
    var link_url: String{
//        if let _link_url = self.dictionary["link_url_abs"] as? String{
//            return _link_url
//        }
//        return ""
        
        return "http://st.cdn.mix166.com/"
    }
    
    var isPlatinum: Bool{
        if let _isPlatinum = self.dictionary["isPlatinum"] as? Int{
            return _isPlatinum > 0
        }
        return false
    }
    
    var likeCount: Int{
        if let _likeCount = self.dictionary["likeCount"] as? Int{
            return _likeCount
        }
        return 0
    }
    
    var money: Int{
        if let _money = self.dictionary["money"] as? Int{
            return _money
        }
        return 0
    }
    
    var name: String{
        if let _name = self.dictionary["name"] as? String{
            return _name
        }
        return ""
    }
    
    var phoneNumber: String{
        if let _phone_number = self.dictionary["phone_number"] as? String{
            return _phone_number
        }
        return ""
    }
    
    var id_playlist: String{
        if let str_id = self.dictionary["playlist_id"] as? Int{
            return String(str_id)
        }
        return ""
    }
    
    var shareCount: String{
        if let _shareCount = self.dictionary["shareCount"] as? Int{
            return String(_shareCount)
        }
        return ""
    }
}
