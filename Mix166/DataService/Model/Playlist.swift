//
//  Playlist.swift
//  Mix166
//
//  Created by ttiamap on 5/18/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import SDWebImage

struct PlaylistCreate {
    var name: String = ""
    var description = ""
    
}

class Playlist_Detail: BaseModel{
    
    //    var playlist: Playlist{
    //        if let dict = self.dictionary["playlist"] as? Dictionary<String,AnyObject>{
    //            let _playlist = Playlist.init(dict: dict)
    //            return _playlist
    //        }
    //        return Playlist.init(dict: [:])
    //    }
    
    var type: Product_UI_Type{
        if let type = self.dictionary["type"] as? String{
            return Product_UI_Type.init(str_type: type)
        }
        return .track_vertical
    }
    
    override func dictionaryDidSet() {
        let queueConcurrent = DispatchQueue(label: "Playlist_DetailQueue", attributes: .concurrent)
        
        if let _dicts = self.dictionary["songs"] as? [Dictionary<String, AnyObject>]{
            for itemDict in _dicts {
                queueConcurrent.sync {
                    let itemSong = Song.init(dict: itemDict)
                    self.songs.append(itemSong)
                }
            }
        }
    }
    
    var songs: [Song] = []
}

class Playlist: BaseModel {
    
    var id: String{
        if let str_id = self.dictionary["id"] as? Int{
            return String(str_id)
        }
        return ""
    }
    
    var name: String{
        if let _name = self.dictionary["name"] as? String{
            return _name
        }
        return ""
    }
    
    var songs: [Song]{
        var arrSong : [Song] = []
        if let _dictSong = self.dictionary["songs"] as? [Dictionary<String,AnyObject>]{
            for item in _dictSong{
                let _song = Song.init(dict: item)
                arrSong.append(_song)
            }
        }
        return arrSong
    }
    
    var create_date: String{
        if let _date = self.dictionary["dateCreate"] as? String{
            return _date
        }
        return ""
    }
    
    var description_playlist: String{
        if let _description = self.dictionary["description"] as? String{
            return _description
        }
        return ""
    }
    
//    var imageCover: String{
//        if let _imageCover = self.dictionary["image_cover"] as? String{
//            return _imageCover
//        }
//        return ""
//    }
    
    var linkShare: String{
        if let _linkShare = self.dictionary["linkShare"] as? String{
            return _linkShare
        }
        return ""
    }
    
    var userCreatedId: String{
        if let userId = self.dictionary["userCreatedId"] as? Int{
            return String(userId)
        }
        return ""
    }
    
    var userCreatedName: String{
        if let _name = self.dictionary["userCreatedName"] as? String{
            return _name
        }
        return ""
    }
    
    var view_count: Int{
        if let _view_count = self.dictionary["view_count"] as? Int{
            return _view_count
        }
        return 0
    }
    
    var like_count: Int{
        if let _like_count = self.dictionary["like_count"] as? Int{
            return _like_count
        }
        return 0
    }
    
    var totalSong: Int{
        if let _totalSong = self.dictionary["totalSong"] as? Int{
            return _totalSong
        }
        return 0
    }
    
    var tags: Int{
        if let _tags = self.dictionary["tags"] as? Int{
            return _tags
        }
        return 0
    }
    
    func mergesCoverImage(mainImageSize: CGSize, highQuality: Bool, callBack: @escaping (_ image: UIImage) -> ()){
        
        callBack(image_songPlaceholder!)
        
        if self.images.count > 0{
            
            if self.images.count < 4{
                
                if let url = URL.init(string: self.images.first!.urlConvertWith(width: mainImageSize.width, height: mainImageSize.height)){
                    
                    SDWebImageManager.shared().loadImage(with: url, options: SDWebImageOptions.cacheMemoryOnly, progress: nil, completed: { (image_download, data, error, cacheType, finish, url) in
                        //
                        if image_download != nil{
                            callBack(image_download!)
                            
                        }else{
                            callBack(image_songPlaceholder!)
                        }
                    })
                }else{
                    callBack(image_songPlaceholder!)
                }
            }else{
                
                var image: UIImage?
                var image2: UIImage?
                var image3: UIImage?
                var image4: UIImage?
                
                if let url = URL.init(string: self.images[0].urlConvertWith(width: mainImageSize.width/4, height: mainImageSize.width/4)){
                    
                    SDWebImageManager.shared().loadImage(with: url, options: SDWebImageOptions.cacheMemoryOnly, progress: nil, completed: { (image_download, data, error, cacheType, finish, url) in
                        //
                        if image_download != nil{
                            image = image_download
                        }
                        self.tryMergeImage(image: image, image2: image2, image3: image3, image4: image4, highQuality:highQuality, callBack: callBack)
                    })
                }
                
                if let url = URL.init(string: self.images[1].urlConvertWith(width: mainImageSize.width/4, height: mainImageSize.width/4)){
                    
                    SDWebImageManager.shared().loadImage(with: url, options: SDWebImageOptions.cacheMemoryOnly, progress: nil, completed: { (image_download, data, error, cacheType, finish, url) in
                        //
                        if image_download != nil{
                            image2 = image_download
                        }
                        self.tryMergeImage(image: image, image2: image2, image3: image3, image4: image4, highQuality:highQuality, callBack: callBack)
                    })
                }
                
                if let url = URL.init(string: self.images[2].urlConvertWith(width: mainImageSize.width/4, height: mainImageSize.width/4)){
                    
                    SDWebImageManager.shared().loadImage(with: url, options: SDWebImageOptions.cacheMemoryOnly, progress: nil, completed: { (image_download, data, error, cacheType, finish, url) in
                        //
                        if image_download != nil{
                            image3 = image_download
                        }
                        self.tryMergeImage(image: image, image2: image2, image3: image3, image4: image4, highQuality:highQuality, callBack: callBack)
                    })
                }
                
                if let url = URL.init(string: self.images[3].urlConvertWith(width: mainImageSize.width/4, height: mainImageSize.width/4)){
                    
                    SDWebImageManager.shared().loadImage(with: url, options: SDWebImageOptions.cacheMemoryOnly, progress: nil, completed: { (image_download, data, error, cacheType, finish, url) in
                        //
                        if image_download != nil{
                            image4 = image_download
                        }
                        
                        self.tryMergeImage(image: image, image2: image2, image3: image3, image4: image4, highQuality:highQuality, callBack: callBack)
                    })
                }
            }
        }
    }
    
    func tryMergeImage(image: UIImage?, image2: UIImage?, image3: UIImage?, image4: UIImage?, highQuality: Bool, callBack: @escaping (_ image: UIImage) -> ()){
        if image != nil && image2 != nil && image3 != nil && image4 != nil{
            self.mergeImage(topLeftImage: image!, topRightImage: image2!, bottomLeftImage: image3!, bottomRightImage: image4!, highQuality: highQuality, callBack: callBack)
        }
    }
    
    private func mergeImage(topLeftImage: UIImage, topRightImage: UIImage, bottomLeftImage: UIImage, bottomRightImage: UIImage, highQuality: Bool, callBack: @escaping (_ image: UIImage) -> ()){
        
        let responseImage = UIImage()
        let areaSize = CGRect(x: 0, y: 0, width: playlistSize.height, height: playlistSize.height)
        
        let size = CGSize(width: playlistSize.height, height: playlistSize.height)
        
        if highQuality{
            UIGraphicsBeginImageContextWithOptions(size, true, UIScreen.main.scale)
        }else{
            UIGraphicsBeginImageContextWithOptions(size, true, 1)
        }
        
        let topLeftSize = CGRect(x: 0, y: 0, width: size.width/2, height: size.height/2)
        topLeftImage.draw(in: topLeftSize)
        
        let topRightSize = CGRect(x: size.width/2, y: 0, width: size.width/2, height: size.height/2)
        topRightImage.draw(in: topRightSize)
        
        let bottomLeftSize = CGRect(x: 0, y: size.height/2, width: size.width/2, height: size.height/2)
        bottomLeftImage.draw(in: bottomLeftSize)
        
        let bottomRightSize = CGRect(x: size.width/2, y: size.height/2, width: size.width/2, height: size.height/2)
        bottomRightImage.draw(in: bottomRightSize)
        
        responseImage.draw(in: areaSize, blendMode: CGBlendMode.normal, alpha: 1)
        
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext() ?? image_songPlaceholder!
        
        UIGraphicsEndImageContext()
        
        callBack(newImage)
        
    }
    
    
    
    var images: [String] = []
    
    override func dictionaryDidSet() {
        let queueConcurrent = DispatchQueue(label: "PlaylistQueue", attributes: .concurrent)
        
        if let _dicts = self.dictionary["images"] as? [String]{
            for item in _dicts {
                queueConcurrent.sync {
                    self.images.append(item)
                }
            }
        }
    }
    
}
