//
//  Feature.swift
//  Mix166
//
//  Created by ttiamap on 5/18/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

enum FeatureType: String {
    case banner = "BANNER"
    case mixset = "MIXSETS HOT"
    case track = "TRACKS HOT"
    case chart = "CHART"
    case video = "MV HOT"
    case deejay = "TOP DJ"
    case unknow = "UNKNOW"
}

class FeatureEntries: BaseModel{
    
    var type :FeatureType = .unknow
    
    var title: String = ""
    
    var entries: [AnyObject]?
    
    required init(_type: FeatureType, _entries: [AnyObject]?) {
        super.init(dict: [:])
        self.type = _type
        self.title = _type.rawValue
        self.entries = _entries
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
}

class Feature: BaseModel {
    
    var entries: [FeatureEntries] = []
    
    private var _entries: [FeatureEntries]{
        
        var _entries: [FeatureEntries] = []
        
        if self.banner.mixs.count > 0 || self.banner.songs.count > 0 || self.banner.videos.count > 0{
            let bannerEntries = FeatureEntries.init(_type: .banner, _entries: [self.banner])
            _entries.append(bannerEntries)
        }
        
        if self.hot_mixsets.count > 0 {
            let mixsetEntries = FeatureEntries.init(_type: .mixset, _entries: self.hot_mixsets)
            _entries.append(mixsetEntries)
        }
        
        if self.hot_tracks.count > 0 {
            let tracksEntries = FeatureEntries.init(_type: .track, _entries: self.hot_tracks)
            _entries.append(tracksEntries)
        }
        
        if self.chart.mixsets.count > 0 || self.chart.tracks.count > 0 || self.chart.videos.count > 0{
            let chartEntries = FeatureEntries.init(_type: .chart, _entries: [self.chart])
            _entries.append(chartEntries)
        }
        
        if self.top_dj.count > 0 {
            let djEntries = FeatureEntries.init(_type: .deejay, _entries: self.top_dj)
            _entries.append(djEntries)
        }
        
        if self.hot_videos.count > 0 {
            let videoEntries = FeatureEntries.init(_type: .video, _entries: self.hot_videos)
            _entries.append(videoEntries)
        }
        
        return _entries
    }
    
    func parsingData(){
        self.banner = self._banner
        self.hot_tracks = self._hot_tracks
        self.hot_mixsets = self._hot_mixsets
        self.hot_videos = self._hot_videos
        self.top_dj = self._top_dj
        self.chart = self._chart
        
        self.entries = self._entries
    }
    
    private var banner: Banner = Banner.init(dict: [:])
    private var hot_mixsets: [Mix] = []
    private var hot_tracks: [Track] = []
    private var chart: Chart = Chart.init(dict: [:])
    private var top_dj: [Deejay] = []
    private var hot_videos: [Video] = []
    
    
    private var _banner: Banner{
        if let dicts = self.dictionary["banners"] as? Dictionary<String, AnyObject>{
            let item = Banner.init(dict: dicts)
            item.parsingBanner()
            return item
        }
        return Banner.init(dict: [:])
    }
    
    private var _hot_mixsets: [Mix]{
        var arrItem : [Mix] = []
        if let dicts = self.dictionary["hot_mixsets"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Mix.init(dict: _itemDict)
                item.parsingSongMasup()
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    private var _hot_tracks: [Track]{
        var arrItem : [Track] = []
        if let dicts = self.dictionary["hot_tracks"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Track.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
   private var _chart: Chart{
        if let dicts = self.dictionary["chart"] as? Dictionary<String, AnyObject>{
            let item = Chart.init(dict: dicts)
            item.parsingData()
            return item
        }
        return Chart.init(dict: [:])
    }
    
   private var _top_dj: [Deejay]{
        var arrItem : [Deejay] = []
        if let dicts = self.dictionary["top_dj"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Deejay.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
   private var _hot_videos: [Video]{
        var arrItem : [Video] = []
        if let dicts = self.dictionary["hot_videos"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Video.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
}
