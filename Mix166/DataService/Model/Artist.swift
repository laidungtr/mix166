//
//  Artist.swift
//  Mix166
//
//  Created by ttiamap on 5/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class Artist: BaseModel {
    
    var id: String{
        if let str_id = self.dictionary["artist_id"] as? Int{
            return String(str_id)
        }else if let str_id = self.dictionary["id"] as? Int{
            return String(str_id)
        }
        return ""
    }
    
    var name: String{
        if let _name = self.dictionary["artist_name"] as? String{
            return _name
        }else if let _name = self.dictionary["name"] as? String{
            return _name
        }
        return ""
    }
    
    var alias: String{
        if let _alias = self.dictionary["artist_alias"] as? String{
            return _alias
        }else if let _alias = self.dictionary["alias"] as? String{
            return _alias
        }
        return ""
    }
    
    private var url: String{
        if let _url = self.dictionary["link_url"] as? String{
            return _url
        }
        return ""
    }
    
    private var location: String{
        if let _location = self.dictionary["location"] as? String{
            return _location
        }
        return ""
    }
    
    var image_cover: String{
        if let _image_cover = self.dictionary["image_cover"] as? String{
            return _image_cover
        }else if let _image_cover = self.dictionary["imageCover"] as? String{
            return _image_cover
        }
        
        return self.url + "/" + self.location
    }
    
    
    
    var biography: String{
        if let str = self.dictionary["biography"] as? String{
            return str
        }
        return ""
    }
    
    var listen_count: String{
        if let _number = self.dictionary["listen_count"] as? Int{
            return _number.covertToThousand()
        }
        return "0"
    }
    
    var like_count: String{
        if let _number = self.dictionary["like_count"] as? Int{
            return _number.covertToThousand()
        }
        return "0"
    }
    
    var followings: String{
        if let _number = self.dictionary["total_following"] as? Int{
            return _number.covertToThousand()
        }
        return "0"
    }
    
    var isFollow: Bool{
        if let _number = self.dictionary["is_follow"] as? Int{
            return _number > 0
        }
        return false
    }
    
}
