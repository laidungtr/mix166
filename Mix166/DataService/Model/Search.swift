//
//  SearchKeyword.swift
//  Mix166
//
//  Created by ttiamap on 6/28/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

class Search: BaseModel {
    
    
    var entries: [Search_Entries]{
        
        var _entries: [Search_Entries] = []
        
        if self.deejays.count > 0 {
            let djEntries = Search_Entries.init(_type: .deejay, _entries: self.deejays)
            _entries.append(djEntries)
        }else if self._datas?.count ?? 0 > 0{
            if let _deejays = self._datas as? [Deejay]{
                let djEntries = Search_Entries.init(_type: .deejay, _entries: _deejays)
                _entries.append(djEntries)
            }
        }
        
        if self.mixsets.count > 0 {
            let mixsetEntries = Search_Entries.init(_type: .mixset, _entries: self.mixsets)
            _entries.append(mixsetEntries)
            
        }else if self._datas?.count ?? 0 > 0{
            if let _mixsets = self._datas as? [Mix]{
                let mixsetEntries = Search_Entries.init(_type: .mixset, _entries: _mixsets)
                _entries.append(mixsetEntries)
            }
        }
        
        if self.tracks.count > 0 {
            let tracksEntries = Search_Entries.init(_type: .track, _entries: self.tracks)
            _entries.append(tracksEntries)
        }else if self._datas?.count ?? 0 > 0{
            if let _tracks = self._datas as? [Track]{
                let tracktEntries = Search_Entries.init(_type: .track, _entries: _tracks)
                _entries.append(tracktEntries)
            }
        }
        
        if self.videos.count > 0 {
            let videoEntries = Search_Entries.init(_type: .video, _entries: self.videos)
            _entries.append(videoEntries)
        }else if self._datas?.count ?? 0 > 0{
            if let _videos = self._datas as? [Video]{
                let videoEntries = Search_Entries.init(_type: .video, _entries: _videos)
                _entries.append(videoEntries)
            }
        }
        
        if let data = self._datas as? [String] {
            let recent_search_entries = Search_Entries.init(_type: .trend, _entries: data as [AnyObject])
            _entries.append(recent_search_entries)
            
            if let recent_search = DataProxy.search.getRecentSearchKeywords(){
                let recent_search_entries = Search_Entries.init(_type: .recent_search, _entries: recent_search as [AnyObject])
                _entries.append(recent_search_entries)
            }
            
        }else if !(_entries.count > 0){
            
            if let trend_search = DataProxy.search.getTrendSearchKeywords(){
                let trend_search_entries = Search_Entries.init(_type: .trend, _entries: trend_search as [AnyObject])
                _entries.append(trend_search_entries)
            }
            
            if let recent_search = DataProxy.search.getRecentSearchKeywords(){
                let recent_search_entries = Search_Entries.init(_type: .recent_search, _entries: recent_search as [AnyObject])
                _entries.append(recent_search_entries)
            }
        }
        
        return _entries
    }
    
    
    var _datas: [AnyObject]?
    
    private var mixsets: [Mix]{
        var arrItem : [Mix] = []
        if let dicts = self.dictionary["mixsets"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Mix.init(dict: _itemDict)
                item.parsingSongMasup()
                
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    private var tracks: [Track]{
        var arrItem : [Track] = []
        if let dicts = self.dictionary["tracks"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Track.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    private var deejays: [Deejay]{
        var arrItem : [Deejay] = []
        if let dicts = self.dictionary["artists"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Deejay.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    private var videos: [Video]{
        var arrItem : [Video] = []
        if let dicts = self.dictionary["videos"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Video.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
}

class Keyword: BaseModel {
    //
    var keywords: [String]{
        return ["Touliver", "Soobin hoàng sơn", "Tóc Tiên"]
    }
}

class Search_Entries: BaseModel {
    
    var type :SearchType = .unknow
    
    var title: String{
        return self.type.rawValue
    }
    
    var entries: [AnyObject]?
    
    required init(_type: SearchType, _entries: [AnyObject]?) {
        super.init(dict: [:])
        self.type = _type
        self.entries = _entries
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

