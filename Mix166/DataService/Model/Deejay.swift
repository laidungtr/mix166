//
//  Deejay.swift
//  Mix166
//
//  Created by ttiamap on 5/18/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class Deejay: Artist {

    
    var imageCover: String{
        if let _image_cover = self.dictionary["image_cover"] as? String{
            return _image_cover
        }
        return ""
    }
    
    var view_count: Int{
        if let _view_count = self.dictionary["view_count"] as? Int{
            return _view_count
        }
        return 0
    }
}
