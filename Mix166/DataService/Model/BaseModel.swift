//
//  BaseModel.swift
//  Karaoke_Plus_Remote
//
//  Created by ttiamap on 5/9/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

enum CacheKey: String {
    case kHostCache = "kHostCache"
}

class BaseModel: NSObject {

    var dictionary : Dictionary<String, AnyObject>
    var isEmpty: Bool{
        return !(self.dictionary.count > 0)
    }
    
    override convenience init() {
        self.init(dict: [:])
    }
    
    init(dict: Dictionary<String, AnyObject>) {
        self.dictionary = dict
    }
    
    func dictionaryDidSet() {
        
    }
    
    /// Encode Object to Archive
    func encodeWithCoder(_ aCoder: NSCoder) {
        aCoder.encode(self.dictionary, forKey:"ModelDictionary")
    }
    
    /// Decode Object when Unarchive
    required init(coder aDecoder: NSCoder) {
        let dict = aDecoder.decodeObject(forKey: "ModelDictionary") as? Dictionary<String, AnyObject>
        self.dictionary = dict ?? [:];
    }
    
    /// Print dictionary of Object
    func printMe() {
        print(self.dictionary.description);
    }
    
    func saveToNsUserDefaut(Key: CacheKey){
        let data:  NSData =  NSKeyedArchiver.archivedData(withRootObject: self.dictionary) as NSData
        UserDefaults.standard.set(data, forKey: Key.rawValue)
    }
    
    func readDataFromUserDefault(Key: CacheKey) -> AnyObject?{
        if let loaded = UserDefaults.standard.object(forKey: Key.rawValue) as? NSData {
            if let data = NSKeyedUnarchiver.unarchiveObject(with: loaded as Data) {
                return data as AnyObject
            }
        }
        return nil
    }
    
    
}
