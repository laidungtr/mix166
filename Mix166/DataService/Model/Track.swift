//
//  Track.swift
//  Mix166
//
//  Created by ttiamap on 5/18/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class Track: Song {
    
    override var id: String{
        if let str_id = self.dictionary["song_id"] as? Int{
            return String(str_id)
        }else if let str_id = self.dictionary["object_id"] as? Int{
            return String(str_id)
        }else if let str_id = self.dictionary["id"] as? Int{
            return String(str_id)
        }
        return ""
    }
    
    override var name: String{
        if let _name = self.dictionary["song_name"] as? String{
            if _name.length > 0 {   return _name    }
        }
        
        if let _name = self.dictionary["object_name"] as? String{
            return _name
        }
        
        if let _name = self.dictionary["mix_name"] as? String{
            return _name
        }
        
        if let _name = self.dictionary["name"] as? String{
            return _name
        }
        
        return ""
    }

}
