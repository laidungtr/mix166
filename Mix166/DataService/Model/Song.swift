//
//  Song.swift
//  Mix166
//
//  Created by ttiamap on 5/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class Song: BaseModel {
    
    var id: String{
        if let str_id = self.dictionary["song_id"] as? Int{
            return String(str_id)
        }else if let str_id = self.dictionary["id"] as? Int{
            return String(str_id)
        }
        return ""
    }
    
    var name: String{
        if let _name = self.dictionary["song_name"] as? String, _name.length > 0{
            return _name
        }else if let _name = self.dictionary["name"] as? String, _name.length > 0{
            return _name
        }
        return ""
    }
    
    var lyric: String{
        if let _lyric = self.dictionary["lyric"] as? String{
            return _lyric
        }
        return ""
    }
    
    var location: String{
        if let _location = self.dictionary["location"] as? String{
            return _location
        }
        return ""
    }
    
    var artists: [Artist]{
        var arrItem : [Artist] = []
        if let dicts = self.dictionary["artists"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let itemArtist = Artist.init(dict: _itemDict)
                arrItem.append(itemArtist)
            }
        }else if let dicts = self.dictionary["singers"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let itemArtist = Artist.init(dict: _itemDict)
                arrItem.append(itemArtist)
            }
        }
        return arrItem
    }
    
    var all_artistName: String{
        var _name = ""
        for _artist in self.artists{
            if _name.length > 0{
                _name += ", "
            }
            _name = _name.appending(_artist.alias)
        }
        return _name
    }
    
    var url: String{
        if let _url = self.dictionary["link_url"] as? String{
            return _url
        }
        return ""
    }
    
    var lossless: [Lossless]{
        var arrItem : [Lossless] = []
        
        if let dicts = self.dictionary["lossless"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Lossless.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    var mp3: Mp3{
        if let _dict = self.dictionary["mp3"] as? Dictionary<String, AnyObject>{
            let item = Mp3.init(dict: _dict)
            return item
        }
        return Mp3.init(dict: [:])
    }
    
    var qualities : Qualities{
        if let dicts = self.dictionary["qualities"] as? [Dictionary<String, AnyObject>]{
            
            if let itemDict = dicts.first{
                let item = Qualities.init(dict: itemDict)
                return item
            }
        }
        
        return Qualities.init(dict: [:])
    }
    
    var composers: [Composer]{
        var arrItem : [Composer] = []
        
        if let dicts = self.dictionary["composers"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Composer.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    var view_count: Int{
        if let _view_count = self.dictionary["view_count"] as? Int{
            return _view_count
        }
        return 0
    }
    
    var like_count: Int{
        if let _like_count = self.dictionary["like_count"] as? Int{
            return _like_count
        }
        return 0
    }
    
    var share_count: Int{
        if let _share_count = self.dictionary["share_count"] as? Int{
            return _share_count
        }
        return 0
    }
    
    var download_count: Int{
        if let _download_count = self.dictionary["download_count"] as? Int{
            return _download_count
        }
        return 0
    }
    
    var comment_count: Int{
        if let _comment_count = self.dictionary["comment_count"] as? Int{
            return _comment_count
        }
        return 0
    }
    
    var date_created: String{
        if let _date_created = self.dictionary["date_created"] as? String{
            return _date_created
        }
        return ""
    }
    
    var linkShare: String{
        if let _linkShare = self.dictionary["linkShare"] as? String{
            return _linkShare
        }
        return ""
    }
    
    var genres: [Genres]{
        var arrItem : [Genres] = []
        
        if let dicts = self.dictionary["genres"] as? [Dictionary<String, AnyObject>]{
            for _itemDict in dicts {
                let item = Genres.init(dict: _itemDict)
                arrItem.append(item)
            }
        }
        return arrItem
    }
    
    var all_genresName: String{
        var _name = ""
        for item in self.genres{
            if _name.length > 0{
                _name += " & "
            }
            _name += item.name
        }
        return _name
    }
    
    var all_genresNameHashTag: String{
        var _name = ""
        for item in self.genres{
            _name += "#"
            _name += item.name + " "
        }
        return _name
    }
    
    var isSell: Bool{
        if let _is_sell = self.dictionary["is_sell"] as? Bool{
            return _is_sell
        }
        return false
    }
    
    var url_preview: String{
        if let _link_url_preview = self.dictionary["link_url_preview"] as? String{
            return _link_url_preview
        }
        return ""
    }
    
    var price: Int{
        if let _price = self.dictionary["money"] as? Int{
            return _price
        }
        return 0
    }
    
    var date_created_time: String{
        if let _date = self.dictionary["date_created_timestamp"] as? String{
            return _date
        }
        return ""
    }
    
    var type: SongType{
        if let _type = self.dictionary["object_type"] as? String{
            return SongType.init(type: _type)
        }
        return .Track
    }
    
    private var location_web: String{
        if let _location = self.dictionary["location_web"] as? String{
            return _location
        }
        return ""
    }
    
    var image_coverBanner: String{
        return self.url + "/" + self.location
    }
    
    var imageCover: String{
        if let _imageCover = self.dictionary["imageCover"] as? String{
            return _imageCover
        }
        return self.url+"/"+self.location
    }
    
    var description_detail: String{
        if let _description = self.dictionary["featured_description"] as? String{
            return _description
        }
        return ""
    }
    
    var ranking_id:  Int{
        if let _ranking_id = self.dictionary["ranking_id"] as? Int{
            return _ranking_id
        }
        return 0
    }
    
    var rank: Int{
        if let _rank = self.dictionary["rank"] as? Int{
            return _rank
        }
        return 0
    }
    
    var point:  Int{
        if let _point = self.dictionary["point"] as? Int{
            return _point
        }
        return 0
    }
    
    var status: Int{
        if let _status = self.dictionary["status"] as? Int{
            return _status
        }
        return 0
    }
    
    func updatePlayingOfflineLink(str_location: String){
        var dict_mp3 = self.mp3.dictionary
        dict_mp3["link_url_mp3_offline"] = str_location as AnyObject
        
        var dict_song = self.dictionary
        dict_song["mp3"] = dict_mp3 as AnyObject
        
        self.dictionary = dict_song
    }
    
    func getArtistItemAtIndex(index: Int) -> Artist?{
        if self.artists.count > index{
            return self.artists[index]
        }
        return nil
    }
}
