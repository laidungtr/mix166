//
//  Mp3.swift
//  Mix166
//
//  Created by ttiamap on 5/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class Mp3: BaseModel {
    
    var url: String{
        if let _url = self.dictionary["link_url_mp3"] as? String{
            return _url
        }
        return ""
    }
    
    var location: String{
        if let _location = self.dictionary["location_mp3"] as? String{
            return _location
        }
        return ""
    }
    
    var location_128: String{
        if let _location = self.dictionary["location_128"] as? String{
            return _location
        }
        return ""
    }
    
    var file_size: Int64{
        if let _file_size_mp3 = self.dictionary["file_size_mp3"] as? Int64{
            return _file_size_mp3
        }
        return 0
    }
    
    
    var duration: String{
        if let _duration = self.dictionary["duration_mp3"] as? String{
            return _duration
        }
        return ""
    }
    
    var bitrate: String{
        if let _bitrate_mp3 = self.dictionary["bitrate_mp3"] as? String{
            return _bitrate_mp3
        }
        return ""
    }
    
    var playing_linkOffline: String{
        if let _url = self.dictionary["link_url_mp3_offline"] as? String{
            return _url
        }
        return ""
    }
    
    var playing_link: String{
        if self.playing_linkOffline.length > 0{
            return DataProxy.download.localFilePath(for: self.playing_linkOffline)
        }else if self.location_128.length > 0{
            return self.url + "/" + self.location_128
        }
        return self.url + "/" + location
    }
    
    var playing_link_320: String{
        return self.url + "/" + location
    }
    
    

}
