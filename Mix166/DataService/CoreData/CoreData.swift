//
//  CoreData.swift
//  Mix166
//
//  Created by ttiamap on 7/21/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//
import Foundation
import UIKit
import CoreData
import Alamofire

class CoreData: NSObject{
    
    //DB
    private static let k_DBSongDownload = "SongDownload"
    private static let k_DBSessionDownload = "SessionDownload"
    
    private static let k_SongInfo = "info"
    private static let k_songType = "type"
    private static let k_id = "id"
    
    private static let queueConcurrent = DispatchQueue(label: "CoreDataQueue", attributes: .concurrent)
    private static let queueConcurrent2 = DispatchQueue(label: "CoreDataQueue2", attributes: .concurrent)
    
    //MARK: - SET
    public class func setDownloadSongToDBWith(type: SongType, itemSong: Song?,
                                              successBlock:@escaping ()->(),
                                              errorBlock: @escaping (_ str_error: String) -> ()){
        
        if itemSong == nil {
            return errorBlock(AppMixError.song_notfound.rawValue)
        }
        queueConcurrent.sync {
            self.archiverSongDataAndInsertToDB(type: type, itemSong: itemSong!,successBlock: successBlock,errorBlock: errorBlock)
        }
    }
    
    public class func setDownloadSessionToDBWith(info: DownloadInfo?,
                                                 successBlock:@escaping ()->(),
                                                 errorBlock: @escaping (_ str_error: String) -> ()){
        
        if info == nil || info?.song == nil || info?.type == nil {
            return errorBlock(AppMixError.song_notfound.rawValue)
        }
        
        queueConcurrent.sync {
            self.archiverDownloadSessionToDB(info: info!, successBlock: successBlock,errorBlock: errorBlock)
        }
    }
    
    private class func archiverDownloadSessionToDB(info: DownloadInfo,
                                                   successBlock:@escaping ()->(),
                                                   errorBlock: @escaping (_ str_error: String) -> ()){
        if !self.checkCurrentSongIsExistsInDB(type: info.type, itemSong: info.song!, entityName: k_DBSessionDownload){
            // if not exists
            // insert into DB
            let data = NSMutableData.init()
            let archiver = NSKeyedArchiver.init(forWritingWith: data)
            
            if let context = self.getObjectContextFromAppDelegate(){
                let entity = NSEntityDescription.entity(forEntityName: k_DBSessionDownload, in: context)!
                
                let song = NSManagedObject(entity: entity, insertInto: context)
                
                archiver.encode(info.song, forKey: k_SongInfo)
                song.setValue(data, forKey: k_SongInfo)
                archiver.finishEncoding()
                
                song.setValue(info.song?.id, forKey: k_id)
                song.setValue(info.type!.rawValue, forKey: k_songType)
                
                self.save(context: context, successBlock: successBlock, errorBlock: errorBlock)
            }
            
        }else{
            // if was exists
            // update it
            // fixme
        }
    }
    
    private class func archiverSongDataAndInsertToDB(type: SongType, itemSong: Song,
                                                     successBlock:@escaping ()->(),
                                                     errorBlock: @escaping (_ str_error: String) -> ()){
        
        if !self.checkCurrentSongIsExistsInDB(type: type, itemSong: itemSong, entityName: k_DBSongDownload){
            
            let data = NSMutableData.init()
            let archiver = NSKeyedArchiver.init(forWritingWith: data)
            
            if let context = self.getObjectContextFromAppDelegate(){
                let entity = NSEntityDescription.entity(forEntityName: k_DBSongDownload, in: context)!
                
                let song = NSManagedObject(entity: entity, insertInto: context)
                
                archiver.encode(itemSong, forKey: k_SongInfo)
                
                song.setValue(itemSong.id, forKey: k_id)
                song.setValue(data, forKey: k_SongInfo)
                song.setValue(type.rawValue, forKey: k_songType)
                
                archiver.finishEncoding()
                
                self.save(context: context, successBlock: successBlock, errorBlock: errorBlock)
            }
        }else{
            return errorBlock("\(itemSong.name) already exists")
        }
    }
    
    //MARK: - UPDATE
    public class func updateDownloadSong(type: SongType?, itemSong: Song?,
                                         successBlock:@escaping ()->(),
                                         errorBlock: @escaping (_ str_error: String) -> ()){
        
        if itemSong == nil || type == nil{
            return errorBlock(AppMixError.song_notfound.rawValue)
        }
        
        let predicate = NSPredicate.init(format: "id == %@ && type == %@", itemSong!.id, type!.rawValue)
        
        let (fetchedObjects, context) = self.getFetchObjectWithEntityName(name: k_DBSongDownload, predicate: predicate)
        
        if fetchedObjects?.count ?? 0 > 0 && context != nil{
            
            let managedObject = fetchedObjects![0]
            
            let data = NSMutableData.init()
            let archiver = NSKeyedArchiver.init(forWritingWith: data)
            
            archiver.encode(itemSong, forKey: k_SongInfo)
            
            managedObject.setValue(itemSong!.id, forKey: k_id)
            managedObject.setValue(data, forKey: k_SongInfo)
            managedObject.setValue(type!.rawValue, forKey: k_songType)
            
            archiver.finishEncoding()
            self.save(context: context!, successBlock: successBlock, errorBlock: errorBlock)
        }
    }
    
    //MARK: - GET
    
    // session download
    public class func getDownloadSessionData(successBlock:(_ results: AnyObject?)->(),
                                             errorBlock:(_ str_error: String) -> ()){
        
        let fetchedObjects = self.getFetchObjectWithEntityName(name: k_DBSessionDownload, predicate: nil)
        return self.unacrhiverSessionDownloadData(fetchedObjects: fetchedObjects.0, successBlock: successBlock, errorBlock: errorBlock)
        
    }
    
    public class func getDownloadSessionData(itemSong: Song, type: SongType? ,successBlock:(_ results: AnyObject?)->(),
                                             errorBlock:(_ str_error: String) -> ()){
        
        var predicate: NSPredicate?
        if type == nil{
            predicate = NSPredicate.init(format: "id == %@", itemSong.id)
        }else{
            predicate = NSPredicate.init(format: "id == %@ && type == %@", itemSong.id, type!.rawValue)
        }
        
        let fetchedObjects = self.getFetchObjectWithEntityName(name: k_DBSessionDownload, predicate: predicate)
        return self.unacrhiverSessionDownloadData(fetchedObjects: fetchedObjects.0, successBlock: successBlock, errorBlock: errorBlock)
        
    }
    
    public class func unacrhiverSessionDownloadData(fetchedObjects: [NSManagedObject]?, successBlock:(_ results: AnyObject?)->(), errorBlock: (_ str_error: String) -> ()){
        
        if fetchedObjects != nil {
            
            queueConcurrent.sync {
                
                var response: [DownloadInfo] = []
                
                for item in fetchedObjects!{
                    queueConcurrent2.sync {
                        if let data = item.value(forKeyPath: k_SongInfo) as? Data{
                            
                            let info = DownloadInfo(_song: nil)//DownloadInfo(_id: nil, _itemSong: nil, _request: nil, _type: nil)
                            
                            // get itemSong
                            let unarchiver = NSKeyedUnarchiver.init(forReadingWith: data)
                            if let itemSong = unarchiver.decodeObject(forKey: k_SongInfo) as? Song{
                                
                                // if exists itemSong
                                info.song = itemSong
                                
                                // get resumeData
                                //                                if let request = item.value(forKey: k_downloadRequest) as? DownloadRequest{
                                //                                    info.request = request
                                //                                }
                            }
                            
                            response.append(info)
                            
                            unarchiver.finishDecoding()
                        }
                    }
                }
                return successBlock(response as AnyObject)
            }
        }
        return errorBlock("Fetch data error")
    }
    
    
    // song download
    public class func getDownloadSongInDBWith(type: SongType?,
                                              successBlock:@escaping (_ results: AnyObject?)->(),
                                              errorBlock: @escaping (_ str_error: String) -> ()){
        var predicate: NSPredicate?
        if type != nil{
            predicate = NSPredicate(format: "type == %@", type!.rawValue)
        }
        
        let fetchedObjects = self.getFetchObjectWithEntityName(name: k_DBSongDownload, predicate: predicate)
        
        return self.unarchiverDownloadSongData(fetchedObjects: fetchedObjects.0,successBlock: successBlock, errorBlock: errorBlock)
    }
    
    private class func unarchiverDownloadSongData(fetchedObjects: [NSManagedObject]?, successBlock:@escaping (_ results: AnyObject?)->(), errorBlock: @escaping (_ str_error: String) -> ()){
        
        if fetchedObjects != nil {
            
            queueConcurrent.sync {
                
                var response: [Song] = []
                
                for item in fetchedObjects!{
                    queueConcurrent2.sync {
                        if let data = item.value(forKeyPath: k_SongInfo) as? Data{
                            
                            let unarchiver = NSKeyedUnarchiver.init(forReadingWith: data)
                            if let itemSong = unarchiver.decodeObject(forKey: k_SongInfo) as? Song{
                                response.append(itemSong)
                            }
                            unarchiver.finishDecoding()
                        }
                    }
                }
                return successBlock(response as AnyObject)
            }
        }
        return errorBlock("Fetch data error")
    }
    
    
    //MARK: - DELETE
    public class func deleteDownloadSongInDB(type: SongType, itemSong: Song,
                                             successBlock:@escaping ()->(),
                                             errorBlock: @escaping (_ str_error: String) -> ()){
        if let context = self.getObjectContextFromAppDelegate(){
            let predicate = NSPredicate(format: "type == %@ && id == %@", type.rawValue,itemSong.id)
            let results = self.getFetchObjectWithEntityName(name: k_DBSongDownload, predicate: predicate)
            
            if results.0?.count ?? 0 > 0{
                if let object = results.0?.first{
                    context.delete(object)
                }
                
                return self.save(context: context, successBlock: successBlock, errorBlock: errorBlock)
            }
            
            // cannot found itemSong in DB
            // error calling
            errorBlock(AppMixError.data_notfound.rawValue)
        }else{
            errorBlock(AppMixError.data_notfound.rawValue)
        }
    }
    
    private class func checkCurrentSongIsExistsInDB(type: SongType?, itemSong: Song, entityName: String) -> Bool{
        
        if type == nil {
            return false
        }
        let predicate = NSPredicate(format: "type == %@ && id == %@", type!.rawValue,itemSong.id)
        let results = self.getFetchObjectWithEntityName(name: entityName, predicate: predicate)
        
        return results.0?.count ?? 0 > 0
    }
    
    
    //MARK: - CORE DATA
    private class func getFetchObjectWithEntityName(name: String, predicate: NSPredicate?) -> ([NSManagedObject]?, NSManagedObjectContext?){
        if let context = self.getObjectContextFromAppDelegate(){
            
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: name)
            fetchRequest.predicate = predicate
            
            //            let entity = NSEntityDescription.entity(forEntityName: name, in: context)
            //            fetchRequest.entity = entity
            
            do{
                let fetchedObjects = try context.fetch(fetchRequest)
                return (fetchedObjects,context)
                
            }catch{
                print("getFetchObjectWithEntityName error: \(error)")
            }
        }
        
        return (nil,nil)
    }
    
    private class func getObjectContextFromAppDelegate() -> NSManagedObjectContext?{
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate{
            if #available(iOS 10.0, *) {
                let context = appDelegate.persistentContainer.viewContext
                return context
            } else {
                // Fallback on earlier versions
                let context = appDelegate.managedObjectContext
                return context
            }
        }
        
        return nil
    }
    
    private class func save(context: NSManagedObjectContext,
                            successBlock:@escaping ()->(),
                            errorBlock: @escaping (_ str_error: String) -> ()){
        do{
            try context.save()
            print("context save success")
            return successBlock()
        }catch{
            print("context save error: \(error.localizedDescription)")
            return errorBlock(error.localizedDescription)
        }
    }
}
