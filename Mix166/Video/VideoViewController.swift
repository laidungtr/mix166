//
//  VideoViewController.swift
//  Mix166
//
//  Created by ttiamap on 7/7/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class VideoViewController: BaseViewController, DLTVideoPlayerDelegate, VideoViewModelDelegate, ProductListViewDelegate, ProductListViewDataSource {
    
    @IBOutlet weak var viewVideoPlayer: DLTVideoPlayerView?
    @IBOutlet weak var layoutVideoPlayerTraillingConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var viewProductList: ProductListView?
    var productlist_info: ProductListInfo?
    
    private var statusBarHidden = false
    
    override var isLockOrientation: Bool{
        if self.viewVideoPlayer != nil {
            return self.viewVideoPlayer!.isScaled
        }
        return false
    }
    
    
    var vmodel = VideoViewModel.init(title: "")
    
    private let videoScaleWidth = screenWidth/2
    private let videoScaleHeight = (screenWidth/2 * 9/16)
    private var gestureRatio : CGFloat = 0
    private var storeMovingPos: CGPoint = CGPoint(x: 0, y: 0)
    
    private let videoScaleXPosition = screenWidth - screenWidth/2
    private let videoScaleYPosition = screenHeight - (48.0) - (screenWidth/2 * 9/16) // (48.0 is tabbar)
    
    override var isNavigationBarHidden: Bool{
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.gestureRatio = (screenWidth - videoScaleWidth)/(screenHeight - videoScaleHeight)
        self.panGestureInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewVideoPlayer?.delegate = self
        self.vmodel.delegate_video = self
    }
    
    func setupProductListView(){
        if self.productlist_info != nil {
            self.viewProductList?.isEnablePullRefresh = false
            self.viewProductList?.delegate = self
            self.viewProductList?.datasource = self
            self.viewProductList?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
            self.viewProductList?.requestWithProductListInfo(_productInfo: self.productlist_info!, newRequest: true)
        }
    }
    
    override func rotateNotificationHandle(){
        self.rotated()
    }
    
    @discardableResult override func rotated() -> Bool {
        if self.lockOrientation(){
            // is Lock
            return true
        }else{
            // unlock rotate
            self.viewVideoPlayer?.layoutIfNeeded()
            if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
                self.viewVideoPlayer?.updatePlayerVideoLayerWithRotateMode(orientation: .landscapeRight)
                print("Landscape")
            }
            
            if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
                self.viewVideoPlayer?.updatePlayerVideoLayerWithRotateMode(orientation: .portrait)
                print("Portrait")
            }
        }
        return false
    }
    
    func panGestureInit(){
        self.viewVideoPlayer?.isUserInteractionEnabled = true
        let panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(self.panGestureRecognizer(gesture:)))
        
        self.viewVideoPlayer?.addGestureRecognizer(panGesture)
    }
    
    func panGestureRecognizer( gesture: UIPanGestureRecognizer){
        
        
        if self.viewVideoPlayer!.btnFullScreen!.isSelected == true{
            return
        }
        
        let translation = gesture.translation(in: self.viewVideoPlayer)
        var frame = self.view.frame
        
        switch gesture.state {
        case .began:
            //
            let velocity = gesture.velocity(in: gesture.view)
            
            let isVerticalGesture = fabs(velocity.y) > fabs(velocity.x)
            
            if isVerticalGesture{
                // vertical
                if velocity.y > 0 {
                    self.viewVideoPlayer?.direction = .down
                }else{
                    self.viewVideoPlayer?.direction = .up
                }
            }else{
                // vertical
                if self.viewVideoPlayer!.isScaled == true{
                    if velocity.x > 0 {
                        self.viewVideoPlayer?.direction = .right
                    }else{
                        self.viewVideoPlayer?.direction = .left
                    }
                }
            }
            
        case .changed:
            //
            if self.viewVideoPlayer?.direction == .up || self.viewVideoPlayer?.direction == .down{
                
                frame.origin.y += translation.y
                frame.origin.x = frame.origin.y * self.gestureRatio
                let alpha = videoScaleHeight/frame.origin.y
                
                if frame.origin.y > videoScaleYPosition{
                    frame.origin.y = videoScaleYPosition
                }else if frame.origin.y < 0{
                    frame.origin.y = 0
                }
                
                self.layoutVideoPlayerTraillingConstraint?.constant = frame.origin.x
                self.viewVideoPlayer?.layoutIfNeeded()
                
                UIView.animate(withDuration: 0.1, animations: {
                    self.view.frame = frame
                    self.view.backgroundColor = UIColor.black.withAlphaComponent(alpha)
                    self.viewProductList?.alpha = alpha
                    
                }, completion: { (finish) in })
                
                if self.storeMovingPos.y > frame.origin.y{
                    self.viewVideoPlayer?.direction = .up
                }else{
                    self.viewVideoPlayer?.direction = .down
                }
                self.storeMovingPos.y = frame.origin.y
                
            }else{
                if  self.viewVideoPlayer?.isScaled == true{
                    
                    let alpha = fabs(frame.origin.x/screenWidth)
                    
                    frame.origin.x += translation.x
                    
                    if frame.origin.x > videoScaleXPosition{
                        frame.origin.x = videoScaleXPosition
                    }
                    
                    self.view.frame = frame
                    self.viewVideoPlayer?.alpha = alpha
                    
                    if self.storeMovingPos.x > frame.origin.x{
                        self.viewVideoPlayer?.direction = .left
                    }else{
                        self.viewVideoPlayer?.direction = .right
                    }
                    
                    self.storeMovingPos.x = frame.origin.x
                }
            }
            gesture.setTranslation(CGPoint.zero, in: self.viewVideoPlayer)
            break
            
            
        case .ended:
            //
            switch self.viewVideoPlayer!.direction {
                
            case .down:
                self.videoViewScale(isScale: true)
                break
                
            case .up:
                self.videoViewScale(isScale: false)
                break
                
            case .left:
                self.removeVideoController(callBack: {})
                break
                
            case .right:
                self.videoViewScale(isScale: true)
                break
                
            default:
                break
            }
            
            self.storeMovingPos = CGPoint(x: 0, y: 0)
            self.viewVideoPlayer?.direction = .unknow
            break
            
        default:
            self.storeMovingPos = CGPoint(x: 0, y: 0)
            self.viewVideoPlayer?.direction = .unknow
        }
    }
    
    func removeVideoController(callBack:@escaping () -> ()){
        var _frame = self.view.frame
        _frame.origin.x = -screenWidth
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.frame = _frame
        }, completion: { (finish) in
            self.videoViewScaleWithRemoveState()
            DLTPlayer.stop(callback: {
                self.removeFromParentViewController()
                callBack()
            })
        })
        
    }
    
    func videoViewScale(isScale: Bool){
        
        var frame = self.view.frame
        
        if isScale{
            frame.origin.x = videoScaleXPosition - 1.0
            frame.origin.y = videoScaleYPosition - 1.0
            self.layoutVideoPlayerTraillingConstraint?.constant = frame.origin.x + 1
            self.viewVideoPlayer?.layoutIfNeeded()
            
            UIView.animate(withDuration: 0.25, animations: {
                self.view.frame = frame
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
                self.viewVideoPlayer?.alpha = 1
            }, completion: { (finish) in
                self.viewVideoPlayer?.isScaled = true
                self.setStatusBarHidden(hidden: self.statusBarHidden)
                self.tabbar_bringToFont()
                
            })
        }else{
            
            self.statusBarHidden = self.getStatusBarHiddenStatus()
            self.setStatusBarHidden(hidden: true)
            
            frame.origin.x = 0
            frame.origin.y = 0
            self.layoutVideoPlayerTraillingConstraint?.constant = frame.origin.x
            self.viewVideoPlayer?.layoutIfNeeded()
            
            UIView.animate(withDuration: 0.25, animations: {
                self.view.frame = frame
                self.view.backgroundColor = UIColor.black.withAlphaComponent(1)
                self.viewProductList?.alpha = 1.0
                self.viewVideoPlayer?.alpha = 1
            }, completion: { (finish) in
                self.viewVideoPlayer?.isScaled = false
                self.tabbar_sendToBack()
            })
        }
    }
    
    
    private func videoViewScaleWithRemoveState(){
        
        self.layoutVideoPlayerTraillingConstraint?.constant = 0.0
        self.viewVideoPlayer?.layoutIfNeeded()
        self.viewVideoPlayer?.isScaled = false
        self.view.backgroundColor = UIColor.black.withAlphaComponent(1)
        self.viewProductList?.alpha = 1.0
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !self.vmodel.isPlayingAutomatically{
            self.vmodel.isPlayingAutomatically = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - VideoModel Delegate
    func videoDelegate_didChangeVideoList(_videos: [Video]) {
        
        
    }
    
    func videoDelegate_willPlayingVideo(_video: Video) {
        //
        DLTPlayer.stop {
            DispatchQueue.global(qos: .background).sync {
                self.vmodel.filterBase.requestObject = _video
                self.productlist_info = ProductListInfo.init(_proxy: .video, _type: .video, filter: self.vmodel.filterBase, _data: nil, _paggingTypes: nil, pullRefresh: true)
                self.setupProductListView()
                
                DispatchQueue.main.async() { () -> Void in
                    self.viewVideoPlayer?.playingVideoWithSteamingLink(str_url: _video.qualities.link_streaming)
                }
            }
        }
    }
    
    func DLTVideoPlayerDelegate_FullScreenMode(active: Bool){
        if active{
            self.rotateWithLandScape()
        }else{
            self.rotateWithPortrait()
        }
    }
    
    func DLTVideoPlayerDelegate_ScaleVideoBackToNormal() {
        self.videoViewScale(isScale: false)
    }
    
    func DLTVideoPlayerDelegate_DidReachEndPlaying(){
        
        if let arrData = self.viewProductList?.getDataSource() as? [ProductList], arrData.count > 0{
            if let videos_ralated = arrData.first?.object as? [Video]{
                self.vmodel.video_didReachEndPlaying(videos: videos_ralated)
            }
        }
    }
    
    func videoDelegate_shouldUpdate(indexPaths: [IndexPath]) {
        //
    }
    
    
    // MARK: - DLTPlayer Delegate
    
    func DLTVideoPlayerDelegate_BtnDissmissDidTap(sender: UIButton) {
        self.videoViewScale(isScale: true)
    }
    
    func DLTVideoPlayerDelegate_PlayingStatusChange(isPlaying status: Bool){
        
    }
    func DLTVideoPlayerDelegate_FullScreenModeChange(isFullScreen status: Bool){
        
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return UIInterfaceOrientationMask.landscape
    }
    
    
    //MARK - ProductList Datasource
    func productListView_spacingBetweenItem() -> CGFloat{
        return 10.0
    }
    
    func productListView_scrollDirection() -> UICollectionViewScrollDirection{
        return .vertical
    }
    
    func productListView_contentInsect() -> UIEdgeInsets{
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    //MARK - ProductList Delegate
    
    func productListView_BtnShuffleSongDidTap(datas: [AnyObject]?, type: SongType){
        
    }
    
    func productListView_shouldDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) -> Bool{
        return false
    }
    
    func productListView_didDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) {
        //
    }
    
    func productListView_MoreTapAtIndexPath(index: Int, withData Datas: [AnyObject]?) {
        return
    }
    
    func productListView_BtnAutoPlayVideoDidChange(isOn: Bool) {
        //
        self.vmodel.isAutoPlayEnable = isOn
    }
    
    func productListView_didSelectedAtIndexPath(indexPath: IndexPath, withData Datas: [AnyObject]?, type: SongType) {
        //
        if let _videos = Datas as? [Video]{
            self.presentVideoViewController(_videos: _videos, playingIndex: indexPath.row)
            
        }else if let _songs = Datas as? [Song]{
            self.presentToPlayerViewController(_songs: _songs, playingIndex: indexPath.row,type:  type, shuffleEnable: false, isFromViewTracklisting: true)
            
        }else if let _artist = Datas as? [Artist], _artist.count > indexPath.row{
            self.presentToArtistDetailViewController(artist_info: _artist[indexPath.row])
        }
    }
    
    func productListView_HeaderTitleDidTapButtonMore(productInfo: ProductListInfo) {
        
        switch productInfo.type {
        case .deejay_vertical, .deejay_horizontal:
            return self.pushToArtistListViewController(info: productInfo)
            
        case .mixset, .track, .trackPlayer, .video:
            return self.pushToProductListViewController(info: productInfo, vmodel: nil)
        default:
            break
        }
    }
    
}
