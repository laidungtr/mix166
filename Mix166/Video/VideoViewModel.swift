//
//  VideoViewModel.swift
//  Mix166
//
//  Created by ttiamap on 7/7/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

protocol VideoViewModelDelegate {
    
    func videoDelegate_didChangeVideoList(_videos: [Video])
    func videoDelegate_willPlayingVideo(_video: Video)
    func videoDelegate_shouldUpdate(indexPaths: [IndexPath])
    
//    func videoDidChangeRepeatStyle(style: RepeatStyle)
//    func videoDidChangeShuffleStyle(isShuffle: Bool)
//    func videoDidUpdateDurationCountDown(str_duration: String, sliderValue: Float)
}

class VideoViewModel: BaseViewModel {
    
    var delegate_video: VideoViewModelDelegate?
    var isExsitVideoController = false
    var isAutoPlayEnable: Bool = true{
        didSet{
            SharePlayer.isAutoPlayVideo = self.isAutoPlayEnable
        }
    }
    
    
    private var storeWasPlayingVideoId: [String] = []
    
    private var playingIndex: Int = 0{
        didSet{
            if let _video = self.getItemAt(index: self.getPlayingIndex()) as? Video {
                
                if !self.storeWasPlayingVideoId.contains(_video.id){
                    storeWasPlayingVideoId.append(_video.id)
                }
                
                self.delegate_video?.videoDelegate_willPlayingVideo(_video: _video)
            }
        }
    }
    
    var isPlayingAutomatically: Bool = false{
        didSet{
            if let _video = self.getItemAt(index: self.getPlayingIndex()) as? Video {
                if !self.storeWasPlayingVideoId.contains(_video.id){
                    storeWasPlayingVideoId.append(_video.id)
                }
                self.delegate_video?.videoDelegate_willPlayingVideo(_video: _video)
            }
        }
    }
    
    var videos: [Video] = []{
        didSet{
            self.delegate_video?.videoDelegate_didChangeVideoList(_videos: self.videos)
        }
    }
    
    //MARK: - Funtion
    private func videoControl_PlayNext(){
        var currentIndex = self.getPlayingIndex()
        
        currentIndex += 1
        if currentIndex > self.videos.count{
            currentIndex = 0
        }
        
        self.playingIndex = currentIndex
    }
    
    func video_didReachEndPlaying(videos: [Video]){
        
        if self.isAutoPlayEnable{
            
            self.getPlayingRelatedIndexExceptDuplicateVideo(videos: videos, callback: { (relateIndex) in
                if let video_related = self.getItemVideoRelatedAt(index: relateIndex, _videoRelateds: videos){
                    // get video related to play for request new video relate baseon this video genres
                    // set request object to filter for request data
                    
                    self.filterBase.requestObject = video_related
                    
                    self.videos = videos
                    
                    self.self.setPlayingIndex(index: relateIndex)
                    
                }else{
                    self.videoControl_PlayNext()
                }
            })
            
        }else{
            
        }
    }
    
    func getPlayingRelatedIndexExceptDuplicateVideo(videos: [Video], callback: @escaping (_ relateVideoIndex: Int) -> ()){
        var newIndex = 0
        for item in videos {
            if self.storeWasPlayingVideoId.contains(item.id){
                newIndex += 1
            }else{
                if newIndex >= videos.count{
                    // have no un seen video
                    // refresh store playing video id to repeat loop video
                    self.storeWasPlayingVideoId = []
                    
                    return callback(0)
                }else{
                    return callback(newIndex)
                }
            }
        }
    }
    
    //MARK: - GET
    func getPlayingIndex() -> Int{
        return self.playingIndex
    }
    
    override func getItemAt(index: Int) -> AnyObject? {
        if self.videos.count > index{
            return self.videos[index]
        }
        
        return nil
    }
    
    func getItemVideoRelatedAt(index: Int, _videoRelateds: [Video]) -> Video?{
        if _videoRelateds.count > index{
            return _videoRelateds[index]
        }
        
        return nil
    }
    
    func getCurrentPlaying() -> Video?{
        if let _video = self.getItemAt(index: self.playingIndex) as? Video{
            return _video
        }
        return nil
    }
 
    //MARK: - SET
    func setPlayingIndex(index: Int){
        self.playingIndex = index
    }

}
