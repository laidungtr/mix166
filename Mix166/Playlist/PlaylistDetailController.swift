//
//  PlaylistDetailController.swift
//  Mix166
//
//  Created by ttiamap on 8/7/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class PlaylistDetailController: BaseViewController, ProductListViewDelegate, ProductListViewDataSource, ProductListScrollingDelegate {
    
    @IBOutlet weak var viewProductList: ProductListView?
    @IBOutlet weak var viewGradient: UIView?
    @IBOutlet weak var viewGradient2: UIView?
    @IBOutlet weak var img_background: UIImageView?
    
    var productlist_info: ProductListInfo?
    var vmodel : PlaylistViewModel = PlaylistViewModel.init(title: AppViewTitle.playlist.rawValue)
    
    override func setNavigationBarStyle() -> NavigationBarStyle {
        return .Transparent
    }
    
    override func setViewTitle() -> String {
        if let currentPlaylist = self.vmodel.filterBase.requestObject as? Playlist{
            return currentPlaylist.name + " playlist"
        }
        return ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.setupFristload()
        self.setupProductListView()
    }
    
    func setupProductListView(){
        
        if self.productlist_info != nil {
            self.viewProductList?.isEnablePullRefresh = self.productlist_info!.isEnablePullToRefresh
            self.viewProductList?.setProductListBackgroundColor(color: UIColor.clear)
            self.viewProductList?.delegate = self
            self.viewProductList?.datasource = self
            self.viewProductList?.delegate_scrolling = self
            self.viewProductList?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
            self.viewProductList?.requestWithProductListInfo(_productInfo: self.productlist_info!, newRequest: true)
        }
    }
    
    func setupFristload(){
        
        let frame = UIScreen.main.bounds
        
        self.viewGradient?.gradientFromTopToBottom(UIColor.black.withAlphaComponent(0.1).cgColor, bottomColor: UIColor.black.cgColor, gradientFrame: frame)
        self.viewGradient2?.gradientFromTopToBottom(UIColor.black.withAlphaComponent(0.05).cgColor, bottomColor: UIColor.black.cgColor, gradientFrame: frame)
        
        if let _playlist = self.vmodel.getCurrentPlaylistInfo(){
            
            if self.img_background != nil {
                _playlist.mergesCoverImage(mainImageSize: self.img_background!.frame.size, highQuality: false) { (image) in
                    self.img_background?.image = image
                }
            }
        }
        
    }
    
    //MARK - ProductList Datasource
    func productListView_spacingBetweenItem() -> CGFloat{
        return 10.0
    }
    
    func productListView_scrollDirection() -> UICollectionViewScrollDirection{
        return .vertical
    }
    
    func productListView_contentInsect() -> UIEdgeInsets{
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    //MARK - ProductList Delegate
    func productListView_ScrollViewDidScroll(scrollView: UIScrollView){
        
        self.navigationBarHideOnSwipe(scrollView: scrollView)
        let offset = scrollView.contentOffset.y
        
        let alpha = offset/playlistDetailHeaderSize.height
        
        self.setStatusBarBackgroundColor(color: UIColor.black.withAlphaComponent(alpha))
        
    }
    
    func productListView_BtnShuffleSongDidTap(datas: [AnyObject]?, type: SongType){
        if let _songs = datas as? [Song]{
            self.presentToPlayerViewController(_songs: _songs, playingIndex: 0,type:  type, shuffleEnable: true, isFromViewTracklisting: true)
        }
    }
    
    func productListView_ScrollViewDidEndDecelerating(scrollView: UIScrollView){
        self.updateLastScrollingOffset(scrollView: scrollView)
    }
    
    func productListView_ScrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool){
        
    }
    
    func productListView_BtnAutoPlayVideoDidChange(isOn: Bool) {
        //
    }
    
    func productListView_HeaderTitleDidTapButtonMore(productInfo: ProductListInfo) {
        
        switch productInfo.type {
        case .deejay_vertical, .deejay_horizontal:
            return self.pushToArtistListViewController(info: productInfo)
            
        case .mixset, .track, .trackPlayer, .video:
            return self.pushToProductListViewController(info: productInfo, vmodel: nil)
        default:
            break
        }
    }
    
    
    func productListView_shouldDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) -> Bool{
        if currentDatas?.count ?? 0 > indexPath.row{
            if let itemSong = currentDatas![indexPath.row] as? Song{
                if let _playlist = self.vmodel.filterBase.requestObject as? Playlist{
                    
                    // do request
                    self.vmodel.removeSong(itemPlaylist: _playlist, itemSong: itemSong, callback: { (success, error) in
                        // do something when remove song success
                    })
                    return true
                }
            }
        }
        
        if let error = HttpUtility.makeNSerror(errorCode: -1, message: AppMixError.data_notfound.rawValue){
            self.baseViewModel_showError(error: error)
        }
        
        return false
    }
    
    func productListView_MoreTapAtIndexPath(index: Int, withData Datas: [AnyObject]?) {
        
        var songType: SongType = .Track
        
        if Datas is [Mix]{
            songType = .Mix
        }
        
        if let items = Datas as? [Song]{
            let itemViewModel = PlayerViewModel.init(title: AppViewTitle.more.rawValue)
            itemViewModel.type = songType
            itemViewModel.songs = items
            itemViewModel.setPlayingIndex(index: index, isFromPlayerTracklisting: false)
            
            self.presentMoreViewController(vmodel: itemViewModel, itemSongSelected: nil)
        }
    }
    
    func productListView_didDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) {
        //
        print("delete song in playlist success")
    }
    
    func productListView_didSelectedAtIndexPath(indexPath: IndexPath, withData Datas: [AnyObject]?, type: SongType) {
        //
        if let _videos = Datas as? [Video]{
            self.presentVideoViewController(_videos: _videos, playingIndex: indexPath.row)
            
        }else if let _songs = Datas as? [Song]{
            self.presentToPlayerViewController(_songs: _songs, playingIndex: indexPath.row,type:  type, shuffleEnable: false, isFromViewTracklisting: true)
            
        }else if let _artist = Datas as? [Artist], _artist.count > indexPath.row{
            self.pushToArtistDetailViewController(artist_info: _artist[indexPath.row])
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
