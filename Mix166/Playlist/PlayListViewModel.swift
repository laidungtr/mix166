//
//  PlayListViewModel.swift
//  Mix166
//
//  Created by ttiamap on 6/26/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import UIKit

protocol playlistViewModel_Delegate: BaseViewModelDelegate {
    func playlistViewModel_DidRemoveObjectAtIndex(indexPath: IndexPath)
}

class PlaylistViewModel: BaseViewModel{
    
    var playlists: [Playlist] = []
    
    var delegate_vmodelPlaylist: playlistViewModel_Delegate?
    
    var songs: [Song]? = []
    // if != nil => user tap to add song to playlist
    // else user only tap to show playlist
    
    
    private var isDataEnd = false
    
    var isEndData: Bool{
        return self.isDataEnd
    }
    
    var isAddSongToPlaylist: Bool{
        return self.songs?.count ?? 0 > 0
    }
    
    func getCurrentPlaylistInfo() -> Playlist?{
        if let item = self.filterBase.requestObject as? Playlist{
            return item
        }
        return nil
    }
    
    override func songCellDelegate_BtnDeleteTap(sender: UIButton?, indexPath: IndexPath) {
        //
        
        if let playlist = self.getPlaylistAt(index: indexPath.row){
            self.deletePlaylist(itemPlaylist: playlist, callback: { (success, error) in
                if success {
                    
                    if self.removePlaylistAt(index: indexPath.row) {
                        self.delegate_vmodelPlaylist?.playlistViewModel_DidRemoveObjectAtIndex(indexPath: indexPath)
                    }
                }else{
                    if error != nil {
                        self.delegate?.baseViewModel_showError(error: error!)
                    }
                }
            })
        }else{
            if let error =  HttpUtility.makeNSerror(errorCode: -1, message: AppMixError.playlist_notfound.rawValue){
                self.delegate?.baseViewModel_showError(error: error)
            }
        }
    }
    
    internal func handleResponseData( response_data: [Playlist]) -> [IndexPath]{
        
        let response_data = response_data
        var indexPaths: [IndexPath] = []
        let lastIndex = self.playlists.count
        
        // detect pagging end data
        self.isDataEnd = response_data.count < limitDefault
        
        self.playlists += response_data
        
        if self.playlists.count > limitDefault{
            // is pagging
            for index in 0...response_data.count - 1{
                indexPaths.append(IndexPath.init(row: (lastIndex + index), section: 1))
            }
        }
        
        return indexPaths
    }
    
    override func getPlaylistAt(index: Int) -> Playlist?{
        if index >= self.playlists.count{
            return nil
        }
        
        let item = self.playlists[index]
        if item.isEmpty{
            return nil
        }
        
        return self.playlists[index]
    }
    
    func removePlaylistAt(index: Int) -> Bool{
        if self.playlists.count > index{
            self.playlists.remove(at: index)
            return true
        }
        return false
    }
    
    override func paggingRequest() {
        if !self.isDataEnd{
            self.paggingCallback?()
        }
    }
    
    //MARK: - Request
    func fetchPlaylistDataWithoutCache(callback:@escaping (_ isSuccess: Bool, _ isDataEnd: Bool, _ insertIndexPath: [IndexPath]?, _ error: NSError?)->()) {
        var request = DataRequest(.Network)
        
        
        if self.playlists.count == 0{
            self.delegate?.baseViewModel_showLoadingProgress()
        }
        
        request.onSuccess = {(data,  moreInfo) in
            
            if self.playlists.count == 0 {
                self.delegate?.baseViewModel_hideLoadingProgress()
            }
            
            var _indexPath: [IndexPath] = []
            if let _playlist = data as? [Playlist]{
                _indexPath = self.handleResponseData(response_data: _playlist)
            }
            
            callback(true, self.isDataEnd, _indexPath, nil)
        };
        
        request.onFailure = {(error, moreInfo) in
            
            if error != nil {
                self.delegate?.baseViewModel_showError(error: error!)
            }
            callback(false, self.isDataEnd, nil, error)
        }
        
        let offset = self.playlists.count
        
        if let _id = DataProxy.user.userInfo?.id {
            DataProxy.playlist.playlist(offset: offset, user_id: _id, dataRequest: request)
        }
    }
    
    func createPlaylist(itemCreate: PlaylistCreate, callback:@escaping (_ isSuccess: Bool, _ isDataEnd: Bool, _ insertIndexPath: [IndexPath]?, _ error: NSError?)->()) {
        
        var request = DataRequest(.Network)
        
        self.delegate?.baseViewModel_showLoadingProgress()
        request.onSuccess = {(data,  moreInfo) in
            
            if let error = moreInfo as? NSError{
                self.delegate?.baseViewModel_showError(error: error)
            }
            
            if let playlist = data as? Playlist{
                if self.playlists.count > 0 {
                    self.playlists.insert(playlist, at: 0)
                }else{
                    self.playlists.append(playlist)
                }
                
                let insertPath = [IndexPath.init(row: 0, section: 1)]
                
                callback(false, self.isDataEnd, insertPath, nil)
                
                return
            }
            
            self.delegate?.baseViewModel_showError(error: HttpUtility.localParseError())
            callback(false, self.isDataEnd, nil, HttpUtility.localParseError())
        };
        
        request.onFailure = {(error, moreInfo) in
            if error != nil {
                self.delegate?.baseViewModel_showError(error: error!)
            }
            callback(false, self.isDataEnd, nil, error)
        };
        
        if let _id = DataProxy.user.userInfo?.id {
            DataProxy.playlist.playlistCreate(itemCreate: itemCreate, user_id: _id, dataRequest: request)
        }
        
    }
    
    func addSongToPlaylist(itemPlaylist: Playlist, callback:@escaping (_ isSuccess: Bool, _ error: NSError?)->()) {
        
        var request = DataRequest(.Network)
        
        request.onSuccess = {(data,  moreInfo) in
            
            if let errorInfo = moreInfo as? NSError{
                self.delegate?.baseViewModel_showError(error: errorInfo)
                callback(true, errorInfo)
                return
            }
            
            self.delegate?.baseViewModel_showError(error: HttpUtility.localParseError())
            callback(false, HttpUtility.localParseError())
        };
        
        request.onFailure = {(error, moreInfo) in
            self.delegate?.baseViewModel_showError(error: HttpUtility.localParseError())
            callback(false, error)
        };
        
        
        if self.songs?.count ?? 0 > 0 {
            if let _id = DataProxy.user.userInfo?.id {
                DataProxy.playlist.playlistAddSong(_playlist: itemPlaylist, itemSong: self.songs!, user_id: _id, dataRequest: request)
            }
        }
        
    }
    
    func removeSong(itemPlaylist: Playlist?, itemSong: Song?, callback:@escaping (_ isSuccess: Bool, _ error: NSError?)->()) {
        var request = DataRequest(.Network)
        
        request.onSuccess = {(data,  moreInfo) in
            
            if let errorInfo = moreInfo as? NSError{
                self.delegate?.baseViewModel_showError(error: errorInfo)
                callback(true, errorInfo)
                return
            }
            callback(false, HttpUtility.localParseError())
        };
        
        request.onFailure = {(error, moreInfo) in
            if error != nil {
                self.delegate?.baseViewModel_showError(error: error!)
            }
            callback(false, error)
        };
        
        
        if itemPlaylist != nil && itemSong != nil {
            
            if let _id = DataProxy.user.userInfo?.id {
                DataProxy.playlist.playlistRemoveSong(_playlist: itemPlaylist!, itemSong: [itemSong!], user_id: _id, dataRequest: request)
            }
            
        }
    }
    
    func deletePlaylist(itemPlaylist: Playlist?, callback:@escaping (_ isSuccess: Bool, _ error: NSError?)->()) {
        
        var request = DataRequest(.Network)
        
        request.onSuccess = {(data,  moreInfo) in
            
            if let errorInfo = moreInfo as? NSError{
                self.delegate?.baseViewModel_showError(error: errorInfo)
                callback(true, errorInfo)
                return
            }
            
            self.delegate?.baseViewModel_showError(error: HttpUtility.localParseError())
            callback(false, HttpUtility.localParseError())
        };
        
        request.onFailure = {(error, moreInfo) in
            
            if error != nil {
                self.delegate?.baseViewModel_showError(error: error!)
            }
            callback(false, error)
        };
        
        
        if itemPlaylist != nil {
            if let _id = DataProxy.user.userInfo?.id {
                DataProxy.playlist.playlistRemove(_playlist: itemPlaylist!, user_id: _id, dataRequest: request)
            }
        }
    }
}
