//
//  PlaylistViewController.swift
//  Mix166
//
//  Created by ttiamap on 6/26/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class PlaylistViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, playlistViewModel_Delegate {
    
    @IBOutlet weak var myCollectionView: UICollectionView?
    
    var appViewTitle: AppViewTitle = .playlist
    var vmodel : PlaylistViewModel = PlaylistViewModel.init(title: AppViewTitle.playlist.rawValue)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.vmodel.delegate_vmodelPlaylist = self
        self.requestData()
        
        self.vmodel.paggingCallback = {() in
            self.requestData()
        }
    }
    
    func requestData(){
        vmodel.fetchPlaylistDataWithoutCache { (success, isDataEnd,insertIndexPaths, error) in
            //
            if success{
                if insertIndexPaths?.count ?? 0 > 0{
                    
                    self.myCollectionView?.collectionViewLayout.invalidateLayout()
                    self.myCollectionView?.performBatchUpdates({
                        self.myCollectionView?.insertItems(at: insertIndexPaths!)
                    }, completion: { (finish) in
                        //
                    })
                    
                }else{
                    self.registerCell()
                }
            }
        }
    }
    
    override func setViewTitle() -> String {
        return self.vmodel.titleNavigationBar ?? ""
    }
    
    override func setNavigationBarStyle() -> NavigationBarStyle {
        return .Playlist
    }
    
    override func leftBarDismissTap() {
        self.dismissViewWithStatusBar(isHidden: true)
    }
    

    //MARK: - Register cell
    private func registerCell(){
        
        if self.myCollectionView?.delegate == nil {
            let cellNib = UINib.init(nibName: PlayListCollectionCell.nameOfClass, bundle: nil)
            let cellNib2 = UINib.init(nibName: LoadingListCollectionCell.nameOfClass, bundle: nil)
            
            self.myCollectionView?.register(cellNib, forCellWithReuseIdentifier: PlayListCollectionCell.nameOfClass)
            self.myCollectionView?.register(cellNib2, forCellWithReuseIdentifier: LoadingListCollectionCell.nameOfClass)
            
            let nibHeader = UINib(nibName: LoadingCollectionReusableCell.nameOfClass, bundle: nil)
            self.myCollectionView?.register(nibHeader, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: LoadingCollectionReusableCell.nameOfClass)
            
            self.myCollectionView?.dataSource = self
            self.myCollectionView?.delegate = self
            
            self.myCollectionView?.contentInset = UIEdgeInsetsMake(0, 0, playerControlHeight, 0)
            
        }else{
            self.myCollectionView?.reloadData()
        }
    }
    
    //MARK: - CollectionView Delegate - Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1 // add playlist
        }
        return self.vmodel.playlists.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            return self.vmodel.configureAddPlaylistCell(collectionView: collectionView, indexPath: indexPath)
        }
        return self.vmodel.configurePlaylistCell(collectionView: collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        if indexPath.section == 0{
            // create playlist
            self.showCreatePlaylist()

        }else{
            // selected playlist
            if self.vmodel.isAddSongToPlaylist{
                // add song to current selected playlist
                if let playlist = self.vmodel.getPlaylistAt(index: indexPath.row){
                    self.requestAddSongToPlaylist(playlist: playlist)
                }
            }else{
                // go to playlist Detail
                if let playlist = self.vmodel.getPlaylistAt(index: indexPath.row){
                    
                    self.vmodel.filterBase.requestObject = playlist as AnyObject
                    let info = ProductListInfo.init(_proxy: .playlistDetail, _type: .track, filter: self.vmodel.filterBase, _data: nil, _paggingTypes: nil, pullRefresh: true)
                    self.pushToPlaylistDetailViewController(info: info, vmodel: self.vmodel)
                }
            }
        }
        return true
    }
    
    //MARK: Playlist ViewModel Delegate
    func playlistViewModel_DidRemoveObjectAtIndex(indexPath: IndexPath) {
        self.myCollectionView?.collectionViewLayout.invalidateLayout()
        self.myCollectionView?.performBatchUpdates({ 
            self.myCollectionView?.deleteItems(at: [indexPath])
        }, completion: { (finish) in
            //
            UIView.setAnimationsEnabled(false)
            self.myCollectionView?.performBatchUpdates({ 
                self.myCollectionView?.reloadSections(IndexSet(integer: indexPath.section))
            }, completion: { (finish) in
                //
                UIView.setAnimationsEnabled(true)
            })
        })
    }

    
    func showCreatePlaylist(){
        let alertController = UIAlertController(title: "Create New Playlist?", message: "enter a name for this playlist", preferredStyle: .alert)
        alertController.view.tintColor = UIColor.init(hexaString: AppColor.appRedColor.rawValue)
        
        let confirmAction = UIAlertAction(title: "Save", style: .default) { (_) in
            if let field = alertController.textFields![0] as? UITextField {
                
                if let text = field.text, text.length > 0{
                    let playlistCreate = PlaylistCreate.init(name: text, description: "")
                    
                    self.requestCreatePlaylist(itemCreate: playlistCreate)
                }
                
            } else {
                // user did not fill field
                // showAlert
                
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Name"
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func requestAddSongToPlaylist(playlist: Playlist){
        
        self.vmodel.addSongToPlaylist(itemPlaylist: playlist) { (success, error) in
                //fixme
                //showAlert
            
            if error?.code == 0 {
                // success
                
                self.dismiss(animated: true, completion: { 
                    //finish
                })
            }
            print(error?.localizedDescription ?? "null Error")
        }
    }
    
    func requestCreatePlaylist(itemCreate: PlaylistCreate){
        
        self.vmodel.createPlaylist(itemCreate: itemCreate) { (success, dataEnd, insertIndexPath, errorResponse) in
            //
            if (errorResponse != nil){
                
            }else{
                // success
                // reload playlist
                if insertIndexPath?.count ?? 0 > 0{
                    self.myCollectionView?.collectionViewLayout.invalidateLayout()
                    self.myCollectionView?.performBatchUpdates({
                        self.myCollectionView?.insertItems(at: insertIndexPath!)
                    }, completion: { (finish) in
                        //
                    })
                }
            }
        }
    }
    
    // Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return playlistSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        
        if section == 0{
            return UIEdgeInsetsMake(20, 0, 10 , 0)
        }
        return UIEdgeInsetsMake(0, 0, 20 , 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 10.0
        
    }
    
    //MARK: Header - Footer
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if section == 0 || self.vmodel.isEndData{
            return sizeZero
        }
        return trackSize
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return self.vmodel.configureLoadingFooterCell(collectionView:collectionView, kind:kind, indexPath:indexPath)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
