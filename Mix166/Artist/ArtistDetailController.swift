//
//  ArtistDetailController.swift
//  Mix166
//
//  Created by ttiamap on 8/10/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ArtistDetailController: ProductListViewController {
    
    @IBOutlet weak var btn_dissmissView: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.btn_dissmissView?.isHidden = self.navigationController != nil
    }
    
    override func setNavigationBarStyle() -> NavigationBarStyle {
        return .Transparent
    }
    
    override func setViewTitle() -> String {
        return ""
    }
    
    override func setupProductListView(){
        if self.productlist_info != nil {
            self.viewProductList?.isEnablePullRefresh = self.productlist_info!.isEnablePullToRefresh
            self.viewProductList?.delegate = self
            self.viewProductList?.datasource = self
            self.viewProductList?.delegate_scrolling = self
            self.viewProductList?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
            self.viewProductList?.requestWithProductListInfo(_productInfo: self.productlist_info!, newRequest: true)
        }
    }
    
    @IBAction func btnDissmissView(sender : AnyObject){
        self.dismiss(animated: true) {
            //
        }
    }
    
    override func productListView_didSelectedAtIndexPath(indexPath: IndexPath, withData Datas: [AnyObject]?, type: SongType) {
        //
        if let _videos = Datas as? [Video]{
                self.presentVideoViewController(_videos: _videos, playingIndex: indexPath.row)
            
        }else if let _songs = Datas as? [Song]{
            self.presentToPlayerViewController(_songs: _songs, playingIndex: indexPath.row,type:  type, shuffleEnable: false,isFromViewTracklisting: true)
            
        }else if let _artist = Datas as? [Artist], _artist.count > indexPath.row{
            self.pushToArtistDetailViewController(artist_info: _artist[indexPath.row])
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
