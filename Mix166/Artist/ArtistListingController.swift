//
//  ArtistListingController.swift
//  Mix166
//
//  Created by ttiamap on 8/9/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ArtistListingController: ProductListViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setNavigationBarStyle() -> NavigationBarStyle {
        if self.productlist_info?.proxyType() == .artist || self.productlist_info?.proxyType() == .artistDetail{
            return .Transparent
            
        }else{
            return .Default
        }
    }
    
    override func setViewTitle() -> String {
        if self.productlist_info?.proxyType() == .artist{
            return AppViewTitle.topDJ.rawValue
            
        }else if self.productlist_info?.proxyType() == .artistDetail{
            return ""
            
        }else{
            return AppViewTitle.profileFollowing.rawValue
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setupProductListView(){
        if self.productlist_info != nil {
            self.viewProductList?.isEnablePullRefresh = self.productlist_info!.isEnablePullToRefresh
            self.viewProductList?.delegate = self
            self.viewProductList?.datasource = self
            self.viewProductList?.delegate_scrolling = self
            self.viewProductList?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
            self.viewProductList?.requestWithProductListInfo(_productInfo: self.productlist_info!, newRequest: true)
        }
    }
}
