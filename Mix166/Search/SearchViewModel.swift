//
//  SearchViewModel.swift
//  Mix166
//
//  Created by ttiamap on 6/28/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import UIKit

class SearchViewModel: BaseViewModel{
    
    var search_data : Search? = Search()
    
    var type_search: Search_Type = .all
    
    var isSearching = false
    
    override var titleNavigationBar: String?{
        return "Tìm tất cả kết quả " + self.filterBase.keyword
    }
    
    override func getItemsAtIndexPath(indexPath: IndexPath) -> (AnyObject?, ProductType?) {
        
        var objects: [AnyObject] = []
        var _type: ProductType?
        
        if let _entries = self.getEntriesAtIndex(index: indexPath.section) as? Search_Entries{
            
            switch _entries.type {
                
            case .mixset:
                
                _type = ProductType.mixset
                break
                
            case .track:

                _type = ProductType.track
                break
                
            case .video:
                
                _type = ProductType.video
                break
                
            case .deejay:
                    _type = ProductType.deejay_vertical
                
                break
                
            case .recent_search:
                //
                if let keywords = _entries.entries as? [String], indexPath.row < keywords.count{
                    _type = .keyword_recently
                    return (keywords[indexPath.row] as AnyObject, _type)
                }
                break
                
            case .trend:
                if let keywords = _entries.entries as? [String], indexPath.row < keywords.count{
                    _type = .keyword_trend
                    return (keywords[indexPath.row] as AnyObject, _type)
                }
                break
                
            case .unknow:
                break
                
            }
            
            
            if let data = _entries.entries{
                objects = data
            }
        }
        
        
        return (objects as AnyObject, _type)
    }
    
    override func getItemAtIndexPath(indexPath: IndexPath) -> AnyObject?{
        if let entry = self.getEntriesAtIndex(index: indexPath.section) as? Search_Entries{
            if entry.entries?.count ?? 0 > indexPath.row{
                if let _song = entry.entries?[indexPath.row] as? Song{
                    return _song
                }
            }
        }
        return nil
    }
    
    override func getItemsAtHorizontalIndexPath(indexPath: IndexPath, parentSection section: Int) -> (AnyObject?, FeatureType?) {
        var _song: [Song] = []
        
        var type:FeatureType?
        if let _entries = self.getEntriesAtIndex(index: section) as? Search_Entries{
            switch _entries.type {
                
            case .mixset:
                if let itemSongs = _entries.entries as? [Song]{
                    _song = itemSongs
                }
                type = FeatureType.mixset
                break
                
            case .track:
                if let itemSongs = _entries.entries as? [Song]{
                    _song = itemSongs
                }
                type = FeatureType.track
                break
                
            case .video:
                if let itemVideos = _entries.entries as? [Video]{
                    _song = itemVideos
                }
                type = FeatureType.video
                break
                
            case .deejay:
                break
                
            case .unknow:
                break
                
            default:
                break
            }
        }
        
        return (_song as AnyObject,type)
    }
    
    override func songCellDelegate_BtnMoreTap(sender: UIButton, indexPath: IndexPath) {
        if let entries = self.getEntriesAtIndex(index: indexPath.section) as? Search_Entries{
            if let items = entries.entries as? [Song]{
                self.delegate?.baseViewModel_moreBtnTapAtIndexPath(indexPath: indexPath, datas: items)
            }
        }
    }
    
    //MARK: Request
    func fetchSearchDataWith(keyword: String, callback:@escaping (_ isSuccess: Bool, _ error: NSError?)->()){
        
        var request = DataRequest(.Network)
        
        request.onSuccess = {(data,  moreInfo) in
            
            if let _search = data as? Search{
                self.search_data = _search
            }
            
            callback(true,nil)
        };
        
        request.onFailure = {(error, moreInfo) in
            callback(false, error)
        };
        
        request.onLoading = {result in
            
        }
        
        self.filterBase.keyword = keyword
        DataProxy.search.searchWith(type: self.type_search, filter: self.filterBase, dataRequest: request)
    }
    
    func fetchSearchTrendDataWith(callback:@escaping (_ isSuccess: Bool, _ error: NSError?)->()){
        
        var request = DataRequest(.Default)
        
        request.onSuccess = {(data,  moreInfo) in
            
            if let _search = data as? Search{
                self.search_data = _search
            }
            
            callback(true,nil)
        };
        
        request.onFailure = {(error, moreInfo) in
            callback(false, error)
        };
        
        request.onLoading = {result in
            
        }
        
        DataProxy.search.searchTrending(dataRequest: request)
    }
    
    //MARK: GET
    
    override func getEntriesAtIndex(index: Int?) -> AnyObject? {
        if self.search_data != nil && index != nil && self.search_data!.entries.count > index!{
            return self.search_data?.entries[index!]
        }
        return nil
    }
    
    func getKeywordAt(indexPath: IndexPath) -> String{
        if let _entry = self.getEntriesAtIndex(index: indexPath.section) as? Search_Entries{
            if let dataSource = _entry.entries as? [String], dataSource.count > indexPath.row{
                return dataSource[indexPath.row]
            }
        }
        return ""
    }
    
    override func horizontal_shouldReloadData(type: HorizontalType, parentSection: Int?) -> Bool {
        return true
    }
    
    //MARK: Configure cell
    
    // MARK: Mixset
    func configureMixSetCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.mixset.rawValue, for: indexPath) as? HorizontalCollectionViewCell{
            
            queueConcurrent.sync(execute: {
                //                if cell.delegate == nil {
                cell.type = .mixset
                cell.currentSection = indexPath.section
                cell.delegate = self
                //                }
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    //    // MARK: Deejay
    //   override func configureDeejayCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
    //        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.deejay.rawValue, for: indexPath) as? HorizontalCollectionViewCell{
    //
    //            queueConcurrent.sync(execute: {
    ////                if cell.delegate == nil {
    //                    cell.currentSection = indexPath.section
    //                    cell.type = .deejay
    //                    cell.delegate = self
    ////                }
    //            })
    //
    //            return cell
    //        }
    //
    //        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    //    }
    
    
    // MARK: Video
    override func configureVideoCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.video.rawValue, for: indexPath) as? HorizontalCollectionViewCell{
            
            queueConcurrent.sync(execute: {
                //                if cell.delegate == nil {
                cell.currentSection = indexPath.section
                cell.type = .video
                cell.delegate = self
                //                }
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    //Search Keyword
    func configureKeywordSearch(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let _cell = collectionView.dequeueReusableCell(withReuseIdentifier:SearchKeywordCollectionCell.nameOfClass, for: indexPath) as? SearchKeywordCollectionCell{
            
            queueConcurrent.async(execute: {
                _cell.configureCell(keyword: self.getKeywordAt(indexPath: indexPath))
            })
            return _cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureKeywordTrend(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let _cell = collectionView.dequeueReusableCell(withReuseIdentifier:SearchKeywordRoundCell.nameOfClass, for: indexPath) as? SearchKeywordRoundCell{
            
            
            queueConcurrent.async(execute: {
                _cell.configureCellTrend(keyword: self.getKeywordAt(indexPath: indexPath))
            })
            return _cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureKeywordSizingCell(sizingCell: SearchKeywordRoundCell, indexPath: IndexPath){
        sizingCell.configureSizingCell(keyword: self.getKeywordAt(indexPath: indexPath))
    }
    
    // MARK: - HeaderTitle
    func configureHeaderTitleCell(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, indexPath: IndexPath) -> UICollectionReusableView{
        
        if kind == UICollectionElementKindSectionHeader{
            if let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: TitleCollectionReusableView.nameOfClass, for: indexPath) as? TitleCollectionReusableView{
                //                header.delegate = self
                //                header.headerSection = indexPath.section
                
                self.queueConcurrent.async(execute: {
                    if let entry = self.getEntriesAtIndex(index: indexPath.section){
                        header.configureHeader(str_title: entry.title)
                    }
                })
                
                return header
            }
        }
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    
}
