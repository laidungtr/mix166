//
//  SearchResultViewController.swift
//  Mix166
//
//  Created by ttiamap on 6/30/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class SearchResultViewController: BaseViewController, ProductListViewDelegate, ProductListViewDataSource, SlideMenuViewDelegate, UIScrollViewDelegate {
    
    @IBOutlet internal weak var myScrollView: UIScrollView?
    @IBOutlet internal weak var viewMix: ProductListView?
    @IBOutlet internal weak var viewTrack: ProductListView?
    @IBOutlet internal weak var viewVideo: ProductListView?
    @IBOutlet internal weak var viewArtist: ProductListView?
    @IBOutlet internal weak var viewSlideMenu: SlideMenuView?

    internal var vmodel: SearchViewModel!
    
    internal var productlist_infos: [ProductListInfo]?
    
    internal var paggingTypes: [Search_Type]?
    
    internal var isScrollingManually = false
    internal var isUserDragging = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.myScrollView?.alwaysBounceHorizontal = true
        self.myScrollView?.alwaysBounceVertical = false
        self.setupRequestProductListInfo()
        self.setupSliderMenu()
    }
    
    override func isNavigationBarLineBottomHidden() -> Bool {
        return true
    }
    
    override func setViewTitle() -> String {
        return self.vmodel.titleNavigationBar ?? ""
    }
    
    func setupSliderMenu(){

        self.viewSlideMenu?.slideMenuStyle = .Title
        self.viewSlideMenu?.delegate = self
        self.viewSlideMenu?.setSlideMenuBackgroundColor(color: colorNavigationBar)
        self.viewSlideMenu?.reloadSlideMenuData()
        self.view.layoutIfNeeded()
    }
    
    func setupRequestProductListInfo(){
        
        let mix_info = ProductListInfo.init(_proxy: .search, _type: .mixset, filter: self.vmodel.filterBase, _data: nil,
                                            _paggingTypes: nil, pullRefresh: false)
        let track_info = ProductListInfo.init(_proxy: .search, _type: .track, filter: self.vmodel.filterBase, _data: nil, _paggingTypes: nil,  pullRefresh: false)
        let video_info = ProductListInfo.init(_proxy: .search, _type: .video, filter: self.vmodel.filterBase, _data: nil, _paggingTypes: nil,  pullRefresh: false)
        let deejay_info = ProductListInfo.init(_proxy: .search, _type: .deejay_vertical, filter: self.vmodel.filterBase, _data: nil, _paggingTypes: nil,  pullRefresh: false)
        
        self.paggingTypes       = [.mixset, .track, .video, .deejay]
        self.productlist_infos  = [mix_info, track_info, video_info, deejay_info]
        
        // setup view mix
        self.viewMix?.isEnablePullRefresh = false
        self.viewMix?.delegate = self
        self.viewMix?.datasource = self
        self.viewMix?.requestWithProductListInfo(_productInfo: mix_info, newRequest: true)
        self.viewMix?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
        
        // setup view track
        self.viewTrack?.isEnablePullRefresh = false
        self.viewTrack?.delegate = self
        self.viewTrack?.datasource = self
        self.viewTrack?.requestWithProductListInfo(_productInfo: track_info, newRequest: true)
        self.viewTrack?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
        
        // setup view video
        self.viewVideo?.isEnablePullRefresh = false
        self.viewVideo?.delegate = self
        self.viewVideo?.datasource = self
        self.viewVideo?.requestWithProductListInfo(_productInfo: video_info, newRequest: true)
        self.viewVideo?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
        
        // setup view artist
        self.viewArtist?.isEnablePullRefresh = false
        self.viewArtist?.delegate = self
        self.viewArtist?.datasource = self
        self.viewArtist?.requestWithProductListInfo(_productInfo: deejay_info, newRequest: true)
        self.viewArtist?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
    }
    
    //MARK - ProductList Datasource
    func productListView_spacingBetweenItem() -> CGFloat{
        return 10.0
    }
    
    func productListView_scrollDirection() -> UICollectionViewScrollDirection{
        return .vertical
    }
    
    func productListView_contentInsect() -> UIEdgeInsets{
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    //MARK - ProductList Delegate
    func productListView_BtnShuffleSongDidTap(datas: [AnyObject]?, type: SongType){
        
    }
    
    func productListView_shouldDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) -> Bool{
        return false
    }
    
    func productListView_didDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) {
        //
    }
    
    func productListView_HeaderTitleDidTapButtonMore(productInfo: ProductListInfo) {
        
        switch productInfo.type {
        case .deejay_vertical, .deejay_horizontal:
            return self.pushToArtistListViewController(info: productInfo)
            
        case .mixset, .track, .trackPlayer, .video:
            return self.pushToProductListViewController(info: productInfo, vmodel: nil)
        default:
            break
        }
    }
    
    func productListView_didSelectedAtIndexPath(indexPath: IndexPath, withData Datas: [AnyObject]?, type: SongType) {
        //
        if let _videos = Datas as? [Video]{
            self.presentVideoViewController(_videos: _videos, playingIndex: indexPath.row)
            
        }else if let _songs = Datas as? [Song]{
            self.presentToPlayerViewController(_songs: _songs, playingIndex: indexPath.row,type:  type, shuffleEnable: false, isFromViewTracklisting: true)
            
        }else if let _artist = Datas as? [Artist], _artist.count > indexPath.row{
            self.pushToArtistDetailViewController(artist_info: _artist[indexPath.row])
        }
    }
    
    func productListView_MoreTapAtIndexPath(index: Int, withData Datas: [AnyObject]?) {
        
        var songType: SongType = .Track
        
        if Datas is [Mix]{
            songType = .Mix
        }
        
        if let items = Datas as? [Song]{
            let itemViewModel = PlayerViewModel.init(title: AppViewTitle.more.rawValue)
            itemViewModel.type = songType
            itemViewModel.songs = items
            itemViewModel.setPlayingIndex(index: index, isFromPlayerTracklisting: false)
            
            self.presentMoreViewController(vmodel: itemViewModel, itemSongSelected: nil)
        }
    }
    
    func productListView_BtnAutoPlayVideoDidChange(isOn: Bool) {
        //
    }
    
    
    //MARK: - SlideMenu Delegate
    func slideMenu_NumberOfItem() -> NSInteger{
        return self.paggingTypes?.count ?? 0
    }
    
    func slideMenu_TitleForItem(index: NSInteger) -> String{
        if self.paggingTypes?.count ?? 0 > index{
            return self.paggingTypes?[index].rawValue.uppercased() ?? ""
        }
        return ""
    }
    
    func slideMenu_DidSelectedItemAtIndex(index: NSInteger){

        if !self.isUserDragging{
            self.isUserDragging = false
            self.isScrollingManually = true
        }
        
        let xOffset = CGFloat(index) * screenWidth
        self.myScrollView?.setContentOffset(CGPoint.init(x: xOffset, y: 0), animated: true)
    }
    
    func slideMenu_FristloadSelected() -> Int {
        return 0
    }
    
    //MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.isScrollingManually{
            return
        }
        
        let xOffset = (scrollView.contentOffset.x / screenWidth)
        self.viewSlideMenu?.moveBottomIndicator(xOffset: xOffset)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.isUserDragging = true
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if self.isScrollingManually{
            return
        }
        
        let _tempOffsetX = scrollView.contentOffset.x + screenWidth
        var pageIndex = Int(_tempOffsetX / screenWidth)
        
        if _tempOffsetX != scrollView.contentSize.width{
            pageIndex -= 1
        }
        
        
        if self.paggingTypes != nil && pageIndex >= self.paggingTypes!.count {
            pageIndex = self.paggingTypes!.count - 1
        }
        
        
        self.viewSlideMenu?.updateCurentSelected(index: pageIndex)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        //
        self.isScrollingManually = false
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
