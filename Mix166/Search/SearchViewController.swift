//
//  SearchViewController.swift
//  Mix166
//
//  Created by ttiamap on 6/27/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class SearchViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var myCollectionView: UICollectionView?
    
    var scrollViewHideKeyboard = false
    var vmodel: SearchViewModel = SearchViewModel.init(title: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.vmodel.delegate = self
        
        self.vmodel.fetchSearchTrendDataWith { (success, error) in
            if error != nil {
                self.baseViewModel_showError(error: error!)
                return
            }else if success{
                self.registerCell()
            }
        }
        
        self.setupGesture()
    }
    
    func setupGesture(){
        self.myCollectionView?.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGestureHandle(gesture:)))
        self.myCollectionView?.addGestureRecognizer(tapGesture)
    }
    
    func tapGestureHandle(gesture: UITapGestureRecognizer){
        gesture.cancelsTouchesInView = false
        
        self.searchBarHideKeyboard()
    }
    
    override var isNavigationBarHiddenWhenSwipe: Bool{
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setNavigationBarStyle() -> NavigationBarStyle {
        return .Search
    }
    
    //MARKK - handle text field request search
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.scrollViewHideKeyboard = true
        self.searchBar?.endEditing(true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollViewHideKeyboard = false
    }
    
    override func artistDetail_didChangeFollowingStatus() {
        //
        if self.myCollectionView == nil { return }
        
        if self.myCollectionView!.numberOfItems(inSection: 0) > 0{
            self.myCollectionView!.performBatchUpdates({
                self.myCollectionView?.reloadSections(IndexSet(integer: 0))
            }, completion: nil)
        }
    }
    
    override func textFieldDidSearchWithKeyword(keyword: String) {
        
        self.vmodel.fetchSearchDataWith(keyword: keyword, callback: { (success, error) in
            if success{
                self.registerCell()
            }else{
                if (error != nil){
                    self.showMessages(messages: error!.localizedDescription)
                }
            }
        })
    }
    
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //
        if textField.text?.length ?? 0 > 0{
            self.vmodel.filterBase.keyword = textField.text ?? ""
            self.pushToSearchResultController(search_vmodel: self.vmodel)
        }
        
        return true
    }
    
    override func textFieldDidClearKeyword(keyword: String) {
        
        self.vmodel.fetchSearchDataWith(keyword: keyword, callback: { (success, error) in
            if success{
                self.registerCell()
            }else{
                if (error != nil){
                    self.showMessages(messages: error!.localizedDescription)
                }
            }
        })
    }
    
    //MARK: - Register cell
    private func registerCell(){
        
        if self.myCollectionView?.delegate == nil {
            
            self.vmodel.registerCell(collectionView: self.myCollectionView, callback: {
                self.myCollectionView?.contentInset = UIEdgeInsetsMake(0, 0, playerControlHeight, 16.0)
                let layout = DLTCenterFlowLayout()
                layout.minimumLineSpacing = paddingBetweenItem * 1.5
                
                layout.minimumInteritemSpacing = paddingBetweenItem * 1.5
                
                self.myCollectionView?.collectionViewLayout = layout
                
                
                self.myCollectionView?.dataSource = self
                self.myCollectionView?.delegate = self
            })
            
            
        }else{
            self.myCollectionView?.reloadData()
        }
    }
    
    //MARK: - CollectionView Delegate - Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if self.searchBar?.text?.length ?? 0 > 0{
            return self.vmodel.search_data?.entries.count ?? 0
        }else{
            return self.vmodel.search_data?.entries.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let entry = self.vmodel.getEntriesAtIndex(index: section) as? Search_Entries{
            switch entry.type {
            case .trend, .recent_search, .deejay, .track:
                return entry.entries?.count ?? 0
                
            case .mixset, .video:
                return entry.entries?.count ?? 0 > 0 ? 1 : 0
                
            default:
                break
            }
        }
        
        return  0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let entry = self.vmodel.getEntriesAtIndex(index: indexPath.section) as? Search_Entries {
            switch entry.type {
            case .trend:
                return self.vmodel.configureKeywordTrend(collectionView:collectionView, indexPath:indexPath)
            case  .recent_search:
                //
                return self.vmodel.configureKeywordSearch(collectionView:collectionView, indexPath:indexPath)
                
            case .mixset:
                return self.vmodel.configureMixSetCell(collectionView:collectionView, indexPath:indexPath)
                
            case .track:
                return self.vmodel.configureTrackCell(collectionView:collectionView, indexPath:indexPath)
                
            case .deejay:
                return self.vmodel.configureDeejayListCell(collectionView:collectionView, indexPath:indexPath, enableFollow: true)
                
            case .video:
                return self.vmodel.configureVideoCell(collectionView:collectionView, indexPath:indexPath)
                
            default:
                break
            }
        }
        
        return self.vmodel.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        self.vmodel.selectedItemAtIndexPath(indexPath: indexPath)
        return true
    }
    
    // Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if let entry = self.vmodel.getEntriesAtIndex(index: indexPath.section) as? Search_Entries {
            switch entry.type {
            case .trend:
                self.vmodel.configureKeywordSizingCell(sizingCell: self.vmodel.sizingKeywordCell, indexPath: indexPath)
                return self.vmodel.sizingKeywordCell.intrinsicContentSize
                
            case .recent_search:
                //
                return searchKeywordSize
                
            case .mixset:
                return CGSize(width: screenWidth, height: mixSize.height)
                
            case .track:
                return trackSize
                
            case .deejay:
                return deejayListSize
                
            case .video:
                return CGSize(width: screenWidth, height: videoSize.height)
                
            default:
                break
            }
        }
        
        return CGSize.init(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        
        if let entry = self.vmodel.getEntriesAtIndex(index: section) as? Search_Entries {
            switch entry.type {
            case .trend, .recent_search:
                //
                return UIEdgeInsetsMake(paddingLeftRight, paddingLeftRight, paddingLeftRight , paddingLeftRight)
                
            case .mixset, .track, .video, .deejay:
                return UIEdgeInsetsMake(paddingLeftRight, paddingLeftRight, 0, paddingLeftRight)
                
            default:
                break
            }
        }
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        if let entry = self.vmodel.getEntriesAtIndex(index: section) as? Search_Entries {
            switch entry.type {
            case .trend, .recent_search:
                //
                return paddingBetweenItem * 1.5
            default:
                break
            }
        }
        return paddingBetweenItem
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        if let entry = self.vmodel.getEntriesAtIndex(index: section) as? Search_Entries {
            switch entry.type {
            case .trend, .recent_search:
                //
                return paddingBetweenItem * 1.5
            default:
                break
            }
        }
        return paddingBetweenItem
    }
    
    //MARK: Header - Footer
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
    //        return headerTitleSize
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if let entry = self.vmodel.getEntriesAtIndex(index: section) as? Search_Entries{
            if entry.entries?.count ?? 0 > 0 {
                return headerTitleSize
            }
        }
        return CGSize.init(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader{
            let cell = self.vmodel.configureHeaderTitleCell(collectionView:collectionView, viewForSupplementaryElementOfKind:kind, indexPath:indexPath) as! TitleCollectionReusableView
            return cell
        }
        
        return self.vmodel.configureLoadingFooterCell(collectionView:collectionView, kind:kind, indexPath:indexPath)
    }
    
    
}
