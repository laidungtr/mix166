//
//  SignInViewController.swift
//  Mix166
//
//  Created by ttiamap on 7/18/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import FBSDKLoginKit

protocol SignInViewDelegate {
    func SignInView_willDissmissView(logginSuccess status: Bool)
    func SignInView_didDissmissView(logginSuccess status: Bool)
}

class SignInViewController: BaseViewController, baseViewKeyBoardDelegate {
    
    @IBOutlet weak var btnLogginFacebook: UIButton?
    @IBOutlet weak var btnLoginNormal: UIButton?
    @IBOutlet weak var txt_UserName: UITextField?
    @IBOutlet weak var txt_Password: UITextField?
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var btnSkip: UIButton?
    
    var delegate: SignInViewDelegate?
    private var login_info: LoginInfo = LoginInfo(user_name: "", password: "", token: "", loginType: .normal)
    
    private var viewSelected: UIView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegateKeyBoard = self
        self.setupButtonUI()
        self.setupTextFieldUI()
    }
    
    func setupButtonUI(){
        self.btnLoginNormal?.backgroundColor = UIColor.init(hexaString: AppColor.appRedColor.rawValue)
        self.btnLoginNormal?.borderWithRadius(3.0)
        self.btnLogginFacebook?.borderWithRadius(3.0)
        self.btnLogginFacebook?.titleLabel?.font = UIFont.fontRegular_medium()
        self.btnSkip?.titleLabel?.font = UIFont.fontRegular_medium()
    }
    
    func setupTextFieldUI(){
        self.txt_UserName?.borderWithRadius(3.0, borderWidth: 0.5, borderColor: UIColor.darkGray)
        self.txt_UserName?.tintColor = UIColor.init(hexaString: AppColor.appRedColor.rawValue)
        self.txt_UserName?.backgroundColor = UIColor.black
        self.txt_UserName?.font = UIFont.fontRegular_medium()
        self.txt_UserName?.attributedPlaceholder = NSAttributedString(string: "U S E R  N A M E",
                                                                      attributes:[NSForegroundColorAttributeName: UIColor.darkGray])
        
        self.txt_Password?.borderWithRadius(3.0, borderWidth: 0.5, borderColor: UIColor.darkGray)
        self.txt_Password?.tintColor = UIColor.init(hexaString: AppColor.appRedColor.rawValue)
        self.txt_Password?.backgroundColor = UIColor.black
        self.txt_Password?.font = UIFont.fontRegular_medium()
        self.txt_Password?.attributedPlaceholder = NSAttributedString(string: "P A S S  W O R D",
                                                                      attributes:[NSForegroundColorAttributeName: UIColor.darkGray])
        
        self.txt_UserName?.delegate = self
        self.txt_Password?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func keyboardDelegate_WillHideNotification(notification: NSNotification!) {
        //
        self.scrollBack(scrollView: self.myScrollView)
    }
    
    func keyboardDelegate_WillShowNotification(notification: NSNotification!) {
        //
        self.scrollWithKeyboardShow(scrollView: self.myScrollView, viewSelected: self.viewSelected)
    }
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        //
        self.viewSelected = textField
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        //
        self.viewSelected = nil
    }
    
    //MARK: - Request
    func requestLogin(){
        var request = DataRequest(.Network)
        
        
        self.showLoadingProgress()
        request.onSuccess = {(data,  moreInfo) in
            
            if let error = moreInfo as? NSError{
                self.showMessages(messages: error.localizedDescription)
            }
            if !DataProxy.user.isLogout{
                self.delegate?.SignInView_willDissmissView(logginSuccess: DataProxy.user.isLogout == false)
                self.dismiss(animated: true) {
                    self.delegate?.SignInView_didDissmissView(logginSuccess: DataProxy.user.isLogout == false)
                }
            }
        }
        
        request.onFailure = {(error, moreInfo) in
            if error != nil {
                self.showMessages(messages: error!.localizedDescription)
            }
        }
        
        DataProxy.user.login(dataRequest: request, info: self.login_info)
    }
    
    //MARK: - IBAction
    @IBAction func btnFacebookLoginTap(sender : AnyObject){
        let login = FBSDKLoginManager.init()
        login.logIn(withReadPermissions: ["public_profile"], from: self) { (result, error) in
            //
            if (error != nil){
                print("facebook login error")
                
                self.showMessages(messages: error!.localizedDescription)
                
            }else if (result?.isCancelled ?? true){
                self.showMessages(messages: AppMixError.facebook_cancelByUser.rawValue)

            }else {
                print("facebook login success")
                self.login_info.loginType = .facebook
                self.login_info.token = result?.token.tokenString ?? ""
                
                self.requestLogin()
            }
        }
    }
    
    @IBAction func btnCancelLogginTap(sender : AnyObject){
        self.delegate?.SignInView_willDissmissView(logginSuccess: false)
        self.dismiss(animated: true) {
            self.delegate?.SignInView_didDissmissView(logginSuccess: DataProxy.user.isLogout == false)
        }
    }
    
    @IBAction func btnLogginNormalTap(sender : AnyObject){
        self.login_info.loginType = .normal
        self.login_info.user_name = self.txt_UserName?.text ?? ""
        self.login_info.password = self.txt_Password?.text ?? ""
        self.requestLogin()
    }
}
