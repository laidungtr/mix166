//
//  ProfileSongDownloadViewModel.swift
//  Mix166
//
//  Created by ttiamap on 7/26/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

class ProfileSongDownloadViewModel: BaseViewModel{

    override var titleNavigationBar: String?{
        return AppViewTitle.profileMusicOffline.rawValue
    }
    
}
