//
//  ProfileFavouriteController.swift
//  Mix166
//
//  Created by ttiamap on 8/17/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ProfileFavouriteController: SearchResultViewController {
    
    var vmodel_profile: ProfileViewModel = ProfileViewModel.init(title: AppViewTitle.profile.rawValue)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.myScrollView?.isScrollEnabled = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setViewTitle() -> String {
        return AppViewTitle.profileFavorutie.rawValue
    }
    
    override  func setupRequestProductListInfo(){
        
        let mix_info = ProductListInfo.init(_proxy: .user, _type: .mixset, filter: self.vmodel_profile.filterBase, _data: nil,
                                            _paggingTypes: nil, pullRefresh: true)
        let track_info = ProductListInfo.init(_proxy: .user, _type: .track, filter: self.vmodel_profile.filterBase, _data: nil, _paggingTypes: nil,  pullRefresh: true)
        let video_info = ProductListInfo.init(_proxy: .user, _type: .video, filter: self.vmodel_profile.filterBase, _data: nil, _paggingTypes: nil,  pullRefresh: true)
        
        self.paggingTypes       = [.mixset, .track, .video]
        self.productlist_infos  = [mix_info, track_info, video_info]
        
        // setup view mix
        self.viewMix?.isEnablePullRefresh = mix_info.isEnablePullToRefresh
        self.viewMix?.delegate = self
        self.viewMix?.datasource = self
        self.viewMix?.requestWithProductListInfo(_productInfo: mix_info, newRequest: true)
        self.viewMix?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
        
        // setup view track
        self.viewTrack?.isEnablePullRefresh = track_info.isEnablePullToRefresh
        self.viewTrack?.delegate = self
        self.viewTrack?.datasource = self
        self.viewTrack?.requestWithProductListInfo(_productInfo: track_info, newRequest: true)
        self.viewTrack?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
        
        // setup view video
        self.viewVideo?.isEnablePullRefresh = video_info.isEnablePullToRefresh
        self.viewVideo?.delegate = self
        self.viewVideo?.datasource = self
        self.viewVideo?.requestWithProductListInfo(_productInfo: video_info, newRequest: true)
        self.viewVideo?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
    }
    
    override func productListView_shouldDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) -> Bool{
        return true
    }
    
    override func productListView_didDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) {
        //
        if currentDatas != nil && currentDatas!.count > indexPath.row{
            self.baseViewModel_UnLikeObject(object: currentDatas![indexPath.row], callback: { (success, error) in
                if error != nil && error!.code != 0{
                    // fail to delete object
                    // show messages to user
                    self.baseViewModel_showError(error: error!)
                    
                }else{
                    // success to delete object
                    // do nothing
                }
            })
        }
        
    }
}
