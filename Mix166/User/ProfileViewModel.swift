//
//  ProfileViewModel.swift
//  Mix166
//
//  Created by ttiamap on 7/25/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

class ProfileViewModel: BaseViewModel{
    
    internal enum MoreType: String {
        case profile = "Profile"
        case favourite_music = "Favourite"
        case followings = "Following"
        case playlist = "Playlist"
        case recently_add = "Recently added"
        case my_music = "my music"
        case history = "history"
        case logout_login = "log out"
        
        func icon() -> String{
            switch self {
            case .favourite_music:
                return Image_Name.ic_favourite.rawValue
                
            case .followings:
                return Image_Name.ic_followings.rawValue
                
            case .playlist:
                return Image_Name.ic_playlist.rawValue
                
            case .recently_add:
                return Image_Name.ic_download.rawValue
                
            case .history:
                return Image_Name.ic_history.rawValue
                
            case .profile:
                return Image_Name.ic_profile.rawValue
                
            case .logout_login:
                return DataProxy.user.isLogout ? "login" : "log out"
                
            default:
                break
            }
            
            return ""
        }
        
        func title() -> String{
            switch self {
            case .logout_login:
                return DataProxy.user.isLogout ? "login" : "log out"
                
            default:
                return self.rawValue
            }
            
            return ""
        }
    }
    
    internal enum MoreSectionType: String{
        case header_profile = "Profile"
        case online = "Online"
        case offline = "Offline"
        case history = "History"
        case Login  = ""
    }
    
    public struct ItemMore {
        var type: MoreSectionType = .header_profile
        var items: [MoreType] = []
        
    }
    
    //MARK - Property
    
    let arrItem = [//ItemMore.init(type: .header_profile, items: [MoreType.profile]),
                   ItemMore.init(type: .online, items: [MoreType.favourite_music, MoreType.followings, MoreType.playlist]),
                   ItemMore.init(type: .offline, items: [MoreType.my_music]),
                   ItemMore.init(type: .Login, items: [MoreType.logout_login])]
    
    
    override var titleNavigationBar: String?{
        return AppViewTitle.profile.rawValue
    }
    
    
    override func getItemAt(index: Int) -> AnyObject? {
        if index < self.arrItem.count {
            return self.arrItem[index] as AnyObject
        }
        return nil
    }
    
    func getItemMoreTypeAtIndexPath(indexPath: IndexPath) -> MoreType?{
        
        if let itemParent = self.getItemAt(index: indexPath.section) as? ItemMore{
            if itemParent.items.count > indexPath.row{
                return itemParent.items[indexPath.row]
            }
        }
        return nil
    }
    
    //MARK:- Request
    
//    func removeObjectFromFavourite(object: AnyObject, callback:@escaping (_ isSuccess: Bool, _ error: NSError?)->()) {
//        
//        let request = DataRequest(.Network)
//        
//        request.onSuccess = {(data,  moreInfo) in
//            
//            if error != nil && error!.code == 0{
//                // success
//                self.delegate.baseViewModel_showError(error: error!)
//                
//            }else{
//                
//            }
//            
//            if let error = moreInfo as? NSError, error.code != 0{
//                self.delegate?.baseViewModel_showError(error: error)
//            }else{
//                self.delegate?.baseViewModel_showError(error: error)
//            }
//        }
//        
//        request.onFailure = {(error, moreInfo) in
//            callback(false, error)
//            
//            if error != nil {
//                self.delegate?.baseViewModel_showError(error: error!)
//            }
//        };
//        
//        DataProxy.user.activities(is_cancel_status: true, object: object, dataRequest: request)
//    }
    
}
    
