//
//  ProfileSongDownloadController.swift
//  Mix166
//
//  Created by ttiamap on 7/26/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ProfileSongDownloadController: SearchResultViewController {
    
    @IBOutlet private weak var viewAll: ProductListView?
    
    var _vmodel: ProfileSongDownloadViewModel = ProfileSongDownloadViewModel.init(title:AppViewTitle.profileMusicOffline.rawValue)
    
    private var currentIndex = 0
    
    override var isNavigationBarHiddenWhenSwipe: Bool{
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.myScrollView?.isScrollEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setViewTitle() -> String {
        return self._vmodel.titleNavigationBar ?? ""
    }
    
    
    override func setupRequestProductListInfo(){
        let mix_info = ProductListInfo.init(_proxy: .download, _type: .mixset, filter: self._vmodel.filterBase, _data: nil, _paggingTypes: nil, pullRefresh: false)
        let track_info = ProductListInfo.init(_proxy: .download, _type: .track, filter: self._vmodel.filterBase, _data: nil, _paggingTypes: nil, pullRefresh: false)
        let all_info = ProductListInfo.init(_proxy: .download, _type: .all, filter: self._vmodel.filterBase, _data: nil, _paggingTypes: nil, pullRefresh: false)
        
        self.paggingTypes       = [.all, .mixset, .track]
        self.productlist_infos  = [all_info, mix_info, track_info]
        
        // setup view all
        self.viewAll?.isEnablePullRefresh = all_info.isEnablePullToRefresh
        self.viewAll?.delegate = self
        self.viewAll?.datasource = self
        self.viewAll?.requestWithProductListInfo(_productInfo: all_info, newRequest: true)
        self.viewAll?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
        
        // setup view mix
        self.viewMix?.isEnablePullRefresh = mix_info.isEnablePullToRefresh
        self.viewMix?.delegate = self
        self.viewMix?.datasource = self
        self.viewMix?.requestWithProductListInfo(_productInfo: mix_info, newRequest: true)
        self.viewMix?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
        
        // setup view track
        self.viewTrack?.isEnablePullRefresh = track_info.isEnablePullToRefresh
        self.viewTrack?.delegate = self
        self.viewTrack?.datasource = self
        self.viewTrack?.requestWithProductListInfo(_productInfo: track_info, newRequest: true)
        self.viewTrack?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
    }
    
    override func productListView_shouldDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) -> Bool {
        return true
    }
    
    override func slideMenu_DidSelectedItemAtIndex(index: NSInteger) {
        
        if !self.isUserDragging{
            self.isUserDragging = false
            self.isScrollingManually = true
        }
        
        if currentIndex == index{
            return
        }
        
        let xOffset = CGFloat(index) * screenWidth
        if index == 0{
            self.viewAll?.requestWithProductListInfo(_productInfo: (self.viewAll?.productInfo)!, newRequest: true)
        }else if index == 1{
            self.viewMix?.requestWithProductListInfo(_productInfo: (self.viewMix?.productInfo)!, newRequest: true)
        }else{
            self.viewTrack?.requestWithProductListInfo(_productInfo: (self.viewTrack?.productInfo)!, newRequest: true)
        }
        
        self.myScrollView?.setContentOffset(CGPoint.init(x: xOffset, y: 0), animated: true)
        
        self.currentIndex = index
    }
}
