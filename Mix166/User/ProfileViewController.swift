//
//  ProfileViewController.swift
//  Mix166
//
//  Created by ttiamap on 7/18/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var myCollectionView: UICollectionView?
    
    private var signInViewWillDismissCall: Bool = false
    private var signInVc: SignInViewController?
    
    var vmodel: ProfileViewModel = ProfileViewModel.init(title: AppViewTitle.profile.rawValue)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override var isNavigationBarHidden: Bool{
        return true
    }
    
    override var isNavigationBarHiddenWhenSwipe: Bool{
        return false
    }
    
    override func setViewTitle() -> String {
        return self.vmodel.titleNavigationBar ?? ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.myCollectionView?.delegate == nil{
            self.registerCell()
        }
//        self.handleSignInViewControllerShowing()
        var topPadding: CGFloat{
            let totalSectionHeight: CGFloat = CGFloat(self.vmodel.arrItem.count) * headerTitleProfileSize.height
            var totalNumberItemHeight: CGFloat = 0
            for item in self.vmodel.arrItem {
                totalNumberItemHeight += CGFloat(item.items.count) * profileMoreItemSize.height
            }
            
            return self.view.frame.size.height - (totalSectionHeight + totalNumberItemHeight)
        }
        
        if SharePlayer.isShowingPlayer_ControlSmall{
            self.myCollectionView?.contentInset = UIEdgeInsetsMake(topPadding - playerControlHeight, 0, playerControlHeight, 0)
        }else{
            self.myCollectionView?.contentInset = UIEdgeInsetsMake(topPadding - paddingLeftRight, 0, paddingLeftRight, 0)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func handleSignInViewControllerShowing(){
        if DataProxy.user.isLogout && !self.signInViewWillDismissCall{
            self.signInVc = self.presentSignInViewController()
            self.signInViewWillDismissCall = false
            
        }else if self.signInVc != nil{
            self.signInVc?.dismiss(animated: false, completion: {
                //
            })
            
        }else{
            // loggin success
            if self.myCollectionView?.delegate == nil{
                self.registerCell()
            }else{
                self.myCollectionView?.reloadData()
            }
        }
    }
    
    override func SignInView_willDissmissView(logginSuccess status: Bool) {
        self.signInViewWillDismissCall = true
        //
        if status{
            // loggin success
            self.signInVc = nil
            
            self.registerCell()
            
        }else{
            // login cancel
            // push back to home
//            self.tabBarController?.selectedIndex = 0
        }
    }
    
    func userlogout(){
        self.SignInView_willDissmissView(logginSuccess: false)
        self.SignInView_didDissmissView(logginSuccess: false)
    }
    
    override func SignInView_didDissmissView(logginSuccess status: Bool) {
        self.signInViewWillDismissCall = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK: - Register cell
    private func registerCell(){
        
        if self.myCollectionView?.delegate == nil {
            
            self.vmodel.registerCell(collectionView: self.myCollectionView, callback: { 
                self.myCollectionView?.dataSource = self
                self.myCollectionView?.delegate = self
            })
            
        }else{
            self.myCollectionView?.reloadData()
        }
    }
    
    //MARK: - Header
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if let item = self.vmodel.getItemAt(index:section) as? ProfileViewModel.ItemMore{
            switch item.type {
            case .header_profile:
                //
                return sizeZero
                
            default:
                return headerTitleProfileSize
            }
        }
        return sizeZero
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        //
        if kind == UICollectionElementKindSectionHeader{
            return self.vmodel.configureHeaderTitleProfileCell(collectionView: collectionView, viewForSupplementaryElementOfKind: kind, indexPath:indexPath)
        }
        
        return UICollectionReusableView.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: 0))
    }
    
    //MARK: - CollectionView Delegate - Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.vmodel.arrItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let item = self.vmodel.getItemAt(index: section) as? ProfileViewModel.ItemMore{
            return item.items.count
        }
        return 0
    }
    
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell = UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        if let item = self.vmodel.getItemAt(index: indexPath.section) as? ProfileViewModel.ItemMore{
            switch item.type {
            case .header_profile:
                //
                cell = self.vmodel.configureProfileUserCell(collectionView:collectionView, indexPath:indexPath)
                
            default:
                cell = self.vmodel.configureProfileMoreItemCell(collectionView:collectionView, indexPath:indexPath)
                break
            }
        }
        //
        //        if indexPath.section == 0{
        //            if let _cell = collectionView.dequeueReusableCell(withReuseIdentifier:SongListCollectionCell2.nameOfClass, for: indexPath) as? SongListCollectionCell2{
        //                if let _song = self.vmodel.getCurrentPlayingSong(){
        //                    _cell.configureCellWith(_song: _song)
        //                    _cell.cell_btnMore?.isHidden = true
        //                }
        //                cell = _cell
        //            }
        //
        //        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        if let moreType = self.vmodel.getItemMoreTypeAtIndexPath(indexPath: indexPath){
            
            switch moreType {
            case .playlist:
                if DataProxy.user.isLogout{
                    self.handleSignInViewControllerShowing()
                }else{
                    self.pushToPlaylistViewController(itemSong: nil)
                }
                break
                
            case .favourite_music:
                //
                if DataProxy.user.isLogout{
                    self.handleSignInViewControllerShowing()
                }else{
                    self.pushToProfileFavouriteController()
                }
                break
                
            case .followings:
                //
                if DataProxy.user.isLogout{
                    self.handleSignInViewControllerShowing()
                }else{
                    let info = ProductListInfo.init(_proxy: .user, _type: .deejay_vertical, filter: self.vmodel.filterBase, _data: nil, _paggingTypes: nil, pullRefresh: true)
                    self.pushToArtistListViewController(info: info)
                }
                break
                
            case .history:
                //
                break
                
            case .profile:
                //
                
                break
                
            case .logout_login:
                //
                
                if DataProxy.user.isLogout{
                    self.handleSignInViewControllerShowing()
                }else{
                    let alert = UIAlertController(title: "", message: "Are you sure you want to log out?", preferredStyle: .actionSheet)
                    alert.view.tintColor = UIColor.init(hexaString: AppColor.appRedColor.rawValue)
                    alert.addAction(UIAlertAction(title: "Log out", style: .default) { action in
                        // perhaps use action.title here
                        DataProxy.user.logout()
                        self.userlogout()
                        self.registerCell()
                    })
                    
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
                        // perhaps use action.title here
                    })
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
                break
                
            case .my_music:
                //
                self.pushToProfileSongDownloadController()
                break
                
            default:
                break
            }
        }
        
        return true
    }
    
    // Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if let item = self.vmodel.getItemAt(index: indexPath.section) as? ProfileViewModel.ItemMore{
            switch item.type {
            case .header_profile:
                //
                return profileUserSize
                
            default:
                return profileMoreItemSize
            }
        }
        
        return profileMoreItemSize
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        
        //        if let item = self.vmodel.getItemAt(index: section) as? ProfileViewModel.ItemMore{
        //            switch item.type {
        //            case .header_profile:
        //                //
        //                return UIEdgeInsetsMake(paddingLeftRight, 0, paddingLeftRight, 0)
        //
        //            default:
        //                break
        //            }
        //        }
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        if let item = self.vmodel.getItemAt(index: section) as? ProfileViewModel.ItemMore{
            switch item.type {
            case .header_profile:
                //
                return paddingBetweenItem * 3
                
            default:
                break
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        //        if section == 0{
        //            return paddingBetweenItem * 20
        //        }
        return 0
    }
    
}
