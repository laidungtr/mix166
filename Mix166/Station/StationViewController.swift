//
//  StationViewController.swift
//  Mix166
//
//  Created by ttiamap on 7/31/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class StationViewController: BaseViewController,ProductListViewDelegate, ProductListViewDataSource {
    
    let vmodel : StationViewModel = StationViewModel.init(title: AppViewTitle.station.rawValue)
    var product_info: ProductListInfo?
    
    @IBOutlet weak var viewProductList: ProductListView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.setupProductListView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPaths = self.viewProductList?.getVisibleIndexPathForVisibleSupplementaryElements(kind: UICollectionElementKindSectionHeader){
            
            for indexPath in indexPaths {
                self.viewProductList?.reloadItemAtSection(section: indexPath.section)
            }
        }
    }
    
    override var isNavigationBarHidden: Bool{
        return true
    }
    
    override func setViewTitle() -> String {
        return self.vmodel.titleNavigationBar ?? ""
    }
    
    func setupProductListView(){
        self.product_info = ProductListInfo.init(_proxy: ProductListInfo.RequestProxy.station, _type: .station, filter: self.vmodel.filterBase, _data: nil, _paggingTypes: nil, pullRefresh: true)
        if self.product_info != nil {
            self.viewProductList?.isEnablePullRefresh = self.product_info!.isEnablePullToRefresh
            self.viewProductList?.delegate = self
            self.viewProductList?.datasource = self
            self.viewProductList?.setProductListBackgroundColor(color: UIColor.black)
            self.viewProductList?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
            self.viewProductList?.requestWithProductListInfo(_productInfo: self.product_info!, newRequest: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK - ProductList Datasource
    func productListView_spacingBetweenItem() -> CGFloat{
        return 10.0
    }
    
    func productListView_scrollDirection() -> UICollectionViewScrollDirection{
        return .vertical
    }
    
    func productListView_contentInsect() -> UIEdgeInsets{
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    //MARK - ProductList Delegate
    
    func productListView_BtnShuffleSongDidTap(datas: [AnyObject]?, type: SongType) {
        //
    }
    
    
    func productListView_shouldDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) -> Bool{
        return false
    }
    
    func productListView_didDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) {
        //
    }
    
    func productListView_didSelectedAtIndexPath(indexPath: IndexPath, withData Datas: [AnyObject]?, type: SongType) {
        if let _videos = Datas as? [Video]{
            self.presentVideoViewController(_videos: _videos, playingIndex: indexPath.row)
            
        }else if let _songs = Datas as? [Song]{
            self.presentToPlayerViewController(_songs: _songs, playingIndex: indexPath.row,type:  type, shuffleEnable: false, isFromViewTracklisting: true)
            
        }else if let _artist = Datas as? [Artist], _artist.count > indexPath.row{
            self.pushToArtistDetailViewController(artist_info: _artist[indexPath.row])
        }
    }
    
    func productListView_MoreTapAtIndexPath(index: Int, withData Datas: [AnyObject]?) {
        if let items = Datas as? [Song]{
            let itemViewModel = PlayerViewModel.init(title: AppViewTitle.more.rawValue)
            itemViewModel.songs = items
            itemViewModel.setPlayingIndex(index: index, isFromPlayerTracklisting: false)
            
            self.presentMoreViewController(vmodel: itemViewModel, itemSongSelected: nil)
        }
    }
    
    func productListView_HeaderTitleDidTapButtonMore(productInfo: ProductListInfo) {
        
        switch productInfo.type {
        case .deejay_vertical, .deejay_horizontal:
            return self.pushToArtistListViewController(info: productInfo)
            
        case .mixset, .track, .trackPlayer, .video:
            return self.pushToProductListViewController(info: productInfo, vmodel: nil)
        default:
            break
        }
    }
    
    func productListView_BtnAutoPlayVideoDidChange(isOn: Bool) {
        //
    }
    
}
