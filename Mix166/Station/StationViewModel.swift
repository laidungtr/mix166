//
//  StationViewModel.swift
//  Mix166
//
//  Created by ttiamap on 7/31/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation


class StationViewModel: BaseViewModel {

    override var titleNavigationBar: String?{
        return AppViewTitle.station.rawValue
    }
    
}
