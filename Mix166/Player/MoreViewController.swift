//
//  MoreViewController.swift
//  Mix166
//
//  Created by ttiamap on 6/23/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class MoreViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    internal enum MoreType: String {
        case sync = "Sync"
        case favourite = "Add to Favourite"
        case addToPlaylist = "Add to Playlist"
        case addToQueue = "Add to Queue"
        case goToQueue = "Go to Queue"
        case share = "Share"
    }
    
    struct ItemMore {
        var type: MoreType = .sync
        var icon_imageStr: String = ""
        
    }
    
    var selectedSong: Song?
    private var signInViewWillDismissCall: Bool = false
    private var signInVc: SignInViewController?
    
    var vmodel = PlayerViewModel.init(title: AppViewTitle.more.rawValue){
        didSet{
            self.selectedSong = self.vmodel.getCurrentPlayingSong()
        }
    }
    
    @IBOutlet weak var myCollectionView: UICollectionView?
    
    var arrItem = [ItemMore.init(type: .sync, icon_imageStr: Image_Name.ic_download.rawValue),
                   ItemMore.init(type: .favourite, icon_imageStr: Image_Name.ic_favourite_white.rawValue),
                    ItemMore.init(type: .addToPlaylist, icon_imageStr: Image_Name.ic_addToPlaylist.rawValue),
//                    ItemMore.init(type: .addToQueue, icon_imageStr: Image_Name.ic_addToQueue.rawValue),
                    ItemMore.init(type: .share, icon_imageStr: Image_Name.ic_share.rawValue)]
    
    let heightItemCell: CGFloat = 50.0
    let heightItemSongCell: CGFloat = 65.0
    let heightButtonCancel: CGFloat = 50.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (self.selectedSong as? Video) != nil{
            arrItem = [ItemMore.init(type: .favourite, icon_imageStr: Image_Name.ic_favourite_white.rawValue),
                       ItemMore.init(type: .share, icon_imageStr: Image_Name.ic_share.rawValue)]
        }else{
            arrItem = [ItemMore.init(type: .sync, icon_imageStr: Image_Name.ic_download.rawValue),
                       ItemMore.init(type: .favourite, icon_imageStr: Image_Name.ic_favourite_white.rawValue),
                       ItemMore.init(type: .addToPlaylist, icon_imageStr: Image_Name.ic_addToPlaylist.rawValue),
                       //                    ItemMore.init(type: .addToQueue, icon_imageStr: Image_Name.ic_addToQueue.rawValue),
                ItemMore.init(type: .share, icon_imageStr: Image_Name.ic_share.rawValue)]
        }
        
        self.registerCell()
        
    }
    
    override var isNavigationBarHidden: Bool{
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Register cell
    private func registerCell(){
        
        if self.myCollectionView?.delegate == nil {
            let cellNib = UINib.init(nibName: MoreItemCollectionCell.nameOfClass, bundle: nil)
            let cellNib2 = UINib.init(nibName: SongListCollectionCell2.nameOfClass, bundle: nil)
            
            self.myCollectionView?.register(cellNib, forCellWithReuseIdentifier: MoreItemCollectionCell.nameOfClass)
            self.myCollectionView?.register(cellNib2, forCellWithReuseIdentifier: SongListCollectionCell2.nameOfClass)
            
            self.myCollectionView?.dataSource = self
            self.myCollectionView?.delegate = self
        }else{
            self.myCollectionView?.reloadData()
        }
    }
    
    //MARK: - CollectionView Delegate - Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            if self.vmodel.getCurrentPlayingSong() != nil{
                return 1
            }else{
                return 0
            }
        }
        return self.arrItem.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell = UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        if indexPath.section == 0{
            if let _cell = collectionView.dequeueReusableCell(withReuseIdentifier:SongListCollectionCell2.nameOfClass, for: indexPath) as? SongListCollectionCell2{
                if let _song = self.vmodel.getCurrentPlayingSong(){
                    _cell.configureCellWith(_song: _song)
                    _cell.cell_viewContain?.backgroundColor = .clear
                    _cell.cell_btnMore?.isHidden = true
                }
                cell = _cell
            }
            
        }else{
            if let _cell = collectionView.dequeueReusableCell(withReuseIdentifier:MoreItemCollectionCell.nameOfClass, for: indexPath) as? MoreItemCollectionCell{
                
                if indexPath.row < self.arrItem.count{
                    let item = self.arrItem[indexPath.row]
                    _cell.configureCell(str_iconName: item.icon_imageStr, str_title: item.type.rawValue)
                }
                
                cell = _cell
            }
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        if indexPath.section == 0{
            
        }else{
            let itemMore = self.arrItem[indexPath.row]
            
            switch itemMore.type {
            case .sync:
                //downloadfixme
                
                let itemSong = self.vmodel.getCurrentPlayingSong()
                
                
                var type : SongType = .Track
                
                if (itemSong as? Mix) != nil{
                    type = .Mix
                }
                
                
                DataProxy.download.startDownload(itemSong: itemSong, type: type)
                
                DataProxy.download.completeBlock = { (str_location, downloadInfo) in
                    // download success
                    itemSong?.updatePlayingOfflineLink(str_location: str_location)
                    
                    DataProxy.download.updateSongDownloadToDB(itemSong: itemSong, type: type, successBlock: { 

                        if let error = HttpUtility.makeNSerror(errorCode: 0, message: "\(itemSong!.name) syncing success"){
                            self.baseViewModel_showError(error: error)
                        }
                    }, errorBlock: { (str_error) in
                        
                        if let error = HttpUtility.makeNSerror(errorCode: -1, message: str_error){
                            self.baseViewModel_showError(error: error)
                        }
                    })
                }
                
                DataProxy.download.failBlock = { (str_error, moreInfo) in
                    if let error = HttpUtility.makeNSerror(errorCode: -1, message: str_error){
                        self.baseViewModel_showError(error: error)
                    }
                    print("song download update error \(str_error)")
                }
                
                break
                
            case .addToPlaylist:
                if DataProxy.user.isLogout{
                    self.handleSignInViewControllerShowing()
                }else{
                    if self.selectedSong != nil{
                        self.presentToPlaylistViewController(itemSong: [self.selectedSong!])
                    }
                }
                break
                
            case .favourite:
                if DataProxy.user.isLogout{
                    self.handleSignInViewControllerShowing()
                }else{
                    if self.selectedSong != nil{
                        self.baseViewModel_LikeObject(sender: nil, object: self.selectedSong!, callback: { (success, error) in
                            
                            if error != nil && error!.code == 0{
                                // success
                                self.baseViewModel_showError(error: error!)
                                
                            }else{
                                
                            }
                        })
                    }
                }
                
            case .addToQueue:
                //
                break
                
            case .goToQueue:
                //
                break
                
            case .share:
                //
                if self.selectedSong != nil{
                    self.shareSocialWithLink(str_link: self.selectedSong?.linkShare ?? "")
                }
                break
                
            }
        }

        return true
    }
    
    func handleSignInViewControllerShowing(){
        if DataProxy.user.isLogout && !self.signInViewWillDismissCall{
            self.signInVc = self.presentSignInViewController()
            self.signInViewWillDismissCall = false
            
        }else if self.signInVc != nil{
            self.signInVc?.dismiss(animated: false, completion: {
                //
            })
            
        }else{
            // loggin success
            if self.myCollectionView?.delegate == nil{
                self.registerCell()
            }else{
                self.myCollectionView?.reloadData()
            }
        }
    }
    
    // Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0{
            return CGSize.init(width: screenWidth, height: heightItemSongCell)
        }
        return CGSize.init(width: screenWidth, height: heightItemCell)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        
        let songHeight = ((self.vmodel.getCurrentPlayingSong() != nil) ? heightItemSongCell : 0)
        let sectionItemHeight = CGFloat(self.arrItem.count + 1) * heightItemCell
        
        var topInsect = self.view.frame.size.height - (songHeight + sectionItemHeight + heightButtonCancel)
        
        if topInsect < playerControlHeight {
            topInsect = playerControlHeight
            
            self.myCollectionView?.isScrollEnabled = true
        }else{
            self.myCollectionView?.isScrollEnabled = false
        }
        
        if section == 0{
            return UIEdgeInsetsMake(topInsect,paddingBetweenItem , 0, 0)
        }
        return UIEdgeInsetsMake(paddingBetweenItem * 3, paddingBetweenItem, 0, 0)
    }
    
//        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
//            if section == 0{
//                return paddingBetweenItem * 20
//            }
//            return 0
//        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
//        if section == 0{
//            return paddingBetweenItem * 20
//        }
        return 0
        
    }
    
    //MARK: - IBAction
    @IBAction func btnCancelTap(sender : UIButton){
        self.hideMoreController()
    }
}
