//
//  PlayerViewTrackListing.swift
//  Mix166
//
//  Created by ttiamap on 6/22/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class PlayerViewTrackListing: BaseViewController, ProductListViewDelegate, ProductListViewDataSource {
    
    @IBOutlet weak var viewProductList: ProductListView?
    @IBOutlet weak var btn_back: UIButton?
    @IBOutlet weak var lb_title: UILabel?
    
    var productlist_info: ProductListInfo?
    var vmodel: PlayerViewModel!{
        didSet{
            if self.vmodel.songs.count > 0{
                
                if let song = self.vmodel.getCurrentPlayingSong(), song.type == .Track{
                    self.productlist_info = ProductListInfo.init(_proxy: .automatically, _type: .trackPlayer, filter: nil, _data: self.vmodel.songs, _paggingTypes: nil, pullRefresh: false)
                    
                }else{
                    
                    if let _currentSong = self.vmodel.getCurrentPlayingSong() as? Mix{
                        self.productlist_info = ProductListInfo.init(_proxy: .automatically, _type: .trackPlayerMasup, filter: nil, _data: _currentSong.songs_masup, _paggingTypes: nil, pullRefresh: false)
                    }
                }
                self.setupProductListView()
            }
        }
    }
    
    override func setNavigationBarStyle() -> NavigationBarStyle {
        return .Transparent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupUI()
        self.setupProductListView()
    }
    
    func setupUI(){
        self.lb_title?.font = UIFont.fontRegular_large()
        self.lb_title?.textColor = UIColor.white
        self.lb_title?.text = "Tracks Listing"
    }
    
    func setupProductListView(){
        if self.productlist_info != nil {
            if self.viewProductList?.delegate == nil {
                self.viewProductList?.delegate = self
                self.viewProductList?.datasource = self
                self.viewProductList?.isEnablePullRefresh = self.productlist_info!.isEnablePullToRefresh
                self.viewProductList?.setProductListBackgroundColor(color: UIColor.clear)
                self.viewProductList?.requestWithProductListInfo(_productInfo: self.productlist_info!, newRequest: true)
            }else{
                self.viewProductList?.requestWithProductListInfo(_productInfo: self.productlist_info!, newRequest: true)
            }
        }
    }
    
    //MARK - ProductList Datasource
    func productListView_spacingBetweenItem() -> CGFloat{
        return 10.0
    }
    
    func productListView_scrollDirection() -> UICollectionViewScrollDirection{
        return .vertical
    }
    
    func productListView_contentInsect() -> UIEdgeInsets{
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    //MARK - ProductList Delegate
    
    func productListView_BtnShuffleSongDidTap(datas: [AnyObject]?, type: SongType){
        
    }
    
    func productListView_shouldDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) -> Bool{
        if self.vmodel.getPlayingIndex() == indexPath.row{
            
            // the music is playing, cant delete it
            // show messages to user
            if let error = HttpUtility.makeNSerror(errorCode: -1, message: AppMixError.delete_songPlayingError.rawValue){
                self.baseViewModel_showError(error: error)
            }
            return false
            
        }else{
            return true
        }
    }
    
    func productListView_didDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) {
        //
        self.vmodel.songs = currentDatas as? [Song] ?? []
        
    }
    
    func productListView_didSelectedAtIndexPath(indexPath: IndexPath, withData Datas: [AnyObject]?, type: SongType) {
        //
        if let _videos = Datas as? [Video]{
            self.presentVideoViewController(_videos: _videos, playingIndex: indexPath.row)
            
        }else if let _songs = Datas as? [Song]{
            self.presentToPlayerViewController(_songs: _songs, playingIndex: indexPath.row,type:  type, shuffleEnable: false, isFromViewTracklisting: true)
            
        }else if let _artist = Datas as? [Artist], _artist.count > indexPath.row{
            self.pushToArtistDetailViewController(artist_info: _artist[indexPath.row])
        }
        
        
        if self.btn_back != nil {
            self.btnBackTap(sender: self.btn_back!)
        }
    }
    
    func productListView_MoreTapAtIndexPath(index: Int, withData Datas: [AnyObject]?) {
        
        var songType: SongType = .Track
        
        if Datas is [Mix]{
            songType = .Mix
        }
        
        if let items = Datas as? [Song]{
            let itemViewModel = PlayerViewModel.init(title: AppViewTitle.more.rawValue)
            itemViewModel.type = songType
            itemViewModel.songs = items
            itemViewModel.setPlayingIndex(index: index, isFromPlayerTracklisting: true)
            
            self.presentMoreViewController(vmodel: itemViewModel, itemSongSelected: nil)
        }
    }
    
    func productListView_HeaderTitleDidTapButtonMore(productInfo: ProductListInfo) {
        
        switch productInfo.type {
        case .deejay_vertical, .deejay_horizontal:
            return self.pushToArtistListViewController(info: productInfo)
            
        case .mixset, .track, .trackPlayer, .video:
            return self.pushToProductListViewController(info: productInfo, vmodel: nil)
        default:
            break
        }
    }
    
    func productListView_BtnAutoPlayVideoDidChange(isOn: Bool) {
        //
    }
    
    func scrollToCurrentPlayingCell(){
        
    }
    
    // IBAction
    @IBAction func btnBackTap(sender : AnyObject){
        self.hidePlayerViewTrackListingController()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
