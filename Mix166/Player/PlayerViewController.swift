//
//  PlayerViewController.swift
//  Mix166
//
//  Created by ttiamap on 6/14/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit
import SDWebImage

class PlayerViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UIScrollViewDelegate, PlayerViewModelDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var myCollectionView: UICollectionView!
    @IBOutlet weak var img_background: UIImageView?
    @IBOutlet weak var viewPlayerControlTop: UIView!
    @IBOutlet weak var view_imageTopShadown: UIView?
    @IBOutlet weak var img_top: UIImageView?
    @IBOutlet weak var lb_titleTop: UILabel!
    @IBOutlet weak var viewGradient: UIView?
    @IBOutlet weak var viewGradient2: UIView?
    
    @IBOutlet weak var lb_title: UILabel!
    @IBOutlet weak var lb_artist: UILabel!
    @IBOutlet weak var lb_duration: UILabel!
    @IBOutlet weak var lb_durationCount: UILabel!
    
    @IBOutlet weak var btn_Dismiss: UIButton!
    @IBOutlet weak var btn_playlist: UIButton!
    @IBOutlet weak var btn_add: UIButton!
    @IBOutlet weak var btn_more: UIButton!
    @IBOutlet weak var btn_shuffle: UIButton!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var btn_play: UIButton!
    @IBOutlet weak var btn_playTop: UIButton!
    @IBOutlet weak var btn_next: UIButton!
    @IBOutlet weak var btn_repeat: UIButton!
    
    @IBOutlet weak var slider: PlayerSlider!
    
    private var observerImageContext = 0
    private var observerTitleContext = 1
    
    
    let vmodel: PlayerViewModel = PlayerViewModel(title: "Mix166")
    var direction: panDirection = .unknow
    var storeMovingPoint = CGPoint.init(x: 0, y: 0)
    var isKvoInit = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.setupFristload()
        self.setupUI()
        self.setupTopPlayerControl()
        self.kvo_init()
        
        if self.vmodel.songs.count > 0{
            self.setupFristload()
            self.collectionViewCellRegister()
        }
        
        if self.vmodel.delegate_player == nil {
            self.vmodel.delegate_player = self
            self.vmodel.isPlaying = true
        }
    }
    
    func setupDelegate(){
        if self.vmodel.delegate_player == nil {
            self.vmodel.delegate_player = self
            self.vmodel.isPlaying = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollToCurrentPlayingCell(isFromTracklisting: Bool){
        
        if isFromTracklisting{
            let currentIndex = self.vmodel.getScrollingIndex()
            if self.vmodel.songs.count > currentIndex{
                self.myCollectionView.scrollToItem(at: IndexPath.init(row: currentIndex, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
            }
        }else{
            let currentIndex = self.vmodel.getPlayingIndex()
            if self.vmodel.songs.count > currentIndex{
                self.myCollectionView.scrollToItem(at: IndexPath.init(row: currentIndex, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
            }
        }
    }
    
    func setupUI(){
        
        self.lb_title.font = UIFont.fontSemiBold_extraBig()
        self.lb_artist.font = UIFont.fontRegular_small()
        self.lb_duration.font = UIFont.fontLight_small()
        self.lb_durationCount.font = UIFont.fontLight_small()
        
        self.lb_title.textColor = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
        self.lb_artist.textColor = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue).withAlphaComponent(0.8)
        self.lb_duration.textColor = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue).withAlphaComponent(0.8)
        self.lb_durationCount.textColor = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue).withAlphaComponent(0.8)
        
        self.slider.maximumTrackTintColor = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue)
        self.slider.minimumTrackTintColor = UIColor.white
        self.slider.setThumbImage(UIImage.init(named: "ic_seekbar"), for: UIControlState.normal)
    }
    
    func setupTopPlayerControl(){
        self.lb_titleTop.textColor = UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue)
        self.lb_titleTop.font = UIFont.fontRegular_big()
        self.img_top?.borderWithRadius(2.0)
        //        self.view_imageTopShadown?.shadownMake(UIColor.darkGray, height: 4.0, width: -4.0, radius: 4.0)
    }
    
    //MARK: KVO
    func kvo_init(){
        if !self.isKvoInit{
            self.lb_title.addObserver(self, forKeyPath: "text", options: [.old, .new], context: &self.observerTitleContext)
            self.img_background?.addObserver(self, forKeyPath: "image", options: .new, context: &self.observerImageContext)
            self.isKvoInit = true
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "text" && context == &self.observerTitleContext {
            if let str = change?[.newKey] as? String{
                self.lb_titleTop.text = str
            }
            //            print("old:", change?[.oldKey])
            //            print("new:", change?[.newKey])
        }
        
        if keyPath == "image" && context == &self.observerImageContext {
            print("image observer call ================")
            if let _image = change?[.newKey] as? UIImage{
                self.img_top?.image = _image
            }
        }
    }
    
    deinit {
        print("deinit calling")
        self.lb_title?.removeObserver(self, forKeyPath: "text")
        self.img_background?.removeObserver(self, forKeyPath: "image", context: &self.observerImageContext)
    }
    
    func setAppBackgroundInfomation(){
        if let imageTop = self.img_top?.image{
            if let currentSong = self.vmodel.getCurrentPlayingSong(){
                DLTPlayer.setBackgroundViewPlayer(image: imageTop, song_name: currentSong.name, artist_name: currentSong.all_artistName, genres_name: currentSong.all_genresName)
            }
        }
    }
    
    
    //MARK: PlayerViewModel Delegate
    func playerDelegate_updateBackgroundInfo() {
        if let currentSong = self.vmodel.getCurrentPlayingSong(){
            if self.img_background?.image != nil {
            DLTPlayer.setBackgroundViewPlayer(image: self.img_background!.image!, song_name: currentSong.name, artist_name: currentSong.all_artistName, genres_name: currentSong.all_genresName)
            }
        }
    }
    
    func playerDelegate_didChangeSongList(_song: [Song]) {
        //
        print("playerDelegate_didChangeSongList")
        self.myCollectionView.reloadData()
        self.updateImageBackground()
    }
    
    func playerDelegate_seekToValue(value: CGFloat) {
        print("player slider seek to value: \(value)")
        self.vmodel.seekTo(value: Float(value))
    }
    
    
    func playerDelegate_willPlayingSong(_song: Song, playingStatus isPlaying: Bool, callBack: (() -> ())?) {
        print("playerDelegate_willPlayingSong")
        
        self.btn_play.isSelected = isPlaying
        self.btn_playTop.isSelected = isPlaying
        self.lb_duration.text = _song.mp3.duration
        self.lb_title.text = _song.name
        self.lb_artist.text = _song.all_artistName
        
        self.slider.value = 0
        
        self.updateImageBackgroundWith(_song: _song)
        
        if callBack != nil{
            DLTPlayer.stop(callback: callBack!)
        }
        
    }
    
    func playerDelegate_shouldUpdate(indexPaths: [IndexPath]) {
        
        self.myCollectionView.collectionViewLayout.invalidateLayout()
        self.myCollectionView.performBatchUpdates({ 
            self.myCollectionView.reloadItems(at: indexPaths)
        }) { (finish) in
            if let playingIndexPath = indexPaths.last{
                self.myCollectionView.scrollToItem(at: playingIndexPath, at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    func playerDidChangeShuffleStyle(isShuffle: Bool) {
        print("playerDidChangeShuffleStyle")
        self.btn_shuffle.isSelected = isShuffle
    }
    
    func playerDidChangeRepeatStyle(style: RepeatStyle) {
        switch style {
        case .none:
            //
            self.btn_repeat.setImage(UIImage.init(named: Image_Name.img_repeat.rawValue), for: .normal)
            break
            
        case .repeatAll:
            //
            self.btn_repeat.setImage(UIImage.init(named: Image_Name.img_repeatSelected.rawValue), for: .normal)
            break
            
        case .repeatOne:
            //
            self.btn_repeat.setImage(UIImage.init(named: Image_Name.img_repeatOne.rawValue), for: .normal)
            break
            
        }
    }
    
    
    func playerDidUpdateDurationCountDown(str_duration: String, sliderValue: Float) {
        self.lb_durationCount.text = str_duration
        
        print(sliderValue)
        self.slider.value = sliderValue
    }
    
    // setup
    func setupFristload(){
        
        var frame = UIScreen.main.bounds
        frame.size.height += playerControlHeight
        
        self.viewGradient?.gradientFromTopToBottom(UIColor.black.withAlphaComponent(0.1).cgColor, bottomColor: UIColor.black.withAlphaComponent(0.8).cgColor, gradientFrame: frame)
//        self.viewGradient2?.gradientFromTopToBottom(UIColor.black.withAlphaComponent(0.1).cgColor, bottomColor: UIColor.black.cgColor, gradientFrame: frame)
        self.updateImageBackground()
        
    }
    
    func updateImageBackgroundWith(_song: Song){
        
        if let url = URL.init(string: _song.imageCover.urlConvertWith(width: screenWidth, height: screenWidth)){
            self.img_background?.sd_setImage(with: url, placeholderImage: image_songPlaceholder)
        }
    }
    
    func updateImageBackground(){
        if let _song = self.vmodel.getItemAt(index: self.vmodel.getPlayingIndex()) as? Song{
            self.updateImageBackgroundWith(_song: _song)
        }
    }
    
    //MARK: - Register CollectionViewCell
    func collectionViewCellRegister(){
        let cellNib = UINib.init(nibName: PlayerSongCollectionCell.nameOfClass, bundle: nil)
        let cellNib2 = UINib.init(nibName: LoadingListCollectionCell.nameOfClass, bundle: nil)
        
        self.myCollectionView.register(cellNib, forCellWithReuseIdentifier: PlayerSongCollectionCell.nameOfClass)
        self.myCollectionView.register(cellNib2, forCellWithReuseIdentifier: LoadingListCollectionCell.nameOfClass)
        
        let layout = PlayerCollectionFlowLayout()
        
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize.init(width: screenWidth/1.15, height: screenWidth/1.15)
//        layout.minimumLineSpacing = paddingBetweenItem * 1.5
//        layout.minimumInteritemSpacing = paddingBetweenItem * 1.5
        
        self.myCollectionView?.collectionViewLayout = layout
        self.myCollectionView.isPagingEnabled = false
        
        self.setupPangesture()
        self.setupTapGesture()
//        self.myCollectionView.isScrollEnabled = false
        
        self.myCollectionView.delegate = self
        self.myCollectionView.dataSource = self
        
    }
    
    func setupPangesture(){
        
        self.viewPlayerControlTop.isUserInteractionEnabled = true
        self.myCollectionView.isUserInteractionEnabled = true
        
        let panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(self.detectPanGesture(gesture:)))
        let panGesture2 = UIPanGestureRecognizer.init(target: self, action: #selector(self.detectPanGesture(gesture:)))
        
        panGesture.delegate = self
        panGesture2.delegate = self
        
        self.myCollectionView.addGestureRecognizer(panGesture)
        self.viewPlayerControlTop.addGestureRecognizer(panGesture2)
    }
    
    func setupTapGesture(){
        self.lb_title.isUserInteractionEnabled = true
        self.lb_artist.isUserInteractionEnabled = true
        self.viewPlayerControlTop.isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.detectTapGesture(gesture:)))
        let tapGesture2 = UITapGestureRecognizer.init(target: self, action: #selector(self.detectTapGesture(gesture:)))
        let tapGesture3 = UITapGestureRecognizer.init(target: self, action: #selector(self.detectTapGesturePlayerControlTop(gesture:)))
        
        self.lb_title.addGestureRecognizer(tapGesture)
        self.lb_artist.addGestureRecognizer(tapGesture2)
        self.viewPlayerControlTop.addGestureRecognizer(tapGesture3)
    }
    
    func detectTapGesture(gesture: UITapGestureRecognizer){
        self.presentInfoController(vmodel: self.vmodel)
    }
    
    func detectTapGesturePlayerControlTop(gesture: UITapGestureRecognizer){
        self.showPlayerViewController(isFromListing: false)
    }
    
//    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
//        if let pan = gestureRecognizer as? UIPanGestureRecognizer {
//            let velocity = pan.velocity(in: self.myCollectionView)
//            return fabs(velocity.y) > fabs(velocity.x)
//        }
//        return true
//    }
    
    func detectPanGesture(gesture: UIPanGestureRecognizer){
        
        switch gesture.state {
        case .began:
            //
            let velocity = gesture.velocity(in: gesture.view)
            
            let isVerticalGesture = fabs(velocity.y) > fabs(velocity.x)
            
            if isVerticalGesture{
                // vertical
                if velocity.y > 0{
                    self.direction = .down
                }else{
                    self.direction = .up
                }
                
            }else{
                if gesture.view == self.myCollectionView{
                    // horizontal
                    if velocity.x > 0{
                        // right
                        self.direction = .right
                    }else{
                        //left
                        self.direction = .left
                    }
                }
            }
            
        case .changed:
            //
            if self.direction == .down || self.direction == .up{
                let translation = gesture.translation(in: self.view)
                
                var movingPoint = CGPoint(x: 0, y: self.view.frame.origin.y + translation.y)
                
                if movingPoint.y < 0{
                    movingPoint.y = 0
                    
                }else if movingPoint.y > screenHeight{
                    movingPoint.y = screenHeight
                }
                
                var frame = self.view.frame
                frame.origin = movingPoint
                self.view.frame = frame
                
                print((frame.origin.y + playerControlHeight + 44.0)/(playerViewheight - playerControlHeight - 44.0))
                self.viewPlayerControlTop?.alpha = (frame.origin.y + playerControlHeight + 44.0)/(playerViewheight - playerControlHeight - 44.0)
                
                if self.storeMovingPoint.y > movingPoint.y{
                    self.direction = .up
                }else{
                    self.direction = .down
                }
                
                self.storeMovingPoint = movingPoint
                
                gesture.setTranslation(CGPoint.zero, in: self.view)
            }
            break
            
            
        case .ended:
            //
            switch self.direction {
            case .left:
                //
                self.vmodel.nextSong()
                break
            case .right:
                //
                self.vmodel.previousSong()
                break
                
            case .up:
                //
                self.completeMovingPlayer(isShowing: true)
                break
                
            case .down:
                self.completeMovingPlayer(isShowing: false)
                break
                
            default:
                break
            }
            
            self.storeMovingPoint = CGPoint.init(x: 0, y: 0)
            self.direction = .unknow
            break
            
        default:
            
            self.direction = .unknow
        }
    }
    
    func completeMovingPlayer(isShowing: Bool){
        
        if isShowing{
            
            UIView.animate(withDuration: 0.25, animations: {
                self.viewPlayerControlTop?.alpha = 0.0
            }, completion: { (finish) in
                //
            })
            self.showPlayerViewController(isFromListing: false)
            
        }else{
            
            UIView.animate(withDuration: 0.25, animations: {
                self.viewPlayerControlTop?.alpha = 1.0
            }, completion: { (finish) in
                //
            })
            self.hidePlayerViewController()
        }
    }
    
    
    //MARK: - CollectionView Delegate - Data Source
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.vmodel.songs.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.vmodel.configureSongPlayerCell(collectionView: collectionView, indexPath: indexPath, currentSongPlaying: self.vmodel.getCurrentPlayingSong())
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: screenWidth/1.15, height: screenWidth/1.15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        
        return UIEdgeInsetsMake(50.0, 30.0, 0, 30.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }

    /*
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = self.myCollectionView.contentOffset
        visibleRect.size = self.myCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        let visibleIndexPath: IndexPath = self.myCollectionView.indexPathForItem(at: visiblePoint)!
        
        self.vmodel.setPlayingIndex(index: visibleIndexPath.row, isFromPlayerTracklisting: t)
        
        print(visibleIndexPath)
        print("scroll end")
    }
 
 */
    
    //MARK: - Btn Action
    @IBAction func btnShuffleTap(sender : AnyObject){
        self.isEnableShuffleMode(enable: !self.vmodel.isShuffle)
    }
    
    func isEnableShuffleMode(enable: Bool){
        self.vmodel.isShuffle = enable
    }
    
    @IBAction func btnRepeatTap(sender : AnyObject){
        var rawValue = self.vmodel.repeatStyle.rawValue + 1
        if rawValue > 2{
            rawValue = 0
        }
        self.vmodel.repeatStyle = RepeatStyle(rawValue: rawValue)!
    }
    
    @IBAction func btnPlayTap(sender : AnyObject?){
        self.vmodel.isPlaying = !self.vmodel.isPlaying
    }
    
    @IBAction func btnDownTap(sender : AnyObject){
        self.hidePlayerViewController()
        self.viewPlayerControlTop?.alpha = 1.0
    }
    
    @IBAction func btnNextTap(sender : AnyObject?){
        self.vmodel.nextSong()
    }
    
    @IBAction func btnPreviousTap(sender : AnyObject?){
        self.vmodel.previousSong()
    }
    
    @IBAction func sliderValueChange(sender : UISlider){
        self.vmodel.seekTo(value: sender.value)
    }
    
    @IBAction func btnTracklistingTap(sender : UIButton){
        self.pushToPlayerViewTracklisting(vmodel: self.vmodel)
    }
    
    @IBAction func btnAddPlaylistTap(sender : UIButton){
        if let _song = self.vmodel.getCurrentPlayingSong(){
            self.presentToPlaylistViewController(itemSong: [_song])
        }
    }
    
    @IBAction func btnMoreTap(sender : UIButton){
        
        let itemViewModel = PlayerViewModel.init(title: AppViewTitle.more.rawValue)
        itemViewModel.type = self.vmodel.type
        itemViewModel.songs = self.vmodel.songs
        itemViewModel.setPlayingIndex(index: self.vmodel.getPlayingIndex(), isFromPlayerTracklisting: false)
        
        self.presentMoreViewController(vmodel: itemViewModel, itemSongSelected: nil)
    }
    
}
