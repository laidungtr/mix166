//
//  SharePlayer.swift
//  Mix166
//
//  Created by ttiamap on 6/16/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import UIKit

class SharePlayer: NSObject{
    
    static var  playerController: PlayerViewController?  //= StoryBoardManager.PlayerViewController() as? PlayerViewController
    static var  playerTracklist:  PlayerViewTrackListing? //= StoryBoardManager.PlayerTracklistingController() as? PlayerViewTrackListing
    static var  playerMoreController:  MoreViewController? //= StoryBoardManager.MoreController() as? MoreViewController
    static var  playerInfoController: PlayerSongInfoController?
    static var  videoPlayerController:  VideoViewController? //= StoryBoardManager.VieoViewController() as? VideoViewController
    
    static var  tabbarController:  UITabBarController?
    
    static var blurView: UIVisualEffectView?
    static var blurViewTrackListing: UIVisualEffectView?
    
    static var isVideoScaling = false
    
    static var isAutoPlayVideo = true
    
    static var isShowingPlayer_ControlSmall: Bool{
        
        if self.playerController == nil {
            return false
        }
        
        let frame = self.playerController?.view.frame
        
        if frame?.origin.y ?? 0 >= (screenHeight - playerControlHeight){
            return false
        }
        
        return true
    }
    
    static var isShowingVideoPlayer: Bool{
        
        if self.videoPlayerController == nil {
            return false
        }
        
        let frame = self.videoPlayerController?.view.frame
        
        if frame?.origin.x ?? 0 < 0{
            return false
        }
        
        return true
    }
    
    static var currentPlayingSong: Song?{
        if let _song = self.playerController?.vmodel.getCurrentPlayingSong(){
            return _song
        }
        return nil
    }
}
