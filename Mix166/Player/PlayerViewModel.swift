//
//  PlayerViewModel.swift
//  Mix166
//
//  Created by ttiamap on 6/14/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import UIKit

protocol PlayerViewModelDelegate {
    
    func playerDelegate_updateBackgroundInfo()
    func playerDelegate_didChangeSongList(_song: [Song])
    func playerDelegate_willPlayingSong(_song: Song, playingStatus isPlaying: Bool, callBack:(()->())?)
    func playerDelegate_shouldUpdate(indexPaths: [IndexPath])
    func playerDelegate_seekToValue(value: CGFloat)
    
    func playerDidChangeRepeatStyle(style: RepeatStyle)
    func playerDidChangeShuffleStyle(isShuffle: Bool)
    func playerDidUpdateDurationCountDown(str_duration: String, sliderValue: Float)
}

protocol PlayerViewModel_SongInfoDelegate {
    func songInfo_didSelectedAritstAtIndex(index: Int)
}

enum RepeatStyle: Int {
    case none = 0
    case repeatAll = 1
    case repeatOne = 2
}


class PlayerSlider: UISlider{
    override func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect {
        let unadjustedThumbrect = super.thumbRect(forBounds: bounds, trackRect: rect, value: value)
        let thumbOffsetToApplyOnEachSide: CGFloat = unadjustedThumbrect.size.width / 2.0
        let minOffsetToAdd = -thumbOffsetToApplyOnEachSide
        let maxOffsetToAdd = thumbOffsetToApplyOnEachSide
        let offsetForValue = minOffsetToAdd + (maxOffsetToAdd - minOffsetToAdd) * CGFloat(value / (self.maximumValue - self.minimumValue))
        var origin = unadjustedThumbrect.origin
        origin.x += offsetForValue
        return CGRect(origin: origin, size: unadjustedThumbrect.size)
    }
}

class PlayerViewModel: BaseViewModel, DLTPlayerDelegate{
    enum Shuffle_Status{
        case none
        case justActive
        case active
    }
    var delegate_player: PlayerViewModelDelegate?
    var delegate_songInfo: PlayerViewModel_SongInfoDelegate?
    var artistSectectIndex: Int = 0
    
    override init(title: String) {
        super.init(title: title)
        
    }
    
    private var playingIndex: Int = 0
    
    private var tmpSongPlaying: Song?
    
    
    var numberUserTapNext_Previous = 0
    var numberHandleUserTapNext_Previous = 0
    
    var isExistsPlayerTracklisting = false
    var isExistsMoreController = false
    var isExistsPlayerSongInfo = false
    var isPlayerShow = false{
        didSet{
            DLTPlayer.sharedInstance.delegate = self
        }
    }
    
    
    var isShuffle: Bool = false{
        didSet{
            
            if self.isShuffle == true{
                self.shuffle_status = .justActive
            }else{
                self.shuffle_status = .none
            }
            
            DispatchQueue.global(qos: .background).async {
                print("This is run on the background queue")
                
                DispatchQueue.main.async {
                    self.delegate_player?.playerDidChangeShuffleStyle(isShuffle: self.isShuffle)
                }
            }
        }
    }
    
    var shuffle_status: Shuffle_Status = .none
    
    var repeatStyle: RepeatStyle = .none{
        didSet{
            if self.repeatStyle == .repeatOne{
                DLTPlayer.sharedInstance.enableLoop = true
            }else{
                DLTPlayer.sharedInstance.enableLoop = false
            }
            
            self.delegate_player?.playerDidChangeRepeatStyle(style: self.repeatStyle)
        }
    }
    
    var type: SongType?
    var songs: [Song] = []{
        didSet{
            
            if self.songs is [Song_Masup]{
                let duration = DLTPlayer.sharedInstance.duration
                if duration != .nan, let masup_song = self.songs.first as? Song_Masup{
                    self.delegate_player?.playerDelegate_seekToValue(value: CGFloat(masup_song.location_second)/CGFloat(duration))
                }
            }else{
                self.delegate_player?.playerDelegate_didChangeSongList(_song: self.songs)
                
                self.shuffleIndex = []
                if self.songs.count > 0{
                    for index in 0...self.songs.count - 1{
                        shuffleIndex.append(index)
                    }
                    self.shuffleIndex = self.shuffleIndex.shuffled()
                }
            }
        }
    }
    
    
    var shuffleIndex: [Int] = []
    
    var isPlaying: Bool = false{
        didSet{
            if let song = self.getItemAt(index: self.playingIndex) as? Song{
                if self.delegate_player != nil {
                    self.delegate_willPlayingSongCall(_song: song, playingStatus: self.isPlaying,callBack: nil)
                    if self.isPlaying == true{
                        DLTPlayer.playWithUrl(song.mp3.playing_link)
                    }else{
                        DLTPlayer.pause()
                    }
                }
            }
        }
    }
    
    //MARK: - Delegate call
    func delegate_willPlayingSongCall(_song: Song, playingStatus isPlaying: Bool, callBack:(()->())?){
        
        self.tmpSongPlaying = _song
        self.delegate_player?.playerDelegate_willPlayingSong(_song: _song, playingStatus: isPlaying, callBack: nil)
    }
    
    //MARK: - GET
    func getPlayingIndex() -> Int{
        return self.isShuffle ? self.shuffleIndex[self.playingIndex] : self.playingIndex
    }
    
    func getScrollingIndex() -> Int{
        return self.playingIndex
    }
    
    //MARK: - SET
    func setPlayingIndex(index: Int, isFromPlayerTracklisting: Bool){
        self.playingIndex = index
        
        if isFromPlayerTracklisting{
            
            let isShuffling = self.isShuffle
            if self.isShuffle{
                // if shuffle mode is enable
                // set back to none for get right current playing index
                // after that set it back to shuffle mode
                self.isShuffle = false
            }
            
            if let song = self.getItemAt(index: self.getPlayingIndex()) as? Song{
                if !self.isPlaying{
                    self.isPlaying = true
                }else{
                    if DLTPlayer.sharedInstance.currentStreamingLink != song.mp3.playing_link{
                        self.delegate_willPlayingSongCall(_song: song, playingStatus: self.isPlaying, callBack: nil)
                        DLTPlayer.playWithUrl(song.mp3.playing_link)
                    }
                }
            }
            
            if isShuffling{
                // set back shuffle status
                self.isShuffle = true
            }
        }
    }
    
    // MARK: - DLTPlayer Delegate
    func DLTPlayer_CurrentLinkSteamingFail(str_linkUrl: String) {
        if let song = self.getItemAt(index: self.playingIndex) as? Song{
            if song.mp3.playing_link_320 != str_linkUrl{
                DLTPlayer.playWithUrl(song.mp3.playing_link_320)
            }else{
                self.nextSong()
            }
        }
    }
    
    func DLTPlayer_UpdateBackgroundSongInfo() {
        //
        self.delegate_player?.playerDelegate_updateBackgroundInfo()
    }
    
    func DLTPlayer_PlayingStatusChange(_ isPlaying: Bool) {
        //
    }
    
    func DLTPlayerBuffering(enable: Bool) {
        //
    }
    
    func DLTPlayer_DidFinishPlaying() {
        self.nextSong()
    }
    
    func DLTPlayer_DurationCountDown(str_durationCount: String, str_duration: String, sliderValue: Float) {
        self.delegate_player?.playerDidUpdateDurationCountDown(str_duration: str_durationCount,sliderValue: sliderValue)
    }
    
    //MARK: - Override from base view model
    override func getItemAtIndexPath(indexPath: IndexPath) -> AnyObject? {
        if self.songs.count > indexPath.row{
            return self.songs[indexPath.row]
        }
        
        return nil
    }
    
    override func getItemAt(index: Int) -> AnyObject? {
        if self.songs.count > index && index >= 0{
            return self.songs[index]
        }
        
        return nil
    }
    
    func getCurrentPlayingSong() -> Song?{
        
        if self.tmpSongPlaying != nil {
            return self.tmpSongPlaying
        }
        if let song = self.getItemAt(index: self.getPlayingIndex()) as? Song{
            return song
        }
        return nil
    }
    
    private func getNextSongIndexPath() -> (IndexPath, IndexPath)?{
        
        let currentIndexPath = (self.shuffle_status != .active) ?
            IndexPath.init(row: self.playingIndex, section: 0):
            IndexPath.init(row: self.shuffleIndex[self.playingIndex], section: 0)
        
        if self.shuffle_status == .justActive{
            self.shuffle_status = .active
        }
        
        self.playingIndex += 1
        if self.playingIndex >= self.songs.count{
            
            if self.repeatStyle == .repeatAll || self.isShuffle{
                self.playingIndex = 0
            }else{
                self.playingIndex -= 1
                DLTPlayer.stop(callback: {
                    //
                })
                return nil
            }
        }
        
        let nextIndexPath = self.isShuffle ?
            IndexPath.init(row: self.shuffleIndex[self.playingIndex], section: 0) :
            IndexPath.init(row: self.playingIndex, section: 0)
        return (currentIndexPath,nextIndexPath)
    }
    
    private func getPreviousSongIndexPath() -> (IndexPath, IndexPath){
        let currentIndexPath = (self.shuffle_status != .active) ?
            IndexPath.init(row: self.playingIndex, section: 0):
            IndexPath.init(row: self.shuffleIndex[self.playingIndex], section: 0)
        
        if self.shuffle_status == .justActive{
            self.shuffle_status = .active
        }
        
        self.playingIndex -= 1
        if self.playingIndex < 0{
            self.playingIndex = self.songs.count - 1
        }
        
        let previousIndexPath = self.isShuffle ?
            IndexPath.init(row: self.shuffleIndex[self.playingIndex], section: 0) :
            IndexPath.init(row: self.playingIndex, section: 0)
        
        return (currentIndexPath,previousIndexPath)
    }
    
    //
    func nextSong(){
        
        //
        if (self.songs is [Mix]){
            return
        }
        
        // handle user taping next button extremly fast
        self.numberUserTapNext_Previous += 1
        
        
        DLTPlayer.stop(callback: {
            
            DispatchQueue.global(qos: .background).sync {
                
                if let (currentIndexPath, nextIndexPath) = self.getNextSongIndexPath(){
                    
                    if let song = self.getItemAt(index: nextIndexPath.row) as? Song{
                        self.delegate_willPlayingSongCall(_song: song, playingStatus: self.isPlaying, callBack: nil)
                        
                        self.delegate_player?.playerDelegate_shouldUpdate(indexPaths: [currentIndexPath,nextIndexPath])
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                            
                            // handle user taping next button extremly fast
                            self.numberHandleUserTapNext_Previous += 1
                            
                            print("\(self.numberUserTapNext_Previous) --------- \(self.numberHandleUserTapNext_Previous)")
                            if self.numberUserTapNext_Previous == self.numberHandleUserTapNext_Previous{
                                // if the number tap is equal to the number call playing
                                // it right tap position to play
                                
                                if self.isPlaying{
                                    DLTPlayer.playWithUrl(song.mp3.playing_link)
                                }
                            }
                            
                            // refresh two flag
                            self.refreshPlayingTapExtremplyFlag()
                        }
                    }
                }
            }
        })
    }
    
    func previousSong(){
        
        // handle user taping next button extremly fast
        self.numberUserTapNext_Previous += 1
        
        DLTPlayer.stop(callback: {
            DispatchQueue.global(qos: .background).sync {
                
                let (currentIndexPath, previousIndexPath) = self.getPreviousSongIndexPath()
                
                if let song = self.getItemAt(index: previousIndexPath.row) as? Song{
                    
                    self.delegate_willPlayingSongCall(_song: song, playingStatus: self.isPlaying, callBack: nil)
                    
                    // put this playerDelegate_willPlayingSong above the playerDelegate_shouldUpdate for the correct playing position
                    
                    self.delegate_player?.playerDelegate_shouldUpdate(indexPaths: [currentIndexPath,previousIndexPath])
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        // handle user taping next button extremly fast
                        self.numberHandleUserTapNext_Previous += 1
                        
                        print("\(self.numberUserTapNext_Previous) --------- \(self.numberHandleUserTapNext_Previous)")
                        
                        if self.numberUserTapNext_Previous == self.numberHandleUserTapNext_Previous{
                            // if the number tap is equal to the number call playing
                            // it right tap position to play
                            if self.isPlaying{
                                DLTPlayer.playWithUrl(song.mp3.playing_link)
                            }
                        }
                        
                        // refresh two flag
                        self.refreshPlayingTapExtremplyFlag()
                    }
                }
            }
        })
    }
    
    func refreshPlayingTapExtremplyFlag(){
        self.numberUserTapNext_Previous = 0
        self.numberHandleUserTapNext_Previous = 0
    }
    
    func seekTo(value: Float){
        DLTPlayer.seekTo(value: value)
    }
    
    func isPlaying(index: Int) -> Bool{
        return self.getPlayingIndex() == index
    }
    
    //MARK: - PlayerSongInfoController
    
    override func numberOfItemInSection(type: HorizontalType, parentSection: Int?) -> Int?{
        if let currentSong = self.getCurrentPlayingSong(){
            return currentSong.artists.count
        }
        return 0
    }
    
    override func horizontal_objectItem(type: HorizontalType, indexPath: IndexPath, parentSection: Int?) -> AnyObject?{
        if let currentSong = self.getCurrentPlayingSong(){
            return currentSong.getArtistItemAtIndex(index: indexPath.row)
        }
        return nil
    }
    
    override func horizontal_shoudSelectedItem(type: HorizontalType, indexPath: IndexPath, parentSection: Int?) -> Bool{
        
        if self.artistSectectIndex != indexPath.row{
            self.artistSectectIndex = indexPath.row
            self.delegate_songInfo?.songInfo_didSelectedAritstAtIndex(index: self.artistSectectIndex)
        }
        
        return true
    }
    
    override func horizontal_shouldReloadData(type: HorizontalType, parentSection: Int?) -> Bool{
        return true
    }
    
    func configureHeaderTitleCell(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, indexPath: IndexPath) -> UICollectionReusableView{
        
        if kind == UICollectionElementKindSectionHeader{
            if let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: TitleCollectionReusableView.nameOfClass, for: indexPath) as? TitleCollectionReusableView{
                
                
                self.queueConcurrent.async(execute: {
                    if indexPath.section == 1{
                        header.configureHeader(str_title: "Artist")
                    }else if indexPath.section == 2{
                        header.configureHeader(str_title: "Information")
                    }
                })
                
                return header
            }
        }
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    // MARK: Description
    
    func configureDescription(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell{
        if let _cell = collectionView.dequeueReusableCell(withReuseIdentifier:DescriptionCollectionCell.nameOfClass, for: indexPath) as? DescriptionCollectionCell{
            
            queueConcurrent.async(execute: {
                if let itemSong = self.getCurrentPlayingSong(){
                    if let itemArtist = itemSong.getArtistItemAtIndex(index: self.artistSectectIndex){
                        _cell.configureCell(str_descriptionHTML: itemArtist.biography, mainQueueEnable: true)
                    }
                }
            })
            return _cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
    
    func configureDescriptionSizingCell(sizingCell: DescriptionCollectionCell, indexPath: IndexPath){
        
        if let itemSong = self.getCurrentPlayingSong(){
            if let itemArtist = itemSong.getArtistItemAtIndex(index: self.artistSectectIndex){
                sizingCell.configureCell(str_descriptionHTML: itemArtist.biography, mainQueueEnable: false)
            }
        }
    }
    
}
