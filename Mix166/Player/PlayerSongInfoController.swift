//
//  PlayerSongInfoController.swift
//  Mix166
//
//  Created by ttiamap on 8/8/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class PlayerSongInfoController: BaseViewController,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, PlayerViewModel_SongInfoDelegate {
    
    @IBOutlet weak var myCollectionView: UICollectionView?
    
    private let heightItemSongCell: CGFloat = 65.0
    private let heightButtonCancel: CGFloat = 50.0
    
    var vmodel: PlayerViewModel!{
        didSet{
            if self.vmodel.getCurrentPlayingSong() != storeCurrentSong{
                storeCurrentSong = self.vmodel.getCurrentPlayingSong()
                self.registerCell()
            }
        }
    }
    
    private var storeCurrentSong: Song?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.vmodel.delegate_songInfo = self
        self.registerCell()

    }
    
    //MARK: - Register cell
    private func registerCell(){
        
        if self.myCollectionView?.delegate == nil {
            self.vmodel.registerCell(collectionView: self.myCollectionView, callback: { 
                self.myCollectionView?.contentInset = UIEdgeInsetsMake(paddingBetweenItem * 4, paddingBetweenItem, 0, 0)
                self.myCollectionView?.dataSource = self
                self.myCollectionView?.delegate = self
            })
            
        }else{
            self.myCollectionView?.reloadData()
        }
    }
    
    //MARK: - CollectionView Delegate - Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if let currentsong = self.vmodel.getCurrentPlayingSong(){
            if  currentsong.artists.count > 0{
                return 3
            }
            return 1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell = UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        if indexPath.section == 0{
            if let _cell = collectionView.dequeueReusableCell(withReuseIdentifier:SongListCollectionCell2.nameOfClass, for: indexPath) as? SongListCollectionCell2{
                if let _song = self.vmodel?.getCurrentPlayingSong(){
                    _cell.cell_viewContain?.backgroundColor = UIColor.clear
                    _cell.configureCellWith(_song: _song)
                    _cell.cell_btnMore?.isHidden = true
                }
                cell = _cell
            }
            
        }else if indexPath.section == 1{
            return self.vmodel!.configureDeejayCell(collectionView: collectionView, indexPath: indexPath, hightLightWhenSelected: true)
            
        }else{
            return self.vmodel.configureDescription(collectionView: collectionView, indexPath: indexPath)
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func songInfo_didSelectedAritstAtIndex(index: Int) {
        self.myCollectionView?.collectionViewLayout.invalidateLayout()
        self.myCollectionView?.performBatchUpdates({ 
            self.myCollectionView?.reloadSections(IndexSet(integer: 2))
        }, completion: { (finish) in
            //
        })
    }
    
    // Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0{
            return CGSize.init(width: screenWidth, height: heightItemSongCell)
        }else if indexPath.section == 1{
            // artist
            return CGSize.init(width: screenWidth, height: deejaySize.height)
        }else if indexPath.section == 2{
            // intrictsize to caculate label
            self.vmodel.configureDescriptionSizingCell(sizingCell: self.vmodel.sizingDescriptionCell, indexPath: indexPath)
            
            let size = self.vmodel.sizingDescriptionCell.intrinsicContentSize
            return CGSize.init(width: screenWidth, height: size.height)
        }
        
        return sizeZero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
 
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{

        return 0
        
    }
    
    //MARK: Header
    //MARK: - Header
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section != 0 {
            return headerTitleSize
        }
        return sizeZero
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        //
        if kind == UICollectionElementKindSectionHeader{
            return self.vmodel.configureHeaderTitleCell(collectionView: collectionView, viewForSupplementaryElementOfKind: kind, indexPath:indexPath)
        }
        
        return UICollectionReusableView.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: 0))
    }
    
    
    
    @IBAction func btnDismissTap(sender : AnyObject){
        self.hidePlayerInfoController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
