//
//  ChartListViewController.swift
//  Mix166
//
//  Created by ttiamap on 7/19/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ChartListViewController: BaseViewController, ProductListViewDelegate, ProductListViewDataSource, ProductListScrollingDelegate {
    
    @IBOutlet private weak var viewProductList: ProductListView?
    
    var vmodel: ChartViewModel = ChartViewModel.init(title: AppViewTitle.chart.rawValue)
    
    private var isScrollingManually = false
    private var isUserDragging = false
    private let bannerHeight: CGFloat = 128.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupProductListView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }
    
    override var isNavigationBarHidden: Bool{
        return false
    }
    
    override func setNavigationBarStyle() -> NavigationBarStyle {
        return .Transparent
    }
    
    override func setViewTitle() -> String {
        return self.vmodel.titleNavigationBar ?? ""
    }
    
    func setupProductListView(){
        if self.vmodel.productlist_info != nil {
            self.viewProductList?.isEnablePullRefresh = self.vmodel.productlist_info!.isEnablePullToRefresh
            self.viewProductList?.delegate = self
            self.viewProductList?.datasource = self
            self.viewProductList?.delegate_scrolling = self
            self.viewProductList?.setProductListBackgroundColor(color: UIColor.black)
            self.viewProductList?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
            self.viewProductList?.requestWithProductListInfo(_productInfo: self.vmodel.productlist_info!, newRequest: true)
        }
    }
    
    //MARK - ProductList Datasource
    func productListView_spacingBetweenItem() -> CGFloat{
        return 10.0
    }
    
    func productListView_scrollDirection() -> UICollectionViewScrollDirection{
        return .vertical
    }
    
    func productListView_contentInsect() -> UIEdgeInsets{
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    //MARK - ProductList Delegate
    func productListView_ScrollViewDidScroll(scrollView: UIScrollView){
        self.navigationBarHideOnSwipe(scrollView: scrollView)
    }
    
    func productListView_ScrollViewDidEndDecelerating(scrollView: UIScrollView){
        self.updateLastScrollingOffset(scrollView: scrollView)
    }
    
    func productListView_ScrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool){
        
    }
    
    func productListView_shouldDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) -> Bool{
        return false
    }
    
    func productListView_didDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) {
        //
    }
    
    func productListView_BtnShuffleSongDidTap(datas: [AnyObject]?, type: SongType){
        
    }
    
    func productListView_didSelectedAtIndexPath(indexPath: IndexPath, withData Datas: [AnyObject]?, type: SongType) {
        if let _videos = Datas as? [Video]{
            self.presentVideoViewController(_videos: _videos, playingIndex: indexPath.row)
            
        }else if let _songs = Datas as? [Song]{
            self.presentToPlayerViewController(_songs: _songs, playingIndex: indexPath.row,type:  type, shuffleEnable: false, isFromViewTracklisting: true)
            
        }else if let _artist = Datas as? [Artist], _artist.count > indexPath.row{
            self.pushToArtistDetailViewController(artist_info: _artist[indexPath.row])
        }
    }
    
    func productListView_HeaderTitleDidTapButtonMore(productInfo: ProductListInfo) {
        
        switch productInfo.type {
        case .deejay_vertical, .deejay_horizontal:
            return self.pushToArtistListViewController(info: productInfo)
        
        case .mixset, .track, .trackPlayer, .video:
            return self.pushToProductListViewController(info: productInfo, vmodel: nil)
        default:
            break
        }
    }
    
    func productListView_MoreTapAtIndexPath(index: Int, withData Datas: [AnyObject]?) {
        
        var songType: SongType = .Track
        
        if Datas is [Mix]{
            songType = .Mix
        }
        
        if let items = Datas as? [Song]{
            let itemViewModel = PlayerViewModel.init(title: AppViewTitle.more.rawValue)
            itemViewModel.type = songType
            itemViewModel.songs = items
            itemViewModel.setPlayingIndex(index: index, isFromPlayerTracklisting: false)
            
            self.presentMoreViewController(vmodel: itemViewModel, itemSongSelected: nil)
        }
    }
    
    func productListView_BtnAutoPlayVideoDidChange(isOn: Bool) {
        //
    }
    

    
//    func setupSliderMenu(){
//
//        self.viewSlideMenu?.slideMenuStyle = .Title
//        self.viewSlideMenu?.delegate = self
//        self.viewSlideMenu?.setSlideMenuBackgroundColor(color: colorNavigationBar)
//        self.viewSlideMenu?.reloadSlideMenuData()
//        self.view.layoutIfNeeded()
//    }
//    
//    func setupRequestProductListInfo(){
//        
//        let mix_info = ProductListInfo.init(_proxy: .chart, _type: .mixset, filter: self.vmodel.filterBase, _data: nil)
//        let track_info = ProductListInfo.init(_proxy: .chart, _type: .track, filter: self.vmodel.filterBase, _data: nil)
//        let video_info = ProductListInfo.init(_proxy: .chart, _type: .video, filter: self.vmodel.filterBase, _data: nil)
//        
//        self.paggingTypes       = [.Mix, .Track, .Video]
//        self.productlist_infos  = [mix_info, track_info, video_info]
//        
//        // setup view mix
//        self.viewMix?.isEnablePullRefresh = false
//        self.viewMix?.delegate = self
//        self.viewMix?.datasource = self
//        self.viewMix?.requestWithProductListInfo(_productInfo: mix_info)
//        self.viewMix?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
//        
//        // setup view track
//        self.viewTrack?.isEnablePullRefresh = false
//        self.viewTrack?.delegate = self
//        self.viewTrack?.datasource = self
//        self.viewTrack?.requestWithProductListInfo(_productInfo: track_info)
//        self.viewTrack?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
//        
//        // setup view video
//        self.viewVideo?.isEnablePullRefresh = false
//        self.viewVideo?.delegate = self
//        self.viewVideo?.datasource = self
//        self.viewVideo?.requestWithProductListInfo(_productInfo: video_info)
//        self.viewVideo?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
//        
//    }
//    
//
//    //MARK: - SlideMenu Delegate
//    func slideMenu_NumberOfItem() -> NSInteger{
//        return self.paggingTypes?.count ?? 0
//    }
//    
//    func slideMenu_TitleForItem(index: NSInteger) -> String{
//        if self.paggingTypes?.count ?? 0 > index{
//            return self.paggingTypes?[index].rawValue.uppercased() ?? ""
//        }
//        return ""
//    }
//    
//    func slideMenu_DidSelectedItemAtIndex(index: NSInteger){
//        
//        if !self.isUserDragging{
//            self.isUserDragging = false
//            self.isScrollingManually = true
//        }
//        
//        let xOffset = CGFloat(index) * screenWidth
//        self.myScrollView?.setContentOffset(CGPoint.init(x: xOffset, y: 0), animated: true)
//    }
//    
//    func slideMenu_FristloadSelected() -> Int {
//        return 0
//    }
//    
//    //MARK: - ScrollView Delegate
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if self.isScrollingManually{
//            return
//        }
//        
//        let xOffset = (scrollView.contentOffset.x / screenWidth)
//        self.viewSlideMenu?.moveBottomIndicator(xOffset: xOffset)
//    }
//    
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        self.isUserDragging = true
//    }
//    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        
//        if self.isScrollingManually{
//            return
//        }
//        
//        let _tempOffsetX = scrollView.contentOffset.x + screenWidth
//        var pageIndex = Int(_tempOffsetX / screenWidth)
//        
//        if _tempOffsetX != scrollView.contentSize.width{
//            pageIndex -= 1
//        }
//        
//        if (self.paggingTypes != nil && pageIndex >= self.paggingTypes!.count){
//            pageIndex = self.paggingTypes!.count - 1
//        }
//        
//        self.viewSlideMenu?.updateCurentSelected(index: pageIndex)
//    }
//    
//    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
//        //
//        self.isScrollingManually = false
//    }
//    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
