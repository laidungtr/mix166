//
//  MixsetViewController.swift
//  Mix166
//
//  Created by ttiamap on 6/13/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class ProductListViewController: BaseViewController, ProductListViewDelegate, ProductListViewDataSource, ProductListScrollingDelegate {
    
    @IBOutlet weak var viewProductList: ProductListView?
    var productlist_info: ProductListInfo?
    
    var vmodel: BaseViewModel = BaseViewModel.init(title: "")
    
    override func setViewTitle() -> String {
        return self.vmodel.titleNavigationBar ?? ""
    }
    
    private var currentSelectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupProductListView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.currentSelectedIndexPath != nil {
            self.viewProductList?.reloadItemAtIndexPaths(indexPaths: [self.currentSelectedIndexPath!])
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupProductListView(){
        if self.productlist_info != nil {
            self.viewProductList?.isEnablePullRefresh = self.productlist_info!.isEnablePullToRefresh
            self.viewProductList?.delegate = self
            self.viewProductList?.datasource = self
            self.viewProductList?.delegate_scrolling = self
            self.viewProductList?.setContentInsect(insect: UIEdgeInsetsMake(0, 0, playerControlHeight, 0))
            self.viewProductList?.requestWithProductListInfo(_productInfo: self.productlist_info!, newRequest: true)
        }
    }
    
    //MARK - ProductList Datasource
    func productListView_spacingBetweenItem() -> CGFloat{
        return 10.0
    }
    
    func productListView_scrollDirection() -> UICollectionViewScrollDirection{
        return .vertical
    }
    
    func productListView_contentInsect() -> UIEdgeInsets{
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    //MARK - ProductList Delegate
    func productListView_ScrollViewDidScroll(scrollView: UIScrollView){
        self.navigationBarHideOnSwipe(scrollView: scrollView)
    }
    
    func productListView_ScrollViewDidEndDecelerating(scrollView: UIScrollView){
        self.updateLastScrollingOffset(scrollView: scrollView)
    }
    
    func productListView_BtnShuffleSongDidTap(datas: [AnyObject]?, type: SongType){
        
    }
    
    func productListView_ScrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool){
        
    }
    
    func productListView_shouldDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) -> Bool{
        return false
    }
    
    func productListView_didDeleteItemAtIndexPath(indexPath: IndexPath, currentDatas: [AnyObject]?) {
        //
    }
    
    func productListView_BtnAutoPlayVideoDidChange(isOn: Bool) {
        //
    }
    
    func productListView_didSelectedAtIndexPath(indexPath: IndexPath, withData Datas: [AnyObject]?, type: SongType) {
        //
        if let _videos = Datas as? [Video]{
            self.presentVideoViewController(_videos: _videos, playingIndex: indexPath.row)
            
        }else if let _songs = Datas as? [Song]{
            self.presentToPlayerViewController(_songs: _songs, playingIndex: indexPath.row,type:  type, shuffleEnable: false, isFromViewTracklisting: true)
            
        }else if let _artist = Datas as? [Artist], _artist.count > indexPath.row{
            self.pushToArtistDetailViewController(artist_info: _artist[indexPath.row])
        }
        
        self.currentSelectedIndexPath = indexPath
    }
    
    func productListView_MoreTapAtIndexPath(index: Int, withData Datas: [AnyObject]?) {
        
        var songType: SongType = .Track
        
        if Datas is [Mix]{
            songType = .Mix
        }
        
        if let items = Datas as? [Song]{
            let itemViewModel = PlayerViewModel.init(title: AppViewTitle.more.rawValue)
            itemViewModel.type = songType
            itemViewModel.songs = items
            itemViewModel.setPlayingIndex(index: index, isFromPlayerTracklisting: false)
            
            self.presentMoreViewController(vmodel: itemViewModel, itemSongSelected: nil)
        }
    }
    
    func productListView_HeaderTitleDidTapButtonMore(productInfo: ProductListInfo) {
        
        switch productInfo.type {
        case .deejay_vertical, .deejay_horizontal:
            return self.pushToArtistListViewController(info: productInfo)
            
        case .mixset, .track, .trackPlayer, .video:
            let vmodel = BaseViewModel.init(title: productInfo.type.rawValue)
            return self.pushToProductListViewController(info: productInfo, vmodel: vmodel)
        default:
            break
        }
    }
}
