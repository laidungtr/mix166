//
//  ChartViewModel.swift
//  Mix166
//
//  Created by ttiamap on 7/19/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

class ChartViewModel: BaseViewModel{
    
    var productlist_info : ProductListInfo?
    var paggingTypes: [SongType]?
    
    override var titleNavigationBar: String?{
        return AppViewTitle.chart.rawValue
    }

}
