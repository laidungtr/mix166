//
//  HomeViewModel.swift
//  Mix166
//
//  Created by ttiamap on 6/8/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import UIKit

class HomeViewModel: BaseViewModel{
    
    var feature: Feature? = nil
    
    override var titleNavigationBar: String?{
        return AppViewTitle.home.rawValue
    }
    
    func fetchHomeDataWithoutCache(callback:@escaping (_ isSuccess: Bool, _ error: NSError?)->()) {
        var request = DataRequest(.Default)
        
        self.delegate?.baseViewModel_showLoadingProgress()
        request.onSuccess = {(data,  moreInfo) in
            
            if let error = moreInfo as? NSError, error.code != 0{
                self.delegate?.baseViewModel_showError(error: error)
            }else{
                
                if let _feature = data as? Feature{
                    self.feature = _feature
                }
                callback(true,nil)
                self.delegate?.baseViewModel_hideLoadingProgress()
            }
        }
        
        request.onFailure = {(error, moreInfo) in
            callback(false, error)
            
            if error != nil {
                self.delegate?.baseViewModel_showError(error: error!)
            }
        };
        
        request.onLoading = {result in
            
        }
        
        DataProxy.feature.feature(dataRequest: request, user_id: "")
    }
    
    
    // MARK: - Override from base View model
    override func getEntriesAtIndex(index: Int?) -> AnyObject? {
        if index == nil {
            return nil
        }
        
        if self.feature?.entries.count ?? 0 > index!{
            return self.feature!.entries[index!]
        }
        
        return nil
    }
    
    
    override func getItemAtIndexPath(indexPath: IndexPath) -> AnyObject? {
        if let featureEntries = self.getEntriesAtIndex(index: indexPath.section) as? FeatureEntries{
            if featureEntries.entries?.count ?? 0 > indexPath.row{
                if let _song = featureEntries.entries?[indexPath.row] as? Song{
                    return _song
                }
            }
        }
        return nil
    }
    
    override func getItemsAtIndexPath(indexPath: IndexPath) -> (AnyObject?, ProductType?) {
        
        var _song: [Song] = []
        var _type: ProductType?
        
        if let _entries = self.getEntriesAtIndex(index: indexPath.section) as? FeatureEntries{
            switch _entries.type {
                
            case .banner:
                
                if let itemSong = self.getItemAtIndexPath(indexPath: indexPath) as? Video{
                    _song.append(itemSong)
                    _type = ProductType.video
                }else if let itemSong = self.getItemAtIndexPath(indexPath: indexPath) as? Mix{
                    _song.append(itemSong)
                    _type = ProductType.mixset
                }else if let itemSong = self.getItemAtIndexPath(indexPath: indexPath) as? Mix{
                    _song.append(itemSong)
                    _type = ProductType.track
                }
                break
                
            case .mixset:
                if let itemSongs = _entries.entries as? [Song]{
                    _song = itemSongs
                }
                _type = ProductType.mixset
                break
            case .track:
                if let itemSongs = _entries.entries as? [Song]{
                    _song = itemSongs
                }
                _type = ProductType.track
                break
                
            case .chart:
                if let itemSongs = self.getSongChartTrackAtIndexPath(indexPath: indexPath){
                    _song = itemSongs
                }
                _type = ProductType.track
                break
                
            case .video:
                if let itemVideos = _entries.entries as? [Video]{
                    _song = itemVideos
                }
                _type = ProductType.video
                break
                
            case .deejay:
                break
                
            case .unknow:
                break
                
            }
        }
        
        return (_song as AnyObject, _type)
    }
    
    
    override func getItemsAtHorizontalIndexPath(indexPath: IndexPath, parentSection section: Int) -> (AnyObject?, FeatureType?) {
        var _items: [AnyObject] = []
        var type: FeatureType?
        if let _entries = self.getEntriesAtIndex(index: section) as? FeatureEntries{
            
            type = _entries.type
            
            switch _entries.type {
                
            case .banner:
                if let itemSong = self.getItemAtIndexPath(indexPath: indexPath) as? Song{
                    _items.append(itemSong)
                }
                break
                
            case .mixset:
                if let itemSongs = _entries.entries as? [Song]{
                    _items = itemSongs
                }
                break
            case .track:
                if let itemSongs = _entries.entries as? [Song]{
                    _items = itemSongs
                }
                break
                
            case .chart:
                if let itemSongs = self.getSongChartTrackAtIndexPath(indexPath: indexPath){
                    _items = itemSongs
                }
                break
                
            case .video:
                if let itemVideos = _entries.entries as? [Video]{
                    _items = itemVideos
                }
                break
                
            case .deejay:
                if let itemArtists = _entries.entries as? [Artist]{
                    _items = itemArtists
                }
                break
                
            case .unknow:
                break
                
            }
        }
        
        return (_items as AnyObject,type)
    }
    
    override func songCellDelegate_BtnMoreTap(sender: UIButton, indexPath: IndexPath) {
        
        var _items: [AnyObject] = []
        
        if let _entries = self.getEntriesAtIndex(index: indexPath.section) as? FeatureEntries{
            
            switch _entries.type {

            case .mixset:
                if let itemSongs = _entries.entries as? [Song]{
                    _items = itemSongs
                }
                break
            case .track:
                if let itemSongs = _entries.entries as? [Song]{
                    _items = itemSongs
                }
                break
                
            case .chart:
                if let itemSongs = self.getSongChartTrackAtIndexPath(indexPath: indexPath){
                    _items = itemSongs
                }
                break
                
            case .video:
                if let itemVideos = _entries.entries as? [Video]{
                    _items = itemVideos
                }
                break
                
            case .deejay:
                if let itemArtists = _entries.entries as? [Artist]{
                    _items = itemArtists
                }
                break
                
            default:
                break
                
            }
        //
            if _items.count > 0{
                self.delegate?.baseViewModel_moreBtnTapAtIndexPath(indexPath: indexPath, datas: _items)
            }
        }
    }
    
    override func configureDeejayCell(collectionView: UICollectionView, indexPath: IndexPath, hightLightWhenSelected: Bool) -> UICollectionViewCell{
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.nameOfClass + HorizontalType.deejay.rawValue, for: indexPath) as? HorizontalCollectionViewCell{
            
            queueConcurrent.sync(execute: {
                
                cell.currentSection = indexPath.section
                cell.type = .deejay
                if cell.delegate == nil {
                    cell.delegate = self
                    cell.isHightlightWhenSelected = hightLightWhenSelected
                }
            })
            
            return cell
        }
        
        return self.configureLoadingListCollectionCell(collectionView:collectionView, indexPath:indexPath)
    }
}
