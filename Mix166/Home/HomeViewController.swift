//
//  FeatureController.swift
//  Mix166
//
//  Created by ttiamap on 6/6/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var myCollectionView: UICollectionView?
    
    private let queueConcurrent = DispatchQueue(label: "HomeParallaxQueue", attributes: .concurrent)
    
    internal var lblError: UILabel?
    let vmodel : HomeViewModel = HomeViewModel.init(title: AppViewTitle.home.rawValue)
    var storingCells: Dictionary<String,UICollectionViewCell?> = [:]
    
    override var isNavigationBarHidden: Bool{
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.setupLabelError()
        self.vmodel.delegate = self
        
        SharePlayer.tabbarController = self.tabBarController
        
        self.checkUpdateVersion()
        
        self.requestData()
    }
    
    func setupLabelError(){
        if self.lblError == nil {
            self.lblError = UILabel.init()
            self.lblError?.textAlignment = .center
            self.lblError?.font = UIFont.fontSemiBold_extraLarge()
            self.lblError?.textColor = UIColor.init(hexaString: ColorText.secondaryTitleColor.rawValue).withAlphaComponent(0.6)
            self.lblError?.numberOfLines = 0
            
            self.lblError?.isUserInteractionEnabled = true
            
            let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.labelErrorDidTap))
            tapGesture.numberOfTapsRequired = 1
            self.lblError?.addGestureRecognizer(tapGesture)
            
            self.lblError?.layer.zPosition = 0
            
            self.view.addSubview(self.lblError!)
            self.view.bringSubview(toFront: self.lblError!)
            self.lblError?.isHidden = true
            
            self.lblError?.addTopConstraint(toView: self.view, attribute: .top, relation: .equal, constant: 0)
            self.lblError?.addLeftConstraint(toView: self.view, attribute: .left, relation: .equal, constant: 10)
            self.lblError?.addBottomConstraint(toView: self.view, attribute: .bottom, relation: .equal, constant: 0)
            self.lblError?.addRightConstraint(toView: self.view, attribute: .right, relation: .equal, constant: -10)
            
            if Thread.isMainThread{
                self.view.layoutIfNeeded()
            }else{
                DispatchQueue.main.async() { () -> Void in
                    self.view.layoutIfNeeded()
                }
            }
            
        }
    }
    
    @objc private func labelErrorDidTap(){
        self.requestData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setNavigationBarStyle() -> NavigationBarStyle {
        return .Default
    }
    
    
    override func setViewTitle() -> String {
        return self.vmodel.titleNavigationBar ?? ""
    }
    
    func requestData(){
        vmodel.fetchHomeDataWithoutCache(callback: { (success, error) in
            if success{
                self.refreshData()
            }else{
                if (error != nil){
                    self.lblError?.text = error!.localizedDescription + "\n Tap screen to try again"
                    
                    self.refreshData()
                }
            }
        })
    }
    
    func refreshData(){
        if self.myCollectionView?.delegate == nil {
            self.vmodel.registerCell(collectionView: self.myCollectionView) {
                //
                self.myCollectionView?.delegate = self
                self.myCollectionView?.dataSource = self
            }
            self.myCollectionView?.contentInset = UIEdgeInsetsMake(0, 0, playerControlHeight, 0)
            
        }else{
            self.myCollectionView?.reloadData()
        }
    }
    
    
    //MARK: - Header
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if let entry = self.vmodel.getEntriesAtIndex(index: section) as? FeatureEntries , entry.type != .banner{
            return headerTitleSize
        }
        return sizeZero
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        //
        if kind == UICollectionElementKindSectionHeader{
            return self.vmodel.configureHeaderTitleCell(collectionView: collectionView, viewForSupplementaryElementOfKind: kind, indexPath:indexPath, colorBlack_Enable: true)
        }
        
        return UICollectionReusableView.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: 0))
    }
    
    
    //MARK: - CollectionView Delegate - Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let number = (self.vmodel.feature == nil) ? 0 : self.vmodel.feature!.entries.count
        
        self.lblError?.isHidden = number > 0
        
        return number
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let _entries = self.vmodel.getEntriesAtIndex(index: section) as? FeatureEntries{
            
            switch _entries.type {
                
            case .banner, .mixset, .deejay, .video:
                return 1
                
            case .track:
                if self.vmodel.feature?.entries.count ?? 0 > section{
                    return self.vmodel.feature!.entries[section].entries?.count ?? 0
                }
                break
                
            case .chart:
                if self.vmodel.feature?.entries.count ?? 0 > section{
                    if let _chart = self.vmodel.feature!.entries[section].entries?[0] as? Chart{
                        return _chart.tracks.count
                    }
                }
                break
                
            case .unknow:
                break
                
            }
        }
        
        return 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell = UICollectionViewCell.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        if let _cell = self.storingCells["\(String(indexPath.section))"] as? UICollectionViewCell{
            return _cell
        }
        
        if let _entries = self.vmodel.getEntriesAtIndex(index: indexPath.section) as? FeatureEntries{
            
            switch _entries.type {
                
            case .banner:
                cell = self.vmodel.configureBannerCell(collectionView:collectionView, indexPath:indexPath)
                
            case .mixset:
                cell = self.vmodel.configureMixSetCell(collectionView:collectionView, indexPath:indexPath, colorBlack_Enable: true)
                
            case .track:
                cell = self.vmodel.configureTrackCell(collectionView:collectionView, indexPath:indexPath)
                
            case .chart:
                cell = self.vmodel.configureChartCell(collectionView:collectionView, indexPath:indexPath)
                
            case .deejay:
                cell = self.vmodel.configureDeejayHomeCell(collectionView:collectionView, indexPath:indexPath, hightLightWhenSelected: false)
                
            case .video:
                cell = self.vmodel.configureVideoCell(collectionView:collectionView, indexPath:indexPath)
                
            case .unknow:
                break
                
            }
            
            if _entries.type != .chart && _entries.type != .track && _entries.type != .unknow{
                self.storingCells[String(indexPath.section)] = cell
            }
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        
        self.vmodel.selectedItemAtIndexPath(indexPath: indexPath)
        return true
    }
    
    
    // Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if let _entries = self.vmodel.getEntriesAtIndex(index: indexPath.section) as? FeatureEntries{
            switch _entries.type {
                
            case .banner:
                return bannerHomeSize
                
            case .mixset:
                return CGSize(width: screenWidth, height: mixSize.height)
            case .track:
                return trackSize
                
            case .chart:
                return chartSize
                
            case .video:
                return CGSize(width: screenWidth, height: videoSize.height)
                
            case .deejay:
                return CGSize(width: screenWidth, height: deejayCircleHomeSize.height)
                
            case .unknow:
                break
                
            }
        }
        return CGSize.init(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        if let _entries = self.vmodel.getEntriesAtIndex(index: section) as? FeatureEntries{
            switch _entries.type {
                
            case .banner:
                return UIEdgeInsetsMake(0, 0, 0, 0)
                
            case .track, .chart, .video, .deejay:
                return UIEdgeInsetsMake(paddingLeftRight, paddingLeftRight, paddingLeftRight, paddingLeftRight)
                
//            case .deejay:
//                return UIEdgeInsetsMake(paddingLeftRight, paddingLeftRight, 0, paddingLeftRight)
                
            default:
                return UIEdgeInsetsMake(0, 0, 0, 0)
                
            }
        }
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        if let _entries = self.vmodel.getEntriesAtIndex(index: section) as? FeatureEntries{
            switch _entries.type {
            case .chart, .track:
                return paddingBetweenItem * 2
            default:
                break
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
//        return paddingBetweenItem
        return 0
    }
    
    //MARK: - ScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        queueConcurrent.sync {
            // parallax scrolling zooming
            if self.myCollectionView == nil {
                return
            }

            for cell in self.myCollectionView!.visibleCells{
                if let parallaxCell = cell as? HorizontalCollectionViewCell{
                    if parallaxCell.currentSection == 0{
                        self.myCollectionView?.sendSubview(toBack: parallaxCell)
                        parallaxCell.superScrollViewDidScrolling(scrollView: scrollView)
                    }
                }
            }
        }
    }
}


//MARK: - force update
extension HomeViewController{
    func checkUpdateVersion(){
        
        let version = Utils.getAppVersion()
        
        DataProxy.user.apiGetVersion(complete: { (response, info) in
            //
            if let cms_version = response as? String{
                if version != cms_version{
                    self.showAlertUpdate(str_version: cms_version)
                }
            }
        }) { (error, info) in
            //
        }
    }
    
    func showAlertUpdate(str_version: String){
        let alert = UIAlertController(title: "New update available! ", message: "Version \(str_version) is available to download. \n update new features, improvements and bug fixes", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.view.tintColor = UIColor.init(hexaString: AppColor.appRedColor.rawValue)
        
        alert.addAction(UIAlertAction(title: "later", style: .default, handler: { (update) in
            //
        }))
        
        alert.addAction(UIAlertAction(title: "update", style: .cancel, handler: { (update) in
            //
            let url = URL(string: "https://itunes.apple.com/vn/app/mix-166/id1049233270?mt=8")!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }))
        
        self.present(alert, animated: true) {
            //
        }
    }
    
    
    func cancelUpdateTap(){
        
    }
}
