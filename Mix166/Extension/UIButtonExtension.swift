//
//  UIButtonExtension.swift
//  Mix166
//
//  Created by ttiamap on 7/26/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import UIKit


extension UIButton{

//    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
//    
//    [bezierPath addArcWithCenter:self.center radius:self.frame.size.width/2 startAngle:0 endAngle:2 * M_PI clockwise:YES];
//    
//    progressLayer = [[CAShapeLayer alloc] init];
//    [progressLayer setPath:bezierPath.CGPath];
//    [progressLayer setStrokeColor:[UIColor redColor].CGColor];
//    [progressLayer setFillColor:[UIColor clearColor].CGColor];
//    [progressLayer setLineWidth:2.0];
//    [progressLayer setStrokeEnd:100/100];
//    [self.layer addSublayer:progressLayer];
//    
//    //    CABasicAnimation *spinAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
//    //    spinAnimation.byValue = [NSNumber numberWithFloat:2.0f*M_PI];
//    //    spinAnimation.duration = 1.3;
//    //    spinAnimation.repeatCount = HUGE_VALF;
//    //    [self.view.layer addAnimation:spinAnimation forKey:@"indeterminateAnimation"];
//    
//    [CATransaction begin];
//    CABasicAnimation *animateStrokeDown =
//    [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
//    animateStrokeDown.fromValue = [NSNumber numberWithFloat:0/100.];
//    animateStrokeDown.toValue = [NSNumber numberWithFloat:100/100.];
//    animateStrokeDown.duration = 1.0;
//    animateStrokeDown.repeatCount = HUGE_VALF;
//    animateStrokeDown.removedOnCompletion = YES;
//    [progressLayer addAnimation:animateStrokeDown forKey:@"animateStrokeDown"];
    
    func downloadWithlayer(progressLayer: CAShapeLayer, _value: Double, color: UIColor, lineWidth: CGFloat){
        
        let bezierPath = UIBezierPath.init(arcCenter: self.center, radius: self.frame.size.width/2, startAngle: 0, endAngle: 2.0 * CGFloat.pi, clockwise: true)
        progressLayer.path = bezierPath.cgPath
        progressLayer.strokeColor = color.cgColor
        progressLayer.fillColor = UIColor.clear.cgColor
        progressLayer.lineWidth = lineWidth
        progressLayer.strokeEnd = 1
        
        self.layer.addSublayer(progressLayer)
        
        CATransaction.begin()
        
        let animateStrokeDown = CABasicAnimation.init(keyPath: "strokeEnd")
        animateStrokeDown.fromValue = _value
        animateStrokeDown.toValue = 1.0
        
        progressLayer.add(animateStrokeDown, forKey: "animateStrokeDown")
//        animateStrokeDown.duration = 0.1

    }
    
    func setBtnFollow(isFollow: Bool){
        if isFollow{
            self.backgroundColor = UIColor.init(hexaString: AppColor.appRedColor.rawValue)
            self.setTitleColor(UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue), for: .normal)
            self.setTitle("Following", for: .normal)
            
        }else{
            self.backgroundColor = UIColor.init(hexaString: AppColor.follow_unactive.rawValue)
            self.setTitleColor(UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue), for: .normal)
            self.setTitle("Follow", for: .normal)
        }
    }
    
    func setupBtnFollow(){
        
        // selected
        self.setTitle("Following", for: .selected)
        self.setTitleColor(UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue), for: .selected)
        self.setBackgroundImage(UIImage.init(color: UIColor.init(hexaString: AppColor.appRedColor.rawValue)), for: .selected)
        
        // normal
        self.setTitle("Follow", for: .normal)
        self.setTitleColor(UIColor.init(hexaString: ColorText.primaryTitleColor.rawValue), for: .normal)
        self.setBackgroundImage(UIImage.init(color: UIColor.init(hexaString: AppColor.follow_unactive.rawValue)), for: .normal)
        
            self.borderWithRadius(self.frame.size.height/2)
    }
    
}
