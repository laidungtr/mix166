//
//  DoubleExtension.swift
//  Mix166
//
//  Created by ttiamap on 6/20/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

extension Double {
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension Int{
    
    func covertToThousand() -> String{
        
        if self < 1000{
            return String(self)
        }
        
        if self > 999_999{
            let number: Float = Float(self)/Float(1_000_000)
            return String(format: "%.1fM", number)
        }else{
            let number: Float = Float(self)/Float(1000)
            return String(format: "%.1fK", number)
        }
    }
}
