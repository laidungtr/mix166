//
//  UIColorExtention.swift
//  Karaoke_Plus_Remote
//
//  Created by ttiamap on 5/10/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import UIKit


extension UIColor{
    
    class func red(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor{
        return UIColor.init(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }
    
    convenience init(hexaString:String) {
        
        if hexaString == "" || hexaString.isEmpty {
            self.init(
                red: 0,
                green: 0,
                blue:0,
                alpha: 0 )
        } else {
            
            self.init(
                red:   CGFloat( strtoul( String(Array(hexaString.characters)[0...1]), nil, 16) ) / 255.0,
                green: CGFloat( strtoul( String(Array(hexaString.characters)[2...3]), nil, 16) ) / 255.0,
                blue:  CGFloat( strtoul( String(Array(hexaString.characters)[4...5]), nil, 16) ) / 255.0, alpha: 1.0 )
            
        }
    }
    
    convenience init(hexaString:String, alpha: CGFloat) {
        
        if hexaString == "" || hexaString.isEmpty {
            self.init(
                red: 0,
                green: 0,
                blue:0,
                alpha: 0 )
        } else {
            
            self.init(
                red:   CGFloat( strtoul( String(Array(hexaString.characters)[0...1]), nil, 16) ) / 255.0,
                green: CGFloat( strtoul( String(Array(hexaString.characters)[2...3]), nil, 16) ) / 255.0,
                blue:  CGFloat( strtoul( String(Array(hexaString.characters)[4...5]), nil, 16) ) / 255.0, alpha: alpha )
            
        }
    }

        var isDarkColor: Bool {
            let RGB = self.cgColor.components
            return (0.2126 * RGB[0] + 0.7152 * RGB[1] + 0.0722 * RGB[2]) < 0.5
        }
        
        var isBlackOrWhite: Bool {
            let RGB = self.cgColor.components
            return (RGB[0] > 0.91 && RGB[1] > 0.91 && RGB[2] > 0.91) || (RGB[0] < 0.09 && RGB[1] < 0.09 && RGB[2] < 0.09)
        }
        
        func isDistinct(compareColor: UIColor) -> Bool {
            let bg = self.cgColor.components
            let fg = compareColor.cgColor.components
            let threshold: CGFloat = 0.25
            
            if fabs(bg[0] - fg[0]) > threshold || fabs(bg[1] - fg[1]) > threshold || fabs(bg[2] - fg[2]) > threshold {
                if fabs(bg[0] - bg[1]) < 0.03 && fabs(bg[0] - bg[2]) < 0.03 {
                    if fabs(fg[0] - fg[1]) < 0.03 && fabs(fg[0] - fg[2]) < 0.03 {
                        return false
                    }
                }
                return true
            }
            return false
        }
        
        func colorWithMinimumSaturation(minSaturation: CGFloat) -> UIColor {
            var hue: CGFloat = 0.0
            var saturation: CGFloat = 0.0
            var brightness: CGFloat = 0.0
            var alpha: CGFloat = 0.0
            self.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
            
            if saturation < minSaturation {
                return UIColor(hue: hue, saturation: minSaturation, brightness: brightness, alpha: alpha)
            } else {
                return self
            }
        }
        
        func isContrastingColor(compareColor: UIColor) -> Bool {
            let bg = self.cgColor.components
            let fg = compareColor.cgColor.components
            
            let bgLum = 0.2126 * bg[0] + 0.7152 * bg[1] + 0.0722 * bg[2]
            let fgLum = 0.2126 * fg[0] + 0.7152 * fg[1] + 0.0722 * fg[2]
            
            let bgGreater = bgLum > fgLum
            let nom = bgGreater ? bgLum : fgLum
            let denom = bgGreater ? fgLum : bgLum
            let contrast = (nom + 0.05) / (denom + 0.05)
            return 1.6 < contrast
        }
}
