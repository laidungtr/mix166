//
//  UITextFieldExtension.swift
//  Mix166
//
//  Created by ttiamap on 6/27/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func modifyClearButton(with image : UIImage) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        clearButton.contentEdgeInsets = UIEdgeInsetsMake(0, -8, 0, 0)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(_:)), for: .touchUpInside)
        rightView = clearButton
        rightViewMode = .whileEditing
    }
    
    func clear(_ sender : AnyObject) {
        if self.delegate?.textFieldShouldClear?(self) ?? false{
            self.text = ""
        }
//        sendActions(for: .editingChanged)
    }
}
