//
//  FontExtension.swift
//  Karaoke_Plus_Remote
//
//  Created by ttiamap on 5/10/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation
import UIKit

enum fontNameType: String {
    case regular = "SFUIDisplay-Regular"
    case bold = "SFUIDisplay-bold"
    case semiBold = "SFUIDisplay-Semibold"
    case light = "SFUIDisplay-Light"
    case italic = "SFUIText-LightItalic"
}

enum fontSizeType: CGFloat {
    case small = 10.0
    case medium = 12.0
    case big = 14.0
    case extraBig = 16.0
    case large = 18.0
    case extraLarge = 20.0
    case extraLargeXX = 22.0
    case extraLargeXXXX = 26.0
}

extension UIFont{
    
    /*
     @Method: font(_ name: fontNameType, fontsize: fontSizeType)
     @Description: set font with fontName and font size manually
     @Input:
     name : fontNameType (options)
     fontsize : fontSizeType (options)
     
     @Output: font : UIFont
     */
    class func font(_ name: fontNameType, fontsize: fontSizeType) -> UIFont {
        return UIFont(name: name.rawValue, size: fontsize.rawValue) ?? UIFont()
    }
    
    
    /* Regular */
    class func fontRegular_small() -> UIFont{
        return self.font(.regular, fontsize: .small)
    }
    
    class func fontRegular_medium() -> UIFont{
        return self.font(.regular, fontsize: .medium)
    }
    
    class func fontRegular_big() -> UIFont{
        return self.font(.regular, fontsize: .big)
    }
    
    class func fontRegular_extraBig() -> UIFont{
        return self.font(.regular, fontsize: .extraBig)
    }
    
    class func fontRegular_large() -> UIFont{
        return self.font(.regular, fontsize: .large)
    }
    
    class func fontRegular_extraLarge() -> UIFont{
        return self.font(.regular, fontsize: .extraLarge)
    }
    
    class func fontRegular_extraLargeXX() -> UIFont{
        return self.font(.regular, fontsize: .extraLargeXX)
    }
    
    /* Semi Bold */
    class func fontSemiBold_small() -> UIFont{
        return self.font(.semiBold, fontsize: .small)
    }
    
    class func fontSemiBold_medium() -> UIFont{
        return self.font(.semiBold, fontsize: .medium)
    }
    
    class func fontSemiBold_big() -> UIFont{
        return self.font(.semiBold, fontsize: .big)
    }
    
    class func fontSemiBold_extraBig() -> UIFont{
        return self.font(.semiBold, fontsize: .extraBig)
    }
    
    class func fontSemiBold_large() -> UIFont{
        return self.font(.semiBold, fontsize: .large)
    }
    
    class func fontSemiBold_extraLarge() -> UIFont{
        return self.font(.semiBold, fontsize: .extraLarge)
    }
    
    /* Bold */
    class func fontBold_small() -> UIFont{
        return self.font(.bold, fontsize: .small)
    }
    
    class func fontBold_medium() -> UIFont{
        return self.font(.bold, fontsize: .medium)
    }
    
    class func fontBold_big() -> UIFont{
        return self.font(.bold, fontsize: .big)
    }
    
    class func fontBold_extraBig() -> UIFont{
        return self.font(.bold, fontsize: .extraBig)
    }
    
    class func fontBold_large() -> UIFont{
        return self.font(.bold, fontsize: .large)
    }
    
    class func fontBold_extraLarge() -> UIFont{
        return self.font(.bold, fontsize: .extraLarge)
    }
    
    class func fontBold_extraLargeXX() -> UIFont{
        return self.font(.bold, fontsize: .extraLargeXX)
    }
    
    class func fontBold_extraLargeXXXX() -> UIFont{
        return self.font(.bold, fontsize: .extraLargeXXXX)
    }
    
    /* Light */
    class func fontLight_small() -> UIFont{
        return self.font(.light, fontsize: .small)
    }
    
    class func fontLight_medium() -> UIFont{
        return self.font(.light, fontsize: .medium)
    }
    
    class func fontLight_big() -> UIFont{
        return self.font(.light, fontsize: .big)
    }
    
    class func fontLight_extraBig() -> UIFont{
        return self.font(.light, fontsize: .extraBig)
    }
    
    class func fontLight_large() -> UIFont{
        return self.font(.light, fontsize: .large)
    }
    
    class func fontLight_extraLarge() -> UIFont{
        return self.font(.light, fontsize: .extraLarge)
    }

}
