//
//  ArrayExtension.swift
//  Karaoke_Plus_Remote
//
//  Created by ttiamap on 5/15/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

/*
 let filteredArray = wordFreqs.filter
 ({
 (e) -> Bool in
 return e.1 > 3
 })// the filteredArray is [("k", 5), ("a", 7)]
 
 
 let finalValue = filteredArray.map {
 $0.0
 }// here you're creating a new array with String. $0 represents object from filteredArray
 
 print(finalValue) // ["k", "a"]
 */


extension MutableCollection where Indices.Iterator.Element == Index {
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        
        for (firstUnshuffled , unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            let d: IndexDistance = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            guard d != 0 else { continue }
            let i = index(firstUnshuffled, offsetBy: d)
            swap(&self[firstUnshuffled], &self[i])
        }
    }
}

extension Sequence {
    /// Returns an array with the contents of this sequence, shuffled.
    func shuffled() -> [Iterator.Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}
