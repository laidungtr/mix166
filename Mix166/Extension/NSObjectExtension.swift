//
//  NSObjectExtension.swift
//  Mix166
//
//  Created by ttiamap on 6/9/17.
//  Copyright © 2017 ttiamap. All rights reserved.
//

import Foundation

extension NSObject{
    
    public class var nameOfClass: String{
        return NSStringFromClass(self).components(separatedBy: ".").last ?? ""
    }
}
